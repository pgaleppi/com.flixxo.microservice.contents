import Promise from 'bluebird';
import {loadService} from '../remoteservice';
import {getStatus} from '../status';
import createLogger from '../logger';
import models from '../models';

const logger = createLogger(__filename);

/**
 * Get ping
 * @return {object}
 */
export function ping() {
    logger.info(`Sending ping information`);
    return getStatus();
}

/**
 * Check unexisting users in local db
 * @return {string[]}
 */
export async function checkUnexistingUsers() {
    const authService = await loadService('@auth');

    return Promise.resolve(models.users.findAll({
        paranoid: false,
        attributes: ['email'],
        raw: true,
    }))
        .map(({email}) => email)
        .filter(
            async (email) => !await authService.execute('auth:exists', email),
            {concurrency: 10}
        );
}
