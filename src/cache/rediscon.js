import Redis from 'ioredis';
import config from '../config';

// const CACHE_TYPE = config.get('cache:type');
const CACHE_PORT = config.get('cache:redis:port');
const CACHE_ENDPOINT = config.get('cache:redis:endpoint');
const CACHE_OPTIONS = config.get('cache:redis:options');

let connection;

/**
 * Get redis connection
 * @return {Redis}
 */
export function getConnection() {
    if (!connection) {
        connection = new Redis(
            CACHE_PORT,
            CACHE_ENDPOINT,
            {
                lazyConnect: true,
                ...CACHE_OPTIONS || {},
            }
        );
    }

    return connection;
}
