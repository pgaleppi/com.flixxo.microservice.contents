
import Promise from 'bluebird';
import {wrapError} from '../../errors';
import Command from './command';
import {createDebug} from '../../logger';

const debug = createDebug(__filename);

let fakeMethods = [];

/**
 * Create RPC Server
 */
export async function createServer() {
    throw new Error('Not implemented in mock');
}

/**
 * Get service connector
 * @param {string} service
 * @return {Promise<Command>}
 */
export async function getServiceConnection(service) {
    return new Command({exec}, service);
}

/**
 * Excute to fake execution of remote command
 * @param {Command} cinstance
 * @param {string} service
 * @param {object} pack
 */
export function exec(cinstance, service, pack) {
    debug(`Exec fake method for ${service}/${pack.cmd}`);

    const meth = fakeMethods.find(
        (x) => x.service === service && x.name === pack.cmd);

    if (meth) {
        const rpack = {
            uid: pack.uid,
            success: true,
            error: null,
            result: null,
        };

        try {
            Promise.resolve(meth.callback(...pack.params))
                .then((result) => {
                    debug(`${pack.uid} (${pack.cmd}) resolve`);
                    return {
                        ...rpack,
                        result,
                    };
                })
                .catch((error) => {
                    debug(`${pack.uid} (${pack.cmd}) reject: `, error.message);
                    return {
                        ...rpack,
                        success: false,
                        error: wrapError(error),
                    };
                })
                .then((fpack) => {
                    process.nextTick(() => {
                        cinstance.handleResponse(fpack);
                    });
                });
        } catch (error) {
            process.nextTick(() => {
                debug(`sync ${pack.uid} (${pack.cmd}) reject`, error.message);

                cinstance.handleResponse({
                    ...rpack,
                    success: false,
                    error: wrapError(error),
                });
            });
        }
    } else {
        debug(`\tNo method faked, wait for a timeout`);
    }
}

/**
 * Define a fake response to a service/method call
 * @param {string} service
 * @param {string} name
 * @param {function} callback
 */
export function fakeResponse(service, name, callback) {
    fakeMethods.push({
        service,
        name,
        callback,
    });
}

/**
 * Clear fake methods definitions
 */
export function clear() {
    fakeMethods = [];
}
