import {createHash} from 'crypto';

/**
 * Generate a SHA1 (hex) froma a Buffer
 * @param {Buffer|string} b
 * @return {string}
 */
export function sha1(b) {
    const h = createHash('sha1');

    if (!Buffer.isBuffer(b)) {
        b = Buffer.from(b, 'utf8');
    }

    h.update(b);
    return h.digest('hex');
}

/**
 * Generate a final key for save/load cache
 * @param {string} prefix
 * @param {any[]} ...params
 * @return {string}
 */
export function createKeyFor(prefix, ...params) {
    const sufflix = params
        .map((p, i) => {
            if (p && typeof p['_toCachePrefix'] === 'function') {
                return p._toCachePrefix();
            }

            const str = JSON.stringify(p);

            return sha1(`${i}:${str}`);
        })
        .join(':');

    return `${prefix}_${sha1(sufflix)}`;
}
