
import Promise from 'bluebird';
import bootstrapServer from '../services';
import {createServer} from '../rpclambda';
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

let server;

/**
 * Get server
 * @return {Promise<RPCServer>}
 */
export async function getServer() {
    if (!server) {
        server = Promise.resolve((async () => {
            const rpcServer = await createServer();
            return bootstrapServer(rpcServer);
        })());
    }

    return server;
}

/**
 * Lambda SQS Handler
 * @param {object} event
 * @param {object} context
 * @return {Promise<Array>}
 */
export default async (event, context) => {
    const services = await getServer();

    context.callbackWaitsForEmptyEventLoop = false;

    debug(`Going to process ${event.Records.length} messages...`);

    const promises = event.Records.map(({messageId, body}) => {
        const startedAt = Date.now();

        return new Promise((resolve, reject) => {
            let cmd = 'Unknow';
            try {
                const message = JSON.parse(body);
                debug('Executing command:');
                debug(body);
                cmd = message.cmd;
                const result = services.executeCommand(message);
                const delta = Date.now() - startedAt;
                logger.info(
                    `[ RPCSQS:EXEC ] Executted ${cmd} as success in `+
                    `${delta}ms`
                );
                resolve(result);
            } catch (err) {
                const delta = Date.now() - startedAt;
                logger.error(
                    `[ RPCSQS:EXEC ] Executted ${cmd} as failed in ` +
                    `${delta}ms ` +
                    `[ MSG:${err.message} NAME:${err.name} ]`
                );
                debug(`Message: ${body}`);
                debug(err);
                reject(err);
            }
        });
    });

    return Promise.all(promises);
};

