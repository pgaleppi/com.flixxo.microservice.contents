
jest.mock('../../src/rpclib');

import {check} from '../../src/antiflood';
import {
    fakeResponse as rpcFakeResponse,
    clear as rpcClear,
} from '../../src/rpclib';
import {AntiFloodOperationLimit} from '../../src/errors';

describe('#check', () => {
    let serviceMethod;

    beforeAll(() => {
        serviceMethod = jest.fn((type, val) => {
            switch (`${type}:${val}`) {
                case 'MOBILE_NUMBER:+549115555':
                    return {
                        blocked: false,
                        strict: false,
                    };
                case 'MOBILE_NUMBER:+549115556':
                    return {
                        blocked: true,
                        strict: false,
                    };
                case 'MOBILE_NUMBER:+549115557':
                    return {
                        blocked: true,
                        strict: true,
                    };
                default:
                    throw new Error('Unexpected params');
            }
        });

        rpcFakeResponse(
            'com.flixxo.microservice.antiflood',
            'resource:check',
            serviceMethod,
        );
    });

    afterEach(() => serviceMethod.mockClear());
 
    afterAll(() => rpcClear());

    test(`Must to get 0 if is valid`, async () => {
        const res = await check('MOBILE_NUMBER', '+549115555');
        expect(serviceMethod)
            .toHaveBeenCalledWith('MOBILE_NUMBER', '+549115555');
        expect(res).toBe(0);
    });

    test(`Must to throw AntiFloodOperationLimit error`, async () => {
        try {
            await check('MOBILE_NUMBER', '+549115556');
        } catch (e) {
            expect(e).toBeInstanceOf(AntiFloodOperationLimit);
            expect(e.message).toBe('Flood prevention error');
            expect(e.methods).toEqual(['captcha']);
            return;
        }

        throw new Error('Pass');
    });

    test(`Must to throw AntiFloodOperationLimit error as strict`, async () => {
        try {
            await check('MOBILE_NUMBER', '+549115557');
        } catch (e) {
            expect(e).toBeInstanceOf(AntiFloodOperationLimit);
            expect(e.message).toBe('Flood prevention error');
            expect(e.methods).toEqual([]);
            return;
        }

        throw new Error('Pass');
    });
});
