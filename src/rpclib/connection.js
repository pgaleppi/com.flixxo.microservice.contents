
import Promise from 'bluebird';
import amqp from 'amqplib/callback_api';
import config from '../config';
import createLogger, {createDebug} from '../logger';

const debug = createDebug(__filename);
const logger = createLogger(__filename);

const AMQP_URI = config.get('amqp:uri');
const AMQP_MAX_TRIES = config.get('amqp:maxTries') || 10;
const AMQP_TRY_TIME = config.get('amqp:tryTime') || 3000;

let connection;

/**
 * Get a RPC connection, if don't exists, create one
 * @return {Promise<amqp.connection>}
 */
export default function getConnection() {
    if (!connection) {
        debug(`Creating RPC connection to ${AMQP_URI}`);

        connection = Promise.resolve((async () => {
            let connected;

            for (let i = 0; i < AMQP_MAX_TRIES; i++) {
                if (connected) break;

                try {
                    logger.info(`Connecting with amqp`);
                    await new Promise((connResolve, connReject) => {
                        amqp.connect(AMQP_URI, (err, conn) => {
                            if (err) {
                                return connReject(err);
                            }

                            connection = conn;
                            connected = true;
                            return connResolve();
                        });
                    });
                } catch (e) {
                    logger.warn(`Error connection to ${AMQP_URI}`, e.message);
                    logger.warn(`trying in ${AMQP_TRY_TIME}ms`);
                    await new Promise((r) => setTimeout(r, AMQP_TRY_TIME));
                }

            }

            if (connected) {
                logger.info(`Connected to ${AMQP_URI}`);
                return connection;
            } else {
                logger.error(`Error connecting with amqp`);
                throw new Error('Error connecing amqp');
            }
        })());
    }

    return connection;
}
