jest.mock('../../../src/config/env', () => ({
    aws: {
        auth: {
            signatureAccessKeyId: 'dummy',
            signatureSecretKey: 'dummy',
        },
        sqs: {
            services: {
                validService: {},
                tunneledService: {
                    tunneled: 'another',
                },
            },
        },
    },
}));

jest.mock('../../../src/rpclib');

import Promise from 'bluebird';
import {Service as SQSService} from '../../../src/sqsrpc';
import {fakeResponse} from '../../../src/rpclib';

describe('Test SQSRCP lib', () => {

    beforeAll(() => {
        jest.mock('aws-sdk');
        fakeResponse(
            'another',
            'test:method',
            () => {
                return true;
            },
        );
    });

    describe('~Service', function() {

        test(`Must to create a valid service`, () => {
            const ins = new SQSService('validService');
            expect(ins).toBeInstanceOf(SQSService);
        });

        test(`Must to throw error with invalid service`, () => {
            try {
                new SQSService('invalidService');
            } catch (e) {
                expect(e).toBeInstanceOf(Error);
                expect(e.message).toBe('Service invalidService not configured');
                return;
            }

            throw new Error('Pass');
        });

        test(`Must to get valid tunneled service`, async () => {
            const service = new SQSService('tunneledService');
            expect(service).toBeInstanceOf(SQSService);
            const res = await service.exec('test:method');
            expect(res).toBeTruthy();
        });

    });

    describe('#getContext', () => {
        test(`Must to get plain context`, async () => {
            const service = new SQSService('validService', {
                a: 1,
                b: [],
                c: 'text',
            });

            expect(await service.getContext()).toEqual({
                a: 1, b: [], c: 'text',
            });
        });

        test(`Must to get promised context`, async () => {
            const p = new Promise((r) => {
                setTimeout(() => {
                    r({
                        a: 1,
                        b: [],
                        c: 'text',
                    });
                }, 10);
            });

            const service = new SQSService('validService', p);

            expect(await service.getContext()).toEqual({
                a: 1, b: [], c: 'text',
            });
        });

        test(`Must to get computed context`, async () => {
            const service = new SQSService('validService', () => {
                return {a: 1, b: [], c: 'text'};
            });

            expect(await service.getContext()).toEqual({
                a: 1, b: [], c: 'text',
            });
        });

        test(`Must to get computed async context`, async () => {
            const service = new SQSService('validService', async () => {
                return {a: 1, b: [], c: 'text'};
            });

            expect(await service.getContext()).toEqual({
                a: 1, b: [], c: 'text',
            });
        });
    });

    describe('#bindContext', () => {
        test(`Must to get a new instance with new context`, async () => {
            const service = new SQSService('validService', {
                a: 1, b: 2, c: 'text',
            });

            const bindedService = service.bindContext({
                z: 9, y: 8, x: 'text',
            });

            expect(await service.getContext()).toEqual({
                a: 1, b: 2, c: 'text',
            });

            expect(await bindedService.getContext()).toEqual({
                z: 9, y: 8, x: 'text',
            });
        });
    });

    describe('#exec', () => {
        let service;

        beforeAll(() => {
            jest.mock('aws-sdk');
            service = new SQSService('validService');
        });

        afterEach(() => {
            service.sqsClient.reset();
        });

        afterAll(() => {
            jest.unmock('aws-sdk');
        });

        test(`Must to send RPC object`, async () => {
            await service.exec('test', 1, 2, 'str', [1, 2, 'str']);
            expect(service.sqsClient.calls).toEqual([
                '{"cmd":"test","params":[1,2,"str",[1,2,"str"]],"context":{}}',
            ]);
        });

        test(`Must to send RPC object with context`, async () => {
            const ns = service.bindContext({
                x: 99,
                y: 98,
                z: 'text',
            });

            await ns.exec('test', 1, 2, 'str', [1, 2, 'str']);
            expect(ns.sqsClient.calls).toEqual([
                '{"cmd":"test","params":[1,2,"str",[1,2,"str"]],"context":{"x":99,"y":98,"z":"text"}}',
            ]);
        });

        test(`Must to thrown error if aws fail`, async () => {
            service.sqsClient.__setError(new Error('Dummy'));

            try {
                await service.exec('test', 1, 2, 'str', [1, 2, 'str']);
            } catch (e) {
                expect(e).toBeInstanceOf(Error);
                expect(e.message).toBe('Dummy');
                return;
            }

            throw new Error('Pass');
        });
    });


});
