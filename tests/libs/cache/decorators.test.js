jest.mock('../../../src/config/env', () => {
    return {
        cache: {
            enabled: true,
        },
    };
});

import Store, {clearCache, isSaved} from '../../../src/cache/stores/memory';
import {cacheable, applyCacheable, serviceCacheable} from '../../../src/cache/decorators';
import {createPrefix} from '../../../src/logger/utils';

const MICROSERVICE_PREFIX = createPrefix(__filename);

describe('Check cache decorators', () => {
    afterEach(() => clearCache());

    describe('@cacheable', () => {

        class TestClass {
            constructor() {
                this.foo = 'bar';
                this._ex = 0;
            }

            one() {
                expect(this.foo).toBe('bar');
            }

            @cacheable(__filename, 'keey', {store: Store})
            two(...params) {
                this.one();
                this._ex++;
                expect(this.foo).toBe('bar');
                expect(this._ex).toBe(1);
                return params.map((x) => `"${x}"`).join(',');
            }
        }

        const kone = `CACHE:${MICROSERVICE_PREFIX}:keey:two_f6ae8d027b54cad402a50db5b844a632dac8485b`;
        test('Must to cache result, without lost binded scope', async () => {
            let ins = new TestClass();
            let first = await ins.two('a', 'b', 3);
            expect(first).toBe('"a","b","3"');
            expect(isSaved(kone, '"a","b","3"')).toBeTruthy();
            let second = await ins.two('a', 'b', 3);
            expect(second).toBe('"a","b","3"');
        });

    });

    describe('@applyCacheable', () => {

        beforeEach(() => clearCache());

        async function testFunc(...params) {
            return params.map((x) => `"${x}"`).join(',');
        }

        test(`Apply to function without prefix`, async () => {
            const k = `CACHE:${MICROSERVICE_PREFIX}:testFunc_b96bc3839e781777c0a7f2011eb49986e6f0b640`;
            const cache = applyCacheable(__filename, testFunc, {store: Store});
            const first = await cache(1, 2, 'c');
            expect(first).toBe('"1","2","c"');
            expect(isSaved(k, '"1","2","c"')).toBeTruthy();
        });

        test(`Apply to function with prefix only`, async () => {
            const k = `CACHE:${MICROSERVICE_PREFIX}:test_func:testFunc_b96bc3839e781777c0a7f2011eb49986e6f0b640`;
            const cache = applyCacheable(__filename, 'test_func', testFunc, {store: Store});
            const first = await cache(1, 2, 'c');
            expect(first).toBe('"1","2","c"');
            expect(isSaved(k, '"1","2","c"')).toBeTruthy();
        });

        test(`Apply to function with prefix and vname`, async () => {
            const k = `CACHE:${MICROSERVICE_PREFIX}:test_func:other_name_b96bc3839e781777c0a7f2011eb49986e6f0b640`;
            const cache = applyCacheable(__filename, 'test_func', 'other_name', testFunc, {store: Store});
            const first = await cache(1, 2, 'c');
            expect(first).toBe('"1","2","c"');
            expect(isSaved(k, '"1","2","c"')).toBeTruthy();
        });
    });

    describe('@serviceCacheable', () => {
        test(`Must to apply cache to a service method`, async () => {

            const testFunc = function(a, b, c) {
                expect(a).toBe(1);
                expect(b).toBe('2');
                expect(c).toEqual({a: 3});

                return {a: 1, b: '2'};
            };

            const k = `CACHE:${MICROSERVICE_PREFIX}:space:testCommand_8aac4eb2d7455faa6b3f3c2fa1629ab549f8709a`;
            const cache = serviceCacheable(__filename, {store: Store})(testFunc, 'space', 'testCommand');
            const first = await cache(1, '2', {a: 3});
            expect(first).toEqual({a: 1, b: '2'});
            expect(isSaved(k, {a: 1, b: '2'})).toBeTruthy();
        });
    });
});
