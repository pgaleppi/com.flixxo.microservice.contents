jest.mock('../../../../src/cache/rediscon');

import RedisStore from '../../../../src/cache/stores/redis';
import {name as microserviceName} from '../../../../package.json';

const prefix = `CACHE:${microserviceName}:.test:`;

describe('RedisStore', () => {

    describe('#get', () => {
        let store, redis;

        beforeEach(() => {
            store = new RedisStore('test', 'test');
            redis = store.connection;
            redis.__rebuild({
                data: {
                    [`${prefix}basic`]: '{"data":"Abc","timestamp":100}',
                    'CACHE:flags:a': '{"flag":"a","timestamp":99}',
                    'CACHE:flags:c': '{"flag":"a","timestamp":101}',
                },
            });
        });

        test(`Must to get basic data`, async () => {
            let res = await store.get('basic');
            expect(res).toBe('Abc');
        });

        test(`Must to get basic data`, async () => {
            let res = await store.get('nbasic');
            expect(res).toBeUndefined();
        });

        test(`Must to get basic data with valid flag`, async () => {
            let res = await store.get('basic', {
                flags: ['a', 'b'],
            });

            expect(res).toBe('Abc');
        });

        test(`Must to get null data newer valid flag`, async () => {
            let res = await store.get('basic', {
                flags: ['a', 'b', 'c'],
            });

            expect(res).toBeUndefined();
        });
    });

    describe('#set', () => {
        let store, redis;

        beforeEach(() => {
            store = new RedisStore('test', 'test');
            redis = store.connection;
            redis.__rebuild({
                data: {
                    [`${prefix}basic`]: '{"data":"Abc","timestamp":100}',
                },
            });
            redis = store.connection;
        });

        test(`Must to set basic data`, async () => {
            let now = Date.now();
            await store.set('nfield', {
                a: 1,
                b: '2',
                c: [3,4,5],
            });

            let body = JSON.parse(await redis.get(`${prefix}nfield`));

            expect(body).toMatchObject({
                data: {a:1,b:'2',c:[3,4,5]},
            });
            expect(Math.floor(body.timestamp / 10000)).toBe(
                Math.floor(now / 10000)
            );
        });

        test(`Must to set existing basic data`, async () => {
            let now = Date.now();
            await store.set('basic', {
                a: 1,
                b: '2',
                c: [3,4,5],
            });

            let body = JSON.parse(await redis.get(`${prefix}basic`));

            expect(body).toMatchObject({
                data: {a:1,b:'2',c:[3,4,5]},
            });
            expect(Math.floor(body.timestamp / 10000)).toBe(
                Math.floor(now / 10000)
            );
        });
    });

    describe('#touchFlag', () => {
        let store, redis;

        beforeEach(() => {
            store = new RedisStore('test', 'test');
            redis = store.connection;
            redis.__rebuild({
                data: {
                    'CACHE:flags:a': '{"flag":"a","timestamp":99}',
                },
            });
            redis = store.connection;
        });

        test(`Must to touch new flag`, async () => {
            let now = Date.now();
            await store.touchFlag('newflag');

            let body = JSON.parse(await redis.get(`CACHE:flags:newflag`));

            expect(body).toMatchObject({
                flag: 'newflag',
            });
            expect(Math.floor(body.timestamp / 10000)).toBe(
                Math.floor(now / 10000)
            );
        });

        test(`Must to touch existing flag`, async () => {
            let now = Date.now();
            await store.touchFlag('a');

            let body = JSON.parse(await redis.get(`CACHE:flags:a`));

            expect(body).toMatchObject({
                flag: 'a',
            });
            expect(Math.floor(body.timestamp / 10000)).toBe(
                Math.floor(now / 10000)
            );
        });
    });
});
