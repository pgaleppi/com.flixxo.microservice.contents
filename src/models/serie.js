import axios from 'axios';
import Promise from 'bluebird';
import {Op} from 'sequelize';
import {Decimal} from 'decimal.js';
import config from '../config';
import {multidialectNow, multidialectCustomOrderItems} from '../storage';
import {getServiceConnection} from '../sqsrpc';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

const SLS_HOST = config.get('serverless:host');
const SLS_CONTENTS = config.get('serverless:contents');
const SLS_SERIES = config.get('serverless:series');

const DELETE_DELTA = config.get('publication:deleteTime');

const FORMAT_MARKDOWN = 0x01;

const STATUS_NEW = 0x10;
const STATUS_ENABLED = 0x20;
const STATUS_CANCEL = 0x30;
const STATUS_PAUSED = 0x40;

const MODSTATUS_NEW = 0x10;
const MODSTATUS_ASSIGNED = 0x20;
const MODSTATUS_INPROGRESS = 0x30;
const MODSTATUS_REJECT = 0x40;
const MODSTATUS_ENABLED = 0x50;
const MODSTATUS_REQUEST_CHANGES = 0x60;
const MODSTATUS_CHANGES_REVIEW = 0x70;

export const VERSION_STATUS_NONE = 0x00;
export const VERSION_STATUS_ACTIVE = 0x10;

export const RATING_ALL = 0x01;
export const RATING_MILD = 0x02;
export const RATING_MATURE = 0x03;

export default (sequelize, DataTypes) => {
    let UsersModel;
    let CategoryModel;
    let TagModel;
    let MediaModel;
    let ContentModel;
    let SeasonModel;
    let SubtitleModel;
    let GeoBlockingModel;
    let CountryModel;

    const instanceMethods = {
        /**
         * Find or create tags and set
         * @param {string[]} tags
         * @param {object} transaction
         * @return {TagModel[]}
         */
        async findAndSetTags(tags, transaction) {
            const otags = await TagModel.getTags(...tags);
            debug(`Setting tags`);
            await this.setTags(otags, {transaction});
            return otags;
        },

        /**
         * Update serie status
         * @param {integer} status
         * @param {string} [comment]
         * @param {Sequelize.Transaction} [transaction]
         * @return {Promise}
         */
        async setStatus(status, comment = null, transaction = null) {
            debug(`Change status of ${this.uuid} ${this.status} -> ${status}`);
            this.status = status;
            this.cancelComment = comment;
            return this.save({transaction});
        },

        /**
         * Fill serie data to display in contents list
         * @return {object}
         */
        fillDataToDisplay() {
            const plainContent = this.get({plain: true});
            const serieContent = plainContent.season[0].content[0];

            let episodesCount = 0;
            for ( let i = 0; i < plainContent.season.length; i++ ) {
                episodesCount += plainContent.season[i].content.length;
            }

            delete plainContent.season;
            delete plainContent.priority;

            return {
                ...plainContent,
                contentType: serieContent.contentType,
                price: serieContent.price,
                releaseDate: serieContent.releaseDate,
                duration: serieContent.duration,
                downloadStatus: serieContent.downloadStatus,
                contentmedia: serieContent.media,
                episodesCount: episodesCount,
            };
        },

        /**
         * Add content to elastic
         */
        async addToElastic() {
            const wrappedContent = await this.denormalize();
            const url = SLS_HOST + SLS_SERIES;
            try {
                await axios.put(url, wrappedContent);
            } catch (e) {
                debug(`Error adding to elasting`, e);
                throw e;
            }
        },

        /**
         * Update content on elastic index
         */
        async updateElastic() {
            const wrappedContent = await this.denormalize();
            const url = SLS_HOST + SLS_CONTENTS;
            try {
                await axios.put(url, wrappedContent);
            } catch (e) {
                debug(`Error updating to elasting`, e);
                throw e;
            }
        },

        /**
         * Remove serie from elastic index
         */
        async removeFromElastic() {
            try {
                // Delete content on ElasticSearch
                const url = SLS_HOST + SLS_CONTENTS;
                await axios.delete(url, {
                    data: {
                        uuid: this.uuid,
                    },
                });
            } catch (e) {
                debug(`Error deleting on ES ${e}`);
                throw e;
            }
        },

        /**
         * Update all contents mod status
         * @param {number} status
         * @param {string} message
         * @param {object} dbTxn
         * @return {Promise}
         **/
        async updateAllContentsModerationStatus(status, message, dbTxn) {
            return ContentModel.update(
                {
                    moderationStatus: status,
                    moderationMessage: message,
                    moderatedAt: new Date(),
                },
                {
                    where: {serieUUID: this.uuid},
                    transaction: dbTxn,
                }
            );
        },

        /**
         * Get rating as description
         * @return {string}
         */
        async getRatingDescription() {
            switch (this.rating) {
                case RATING_ALL: {
                    return 'All audiences';
                }
                case RATING_MILD: {
                    return 'Mild-Mature';
                }
                case RATING_MATURE: {
                    return 'Mature';
                }
            }
            return 'All audiences';
        },


        async denormalize() {
            const me = await SerieModel.scope('toDenormalize')
                .findByPk(this.uuid);

            const tags = await me.getTags({
                attributes: ['tag'],
                through: {
                    attributes: [],
                },
            })
                .map((t) => t.tag);

            const firstChap = me.content[0];
            const ratingDescription = await me.getRatingDescription();

            let cover = me.media.find((i) => i.type === 1);
            // If cover not found use the first chap cover
            if (!cover) {
                cover = firstChap.media.find((i) => i.type === 1);
            }

            const geoBlockObject = await this.getGeoBlockingCountries();

            const s = {
                uuid: me.uuid,
                contentType: 2,
                title: {
                    [me.audioLang]: me.title,
                },
                body: {
                    [me.audioLang]: me.body,
                },
                category: me.category.name,
                author: me.author.nickname,
                authorId: me.authorId,
                seederFee: new Decimal(firstChap.seederFee).toNumber(),
                authorFee: new Decimal(firstChap.authorFee).toNumber(),
                status: STATUS_ENABLED,
                moderationStatus: MODSTATUS_ENABLED,
                createdAt: me.createdAt,
                updatedAt: me.updatedAt,
                boost: new Decimal(firstChap.boost).toNumber(),
                releaseDate: firstChap.releaseDate,
                rating: me.rating,
                ratingDescription: ratingDescription,
                credits: me.credits,
                audioLang: me.audioLang,
                price: firstChap.price,
                tags: tags,
                duration: firstChap.duration,
                downloadStatus: firstChap.downloadStatus,
                media: firstChap.media.map((m) => m.get({plain: true})),
                cover: cover.get({plain: true}),
                subtitle: firstChap.subtitle,
                priority: firstChap.priority,
                ...geoBlockObject,
            };

            return s;
        },

        /**
         * Replace cover, getting new URL
         * @param {string} name
         * @param {string} url
         * @return {Promise<MediaModel>}
         */
        async replaceCover(name, url) {
            debug(`Replacing cover of ${this.uuid} for ${url}`);
            return sequelize.transaction(async (dbTx) => {
                const media = await this.getMedia({
                    where: {
                        type: MediaModel.TYPE_COVER,
                    },
                    transaction: dbTx,
                });

                debug(`\tDeleting previus media`);
                await Promise.map(media, (m) => m.destroy({
                    transaction: dbTx,
                }));

                debug(`\tCreate new media`);
                const nmedia = await MediaModel.create({
                    name,
                    url,
                    uuid: this.uuid,
                    type: MediaModel.TYPE_COVER,
                }, {transaction: dbTx});

                // @TODO
                // await this.updateElastic();

                return nmedia;
            })
                .catch((e) => {
                    debug(`\tError replacing cover:`, e);
                    throw e;
                });
        },

        /**
         * Get geoBlocking configuration from serie
         * @return {Promise<object>}
         */
        async getGeoBlockingCountries() {

            const seasons = await this.getSeason({
                attributes: [],
                scope: ['enabled'],
                include: [{
                    model: GeoBlockingModel,
                    as: 'geoBlocking',
                    attributes: ['type'],
                    required: false,
                    include: [{
                        model: CountryModel,
                        as: 'countries',
                        attributes: ['iso'],
                        required: false,
                    }],
                }],
            })
                .map((x) => x.geoBlocking ?
                    x.geoBlocking.get({plain: true}) :
                    {type: 0, countries: []}
                )
            ;

            debug('Seasons found');
            debug(JSON.stringify(seasons));

            const unBlocked = seasons
                .filter((s) => s.type === 0);

            const blackListeneds = seasons
                .filter((s) => s.type === GeoBlockingModel.TYPE_BLACK)
                .map((x) => x.countries);

            const whiteListeneds = seasons
                .filter((s) => s.type === GeoBlockingModel.TYPE_WHITE)
                .map((x) => x.countries);

            let blackList;
            let whiteList;
            let type;

            // If seasons with unblocking exists, return not blocking
            if (
                unBlocked.length
            ) {
                debug('Found unBlocked seasons');
                type = 0;
                whiteList = blackList = [];
            // If has black listened
            } else if (blackListeneds.length) {
                debug('Found blacklisted seasons');
                whiteList = [];
                blackList = blackListeneds.reduce((b, countries) => {
                    const isos = countries.map((x) => x.iso);
                    if (b === null) {
                        return [...isos];
                    }

                    return b.filter((x) => isos.includes(x));
                }, null);

                debug('Blacklist');
                debug(JSON.stringify(blackList));

                const whites = whiteListeneds
                    .reduce((b, c) => [
                        ...new Set([...b, ...c.map((x) => x.iso)]),
                    ], []);

                debug('Whites');
                debug(JSON.stringify(whites));

                // If there are white listed, exclude that countries from
                // blocking
                if (whites.length) {
                    blackList = blackList
                        .filter((x) => !whites.includes(x));
                }

                debug('Blacklist after');
                debug(JSON.stringify(blackList));

                type = blackList.length ? GeoBlockingModel.TYPE_BLACK : 0;
            // If only has white listened
            } else if (whiteListeneds.length) {
                debug('Found whitelisted seasons');
                blackList = [];
                whiteList = whiteListeneds.reduce((r, c) => [
                    ...new Set([...r, ...c.map((x) => x.iso)]),
                ], []);
                type = GeoBlockingModel.TYPE_WHITE;

                debug('Whites');
                debug(JSON.stringify(whiteList));
            // Wherever
            } else {
                type = 0;
                whiteList = blackList = [];
            }

            return {
                geoBlockingType: type,
                availableCountries: whiteList,
                blockedCountries: blackList,
            };
        },
    };

    const SerieModel = sequelize.define('Serie', {

        uuid: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },

        title: {
            type: DataTypes.STRING(2014),
            allowNull: false,
            validate: {
                is: /^.{1,}$/,
            },
        },

        body: {
            type: DataTypes.TEXT,
            allowNull: false,
            validate: {
                is: /^(\n|.){10,}$/,
            },
        },

        bodyFormat: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: FORMAT_MARKDOWN,
            validate: {
                isIn: [[
                    FORMAT_MARKDOWN,
                ]],
            },
        },

        audioLang: {
            type: DataTypes.STRING(5),
            allowNull: false,
            defaultValue: 'en',
            validate: {
                is: {
                    args: /^[a-z]{2}(?:_[A-Z]{2})?/,
                    msg: 'Invalid lang format',
                },
            },
        },

        categoryId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
        },

        authorId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
        },

        status: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: STATUS_NEW,
            validate: {
                isIn: {
                    args: [[
                        STATUS_NEW,
                        STATUS_CANCEL,
                        STATUS_PAUSED,
                        STATUS_ENABLED,
                    ]],
                    msg: 'Invalid status',
                },
            },
        },

        priority: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },

        cancelDate: {
            type: DataTypes.DATE,
            allowNull: true,
        },

        cancelComment: {
            type: DataTypes.TEXT,
            allowNull: true,
            get() {
                const {status} = this;

                if (status === STATUS_CANCEL || status === STATUS_PAUSED) {
                    return this.getDataValue('cancelComment');
                }

                return null;
            },
        },

        moderationStatus: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: MODSTATUS_NEW,
            validate: {
                isIn: [[
                    MODSTATUS_NEW,
                    MODSTATUS_ASSIGNED,
                    MODSTATUS_INPROGRESS,
                    MODSTATUS_ENABLED,
                    MODSTATUS_REJECT,
                    MODSTATUS_REQUEST_CHANGES,
                    MODSTATUS_CHANGES_REVIEW,
                ]],
            },
        },

        moderatedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },

        moderatorId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true,
        },

        moderationMessage: {
            type: DataTypes.TEXT,
            allowNull: true,
        },

        rating: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: RATING_ALL,
            validate: {
                isIn: [[
                    RATING_ALL,
                    RATING_MILD,
                    RATING_MATURE,
                ]],
            },
        },

        credits: {
            type: DataTypes.TEXT,
            allowNull: true,
            validate: {
                is: /^(\n|.){5,}$/,
            },
        },

        tags: {
            type: DataTypes.VIRTUAL,
        },

        versionStatus: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: VERSION_STATUS_NONE,
            validate: {
                isIn: {
                    args: [[
                        VERSION_STATUS_NONE,
                        VERSION_STATUS_ACTIVE,
                    ]],
                    msg: 'Invalid version status',
                },
            },
        },

        isIndexable: {
            type: DataTypes.VIRTUAL(
                DataTypes.BOOLEAN,
                ['status', 'moderationStatus'],
            ),
            get() {
                return this.get('status') === STATUS_ENABLED &&
                       this.get('moderationStatus') === MODSTATUS_ENABLED;
            },
        },

    }, {
        timestamps: true,
        paranoid: true,
        instanceMethods,

        validate: {
            statusFlow() {
                if (this.isNewRecord) {
                    return;
                }

                if (this.changed('status') &&
                    this.previous('status') === STATUS_CANCEL) {
                    throw new Error('A canceled serie can\'t change status');
                }
            },

            canceledStatus() {
                if (this.isNewRecord) {
                    return;
                }

                if (this.status === STATUS_CANCEL &&
                    this.changed('moderationStatus')
                ) {
                    debug(`moderationStatus changhed in a canceled series`);
                    throw new Error('A canceled serie can\'t be moderated');
                }
            },

            requiredCancelComment() {
                let {status, cancelComment} = this;

                if (!cancelComment) {
                    cancelComment = '';
                }

                if (this.changed('status') &&
                    (status === STATUS_CANCEL || status === STATUS_PAUSED) &&
                    cancelComment.trim().length < 3
                ) {
                    throw new Error('You must to write a reason');
                }
            },
        },

        hooks:
        {
            afterCreate(instance, options) {
                // @see https://github.com/sequelize/sequelize/issues/6800
                return Promise.resolve((async () => {
                    if (instance.tags && instance.tags.length > 0) {
                        debug(`Adding tags`);
                        await instance.findAndSetTags(
                            instance.tags,
                            options.transaction,
                        );
                    }
                })());
            },

            beforeUpdate(instance, options) {
                // @see https://github.com/sequelize/sequelize/issues/6800
                return Promise.resolve((async () => {
                    const {status} = instance;
                    const backStatus = instance.previous('status');
                    const hasNewStatus = instance.changed('status');

                    if (hasNewStatus && (status === STATUS_CANCEL ||
                        status === STATUS_PAUSED) &&
                        (backStatus !== STATUS_CANCEL &&
                            backStatus !== STATUS_PAUSED)
                    ) {
                        debug(`Setting cancelDate`);
                        const delta = Math.floor(
                            Date.now() + (DELETE_DELTA * 60000));
                        instance.setDataValue('cancelDate', delta);
                    } else if (hasNewStatus &&
                                    status === STATUS_ENABLED &&
                                    backStatus === STATUS_PAUSED) {
                        debug(`Clean cancelDate`);
                        instance.setDataValue('cancelDate', null);
                    }

                    if (instance.tags) {
                        debug(`Setting tags`);
                        await instance.findAndSetTags(
                            instance.tags,
                            options.transaction,
                        );
                    }
                })());
            },

            async afterUpdate(instance, options) {
                if (instance.changed('status')) {
                    debug(`Status change, running hook`);
                    const suggestions = await getServiceConnection(
                        'suggestions'
                    );
                    await suggestions.exec(
                        'hooks:changeSerieCaps',
                        instance.uuid
                    );
                }
            },

        },
    });

    if (typeof SerieModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                SerieModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    /**
     * Formats
     * @type {integer}
     */
    SerieModel.FORMAT_MARKDOWN = FORMAT_MARKDOWN;

    SerieModel.STATUS_NEW = STATUS_NEW;
    SerieModel.STATUS_ENABLED = STATUS_ENABLED;
    SerieModel.STATUS_CANCEL = STATUS_CANCEL;
    SerieModel.STATUS_PAUSED = STATUS_PAUSED;

    SerieModel.MODSTATUS_NEW = MODSTATUS_NEW;
    SerieModel.MODSTATUS_ASSIGNED = MODSTATUS_ASSIGNED;
    SerieModel.MODSTATUS_INPROGRESS = MODSTATUS_INPROGRESS;
    SerieModel.MODSTATUS_REJECT = MODSTATUS_REJECT;
    SerieModel.MODSTATUS_ENABLED = MODSTATUS_ENABLED;
    SerieModel.MODSTATUS_REQUEST_CHANGES = MODSTATUS_REQUEST_CHANGES;
    SerieModel.MODSTATUS_CHANGES_REVIEW = MODSTATUS_CHANGES_REVIEW;

    SerieModel.VERSION_STATUS_NONE = VERSION_STATUS_NONE;
    SerieModel.VERSION_STATUS_ACTIVE = VERSION_STATUS_ACTIVE;

    SerieModel.associate = function(models, db) {
        UsersModel = models.users;
        CategoryModel = models.categories;
        TagModel = models.tags;
        MediaModel = models.media;
        ContentModel = models.contents;
        SeasonModel = models.season;
        SubtitleModel = models.subtitle;
        GeoBlockingModel = models.geoblocking;
        CountryModel = models.countries;

        SerieModel.belongsTo(UsersModel, {
            as: 'author',
            foreignKey: 'authorId',
            targetKey: 'id',
            constraints: true,
        });

        SerieModel.belongsTo(UsersModel, {
            as: 'moderator',
            foreignKey: 'moderatorId',
            targetKey: 'id',
            constraints: true,
        });

        SerieModel.belongsTo(CategoryModel, {
            as: 'category',
            foreignKey: 'categoryId',
            targetKey: 'id',
            constraints: true,
        });

        SerieModel.belongsToMany(TagModel, {
            through: {
                model: 'SerieTags',
                attributes: [],
                uniq: true,
            },
            timestamps: false,
            paranoid: false,
        });

        SerieModel.hasMany(MediaModel, {
            as: 'media',
            foreignKey: 'uuid',
            targetKey: 'uuid',
            constraints: false,
            scope: {
                isDraft: false,
            },
        });

        SerieModel.hasMany(ContentModel, {
            as: 'content',
            foreignKey: 'serieUUID',
            sourceKey: 'uuid',
            constraints: false,
        });

        SerieModel.hasMany(SeasonModel, {
            as: 'season',
            foreignKey: 'serieUUID',
            sourceKey: 'uuid',
        });

        // Scopes
        SerieModel.addScope('statusEnabled', {
            where: {
                status: STATUS_ENABLED,
            },
        });

        SerieModel.addScope('statusEnabledOrPaused', {
            where: {
                status: {[Op.or]: [STATUS_ENABLED, STATUS_PAUSED]},
            },
        });

        SerieModel.addScope('enabled', {
            where: {
                status: STATUS_ENABLED,
                moderationStatus: MODSTATUS_ENABLED,
            },
        });

        SerieModel.addScope('byIds', (uuids, forceOrder = false) => {
            return {
                where: {
                    uuid: {
                        [Op.in]: uuids,
                    },
                },
                ...(forceOrder && uuids.length?
                    {
                        order: multidialectCustomOrderItems(
                            'Serie.uuid',
                            ...uuids
                        ),
                    }: {}
                ),
            };
        });

        SerieModel.addScope('withSeasons', (required = true) => ({
            include: [{
                model: SeasonModel,
                as: 'season',
                attributes: ['uuid', 'title', 'number'],
                required,
                include: [{
                    model: ContentModel,
                    as: 'content',
                    attributes: [
                        'uuid', 'title', 'order', 'status',
                        'contentType', 'price', 'releaseDate',
                        'audioLang', 'duration', 'downloadStatus',
                        'hash', 'body', 'boost',
                    ],
                    where: {
                        'status': ContentModel.STATUS_ENABLED,
                        'moderationStatus': ContentModel.MODSTATUS_ENABLED,
                    },
                    required: true,
                    include: [{
                        model: MediaModel,
                        as: 'media',
                        attributes: [
                            'id', 'name', 'type',
                            'url', 'order', 'processed',
                            'sizes',
                        ],
                        required: false,
                    }, {
                        model: SubtitleModel,
                        as: 'subtitle',
                        attributes: ['id', 'lang', 'label', 'url'],
                        required: false,
                    }],
                }],
            }],
        }));

        SerieModel.addScope('withGeoAvailableSeasons',
            (countryIso, required = true, lang = '') => {

                const contentScope = GeoBlockingModel.extendScopeDefinition(
                    countryIso,
                    GeoBlockingModel.ETYPE_CONTENT,
                    'season->content.uuid',
                    'geoBlocking',
                    '$season.content.geoBlocking.type$',
                )({
                    model: ContentModel.scope(
                        'enabled',
                        {method: ['i18n', lang]}
                    ),
                    as: 'content',
                    attributes: [
                        'uuid', 'title', 'order', 'status',
                        'contentType', 'authorFee', 'seederFee',
                        'price', 'releaseDate',
                        'audioLang', 'duration', 'downloadStatus',
                        'hash', 'body', 'boost',
                    ],
                    include: [{
                        model: MediaModel,
                        as: 'media',
                        attributes: [
                            'id', 'name', 'type',
                            'url', 'order', 'processed',
                            'sizes',
                        ],
                        required: false,
                        order: [
                            ['order', 'ASC'],
                        ],
                    }, {
                        model: SubtitleModel,
                        as: 'subtitle',
                        attributes: [
                            'uuid', 'label', 'lang',
                            'url', 'closeCaption',
                        ],
                        required: false,
                    }],
                    required: true,
                    order: [
                        ['order', 'ASC'],
                    ],
                });

                const seasonScope = GeoBlockingModel.extendScopeDefinition(
                    countryIso,
                    GeoBlockingModel.ETYPE_SERIE_SEASON,
                    'season.uuid',
                    'geoBlocking',
                    '$season.geoBlocking.type$',
                    {
                        include: [{
                            model: CountryModel.scope('simple'),
                            as: 'countries',
                            required: false,
                            through: {
                                attributes: [],
                            },
                        }],
                    },
                )({
                    model: SeasonModel,
                    as: 'season',
                    attributes: ['uuid', 'title', 'number'],
                    required,
                    include: [{
                        ...contentScope,
                    }],
                    order: [
                        ['number', 'ASC'],
                        ['content', 'order', 'ASC'],
                    ],
                });

                const where = seasonScope.where;
                delete seasonScope.where;

                return {
                    include: [{
                        ...seasonScope,
                    }],
                    where: {
                        ...where,
                    },
                };
            });

        SerieModel.addScope('geoAvailable', (countryIso) => {

            const contentScope = GeoBlockingModel.extendScopeDefinition(
                countryIso,
                GeoBlockingModel.ETYPE_CONTENT,
                'season->content.uuid',
                'geoBlocking',
                '$season.content.geoBlocking.type$',
            )({
                model: ContentModel.scope(
                    'enabled',
                ),
                as: 'content',
                attributes: ['uuid'],
                required: true,
            });

            const seasonScope = GeoBlockingModel.extendScopeDefinition(
                countryIso,
                GeoBlockingModel.ETYPE_SERIE_SEASON,
                'season.uuid',
                'geoBlocking',
                '$season.geoBlocking.type$',
            )({
                model: SeasonModel,
                as: 'season',
                attributes: ['uuid'],
                required: true,
                include: [{
                    ...contentScope,
                }],
            });

            const where = seasonScope.where;
            delete seasonScope.where;

            return {
                include: [{
                    ...seasonScope,
                }],
                where: {
                    ...where,
                },
            };
        });

        SerieModel.addScope('detailed', () => {
            return {
                attributes: [
                    'uuid',
                    'title',
                    'bodyFormat',
                    'body',
                    'status',
                    'moderationStatus',
                    'authorId',
                    'audioLang',
                    'credits',
                    'rating',
                ],
                include: [{
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'id',
                        'uuid',
                        'name',
                        'type',
                        'url',
                        'order',
                        'sizes',
                        'processed',
                    ],
                    order: [
                        ['order', 'ASC'],
                    ],
                    required: false,
                    separate: true,
                }, {
                    model: CategoryModel.scope('public'),
                    as: 'category',
                    required: true,
                }, {
                    model: UsersModel.scope('minimalProfile'),
                    as: 'author',
                    required: true,
                }, {
                    model: TagModel,
                    attributes: ['tag'],
                    required: false,
                    through: {
                        attributes: [],
                    },
                }],
            };
        });

        SerieModel.addScope('asAuthor', (authorId) => {
            return {
                where: {
                    authorId,
                },
                paranoid: true,
            };
        });

        SerieModel.addScope('authorDetailed', (lang = '') => {
            return {
                attributes: [
                    'uuid',
                    'title',
                    'bodyFormat',
                    'body',
                    'status',
                    'cancelComment',
                    'moderationStatus',
                    'moderationMessage',
                    'moderatedAt',
                    'audioLang',
                    'credits',
                    'rating',
                    'updatedAt',
                    'createdAt',
                    'cancelDate',
                ],
                order: [
                    ['media', 'order', 'ASC'],
                    ['season', 'number', 'ASC'],
                    ['season', 'content', 'order', 'ASC'],
                    ['season', 'content', 'media', 'order', 'ASC'],
                ],
                include: [{
                    model: SeasonModel,
                    as: 'season',
                    attributes: ['uuid', 'title', 'number', 'status'],
                    required: false,
                    include: [{
                        model: ContentModel.scope({method: ['i18n', lang]}),
                        as: 'content',
                        attributes: [
                            'uuid', 'title', 'body', 'order', 'status',
                            'contentType', 'authorFee', 'seederFee',
                            'price', 'releaseDate',
                            'duration', 'downloadStatus', 'hash',
                            'moderationStatus', 'moderationMessage',
                            'audioLang',
                        ],
                        required: false,
                        include: [{
                            model: MediaModel,
                            as: 'media',
                            attributes: [
                                'id', 'name', 'type',
                                'url', 'order', 'processed',
                                'sizes',
                            ],
                            required: false,
                        }, {
                            model: TagModel,
                            attributes: ['tag'],
                            required: false,
                            through: {
                                attributes: [],
                            },
                        }, {
                            model: SubtitleModel,
                            as: 'subtitle',
                            attributes: [
                                'id', 'uuid', 'label', 'lang',
                                'url', 'closeCaption',
                            ],
                            required: false,
                        }],
                    }],
                },
                {
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'id', 'name', 'type',
                        'url', 'order', 'processed',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: CategoryModel.scope('public'),
                    as: 'category',
                    required: true,
                }, {
                    model: UsersModel.scope('minimalProfile'),
                    as: 'author',
                    required: true,
                }, {
                    model: TagModel,
                    attributes: ['tag'],
                    required: false,
                    through: {
                        attributes: [],
                    },
                }],
            };
        });

        SerieModel.addScope('authorListing', () => {
            return {
                attributes: [
                    'uuid',
                    'title',
                    'status',
                    'moderationStatus',
                    'moderationMessage',
                    'moderatedAt',
                    'audioLang',
                    'rating',
                    'updatedAt',
                    'createdAt',
                    'cancelDate',
                    'cancelComment',
                    'body',
                    'categoryId',
                ],
                order: [
                    ['updatedAt', 'DESC'],
                    ['createdAt', 'DESC'],
                    ['media', 'order', 'ASC'],
                    ['season', 'number', 'ASC'],
                ],
                include: [{
                    model: UsersModel.scope('minimal'),
                    as: 'author',
                    required: true,
                }, {
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: SeasonModel,
                    as: 'season',
                    attributes: ['uuid', 'title', 'number'],
                    required: false,
                }, {
                    model: TagModel,
                    attributes: ['tag'],
                    required: false,
                    through: {
                        attributes: [],
                    },
                }],
            };
        });

        SerieModel.addScope('byCategory', (categoryId) => {
            return {
                where: {categoryId},
            };
        });

        SerieModel.addScope('listing', () => {
            return {
                subQuery: false,
                attributes: [
                    'uuid',
                    'title',
                    'audioLang',
                    'rating',
                    'updatedAt',
                    'createdAt',
                    'categoryId',
                    'priority',
                ],
                order: [
                    ['priority', 'DESC'],
                    ['updatedAt', 'DESC'],
                    ['createdAt', 'DESC'],
                    ['media', 'order', 'ASC'],
                    ['season', 'number', 'ASC'],
                ],
                include: [{
                    model: UsersModel.scope('minimal'),
                    as: 'author',
                    required: true,
                }, {
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: SeasonModel,
                    as: 'season',
                    attributes: ['uuid', 'title', 'number'],
                    required: false,
                }],
            };
        });

        SerieModel.addScope('contentRelation', (authorId, seasonUUID) => {
            return {
                attributes: [
                    'uuid',
                ],
                include: [{
                    model: SeasonModel,
                    as: 'season',
                    attributes: ['uuid', 'number'],
                    required: true,
                    where: {
                        uuid: seasonUUID,
                    },
                }],
                where: {
                    authorId,
                },
            };
        });

        SerieModel.addScope('serieEpisodesCount', () => {
            return {
                attributes: [
                    'uuid',
                    'title',
                    'audioLang',
                    'createdAt',
                    'categoryId',
                ],
                order: [
                    ['season', 'number', 'ASC'],
                ],
                include: [{
                    model: SeasonModel,
                    as: 'season',
                    attributes: ['uuid', 'title', 'number'],
                    required: true,
                    include: [{
                        model: ContentModel,
                        as: 'content',
                        attributes: [
                            'uuid', 'title', 'order',
                            'contentType', 'price', 'releaseDate',
                            'downloadStatus',
                        ],
                        where: {
                            'status': ContentModel.STATUS_ENABLED,
                            'downloadStatus': {[Op.ne]: 0},
                            'moderationStatus': ContentModel.MODSTATUS_ENABLED,
                            'releaseDate': {
                                [Op.lte]: multidialectNow(),
                            },
                        },
                        required: true,
                    }],
                }, {
                    model: UsersModel.scope('minimalProfile'),
                    as: 'author',
                    required: true,
                }],
            };
        });

        SerieModel.addScope('seriesByTitle', (param) => {
            return {
                where: {
                    title: {
                        [Op.iLike]: `%${param}%`,
                    },
                },
            };
        });

        SerieModel.addScope('serieContentsList', (hasBody = false) => {
            const serieAttributes = [
                'uuid',
                'title',
                'audioLang',
                'rating',
                'updatedAt',
                'createdAt',
                'categoryId',
                'priority',
            ];

            if (hasBody) {
                serieAttributes.push('body');
            }

            return {
                attributes: serieAttributes,
                order: [
                    ['priority', 'DESC'],
                    ['media', 'order', 'ASC'],
                    ['season', 'number', 'ASC'],
                    ['season', 'content', 'order', 'ASC'],
                    ['season', 'content', 'media', 'order', 'ASC'],
                ],
                include: [{
                    model: SeasonModel,
                    as: 'season',
                    attributes: ['uuid', 'title', 'number'],
                    required: true,
                    include: [{
                        model: ContentModel,
                        as: 'content',
                        attributes: [
                            'uuid', 'title', 'order', 'boost',
                            'contentType', 'price', 'releaseDate',
                            'duration', 'downloadStatus', 'authorFee',
                            'seederFee',
                        ],
                        where: {
                            'status': ContentModel.STATUS_ENABLED,
                            'downloadStatus': {[Op.ne]: 0},
                            'moderationStatus': ContentModel.MODSTATUS_ENABLED,
                            'releaseDate': {
                                [Op.lte]: multidialectNow(),
                            },
                        },
                        required: true,
                        include: [{
                            model: MediaModel,
                            as: 'media',
                            attributes: [
                                'id', 'name', 'type',
                                'url', 'order', 'processed',
                                'sizes',
                            ],
                            required: false,
                        }],
                    }],
                }, {
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'id', 'name', 'type',
                        'url', 'order', 'processed',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: UsersModel.scope('minimalProfile'),
                    as: 'author',
                    required: true,
                }],
            };
        });

        SerieModel.addScope('toDenormalize', {
            attributes: [
                'uuid',
                'title',
                'body',
                'authorId',
                'createdAt',
                'updatedAt',
                'rating',
                'credits',
                'audioLang',
            ],
            include: [{
                model: UsersModel,
                attributes: [
                    'nickname',
                ],
                as: 'author',
                required: true,
            }, {
                model: CategoryModel,
                as: 'category',
                attributes: ['name'],
                required: true,
            }, {
                model: MediaModel,
                as: 'media',
                attributes: [
                    'name',
                    'type',
                    'url',
                    'order',
                    'processed',
                    'sizes',
                ],
                required: false,
            }, {
                model: ContentModel,
                as: 'content',
                attributes: [
                    'uuid',
                    'serieUUID',
                    'order',
                    'seederFee',
                    'authorFee',
                    'releaseDate',
                    'boost',
                    'price',
                    'duration',
                    'downloadStatus',
                    'priority',
                ],
                include: [{
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                }],
                order: [
                    ['releaseDate', 'ASC'],
                    ['order', 'DESC'],
                ],
                limit: 1,
                separated: true,
                required: true,
            }],
        });

        SerieModel.addScope('toExport', () => ({
            paranoid: false,
            include: [{
                model: UsersModel.scope('toExport'),
                as: 'author',
                required: true,
            }, {
                model: UsersModel.scope('toExport'),
                as: 'moderator',
                required: false,
            }, {
                model: SeasonModel.scope('toExport'),
                as: 'season',
                required: false,
            }, {
                model: MediaModel.scope('toExport'),
                as: 'media',
                required: false,
            }],
        }));

        SerieModel.addScope('capsInfo', () => {
            return {
                attributes: [
                    'uuid',
                ],
                include: [{
                    model: SeasonModel.scope('enabled'),
                    as: 'season',
                    attributes: [
                        'uuid',
                        'number',
                    ],
                    include: [{
                        model: ContentModel.scope('enabled'),
                        as: 'content',
                        attributes: [
                            'uuid',
                            'order',
                        ],
                    }],
                }],
                order: [
                    ['season', 'number', 'ASC'],
                    ['season', 'content', 'order', 'ASC'],
                ],
            };
        });
    };

    /**
     * Search one record by hash
     * @param {string} hash
     * @param {object} [options]
     * @return {Promise<SerieModel|null>}
     */
    SerieModel.findByHash = function(hash, options = {}) {
        hash = hash.toLowerCase();
        return this.findOne({
            where: {hash},
            ...options,
        });
    };

    SerieModel.getCountryId = async function(countryISO) {
        const [result] = await sequelize
            .query(`SELECT id FROM "Countries" WHERE iso = :iso`,
                {replacements: {iso: countryISO}});
        return result;
    };

    SerieModel.readAvailableSeasons = async function(countryId, serieIds) {
        const [result] = await sequelize
            .query(`SELECT "Seasons".uuid, "Seasons".title,
                  "Seasons"."serieUUID",
                  "GeoBlockings"."type" AS type,
                  "GeoBlockingCountry"."CountryId" AS country
                FROM public."Seasons"
                FULL JOIN "GeoBlockings"
                  ON "GeoBlockings"."targetUUID" = "Seasons".uuid
                  AND "GeoBlockings"."entityType" = 32
                FULL JOIN "GeoBlockingCountry"
                  ON "GeoBlockingCountry"."GeoBlockingUuid"
                    = "GeoBlockings".uuid
                  AND "GeoBlockingCountry"."CountryId" = :countryId
                WHERE "Seasons"."deletedAt" IS NULL
                  AND "Seasons"."uuid" IS NOT NULL
                  AND ( 
                      ( "GeoBlockings"."type" <> 16
                        AND "GeoBlockingCountry"."CountryId" <> :countryId )
                      OR ( "GeoBlockings"."type" = 32
                        AND "GeoBlockingCountry"."CountryId" = :countryId )
                      OR ( "GeoBlockings"."type" IS NULL
                        AND "GeoBlockingCountry"."CountryId" IS NULL )
                ) AND "Seasons"."serieUUID" IN (:ids);`,
            {replacements: {countryId: countryId, ids: serieIds}});

        return result;
    };

    return SerieModel;
};
