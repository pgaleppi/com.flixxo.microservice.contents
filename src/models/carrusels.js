import {Op} from 'sequelize';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

export const TYPE_VIDEO = 0x01;
export const TYPE_SERIE = 0x02;

/**
 * CarruselModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {
    let CarruselItemModel;
    let MetadataModel;
    let ContentModel;
    let SerieModel;

    const instanceMethods = {

        /**
         * Allow to display metadata
         */
        showMetadata() {
            this.__showMetadata = true;
        },

        /**
         * @override
         */
        toJSON() {
            const data = this.get({plain: true});
            const sm = this.__showMetadata || false;

            if (typeof data.Metadata !== 'undefined' && !sm) {
                delete data.Metadata;
            }

            return data;
        },

        async saveNames(names, dbTx) {
            debug(`Setting names to ${this.uuid}`);

            await MetadataModel.destroy({
                where: {
                    entityType: MetadataModel.ENTITY_TYPE_CARRUSEL,
                    entityId: this.uuid,
                },
                transaction: dbTx,
            });

            for (const name of names) {
                await MetadataModel.create({
                    ...name,
                    entityType: MetadataModel.ENTITY_TYPE_CARRUSEL,
                    entityId: this.uuid,
                }, {
                    transaction: dbTx,
                });
            }
        },
    };

    const CarruselModel = sequelize.define('Carrusel', {

        uuid: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },

        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },

        enabled: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false,
        },

        priority: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },

        title: {
            type: DataTypes.VIRTUAL(
                DataTypes.STRING(1024),
            ),
            get() {
                const meta = this.Metadata;
                if (meta && meta.length) {
                    return meta[0].title;
                } else {
                    return '';
                }
            },
        },

        body: {
            type: DataTypes.VIRTUAL(
                DataTypes.TEXT,
            ),
            get() {
                const meta = this.Metadata;
                if (meta && meta.length) {
                    return meta[0].body;
                } else {
                    return '';
                }
            },
        },

        groupId: {
            type: DataTypes.UUID,
            allowNull: true,
        },

    }, {
        name: {
            singular: 'Carrusel',
            plural: 'Carrusels',
        },

        defaultScope: {
            order: [
                ['priority', 'DESC'],
                ['createdAt', 'ASC'],
            ],
        },

        scopes: {
            enableds: {
                where: {
                    enabled: true,
                },
            },
        },

        indexes: [
            {
                name: 'event_group_id',
                fields: ['groupId'],
                where: {
                    [Op.not]: {groupId: null},
                },
            },
        ],

        instanceMethods: instanceMethods,
    });

    if (typeof CarruselModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                CarruselModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    CarruselModel.TYPE_VIDEO = TYPE_VIDEO;
    CarruselModel.TYPE_SERIE = TYPE_SERIE;

    CarruselModel.associate = function(models) {
        CarruselItemModel = models.carruselitem;
        MetadataModel = models.metadata;
        ContentModel = models.contents;
        SerieModel = models.serie;

        MetadataModel.createAssociation(
            CarruselModel,
            MetadataModel.ENTITY_TYPE_CARRUSEL
        );

        CarruselModel.belongsToMany(ContentModel, {
            scope: {
                contentType: 1,
            },
            as: 'Contents',
            foreignKey: 'carruselUUID',
            otherKey: 'entityId',
            unique: true,
            through: {
                model: CarruselItemModel,
                scope: {
                    entityType: TYPE_VIDEO,
                },
            },
            constraints: false,
        });

        CarruselModel.belongsToMany(SerieModel, {
            as: {
                singular: 'Serie',
                plural: 'Series',
            },
            foreignKey: 'carruselUUID',
            otherKey: 'entityId',
            unique: true,
            through: {
                model: CarruselItemModel,
                scope: {
                    entityType: TYPE_SERIE,
                },
            },
            constraints: false,
        });

        CarruselModel.addScope(
            'i18n',
            MetadataModel.createI18nScope()
        );

        CarruselModel.addScope(
            'withmetadata',
            MetadataModel.createMetadataScope()
        );

        CarruselModel.addScope('listing', {
            attributes: [
                'uuid',
                'name',
                'title',
                'groupId',
            ],
            order: [
                ['priority', 'DESC'],
            ],
        });

        CarruselModel.addScope('detailled', {
            attributes: [
                'uuid',
                'name',
                'title',
                'body',
            ],
        });

        CarruselModel.addScope('byGroupId', (groupId) => {
            return {
                where: {
                    groupId,
                },
            };
        });

    };

    return CarruselModel;
};
