module.exports = {
    parser: 'babel-eslint',
    plugins: [
        "jest",
    ],
    env: {
        node: true,
        "jest/globals": true,
        "es6": true,
    },
    extends: [
        "eslint:recommended",
        "google",
    ],
    parserOptions: {
        ecmaVersion: 7,
        sourceType: "module",
        ecmaFeatures: {
            spread: true,
            experimentalObjectRestSpread: true,
        }
    },
    rules: {
        indent: ['error', 4, {
            SwitchCase: 1,
            FunctionExpression: {
                parameters: "first",
            },
        }],
        "eqeqeq": 2,
        "padded-blocks": 0,
        "no-constant-condition": 0,
        "require-atomic-updates": 1,
        "new-cap": [2, {
            "capIsNewExceptions": [
                "DataTypes.STRING",
                "DataTypes.ENUM",
                "DataTypes.VIRTUAL",
                "DataTypes.DECIMAL",                
            ],
        }],
        "require-atomic-updates": 1,
    }
};
