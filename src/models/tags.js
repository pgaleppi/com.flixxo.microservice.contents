
import {createDebug} from '../logger';

const debug = createDebug(__filename);

export default (sequelize, DataTypes) => {
    const instanceMethods = {};

    const TagModel = sequelize.define('Tag', {

        id: {
            type: DataTypes.BIGINT.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },

        tag: {
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true,
        },

        enabled: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false,
        },

    }, {
        timestamps: false,
        paranoid: false,

        defaultScope: {
            attributes: ['id', 'tag'],
            where: {
                enabled: true,
            },
        },

        scopes: {
            minimal: {
                attributes: ['tag'],
            },
        },

        instanceMethods,
    });

    if (typeof TagModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                TagModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    TagModel.associate = function(models) {};

    TagModel.getTags = async function(...tags) {
        debug(`Getting tags objects`, tags);

        const stags = tags.map((tag) => {
            return tag.trim().toLowerCase();
        });

        let existing = await TagModel.findAll({
            where: {
                tag: stags,
            },
        });

        debug(`There are ${existing.length} existins tags`,
            existing.map((x) => x.get({plain: true})));

        const news = stags.reduce((ntags, tag) => {
            if (!existing.find((x) => x.tag.toLowerCase() === tag)) {
                ntags.push({tag});
            }
            return ntags;
        }, []);

        if (news.length) {
            debug(`There are ${news.length} new tags, creating`, news);
            await TagModel.bulkCreate(news);
            const ntagso = await TagModel.findAll({
                where: {
                    tag: news.map((x) => x.tag),
                },
            });
            existing = [...existing, ...ntagso];
        }

        debug(`Final`, existing.map((x) => x.get({plain: true})));
        return existing;
    };

    return TagModel;
};
