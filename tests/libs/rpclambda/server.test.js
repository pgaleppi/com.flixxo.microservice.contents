import Promise from 'bluebird';
import {createServer} from '../../../src/rpclambda';

describe('Test RPCLib server', () => {

    describe('#executeCommand', () => {

        test(`Must to execute sync test `, async () => {
            const server = createServer();

            server.define('test', function(a, b, c) {
                expect(a).toBe('a');
                expect(b).toBe(2);
                expect(c).toBeUndefined();

                return {
                    a: '1',
                    b: 3,
                };
            });

            let res = await server.executeCommand({
                uid: '00000000-0000-0000-0000-000000000000',
                cmd: 'test',
                params: ['a', 2],
            });

            expect(res.data).toEqual({
                error: null,
                result: {
                    a: '1',
                    b: 3,
                },
                succes: true,
                uid: '00000000-0000-0000-0000-000000000000',
            });

        });

        test(`Must to execute async test `, async () => {
            const server = createServer();

            server.define('atest', async function() {
                let res = await new Promise((resolve) => {
                    setTimeout(() => resolve(true), 500);
                });
                return res;
            });

            let res = await server.executeCommand({
                uid: '00000000-0000-0000-0000-000000000001',
                cmd: 'atest',
                params: [],
            });

            expect(res.data).toEqual({
                error: null,
                result: true,
                succes: true,
                uid: '00000000-0000-0000-0000-000000000001',
            });

        });

        test(`Must to get a sync error `, async () => {
            const server = createServer();

            server.define('etest', function() {
                throw new Error('Error');
            });

            let res = await server.executeCommand({
                uid: '00000000-0000-0000-0000-000000000001',
                cmd: 'etest',
                params: [],
            });

            expect(res.data.succes).toBeFalsy();

            expect(res.data.error).toMatchObject({
                internal: true,
                message: 'Error',
                name: 'UnknownError',
                showable: false,
            });

        });

        test(`Must to get an async error `, async () => {
            const server = createServer();

            server.define('aetest', async function() {
                return new Promise((resolve, reject) => {
                    setTimeout(() => reject(new Error('Async error')), 400);
                });
            });

            let res = await server.executeCommand({
                uid: '00000000-0000-0000-0000-000000000001',
                cmd: 'aetest',
                params: [],
            });

            expect(res.data.succes).toBeFalsy();

            expect(res.data.error).toMatchObject({
                internal: true,
                message: 'Async error',
                name: 'UnknownError',
                showable: false,
            });

        });

        test(`Must to execute sync test with context`, async () => {
            const server = createServer();

            server.define('testcon', [function($context, a, b, c) {
                expect($context).toEqual({
                    x: 99,
                    y: 98,
                    z: 97,
                });
                expect(a).toBe('a');
                expect(b).toBe(2);
                expect(c).toBeUndefined();

                return {
                    a: '1',
                    b: 3,
                };
            }]);

            let res = await server.executeCommand({
                uid: '00000000-0000-0000-0001-000000000000',
                cmd: 'testcon',
                params: ['a', 2],
                context: {
                    x: 99,
                    y: 98,
                    z: 97,
                },
            });

            expect(res.data).toEqual({
                error: null,
                result: {
                    a: '1',
                    b: 3,
                },
                succes: true,
                uid: '00000000-0000-0000-0001-000000000000',
            });

        });

        test(`Must to execute sync test with selected context`, async () => {
            const server = createServer();

            server.define('testcon', ['x', 'z', function(x, z, a, b, c) {
                expect(x).toBe(99);
                expect(z).toBe(97);
                expect(a).toBe('a');
                expect(b).toBe(2);
                expect(c).toBeUndefined();

                return {
                    a: '1',
                    b: 3,
                };
            }]);

            let res = await server.executeCommand({
                uid: '00000000-0000-0000-0001-000000000001',
                cmd: 'testcon',
                params: ['a', 2],
                context: {
                    x: 99,
                    y: 98,
                    z: 97,
                },
            });

            expect(res.data).toEqual({
                error: null,
                result: {
                    a: '1',
                    b: 3,
                },
                succes: true,
                uid: '00000000-0000-0000-0001-000000000001',
            });

        });

        test(`Must to execute sync test with one decorator`, async () => {
            const server = await createServer();
            let applied = false;
            let dexecuted = false;

            const decorator = function(f, ns, cmd) {
                expect(ns).toBe('default');
                expect(cmd).toBe('testcon');
                applied = true;
                return (...p) => {
                    dexecuted = true;
                    return f(...p);
                };
            };

            server.define('testcon', [
                null,
                decorator,
                function(a, b) {
                    expect(a).toBe('a');
                    expect(b).toBe(2);

                    return {
                        a: '1',
                        b: 3,
                    };
                },
            ]);

            expect(dexecuted).toBe(false);
            expect(applied).toBe(true);

            let res = await server.executeCommand({
                uid: '00000000-0000-0000-0001-000000000003',
                cmd: 'testcon',
                params: ['a', 2],
                context: {},
            });

            expect(dexecuted).toBe(true);

            expect(res.data).toEqual({
                uid: '00000000-0000-0000-0001-000000000003',
                error: null,
                result: {
                    a: '1',
                    b: 3,
                },
                succes: true,
            });

        });

        test(`Must to execute sync test with two decorators`, async () => {
            const server = await createServer();
            let appliedA = false;
            let dexecutedA = false;
            let appliedB = false;
            let dexecutedB = false;

            const decoratorA = function(f, ns, cmd) {
                expect(ns).toBe('default');
                expect(cmd).toBe('testcon');
                expect(appliedA).toBeFalsy();
                appliedA = true;
                return (...p) => {
                    dexecutedA = true;
                    expect(dexecutedB).toBeTruthy();
                    return f(...p);
                };
            };

            const decoratorB = function(f, ns, cmd) {
                expect(ns).toBe('default');
                expect(cmd).toBe('testcon');
                expect(appliedA).toBeTruthy();
                appliedB = true;
                return (...p) => {
                    dexecutedB = true;
                    expect(dexecutedA).toBeFalsy();
                    return f(...p);
                };
            };

            server.define('testcon', [
                null,
                decoratorA,
                decoratorB,
                function(a, b) {
                    expect(a).toBe('a');
                    expect(b).toBe(2);

                    return {
                        a: '1',
                        b: 3,
                    };
                },
            ]);

            expect(dexecutedA).toBe(false);
            expect(dexecutedB).toBe(false);
            expect(appliedA).toBe(true);
            expect(appliedB).toBe(true);

            let res = await server.executeCommand({
                uid: '00000000-0000-0000-0001-000000000003',
                cmd: 'testcon',
                params: ['a', 2],
                context: {},
            });

            expect(dexecutedA).toBe(true);
            expect(dexecutedB).toBe(true);

            expect(res.data).toEqual({
                uid: '00000000-0000-0000-0001-000000000003',
                error: null,
                result: {
                    a: '1',
                    b: 3,
                },
                succes: true,
            });

        });

        test(`Must to throw error with bad decorator`, async () => {
            const server = await createServer();

            const decorator = function(f, ns, cmd) {
                return 'Banana';
            };

            try {

                server.define('testcon', [
                    null,
                    decorator,
                    function(a, b) {
                        expect(a).toBe('a');
                        expect(b).toBe(2);

                        return {
                            a: '1',
                            b: 3,
                        };
                    },
                ]);
            } catch (e) {
                expect(e).toBeInstanceOf(Error);
                expect(e.message).toBe('Result of decorator must to be a funciton');
                return;
            }

            throw new Error('Pass');
        });

        test(`Must to throw error with failed decorator`, async () => {
            const server = await createServer();

            const decorator = function(f, ns, cmd) {
                throw new Error('Decorator failed');
            };

            try {

                server.define('testcon', [
                    null,
                    decorator,
                    function(a, b) {
                        expect(a).toBe('a');
                        expect(b).toBe(2);

                        return {
                            a: '1',
                            b: 3,
                        };
                    },
                ]);
            } catch (e) {
                expect(e).toBeInstanceOf(Error);
                expect(e.message).toBe('Decorator failed');
                return;
            }

            throw new Error('Pass');
        });
    });
});
