
import {createDebug} from '../logger';

const debug = createDebug(__filename);

export const TYPE_VIDEO = 0x01;
export const TYPE_SERIE = 0x02;

/**
 * CarruselItemModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {
    let CarruselModel;

    const instanceMethods = {};

    const CarruselItemModel = sequelize.define('CarruselItem', {

        carruselUUID: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
        },

        entityType: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            validate: {
                isIn: [[
                    TYPE_VIDEO,
                    TYPE_SERIE,
                ]],
            },
        },

        entityId: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
        },

        order: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: 0,
        },

    }, {
        name: {
            singular: 'CarruselItem',
            plural: 'CarruselItems',
        },

        instanceMethods: instanceMethods,
    });

    if (typeof CarruselItemModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                CarruselItemModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    CarruselItemModel.TYPE_VIDEO = TYPE_VIDEO;
    CarruselItemModel.TYPE_SERIE = TYPE_SERIE;

    CarruselItemModel.associate = function(models) {
        CarruselModel = models.carrusels;

        CarruselItemModel.belongsTo(CarruselModel, {
            as: 'Carrusel',
            foreignKey: 'carruselUUID',
            constraints: true,
        });

        CarruselItemModel.addScope('ordered', () => ({
            order: [['order', 'ASC']],
        }));

        CarruselItemModel.addScope('byType', (entityType) => {
            return {where: {entityType}};
        });

        CarruselItemModel.addScope('byCarrusel', (carruselUUID) => {
            return {
                where: {carruselUUID},
                include: [{
                    model: CarruselModel.scope('enableds'),
                    as: 'Carrusel',
                    required: true,
                    attributes: ['uuid'],
                }],
            };
        });
    };

    return CarruselItemModel;
};
