
import sequelizeFixtures from 'sequelize-fixtures';
import models from '../models';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

/**
 * Load data task
 * @param {object} params
 */
export default async function({path}) {
    debug(`Loading data from ${path}`);
    process.stdout.write(`Loading data\n`);

    try {
        await sequelizeFixtures.loadFile(
            path,
            models
        );
    } catch (e) {
        process.stderr.write(`${e.message}\n`);
    }

    process.stdout.write(`Data loaded\n`);
}
