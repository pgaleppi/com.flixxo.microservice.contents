
/**
 * Parse JSON inputs
 * @param {string} data
 * @return {object}
 */
export function parseJSON(data) {
    return data ? JSON.parse(data) : false;
}

