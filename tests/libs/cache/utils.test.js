
import * as utils from '../../../src/cache/utils';

describe('sha1', () => {

    test('Using string', () => {
        const res = utils.sha1('&e,yoQ%d<Hr~2}bwZ8zGFCg!X,UuWE');
        expect(res).toBe('3bec09b2f77cebf7aad1642e7d0e907506fe1968');
    });

    test('Using buffer', () => {
        const res = utils.sha1(Buffer.from(
        'UazQf1b9AQhJTgpQFOpWdDmSCLhBMbQSOoPHAXv8bR0='
        , 'base64'));
        expect(res).toBe('3af2e8745d8f06f4295202cd18a5020a1bd0e69d');
    });
});

describe('createKeyFor', () => {

    test('Using string params', () => {
        const pref = 'CACHE:test';
        let key = utils.createKeyFor(pref, 'a', 'b', 'c');
        expect(key).toBe('CACHE:test_b86bb084054dcc7cbf7633fc2c56157ea4c676a5');
    });

    test('Using mixin types params', () => {
        const pref = 'CACHE:test';
        let key = utils.createKeyFor(pref, 'a', [1, 2, 3], null);
        expect(key).toBe('CACHE:test_c15df36e346382a0b331cc9fc57d239c08a59807');
    });

    test('Using cacheable object', () => {
        const pref = 'CACHE:test';

        const obj = {
            a: 1,
            b: 2,
            c: 3,
            _toCachePrefix() {
                return 'cacheble object';
            },
        };

        let key = utils.createKeyFor(pref, 'a', obj, null);
        expect(key).toBe('CACHE:test_83ab603f13905cd67e8f39f842b31f591fd93475');
    });

    test('Not cacheable object must to thorw error', () => {
        const pref = 'CACHE:test';

        const obj = {
            a: 1,
            b: 2,
            c: 3,
        };

        obj.d = obj;

        try {
            utils.createKeyFor(pref, 'a', obj, null);
        } catch (e) {
            expect(e.message.match(/^Converting circular structure to JSON/));
            return;
        }

        throw new Error('Pass');
    });
});
