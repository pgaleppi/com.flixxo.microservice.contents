import {syncDb, loadFixture, loadData} from '../helpers/db';
import {
    ForeignKeyConstraintError,
} from 'sequelize';
import models from '../../src/models';

describe('Followers', () => {
    let user;

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users-followers');
        user = await models.users.findByEmail('user@1.com');
    });

    describe('addFollower', () => {
        test('Must to follow user', async () => {
            await user.addFollower(101);
            let followers = await user.getFollowers({
                attributes: ['id'],
                raw: true,
            });
            expect(followers.map((x) => x.id)).toEqual([101, 103]);
        });

        test('Must throw error with invlid user', async () => {
            return expect(user.addFollower(666))
                .rejects.toThrow(ForeignKeyConstraintError);
        });
    });

});

describe('.displayName', function() {
    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users-display-name');
    });

    describe('verifyDisplay', () => {
        test('Must bring display name with the real name', async () => {
            const user = await models.users.scope('profile').findOne({where: {id: 100}});
            return expect(user.displayName).toBe('Cristian Dias');
        });
        test('Must bring display name with nickname', async () => {
            const user = await models.users.scope('profile').findOne({where: {id: 101}});
            return expect(user.displayName).toBe('exos');
        });
        test('Must bring display name with nickname without profile', async () => {
            const user = await models.users.scope('profile').findOne({where: {id: 102}});
            return expect(user.displayName).toBe('felix123');
        });
    })

});

describe('#resolveAdMetter', () => {
    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadFixture('admetters-for-get-admetter');
    });

    test(`Must to get admetter on user without`, async () => {
        const user = await models.users.findByEmail('user1@fakemail.com');
        const metter = await user.resolveAdMetter();

        expect(metter).toBeInstanceOf(models.admetters);
        expect(metter.metter.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.cicle.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.lastCicle.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.type).toBe(1);
    });

    test(`Must to get admetter on user with previus reg`, async () => {
        const user = await models.users.findByEmail('user2@fakemail.com');
        const metter = await user.resolveAdMetter();

        expect(metter).toBeInstanceOf(models.admetters);
        expect(metter.metter.toFixed(18)).toBe('1.123456724542723600');
        expect(metter.cicle.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.lastCicle.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.type).toBe(1);
    });

});

describe('#cancel', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadData('00_countries');
    });

    test(`Must to cancel recently created user`, async () => {
        const uuid = 'adef27ef-0986-4225-9fe4-4f7832fe9e46';
        const user = await models.users.create({
            uuid,
            email: 'test@user.net',
            nickname: 'testuser',
        });

        await user.saveProfile({
            realName: 'Luis',
            countryId: 70,
        });

        await user.cancel();

        const c = await models.users.count({
            where: {uuid},
            paranoid: false,
        });

        expect(c).toBe(0);
    });

    test(`Must to throw error on old created user`, async () => {
        const uuid = 'adef27ef-0986-4225-9fe4-4f7832fe9e46';
        const user = await models.users.create({
            uuid,
            email: 'test@user.net',
            nickname: 'testuser',
            createdAt: '2000-01-01 00:00:00',
        });

        await user.saveProfile({
            realName: 'Luis',
            countryId: 70,
        });

        try {
            await user.cancel();
        } catch (e) {
            expect(e).toBeInstanceOf(Error);
            expect(e.message).toBe('User created more than 1 minute');
            return;
        }

        throw new Error('Pass');
    });
});

describe('Scopes', () => {

    beforeEach(async () => {
        await syncDb(true);
    });

    describe('withFollowersMetters', () => {
        const scopedModel = models.users
            .scope({method: ['withFollowersMetters']});

        beforeEach(async () => {
            await loadFixture('users');
            await loadFixture('followers-for-with-followers-metter-scope');
        });

        test('Should return the count of followers/followed', async () => {
            const res = await scopedModel.findOne({where: {id: 100}});

            expect(res.get({plain: true})).toEqual({
                followers: 2,
                following: 3,
            });
        });

        test('Should return the count of followers/followed with other scope', async () => {
            const res = await models.users
                .scope([
                    'profile',
                    {method: ['withFollowersMetters']},
                ])
                .findOne({where: {id: 100}});
            
            expect(res.get({plain: true})).toEqual({
                displayName: 'Ada Lovelace',
                id: 100,
                uuid: '00000000-b5aa-4507-913a-a387419b6ef8',
                nickname: 'user1@fakemail.com',
                mobileNumber: null,
                followers: 2,
                following: 3,
                Profile: {
                    realName: 'Ada Lovelace',
                    gender: 2,
                    lang: 'en',
                    avatar: null,
                },
            });
        });
    });

    describe('isFollowing', () => {

        beforeEach(async () => {
            await loadFixture('users');
            await loadFixture('followers-for-with-followers-metter-scope');
        });

        test('Should to return user who is follower', async () => {
            const res = await models.users.scope(
                {method: ['isFollowing', 101]},
            )
                .findOne({where: {id: 100}});

            expect(res.get({plain: true})).toEqual({
                isFollowing: 1,
                isFollower: 0,
            });
        });

        test('Should to return user who is follower and followed', async () => {
            const res = await models.users.scope(
                {method: ['isFollowing', 102]},
            )
                .findOne({where: {id: 100}});

            expect(res.get({plain: true})).toEqual({
                isFollowing: 1,
                isFollower: 1,
            });
        });

        test('Should to return user who is followed', async () => {
            const res = await models.users.scope(
                {method: ['isFollowing', 103]},
            )
                .findOne({where: {id: 100}});

            expect(res.get({plain: true})).toEqual({
                isFollowing: 0,
                isFollower: 1,
            });
        });

        test('Should to return user without follower/following', async () => {
            const res = await models.users.scope(
                {method: ['isFollowing', 101]},
            )
                .findOne({where: {id: 102}});

            expect(res.get({plain: true})).toEqual({
                isFollowing: 0,
                isFollower: 0,
            });
        });

        test('Should to be combined', async () => {
            const res = await models.users.scope(
                'profile',
                {method: ['isFollowing', 103]},
                {method: ['withFollowersMetters']},
            )
                .findOne({where: {id: 100}});

            expect(res.get({plain: true})).toEqual({
                displayName: 'Ada Lovelace',
                id: 100,
                uuid: '00000000-b5aa-4507-913a-a387419b6ef8',
                nickname: 'user1@fakemail.com',
                mobileNumber: null,
                followers: 2,
                following: 3,
                isFollowing: 0,
                isFollower: 1,
                Profile: {
                    realName: 'Ada Lovelace',
                    gender: 2,
                    lang: 'en',
                    avatar: null,
                },
            });
        });
    });
    
    describe('withContentMetters', () => {
        const scopedModel = models.users
            .scope({method: ['withContentMetters']});

        beforeEach(async () => {
            await loadData('categories');
            await loadFixture('users');
            await loadFixture('content-for-with-content-metters');
            await loadFixture('followers-for-with-followers-metter-scope');
        });

        test('Should return the count of enabled contents', async () => {
            const res = await scopedModel.findOne({where: {id: 100}});

            expect(res.get({plain: true})).toEqual({
                numContents: 4,
            });
        });

        test('Should return zero if user have not cotnents', async () => {
            const res = await scopedModel.findOne({where: {id: 101}});

            expect(res.get({plain: true})).toEqual({
                numContents: 0,
            });
        });

        test('Should to be combined', async () => {
            const res = await models.users.scope(
                'profile',
                {method: ['isFollowing', 103]},
                {method: ['withFollowersMetters']},
                {method: ['withContentMetters']}
            )
                .findOne({where: {id: 100}});

            expect(res.get({plain: true})).toEqual({
                displayName: 'Ada Lovelace',
                id: 100,
                uuid: '00000000-b5aa-4507-913a-a387419b6ef8',
                nickname: 'user1@fakemail.com',
                mobileNumber: null,
                followers: 2,
                following: 3,
                isFollowing: 0,
                isFollower: 1,
                numContents: 4,
                Profile: {
                    realName: 'Ada Lovelace',
                    gender: 2,
                    lang: 'en',
                    avatar: null,
                },
            });
        });

    });
});
