import Promise from 'bluebird';
import createLogger, {createDebug} from '../logger';
import {S3} from 'aws-sdk';
import config from '../config';
import sharp from 'sharp';
import path from 'path';
import models from '../models';
import {ValidationError} from '../errors';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

const BUCKET_NAME = config.get('aws:s3:bucket');
const BUCKET_HOST = config.get('aws:s3:host');
const MEDIA_SIZES = config.get('media:sizes');
const S3_CACHE_CONTROL = config.get('media:s3:cacheControl');

const MEDIA_CONFIG = {
    'content': config.get('aws:s3:contentmedia'),
    'serie': config.get('aws:s3:seriemedia'),
    'avatar': config.get('aws:s3:avatarmedia'),
};

/**
 * Get media configuration for a given path
 * @param {string} s3path
 * @return {object}
 */
export function getMediaConfig(s3path) {
    debug(`MEDIA_CONFIG:`, MEDIA_CONFIG);
    debug(`s3path:`, s3path);
    const configKeys = Object.keys(MEDIA_CONFIG);
    debug(`configKeys:`, configKeys);
    const targetKey = configKeys.find((key) =>
        s3path.startsWith(MEDIA_CONFIG[key].folder));
    debug(`targetKey:`, targetKey);

    if (targetKey) {
        return MEDIA_CONFIG[targetKey];
    }

    return MEDIA_CONFIG['content'];
}

/**
 * Resize a given image in S3
 * @param {string} s3path
 * @return {Promise}
 */
export async function resize(s3path) {
    debug(`Resizing S3 image '${s3path}'...`);
    const ts = Date.now();

    const mediaConfig = getMediaConfig(s3path);

    if (!mediaConfig) {
        debug(`Media configuration not found for path '${s3path}'.`);

        logger.error(
            `[ MEDIARESIZE:ERROR ] ` +
            `Media configuration not found for path '${s3path}'.`
        );
        throw new ValidationError(
            'Media configuration not found for the given path',
            [{
                type: 'Invalid',
                field: 's3path',
                message: `Media configuration not found for ${s3path}`,
            }]
        );
    }

    const s3 = new S3();

    let images = [];

    const params = {
        Bucket: BUCKET_NAME,
        Key: s3path,
    };

    debug(`S3 parameters:`);
    debug(params);

    const s3readStartAt = Date.now();
    const imageData = await s3.getObject(params).promise();
    const s3readDelta = Date.now() - s3readStartAt;
    const imageUrl = `${BUCKET_HOST}${BUCKET_NAME}/${s3path}`;

    const mediaSizes = MEDIA_SIZES[mediaConfig.sizesSet];

    const redimStartAt = Date.now();
    await Promise.map(Object.keys(mediaSizes), async (mediaSize) => {
        const resultBuffer = await sharp(imageData.Body)
            .resize(
                mediaSizes[mediaSize].width,
                mediaSizes[mediaSize].height,
            )
            .toFormat('jpg')
            .toBuffer();

        const targetKey = path.join(
            mediaConfig.processedFolder,
            mediaSize,
            path.basename(s3path),
        );

        images.push(targetKey);

        const newObjectParams = {
            Body: resultBuffer,
            Bucket: BUCKET_NAME,
            ContentType: 'image/jpg',
            Key: targetKey,
            CacheControl: S3_CACHE_CONTROL,
            ACL: mediaConfig.acl,
        };

        debug('New object params:');
        debug(newObjectParams);
        return s3.putObject(newObjectParams).promise();
    });
    const redimDelta = Date.now() - redimStartAt;

    const media = await models.media.findOne({
        where: {url: imageUrl},
        rejectOnEmpty: true,
    });

    await media.update({processed: true});

    images = images.map((image) => `${BUCKET_HOST}${BUCKET_NAME}/${image}`);

    const deltaTotal = Date.now() - ts;
    logger.info(
        '[ MEDIARESIZE:SUCCESS ]  ' +
        `${Object.keys(mediaSizes).length} images processed in ` +
        `${s3readDelta}ms read, ${redimDelta}ms saved, ` +
        `${deltaTotal}ms total`
    );
    debug(images);

    return {
        success: true,
        images,
    };
}

