
import {fromCache} from '../cache';
import {ValidationError} from '../errors';
import {serviceCacheable} from '../cache/decorators';
import models from '../models';

/**
 * List carrusels
 * @param {string} lang
 * @param {string} groupId
 * @return {Promise<object[]>}
 */
export const list = [
    'lang',
    async function list(lang, groupId) {
        const scopes = [
            'enableds',
            'listing',
            {method: ['i18n', lang]},
        ];
        if (groupId) {
            scopes.push({method: ['byGroupId', groupId]});
        }
        return models.carrusels.scope(scopes)
            .findAll();
    },
];

/**
 * List all carrusels (for admin use)
 * @param {string} lang
 * @return {CarruselModel[]}
 */
export const listAll = [
    'lang',
    async function listAll(lang) {
        return models.carrusels.scope([{method: ['i18n', lang]}])
            .findAll({
                attributes: {exclude: ['body']},
            });
    },
];

/**
 * Create carrusel
 * @param {object} data
 * @return {CarruselModel}
 */
export async function create(data) {

    const {names} = data;
    delete data.names;

    const instance = await models.sequelize.transaction(async (tx) => {
        const instance = await models.carrusels.create(data, {transaction: tx});
        await instance.saveNames(names, tx);
        return instance;
    });

    // Reload with metadata
    const final = await models.carrusels.scope('withmetadata')
        .findByPk(instance.uuid, {
            attributes: {
                exclude: ['title', 'body'],
            },
        });

    final.showMetadata();
    return final;
}

/**
 * Update carrusel
 * @param {string} uuid
 * @param {object} data
 * @return {CarruselModel}
 */
export async function update(uuid, data) {
    const {names} = data;

    if (typeof data.names !== 'undefined') {
        delete data.names;
    }

    const carrusel = await models.carrusels.findByPk(uuid, {
        attributes: ['uuid'],
        rejectOnEmpty: true,
    });

    await models.sequelize.transaction(async (tx) => {
        await carrusel.update(data, {
            transaction: tx,
        });

        if (names) {
            await carrusel.saveNames(names, tx);
        }

    });

    // Reload with metadata
    const final = await models.carrusels.scope('withmetadata')
        .findByPk(uuid, {
            attributes: {
                exclude: ['title', 'body'],
            },
        });

    final.showMetadata();
    return final;
}

/**
 * Delete carrusel
 * @param {string} uuid
 * @return {bool}
 */
export async function destroy(uuid) {

    const carrusel = await models.carrusels.findByPk(uuid, {
        attributes: ['uuid'],
        rejectOnEmpty: true,
    });

    await models.sequelize.transaction(async (tx) => {
        return carrusel.destroy({
            transaction: tx,
        });
    });

    return true;
}

/**
 * Get corrusel detail for views
 * @param {string} lang
 * @param {string} uuid
 * @return {CarruselModel}
 */
export const get = [
    'lang',
    async function get(lang, uuid) {
        return models.carrusels.scope([
            'enableds',
            {method: ['i18n', lang]},
            'detailled',
        ])
            .findByPk(uuid, {
                rejectOnEmpty: true,
            });
    },
];

/**
 * Get contents of carrusel
 * @param {string} lang
 * @param {object} geo
 * @param {string} uuid
 * @param {boolean} hasBody
 * @return {ContentModel[]}
 */
export const getContents = [
    'lang',
    'geoIp',
    serviceCacheable(__filename),
    async function getContents(lang, geo, uuid, hasBody) {

        const ids = await fromCache(
            __filename,
            'getContents',
            uuid,
            async () => {
                await models.carrusels.scope('enableds').findByPk(uuid, {
                    rejectOnEmpty: true,
                    attributes: ['uuid'],
                });

                return await models.carruselitem.scope([
                    {method: ['ordered']},
                    {method: ['byType', models.carruselitem.TYPE_VIDEO]},
                    {method: ['byCarrusel', uuid]},
                ])
                    .findAll({
                        attributes: ['entityId'],
                        raw: true,
                    })
                    .then((res) => res.map(({entityId}) => entityId))
                ;
            }
        );

        if (!ids.length) {
            return {
                counts: 0,
                contents: [],
            };
        }

        // GeoBlocking breaks with association, finding directly
        const contents = await models.contents.scope(
            'enabled',
            {method: ['i18n', lang]},
            {method: ['listing', hasBody]},
            {method: ['geoAvailable', geo.country]},
            {method: ['byIds', ids, true]},
        )
            .findAll()
            .then((contents) => contents.map((c) => {
                const plain = c.get({plain: true});
                delete plain.geoBlocking;
                delete plain.metadata;
                return plain;
            }));

        return {
            count: contents.length,
            contents,
        };
    },
];

/**
 * Get all contents of carrusel
 * @param {string} lang
 * @param {string} uuid
 * @return {ContentModel[]}
 */
export const getAllContents = [
    'lang',
    async function getAllContents(lang, uuid) {
        const carrusel = await models.carrusels.scope()
            .findByPk(uuid, {
                attributes: ['uuid'],
                include: [{
                    model: models.contents,
                    attributes: ['uuid'],
                    as: 'Contents',
                    through: {
                        attributes: [],
                    },
                }],
                rejectOnEmpty: true,
            });

        const contentsIds = carrusel.Contents.map((c) => c.uuid);

        // GeoBlocking breaks with association, finding directly
        const contents = models.contents.scope(
            {method: ['i18n', lang]},
            {method: ['listing']},
            {method: ['byIds', contentsIds]},
        )
            .findAll();

        return contents.map((c) => {
            const plain = c.get({plain: true});
            delete plain.metadata;
            return plain;
        });
    },
];

/**
 * Get series of carrusel
 * @param {string} lang
 * @param {object} geo
 * @param {string} uuid
 * @param {boolean} hasBody
 * @return {SerieModel[]}
 */
export const getSeries = [
    'lang',
    'geoIp',
    serviceCacheable(__filename),
    async function getSeries(lang, geo, uuid, hasBody) {

        const ids = await fromCache(__filename, 'getSeries', uuid, async () => {
            await models.carrusels.scope('enableds').findByPk(uuid, {
                rejectOnEmpty: true,
                attributes: ['uuid'],
            });

            return await models.carruselitem.scope([
                {method: ['ordered']},
                {method: ['byType', models.carruselitem.TYPE_SERIE]},
                {method: ['byCarrusel', uuid]},
            ])
                .findAll({
                    attributes: ['entityId'],
                    raw: true,
                })
                .then((res) => res.map(({entityId}) => entityId))
            ;
        });

        if (!ids.length) {
            return {
                counts: 0,
                contents: [],
            };
        }

        // GeoBlocking breaks with association, finding directly
        const series = await models.serie.scope(
            'enabled',
            {method: ['serieContentsList', hasBody]},
            {method: ['geoAvailable', geo.country]},
            {method: ['byIds', ids, true]},
        )
            .findAll()
            .then((series) => series.map((c) => c.fillDataToDisplay()));

        return {
            count: series.length,
            contents: series,
        };
    },
];

/**
 * Get all series of carrusel
 * @param {string} lang
 * @param {string} uuid
 * @return {SerieModel[]}
 */
export const getAllSeries = [
    'lang',
    async function getAllSeries(lang, uuid) {
        const carrusel = await models.carrusels
            .findByPk(uuid, {
                attributes: ['uuid'],
                include: [{
                    model: models.serie,
                    attributes: ['uuid'],
                    as: 'Series',
                    through: {
                        attributes: [],
                    },
                }],
                rejectOnEmpty: true,
            });

        const seriesIds = carrusel.Series.map((c) => c.uuid);

        // GeoBlocking breaks with association, finding directly
        const series = models.serie.scope(
            'listing',
            {method: ['byIds', seriesIds]},
        )
            .findAll();

        return series.map((c) => {
            const plain = c.get({plain: true});
            delete plain.season;
            delete plain.priority;
            return plain;
        });
    },
];

/**
 * Get corrusel detail for admins
 * @param {string} uuid
 * @return {CarruselModel}
 */
export async function getComplete(uuid) {
    const carrusel = await models.carrusels.scope([
        'withmetadata',
    ])
        .findByPk(uuid, {
            attributes: {
                exclude: ['title', 'body'],
            },
            rejectOnEmpty: true,
        });

    // Allow list metadata info
    carrusel.showMetadata();

    return carrusel;
}

/**
 * Assign content to carrusel
 * @param {string} carruselUUID
 * @param {string} contentUUID
 * @param {int} [order=0]
 * @return {Promise<bool>}
 */
export async function assignContent(carruselUUID, contentUUID, order = 0) {
    const carrusel = await models.carrusels
        .findByPk(carruselUUID, {
            attributes: ['uuid', 'name'],
            rejectOnEmpty: true,
        });

    const content = await models.contents.scope('filterCommunityContents')
        .findByPk(contentUUID, {
            attributes: ['uuid'],
            rejectOnEmpty: false,
        });

    if (!content) {
        throw new ValidationError(
            `Unknow content ${contentUUID}`, [{
                field: 'contentUUID',
                type: 'NotFound',
                message: 'Content does not exists',
            }]
        );
    }

    await carrusel.addContent(content, {
        through: {order},
    });

    return true;
}

/**
 * Assign serie to carrusel
 * @param {string} carruselUUID
 * @param {string} serieUUID
 * @param {int} [order=0]
 * @return {Promise<bool>}
 */
export async function assignSerie(carruselUUID, serieUUID, order = 0) {
    const carrusel = await models.carrusels
        .findByPk(carruselUUID, {
            attributes: ['uuid', 'name'],
            rejectOnEmpty: true,
        });

    const serie = await models.serie
        .findByPk(serieUUID, {
            attributes: ['uuid'],
            rejectOnEmpty: false,
        });

    if (!serie) {
        throw new ValidationError(
            `Unknow serie ${serieUUID}`, [{
                field: 'serieUUID',
                type: 'NotFound',
                message: 'Serie does not exists',
            }]
        );
    }

    await carrusel.addSerie(serie, {
        through: {order},
    });

    return true;
}

/**
 * Remove content to carrusel
 * @param {string} carruselUUID
 * @param {string} contentUUID
 * @return {Promise<bool>}
 */
export async function removeContent(carruselUUID, contentUUID) {
    const carrusel = await models.carrusels
        .findByPk(carruselUUID, {
            attributes: ['uuid', 'name'],
            rejectOnEmpty: true,
        });

    const content = await models.contents
        .findByPk(contentUUID, {
            attributes: ['uuid'],
            rejectOnEmpty: false,
        });

    if (!content) {
        throw new ValidationError(
            `Unknow content ${contentUUID}`, [{
                field: 'contentUUID',
                type: 'NotFound',
                message: 'Content does not exists',
            }]
        );
    }

    await carrusel.removeContent(content);

    return true;
}

/**
 * Remove serie to carrusel
 * @param {string} carruselUUID
 * @param {string} serieUUID
 * @return {Promise<bool>}
 */
export async function removeSerie(carruselUUID, serieUUID) {
    const carrusel = await models.carrusels
        .findByPk(carruselUUID, {
            attributes: ['uuid', 'name'],
            rejectOnEmpty: true,
        });

    const serie = await models.serie
        .findByPk(serieUUID, {
            attributes: ['uuid'],
            rejectOnEmpty: false,
        });

    if (!serie) {
        throw new ValidationError(
            `Unknow serie ${serieUUID}`, [{
                field: 'serieUUID',
                type: 'NotFound',
                message: 'Serie does not exists',
            }]
        );
    }

    await carrusel.removeSerie(serie);

    return true;
}
