
import net from 'net';
import {Promise} from 'bluebird';
import config from '../config';
import {getStatus} from '../status';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

const PORT = config.get('healthcheck:port');

/**
 * Health status server
 */
export default class StatusServer {

    /**
     * Create new server
     */
    constructor() {
        this._server = net.createServer(async (sock) => {
            debug(`Resolve status `);
            const status = await getStatus();
            sock.write(JSON.stringify(status), 'utf8');
            sock.end();
        });
    }

    /**
     * Listen
     */
    async listen() {
        debug(`Listening health server on port ${PORT}`);
        await new Promise((resolve) => {
            this._server.listen(PORT, () => {
                debug(`Listening health server`);
                return resolve();
            });
        });
    }

    /**
     * Create new server
     * @return {StatusServer}
     */
    static async createServer() {
        const server = new StatusServer();
        await server.listen();
        return server;
    }
}
