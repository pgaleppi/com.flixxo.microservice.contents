import Sequelize from 'sequelize';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

export default (sequelize, DataTypes) => {

    let ContentModel;
    const instanceMethods = {};

    const SubtitleModel = sequelize.define('Subtitle', {

        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },

        lang: {
            type: DataTypes.STRING(5),
            allowNull: false,
            defaultValue: 'en',
            validate: {
                is: {
                    args: /^[a-z]{2}(?:_[A-Z]{2})?/,
                    msg: 'Invalid lang format',
                },
            },
        },

        label: {
            type: DataTypes.STRING(2014),
            allowNull: true,
            validate: {
                is: /^.{3,}$/,
            },
        },

        url: {
            type: DataTypes.STRING(1024),
            allowNull: false,
            validate: {
                isUrl: true,
            },
        },

        uuid: {
            type: DataTypes.UUID,
            allowNull: false,
        },

        closeCaption: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },

        isDraft: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },

    }, {
        timestamps: true,
        paranoid: true,
        instanceMethods,

        defaultScope: {
            isDraft: false,
        },

        scopes: {
            historical: {
                paranoid: false,
            },
        },

    });

    if (typeof SubtitleModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                SubtitleModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    SubtitleModel.associate = function(models) {
        ContentModel = models.contents;

        SubtitleModel.belongsTo(ContentModel, {
            as: 'content',
            foreignKey: 'uuid',
            targetKey: 'uuid',
            constraints: false,
        });

    };

    SubtitleModel.addScope('queryList', (subtitleIds) => {
        return {
            where: {
                id: {
                    [Sequelize.Op.in]: subtitleIds,
                },
            },
        };
    });


    return SubtitleModel;
};
