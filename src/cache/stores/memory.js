import {createDebug} from '../../logger';
import Store from './base';

const debug = createDebug(__filename);

let cache = [];
let flags = [];

/**
 * Store cache
 */
export default class MemStore extends Store {

    /**
     * Get value
     * @param {string} rkey
     * @return {any}
     */
    async get(rkey) {
        const key = `${this._prefix}:${rkey}`;
        const res = cache.find((x) => x.key === key);
        debug(`Getting ${key}`, !!res);
        return res && res.expire >= Date.now() ? JSON.parse(res.value) : null;
    }

    /**
     * Set value
     * @param {string} rkey
     * @param {any} val
     * @param {object} [options]
     * @param {integer} [options.expiration]
     */
    async set(rkey, val, options = {}) {
        const exp = options.expiration || this._options.expiration;
        const key = `${this._prefix}:${rkey}`;
        const jval = JSON.stringify(val);
        const res = cache.find((x) => x.key === key);
        const exps = Date.now() + exp * 1000;

        debug(
            `Saving ${key} with ${exp} expiration as ` +
            res ? 'existing': 'new');

        if (res) {
            debug('\tupdate');
            res.value = jval;
            res.expire = exps;
        } else {
            debug(`\tpushing`);
            cache.push({
                key,
                rkey,
                expire: exps,
                value: jval,
            });
        }
    }

    /**
     * Touch flag
     * @param {string} flag
     */
    async touchFlag(flag) {
        const timestamp = Date.now();
        flags = flags.filter((x) => x.flag !== flag);
        flags.push({
            flag,
            timestamp,
        });
    }
}

/**
 * Clear expired cache
 */
export function clearExpireCahce() {
    const now = Date.now();
    cache = cache.filter((x) => x.expire < now);
}

/**
 * Clear all cache
 * @param {bool} [noFlags=false] If true don't clear flags
 */
export function clearCache(noFlags = false) {
    debug(`Clear cache!`);
    cache = [];
    if (!noFlags) {
        flags = [];
    }
}

/**
 * Return true if key and value exists
 * @param {string} key
 * @param {any} val
 * @return {boolean}
 */
export function isSaved(key, val) {
    debug(`Search ${key}`);
    return !!cache
        .find((c) => (c.key === key) &&
              (!val || JSON.stringify(val) === c.value));
}

/**
 * Get flag timestamp
 * @param {string} flag
 * @return {number|null}
 */
export function getFlag(flag) {
    const iflag = flags.find((x) => x.flag === flag);
    return iflag ? iflag.timestamp : null;
}
