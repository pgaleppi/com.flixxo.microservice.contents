
import Promise from 'bluebird';
import {createDebug} from '../../logger';

const debug = createDebug(__filename);

/**
 * ErrorMapper
 * Mapping errors with to RPC
 */
export default class ErrorMapper {

    /**
     * Create new mapper
     */
    constructor() {
        this._maps = [];
    }

    /**
     * Set new error handler
     * @param {string|Error} name
     * @param {function} retErrorCallback
     * @return {this}
     */
    use(name, retErrorCallback) {

        if (name instanceof Error) {
            name = name.name;
        }

        this._maps.push({
            name: name,
            callback: retErrorCallback,
        });

        return this;
    }

    /**
     * Resolve error
     * @param {Error} err
     * @return {Error}
     */
    async resolve(err) {
        const name = err.name;

        const resolved = await Promise.reduce(
            this._maps,
            async (resolved, def) => {
                if (def.name === name || def.name === '*') {
                    debug(`Translating error ${def.name}`);
                    try {
                        resolved = await Promise.resolve(def.callback(err));
                        debug(`\tTranlated by ${resolved.name}`);
                    } catch (e) {
                        debug(`Error translating error`, e);
                        throw e;
                    }
                }

                return resolved;
            },
            err
        );

        return resolved;
    }

    /**
     * Apply as middlware
     * @return {function}
     */
    applyMiddleware() {
        return ['errorMapper', 'rejected', async (error) => {
            try {
                error = await this.resolve(error);
            } catch (e) {
                debug(`Error resolving error`, e);
                throw e;
            }

            return error;
        }];
    }
}
