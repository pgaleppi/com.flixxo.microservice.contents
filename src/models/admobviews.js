
import Verifier from '@exoshtw/admob-ssv';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

const SSV_PICK_KEYS = [
    'ad_network',
    'ad_unit',
    'custom_data',
    'key_id',
    'reward_amount',
    'reward_item',
    'signature',
    'timestamp',
    'transaction_id',
    'user_id',
];

const STATUS_NEW = 0x01;
const STATUS_PAYED = 0x02;

/**
 * AdMobViewsModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {
    let UsersModel;
    let verifier;

    const instanceMethods = {};

    const getVerifier = function() {
        if (!verifier) {
            verifier = new Verifier();
        }

        return verifier;
    };

    const AdMobViewsModel = sequelize.define('AdMobView', {

        uuid: {
            type: DataTypes.UUID,
            primaryKey: true,
        },

        signature: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        rewardedAt: {
            type: DataTypes.DATE,
            allowNull: false,
        },

        transactionId: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate: {
                is: /^[a-f0-9]+$/i,
            },
        },

        userId: {
            type: DataTypes.UUID,
            allowNull: false,
        },

        rawData: {
            type: DataTypes.TEXT,
            allowNull: false,
            get() {
                const val = this.getDataValue('rawData');
                return val ? JSON.parse(val) : null;
            },
            set(val) {
                if (typeof val === 'string') {
                    val = JSON.parse(val);
                }

                const picked = Object.fromEntries(
                    Object.entries(val)
                        .filter(([key]) => SSV_PICK_KEYS.includes(key))
                );

                this.setDataValue('rawData', JSON.stringify(picked));
            },
        },

        status: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: STATUS_NEW,
            validate: {
                isIn: [[
                    STATUS_NEW,
                    STATUS_PAYED,
                ]],
            },
        },

    }, {
        name: {
            singular: 'AdMobView',
            plural: 'AdMobViews',
        },
        timestamps: true,
        instanceMethods: instanceMethods,
        validate: {
            async validSSVSignature() {
                const verifier = getVerifier();
                const {rawData} = this;
                let isValid = false;

                try {
                    isValid = await verifier.verify(rawData);
                } catch (e) {
                    if (e.message === 'Key not found') {
                        isValid = false;
                    } else {
                        throw e;
                    }
                }

                if (!isValid) {
                    throw new Error('Not valid signature');
                }
            },
        },
    });

    if (typeof AdMobViewsModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                AdMobViewsModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    AdMobViewsModel.STATUS_NEW = STATUS_NEW;
    AdMobViewsModel.STATUS_PAYED = STATUS_PAYED;

    AdMobViewsModel.associate = function(models) {
        UsersModel = models.users;

        AdMobViewsModel.belongsTo(UsersModel, {
            as: 'Viewer',
            targetKey: 'uuid',
            foreignKey: 'userId',
            constraints: true,
        });

        // Scopes
        AdMobViewsModel.addScope('availables', {
            where: {
                status: STATUS_NEW,
            },
        });

        AdMobViewsModel.addScope('byUser', (userId) => ({
            where: {userId},
        }));
    };

    /**
     * Get new AdMobViewsModel from rawData
     * @param {object} rawData
     * @return {AdMobViewsModel}
     */
    AdMobViewsModel.fromRaw = function(rawData) {
        return this.build({
            rawData,
            uuid: rawData['custom_data'],
            signature: rawData['signature'],
            rewardedAt: parseInt(rawData['timestamp'], 10),
            transactionId: rawData['transaction_id'],
            userId: rawData['user_id'],
        });
    };

    return AdMobViewsModel;
};
