
import ExtendableError from 'extendable-error';

/**
 * RPC error
 */
export default class RPCError extends ExtendableError {
    /**
     * Create new RPC error
     * @param {string} message Error message
     * @param {object} command Command object
     */
    constructor(message, command) {
        super(message);
        this._command = command;
    }

    /**
     * Command object
     * @return {object}
     */
    get command() {
        return this._command;
    }
}

/**
 * Timeout error for RPC commands
 */
export class RPCTimeOutError extends RPCError {
    /**
     * @param {object} command Command object
     */
    constructor(command) {
        super(`Timeout for command ${command.uid}`, command);
    }
}

/**
 * Remote error wrapper
 */
export class RPCRemoteError extends ExtendableError {

    /**
     * Create new RPCRemoteError
     * @param {object} errObject Remote object error
     */
    constructor(errObject) {
        super(errObject.message);
        this._errObject = errObject;
        this.name = `Remote${this._errObject.name}`;
    }

    /**
     * @type {object}
     */
    get errorObject() {
        return this._errObject;
    }
}
