import Sequelize, {Op, EmptyResultError} from 'sequelize';
import sqlString from 'sequelize/lib/sql-string';
import Promise from 'bluebird';
import {createDebug} from '../logger';
import {
    RATING_ALL,
    RATING_MILD,
    RATING_MATURE,
} from './contents';

import {multidialectCustomOrder} from '../storage';

const debug = createDebug(__filename);

export const STATUS_DRAFT = 0x10;
export const STATUS_REVIEW = 0x20;
export const STATUS_REQUEST_CHANGES = 0x30;
export const STATUS_CANCELED = 0x40;
export const STATUS_ACCEPTED = 0x50;
export const STATUS_REJECTED = 0x60;
export const STATUS_REQUEST_CHANGES_HISTORIC = 0x70;

export default (sequelize, DataTypes) => {
    let ContentModel;
    let SubtitleModel;
    let CategoryModel;
    let MetadataModel;
    let TagModel;
    let MediaModel;

    const instanceMethods = {

        /**
         * Find or create tags and set
         * @param {string[]} tags
         * @param {object} transaction
         * @return {TagModel[]}
         */
        async findAndSetTags(tags, transaction) {
            const otags = await TagModel.getTags(...tags);
            debug(`Setting tags`);
            await this.setTags(otags, {transaction});
            return otags;
        },

        /**
         * Set status
         * @param {integer} nstatus
         * @param {string} [comment]
         * @param {Sequelize.Transaction} [transaction]
         * @return {Promise}
         */
        async setStatus(nstatus, comment = '', transaction = null) {
            debug(`Set status of ${this.uuid} ${this.status} -> ${nstatus}`);
            this.status = nstatus;
            this.statusComment = comment;
            return this.save({transaction});
        },

        /**
         * Clone version
         * @TODO clone tags and media
         * @param {boolean} [useRef=true] Use this as reference
         * @param {Sequelize.Transaction} transaction
         * @return {Promise<ContentVersionModel>}
         */
        async clone(useRef = true, transaction) {
            debug(`Clonning ${this.uuid}`);
            let medias = [];
            let subtitles = [];
            let metas = [];

            if (this.media) {
                medias = this.media.map(((m) => {
                    m = m.get({plain: true});
                    delete m.id;
                    delete m.sizes;
                    return m;
                }));
            }

            if (this.subtitle && this.subtitle.length) {
                subtitles = this.subtitle.map(((s) => {
                    s = s.get({plain: true});
                    delete s.id;
                    return s;
                }));
            }

            const metadata = await this.getMetadata();

            if (metadata && metadata.length) {
                metas = metadata.map(((m) => {
                    m = m.get({plain: true});
                    delete m.uuid;
                    delete m.entityId;
                    return m;
                }));
            }

            const cloned = await ContentVersionModel.create({
                ...this.get({plain: true}),
                media: medias,
                subtitle: subtitles,
                metadata: metas,
                uuid: undefined,
                createdAt: undefined,
                updatedAt: undefined,
                statusComment: null,
                status: STATUS_DRAFT,
                internalReference: useRef ? this.uuid : null,
            }, {
                include: ['media', 'subtitle', 'metadata'],
                transaction,
            });
            return cloned;
        },

        /**
         * Get plain object to represent new data
         * @TODO medias
         * @return {object}
         */
        async getPlain() {
            const {
                title,
                body,
                audioLang,
                seederFee,
                authorFee,
                categoryId,
                rating,
                credits,
                duration,
            } = this;
            const plainTags = await this.getTags().map(
                (tagModel) => tagModel.tag
            );
            return {
                title,
                body,
                audioLang,
                seederFee,
                authorFee,
                categoryId,
                rating,
                credits,
                duration,
                tags: plainTags,
            };
        },

        /**
         * Set content parent version status
         * @param {integer} status
         * @param {sequelize.Transaction} [transaction]
         * @return {Promise}
         */
        async setContentVersionStatus(status, transaction = null) {
            debug(`Setting status of content of version ${this.uuid}`, status);
            return ContentModel.update({
                versionStatus: status,
            }, {
                where: {uuid: this.contentUUID},
                validate: true,
                hooks: false,
                transaction: transaction,
            });
        },

        /**
         * Get changed fields
         * @param {object} baseContent
         * @return {Promise}
         */
        async getChangedFields(baseContent) {
            debug('Filtering changed fields..');
            const fieldsToCheck = Object.keys((await this.getPlain()));

            const changedFields = fieldsToCheck.reduce((total, field) => {
                if (baseContent[field] !== this[field]) {
                    total.push(field);
                }
                return total;
            }, []);

            return changedFields;
        },

        /**
         * Only price has changed
         * @param {object} baseContent
         * @return {Promise}
         */
        async onlyPriceHasChanged(baseContent) {
            const changedFields = await this.getChangedFields(baseContent);
            const priceFields = changedFields.filter((field) => {
                return (['seederFee', 'authorFee'].includes(field));
            });
            // We need to blacklist tags because there is a conflict
            // between tags (virtual) & Tags (relation) fields
            const blackListFields = changedFields.filter((field) => {
                return !(['seederFee', 'authorFee', 'tags'].includes(field));
            });
            return (
                blackListFields.length === 0 &&
                (
                    priceFields.includes('seederFee') ||
                    priceFields.includes('authorFee')
                )
            );
        },
    };

    const ContentVersionModel = sequelize.define('ContentVersion', {

        uuid: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },

        contentUUID: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            index: true,
        },

        title: {
            type: DataTypes.VIRTUAL(
                DataTypes.STRING(1024),
            ),
            get() {
                const meta = this.metadata;
                if (meta && meta.length) {
                    return meta[0].title;
                } else {
                    return '';
                }
            },
        },

        body: {
            type: DataTypes.VIRTUAL(
                DataTypes.TEXT,
            ),
            get() {
                const meta = this.metadata;
                if (meta && meta.length) {
                    return meta[0].body;
                } else {
                    return '';
                }
            },
        },

        audioLang: {
            type: DataTypes.STRING(5),
            allowNull: false,
            defaultValue: 'en',
            validate: {
                is: {
                    args: /^[a-z]{2}(?:_[A-Z]{2})?/,
                    msg: 'Invalid lang format',
                },
            },
        },

        categoryId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
        },

        rating: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: RATING_ALL,
            validate: {
                isIn: [[
                    RATING_ALL,
                    RATING_MILD,
                    RATING_MATURE,
                ]],
            },
        },

        credits: {
            type: DataTypes.TEXT,
            allowNull: true,
            validate: {
                is: /^(\n|.){5,}$/,
            },
        },

        tags: {
            type: DataTypes.VIRTUAL,
        },

        duration: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },

        authorFee: {
            type: DataTypes.DECIMAL(36, 18),
            allowNull: false,
        },

        seederFee: {
            type: DataTypes.DECIMAL(36, 18),
            allowNull: true,
        },

        internalReference: {
            type: DataTypes.UUID,
            allowNull: true,
        },

        status: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: STATUS_DRAFT,
            validate: {
                isIn: {
                    args: [[
                        STATUS_DRAFT,
                        STATUS_REVIEW,
                        STATUS_REQUEST_CHANGES,
                        STATUS_CANCELED,
                        STATUS_ACCEPTED,
                        STATUS_REJECTED,
                        STATUS_REQUEST_CHANGES_HISTORIC,
                    ]],
                    msg: 'Invalid version status',
                },
            },
        },

        statusComment: {
            type: DataTypes.STRING(512),
            allowNull: true,
        },

    }, {
        timestamps: true,
        paranoid: true,

        indexes: [{
            name: 'only_one_active_version',
            unique: true,
            fields: ['contentUUID'],
            where: {
                status: {
                    [Op.in]: [
                        STATUS_DRAFT,
                        STATUS_REVIEW,
                    ],
                },
            },
        }],

        validate: {

            statusFlow() {
                if (this.isNewRecord) {
                    return;
                }

                const changeStatus = this.changed('status');
                const lastStatus = this.previous('status');

                if (changeStatus && lastStatus === STATUS_CANCELED) {
                    throw new Error('Canceled version can\'t change status');
                } else if (changeStatus && lastStatus === STATUS_REJECTED) {
                    throw new Error('Rejected version can\'t change status');
                }
            },

            /**
             * @TODO IF is canceled (rejected, etc) not set other status
             */
            canceledStatus() {
                if (this.isNewRecord) {
                    return;
                }
            },
        },
        hooks: {

            afterCreate(instance, options) {
                // @see https://github.com/sequelize/sequelize/issues/6800
                return Promise.resolve((async () => {
                    if (instance.tags && instance.tags.length > 0) {
                        debug(`Adding content tags`);
                        await instance.findAndSetTags(
                            instance.tags,
                            options.transaction,
                        );
                    }
                })());
            },

            beforeUpdate(instance, options) {
                // @see https://github.com/sequelize/sequelize/issues/6800
                return Promise.resolve((async () => {
                    if (instance.tags) {
                        debug(`Setting content tags`);
                        await instance.findAndSetTags(
                            instance.tags,
                            options.transaction,
                        );
                    }
                })());
            },

            afterSave(instance, options) {
                // @see https://github.com/sequelize/sequelize/issues/6800
                return Promise.resolve((async () => {
                    const isStatusChanged = instance.changed('status');
                    const isActive = instance.status === STATUS_REVIEW;
                    debug(
                        'Checking if status changed', isStatusChanged, isActive
                    );

                    if (instance.isNewRecord &&
                        instance.status === STATUS_REVIEW
                    ) {
                        debug(`\tIs new reconrd, and status is review`);
                        await instance.setContentVersionStatus(
                            ContentModel.VERSION_STATUS_ACTIVE,
                            options.transaction
                        );
                    } else if (isStatusChanged && isActive) {
                        debug(`\tStatus change and is active`);
                        await instance.setContentVersionStatus(
                            ContentModel.VERSION_STATUS_ACTIVE,
                            options.transaction
                        );
                    } else if (isStatusChanged) {
                        debug(`\tStatus change and is NOT active`);
                        await instance.setContentVersionStatus(
                            ContentModel.VERSION_STATUS_NONE,
                            options.transaction
                        );
                    }
                })());
            },
        },

        instanceMethods,
    });

    if (typeof ContentVersionModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                ContentVersionModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    ContentVersionModel.STATUS_DRAFT = STATUS_DRAFT;
    ContentVersionModel.STATUS_REVIEW = STATUS_REVIEW;
    ContentVersionModel.STATUS_REQUEST_CHANGES = STATUS_REQUEST_CHANGES;
    ContentVersionModel.STATUS_CANCELED = STATUS_CANCELED;
    ContentVersionModel.STATUS_ACCEPTED = STATUS_ACCEPTED;
    ContentVersionModel.STATUS_REJECTED = STATUS_REJECTED;
    ContentVersionModel.STATUS_REQUEST_CHANGES_HISTORIC =
        STATUS_REQUEST_CHANGES_HISTORIC;

    /**
     * @override
     */
    ContentVersionModel.associate = function(models, db) {
        ContentModel = models.contents;
        SubtitleModel = models.subtitle;
        CategoryModel = models.categories;
        MetadataModel = models.metadata;
        TagModel = models.tags;
        MediaModel = models.media;

        ContentVersionModel.belongsTo(ContentModel, {
            as: 'Content',
            foreignKey: 'contentUUID',
            targetKey: 'uuid',
            constraints: false,
        });

        ContentVersionModel.belongsTo(CategoryModel, {
            as: 'Category',
            foreignKey: 'categoryId',
            targetKey: 'id',
            constraints: true,
        });

        ContentVersionModel.hasMany(MetadataModel, {
            as: 'metadata',
            foreignKey: 'entityId',
            targetKey: 'uuid',
            constraints: false,
            scope: {
                entityType: MetadataModel.ENTITY_TYPE_CONTENTVERSION,
            },
        });

        ContentVersionModel.belongsTo(ContentVersionModel, {
            as: 'Ref',
            foreignKey: 'internalReference',
            targetKey: 'contentUUID',
            constraints: false,
        });

        ContentVersionModel.belongsToMany(TagModel, {
            through: {
                model: 'ContentVersionTags',
                attributes: [],
                unique: false,
            },
            timestamps: false,
            paranoid: false,
        });

        ContentVersionModel.hasMany(MediaModel, {
            as: 'media',
            foreignKey: 'uuid',
            targetKey: 'uuid',
            constraints: false,
            scope: {
                isDraft: true,
            },
        });

        ContentVersionModel.hasMany(SubtitleModel, {
            as: 'subtitle',
            foreignKey: 'uuid',
            targetKey: 'uuid',
            constraints: false,
            scope: {
                isDraft: true,
            },
        });

        ContentVersionModel.addScope('historicalByContent', (contentUUID) => {
            return {
                where: {
                    contentUUID,
                    status: STATUS_ACCEPTED,
                },
                order: [
                    ['updatedAt', 'DESC'],
                ],
            };
        });

        // Scopes
        ContentVersionModel.addScope('categoryJoin', () => {
            return {
                include: [{
                    model: CategoryModel.scope('public'),
                    as: 'Category',
                    required: true,
                }],
            };
        });

        ContentVersionModel.addScope('actualByContent', (contentUUID) => {
            return {
                where: {
                    contentUUID,
                    status: [
                        STATUS_DRAFT,
                        STATUS_REVIEW,
                        STATUS_REQUEST_CHANGES,
                    ],
                },
                order: [
                    ['updatedAt', 'DESC'],
                ],
                include: [{
                    model: ContentModel,
                    as: 'Content',
                    attributes: [
                        'contentType',
                    ],
                    required: false,
                },
                {
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'id',
                        'name',
                        'type',
                        'url',
                        'order',
                        'isDraft',
                        'uuid',
                        'uuid',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: [
                        'id',
                        'lang',
                        'label',
                        'url',
                        'closeCaption',
                    ],
                    required: false,
                }, {
                    model: TagModel,
                    attributes: ['tag'],
                    required: false,
                    through: {
                        attributes: [],
                    },
                },
                {
                    model: MetadataModel,
                    as: 'metadata',
                    attributes: [
                        'lang',
                        'title',
                        'body',
                    ],
                    required: false,
                }],
            };
        });

        ContentVersionModel.addScope(
            'byModerator',
            (moderatorId) => ({
                include: [{
                    model: ContentModel,
                    as: 'Content',
                    attributes: [
                        'uuid',
                        'moderatorId',
                    ],
                    include: [{
                        model: SubtitleModel,
                        as: 'subtitle',
                        attributes: [
                            'id',
                            'lang',
                            'label',
                            'url',
                            'closeCaption',
                        ],
                        required: false,
                    }],
                    where: {
                        moderatorId,
                    },
                    required: true,
                }],
            })
        );

        ContentVersionModel.addScope(
            'activeForModeration',
            (moderatorId) => ({
                where: {
                    status: STATUS_REVIEW,
                },
                order: [
                    ['updatedAt', 'DESC'],
                ],
                include: [{
                    model: ContentModel,
                    as: 'Content',
                    attributes: [
                        'uuid',
                        'moderatorId',
                    ],
                    where: {
                        moderatorId,
                    },
                    required: true,
                }],
            })
        );

        ContentVersionModel.addScope('listing', {
            attributes: [
                'uuid',
                'internalReference',
                'status',
                'authorFee',
                'seederFee',
                'statusComment',
                'createdAt',
                'updatedAt',
            ],
            order: [
                ['updatedAt', 'DESC'],
            ],
        });

        ContentVersionModel.addScope('detailled', {
            attributes: [
                'contentUUID',
                'uuid',
                'title',
                'body',
                'audioLang',
                'rating',
                'credits',
                'duration',
                'internalReference',
                'status',
                'statusComment',
                'authorFee',
                'seederFee',
                'createdAt',
                'updatedAt',
            ],
            include: [{
                model: MediaModel,
                as: 'media',
                attributes: [
                    'name', 'type',
                    'url', 'order',
                    'isDraft', 'uuid',
                ],
                required: false,
            }, {
                model: TagModel,
                attributes: ['tag'],
                required: false,
                through: {
                    attributes: [],
                },
            }, {
                model: CategoryModel,
                attributes: [
                    'id',
                    'name',
                ],
                as: 'Category',
                required: false,
            },
            {
                model: ContentModel,
                as: 'Content',
                attributes: [
                    'uuid',
                ],
                include: [{
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: [
                        'id',
                        'lang',
                        'label',
                        'url',
                        'closeCaption',
                    ],
                    required: false,
                }],
                required: true,
            }],
        });

        ContentVersionModel.addScope('i18n', (lang) => {
            lang = lang ? lang.split('_')[0].toLowerCase() : '';

            return {
                include: [
                    {
                        model: MetadataModel,
                        as: 'metadata',
                        attributes: [
                            [
                                Sequelize.literal(sqlString.escape(lang)),
                                'context_lang',
                            ],
                            'lang',
                            'title',
                            'body',
                        ],
                        required: false,
                        separate: true,
                        where: {
                            lang: [
                                lang,
                                Sequelize.literal(`audioLang`),
                            ],
                        },
                        include: [
                            {
                                model: ContentVersionModel,
                                as: 'contentversion',
                                required: false,
                                attributes: [
                                    'audioLang',
                                ],
                            },
                        ],
                        order: multidialectCustomOrder(
                            'lang',
                            lang,
                            'audioLang',
                        ),
                        // limit: 1, Esto rompe todo. sequelize WTF
                    },
                ],

            };
        });

    };

    ContentVersionModel.findByUUID =
        async function(contentUUID, uuid, lang, options = {}) {
            const version = await ContentVersionModel
                .scope({method: ['i18n', lang]})
                .findOne({
                    where: {contentUUID, uuid},
                    rejectOnEmpty: false,
                    ...options,
                });

            if (!version) {
                throw new EmptyResultError();
            }

            return version;
        };

    ContentVersionModel.getSeederFeeAt =
    async function(contentUUID, ts, options = {}) {
        debug(`Get last applied version seederFee`);
        let time;

        if (typeof time === 'number') {
            time = new Date(ts);
        }

        const versions = await ContentVersionModel.scope([
            {method: ['historicalByContent', contentUUID]},
        ])
            .findAll({
                attributes: ['seederFee'],
                where: {
                    'updatedAt': {
                        [Op.lte]: ts, // The last before (or inclusive) the date
                    },
                    'seederFee': {
                        [Op.not]: null, // Ignore version wihout price
                    },
                },
                limit: 1, // Only the last
                ...options,
            });

        if (versions.length) {
            return versions[0].seederFee;
        } else {
            return null;
        }
    };

    return ContentVersionModel;
};
