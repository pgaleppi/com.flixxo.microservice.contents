
import models from '../models';

/**
 * List all availables langs
 * @return {Promise<object[]>}
 */
export async function list() {
    const langs = await models.langs.findAll();
    return langs
        .map((x) => x.get({plain: true}));
}
