
import getConnection from './connection';
import Server from './server';
import packageData from '../../package.json';
import Command from './command';

/**
 * Create RPC Server
 * @return {Server}
 */
export async function createServer() {
    const con = await getConnection();
    return new Server(con, packageData.name);
}

/**
 * Get service connector
 * @param {string} service
 * @return {Promise<Command>}
 */
export async function getServiceConnection(service) {
    const con = await getConnection();

    return new Command(con, service);
}
