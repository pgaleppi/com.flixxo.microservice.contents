import extendableError from 'extendable-error';

/**
 * Showable Error
 */
export class ShowableError extends extendableError {

    /**
     * @abstract
     * @return {object}
     */
    extendShowableError() {
        return {};
    }

    /**
     * Get text message
     * @return {string}
     */
    getMessage() {
        return this.message;
    }

    /**
     * Represent error as JSON object
     * @return {object}
     */
    toJSON() {
        return {
            message: this.getMessage(),
            name: this.name,
            showable: true,
            internal: false,
            ...this.extendShowableError(),
        };
    }
}

/**
 * Not found error
 */
export class NotFoundError extends ShowableError {

    /**
     * @override
     * @param {string}  [message]
     * @param {any} [item]
     */
    constructor(message = 'Item not found', item = null) {
        super(message);
        this._item = item;
    }

    /**
     * @type {any}
     */
    get item() {
        return this._item;
    }

    /**
     * @override
     */
    extendShowableError() {
        return {
            item: this.item,
        };
    }
}

/**
 * Validation Error
 */
export class ValidationError extends ShowableError {
    /**
     * @override
     * @param {string}  [message]
     * @param {object[]} [errors]
     */
    constructor(message = 'Validation error', errors = []) {
        super(message);
        this._errors = errors;
    }

    /**
     * @type {object[]}
     */
    get errors() {
        return this._errors;
    }

    /**
     * @override
     */
    extendShowableError() {
        return {
            errors: this.errors,
        };
    }

    /**
     * Create new ValidationError from SequelizeValidationError
     * @param {SequelizeValidationError} error
     * @return {ValidationError}
     */
    static fromSequelizeValidationError(error) {
        const {errors} = error;

        const errorItems = errors.map((err) => {
            return {
                field: err.path,
                message: err.message,
                type: err.type,
            };
        });

        return new ValidationError(error.message, errorItems);
    }
}

/**
 * @override
 */
export class UniqueConstraintError extends ValidationError {
    /**
     * @override
     */
    static fromSequelizeValidationError(error) {
        const {errors} = error;

        const errorItems = errors.map((err) => {
            return {
                field: err.path,
                message: err.message,
            };
        });

        return new ValidationError(error.message, errorItems);
    }
}

/**
 * Error throwed when the operation is limited
 */
export class OperationLimit extends ShowableError {}

/**
 * Error throwd when the operation need a time to be enabled again
 */
export class TimeOperationLimit extends OperationLimit {
    /**
     * @override
     * @param {string}  [message]
     * @param {data|integer} [freeAt]
     */
    constructor(message = 'Operation not permitted by limit', freeAt = null) {
        super(message);
        if (typeof freeAt === 'number') {
            freeAt = new Date(freeAt);
        }
        this._freeAt = freeAt;
    }

    /**
     * @override
     */
    extendShowableError() {
        if (this._freeAt) {
            return {
                freeAt: this._freeAt,
            };
        } else {
            return {};
        }
    }
}

/**
 * Wrap error and return JSON
 * @param {Error} error
 * @return {object}
 */
export function wrapError(error) {
    if (error instanceof ShowableError) {
        return error.toJSON();
    } else {
        return {
            message: error.message,
            showable: false,
            internal: true,
            name: 'UnknownError',
            stack: error.stack,
        };
    }
}
