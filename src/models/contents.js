
import axios from 'axios';
import Promise from 'bluebird';
import {Op} from 'sequelize';
import {Decimal} from 'decimal.js';
import {multidialectNow, multidialectCustomOrderItems} from '../storage';
import {getServiceConnection} from '../sqsrpc';
import config from '../config';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

const SLS_HOST = config.get('serverless:host');
const SLS_CONTENTS = config.get('serverless:contents');

const DELETE_DELTA = config.get('publication:deleteTime');

const TYPE_VIDEO = 0x01;
const TYPE_SERIE = 0x02;
const FORMAT_MARKDOWN = 0x01;
const PAYTYPE_VIEW = 0x01;

const STATUS_NEW = 0x10;
const STATUS_ENABLED = 0x20;
const STATUS_CANCEL = 0x30;
const STATUS_PAUSED = 0x40;

const MODSTATUS_NEW = 0x10;
const MODSTATUS_ASSIGNED = 0x20;
const MODSTATUS_INPROGRESS = 0x30;
const MODSTATUS_REJECT = 0x40;
const MODSTATUS_ENABLED = 0x50;
const MODSTATUS_REQUEST_CHANGES = 0x60;
const MODSTATUS_CHANGES_REVIEW = 0x70;

export const VERSION_STATUS_NONE = 0x00;
export const VERSION_STATUS_ACTIVE = 0x10;

const PRIVACY_LISTED = 0x01;
const PRIVACY_PUBLIC_URL = 0x02;
const PRIVACY_PUBLIC = PRIVACY_LISTED | PRIVACY_PUBLIC_URL;

export const RATING_ALL = 0x01;
export const RATING_MILD = 0x02;
export const RATING_MATURE = 0x03;

export default (sequelize, DataTypes) => {
    let UsersModel;
    let CategoryModel;
    let ContentUpdateModel;
    let ContentVersionModel;
    let TagModel;
    let MediaModel;
    let MetadataModel;
    let TorrentFileModel;
    let SerieModel;
    let SeasonModel;
    let SubtitleModel;
    let GeoBlockingModel;

    const instanceMethods = {

        /**
         * Denormalized to index form
         * @return {object}
         */
        async denormalize() {
            const me = await ContentModel.scope({method: ['toDenormalize']})
                .findByPk(this.uuid);

            const authorName = me.author.Profile ?
                me.author.Profile.realName :
                me.author.nickname
            ;

            const categoryName = me.category.name;

            const tags = me.Tags.map((tagModel) => tagModel.tag);

            const ratingDescription = await me.getRatingDescription();

            const {geoBlocking, media, metadata} = me;

            const geoBlock = {
                availableCountries: [],
                blockedCountries: [],
            };

            if (geoBlocking) {
                const countries = geoBlocking.countries
                    .map((country) => country.iso);

                if (geoBlocking.type === GeoBlockingModel.TYPE_BLACK) {
                    geoBlock.blockedCountries = countries;
                } else if (geoBlocking.type === GeoBlockingModel.TYPE_WHITE) {
                    geoBlock.availableCountries = countries;
                }
            }

            const metaTitle = metadata.reduce(
                (o, item) => {
                    o[item.lang] = item.title;
                    return o;
                }, {});

            const metaBody = metadata.reduce(
                (o, item) => {
                    o[item.lang] = item.body;
                    return o;
                }, {});

            return {
                ...me.get({plain: true}),
                title: metaTitle,
                body: metaBody,
                category: categoryName,
                author: authorName,
                seederFee: new Decimal(me.seederFee).toFixed(18),
                authorFee: new Decimal(me.authorFee).toFixed(18),
                boost: new Decimal(me.boost).toFixed(18),
                ratingDescription,
                media,
                geoBlockingType: geoBlocking ? geoBlocking.type || 0 : 0,
                ...geoBlock,
                geoBlocking: undefined,
                metadata: undefined,
                Tags: tags,
            };
        },

        /**
         * Get plain object to represent new data
         * @TODO medias
         * @return {object}
         */
        async getPlain() {
            const {
                title,
                body,
                audioLang,
                categoryId,
                rating,
                credits,
                duration,
                authorFee,
                seederFee,
            } = this;
            const plainTags = await this.getTags().map(
                (tagModel) => tagModel.tag
            );
            return {
                title,
                body,
                audioLang,
                categoryId,
                rating,
                credits,
                duration,
                authorFee,
                seederFee,
                tags: plainTags,
            };
        },

        /**
         * Get rating as description
         * @return {string}
         */
        async getRatingDescription() {
            switch (this.rating) {
                case RATING_ALL: {
                    return 'All audiences';
                }
                case RATING_MILD: {
                    return 'Mild-Mature';
                }
                case RATING_MATURE: {
                    return 'Mature';
                }
            }
        },

        /**
         * Find or create tags and set
         * @param {string[]} tags
         * @param {object} transaction
         * @return {TagModel[]}
         */
        async findAndSetTags(tags, transaction) {
            const otags = await TagModel.getTags(...tags);
            debug(`Setting tags`);
            await this.setTags(otags, {transaction});
            return otags;
        },

        /**
         * Update moderation status
         * @param {integer} status
         * @param {string} message
         * @param {object} transaction
         * @return {Promise<ContentModel>}
         */
        async updateModerationStatus(status, message, transaction) {
            debug(`Updating moderation status ${message}`);

            const updatedContent = this.update(
                {
                    moderationStatus: status,
                    moderationMessage: message,
                    moderatedAt: new Date(),
                },
                {
                    transaction,
                },
            );

            return updatedContent;
        },

        /**
         * Update moderation status
         * @return {Promise<ContentModel>}
         */
        async unassignModerator() {
            debug(`Updating moderatorId`);
            return this.update({
                moderatorId: null,
            });
        },

        /**
         * Update content status
         * @param {integer} status
         * @param {string} [comment]
         * @param {Sequelize.Transaction} [transaction]
         * @return {Promise}
         */
        async setStatus(status, comment = null, transaction = null) {
            debug(`Change status of ${this.uuid} ${this.status} -> ${status}`);
            this.status = status;
            this.cancelComment = comment;
            return this.save({transaction});
        },

        /**
         * Add content to elastic
         */
        async addToElastic() {

            const wrappedContent = await this.denormalize();
            // Index content on ElasticSearch
            const url = SLS_HOST + SLS_CONTENTS;
            debug(`Calling to Serverless POST ${url}`);
            try {
                await axios.post(url, wrappedContent);
            } catch (e) {
                debug(`Error adding to elasting`, e);
                throw e;
            }
        },

        /**
         * Update content on elastic index
         */
        async updateElastic() {
            const wrappedContent = await this.denormalize();
            // Index content on ElasticSearch
            const url = SLS_HOST + SLS_CONTENTS;
            debug(`Calling to Serverless PUT ${url}`);
            try {
                await axios.put(url, wrappedContent);
            } catch (e) {
                debug(`Error updating to elasting`, e);
                throw e;
            }
        },

        /**
         * Remove content from elastic
         */
        async removeFromElastic() {
            try {
                // Delete content on ElasticSearch
                const url = SLS_HOST + SLS_CONTENTS;
                debug(`Calling to Serverless DELETE ${url}`);
                await axios.delete(url, {
                    data: {
                        uuid: this.uuid,
                    },
                });
            } catch (e) {
                debug(`Error deleting on ES ${e}`);
                throw e;
            }
        },

        /**
         * Set S3 availability
         * @param {boolean} available
         * @return {Promise<ContentModel>}
         */
        async setS3Availability(available) {
            debug(`Updating content S3 availability to ${available}`);

            return this.update(
                {
                    s3_available: available,
                },
            );
        },

        /**
         * Prepare new version
         * @param {object} data
         * @return {ContentVersionModel}
         */
        prepareVersion(data = {}) {
            return ContentVersionModel.build({
                title: this.title,
                body: this.body,
                audioLang: this.audioLang,
                categoryId: this.categoryId,
                rating: this.rating,
                credits: this.credits,
                duration: this.duration,
                ...data,
                contentUUID: this.uuid,
            });
        },

        /**
         * Apply version to contents
         * @TODO tags and media
         * @param {string} uuid - uuid of the version
         * @param {string} [message]
         * @param {sequelize.Transaction} [dbTransaction] - Database transaction
         * @return {Promise}
         */
        async applyVersion(uuid, message, dbTransaction = null) {
            let transaction;

            if (typeof message !== 'string') {
                dbTransaction = message;
                message = undefined;
            }

            debug(`Apply version ${uuid} to content ${this.uuid}`);
            const versions = await this.getVersions({
                where: {uuid},
                rejectOnEmpty: true,
            });

            if (versions.length !== 1) {
                throw new Error('No version');
            }

            const version = versions[0];

            if (dbTransaction) {
                transaction = dbTransaction;
            } else {
                debug(`\ttransacation does not given, creating`);
                transaction = await sequelize.transaction();
            }

            try {
                const plainVersion = await version.getPlain();

                debug(`Deleting actual medias`);
                const actualMedias = await this.getMedia({
                    transaction,
                });
                actualMedias.map(async (media) => {
                    await media.destroy({transaction});
                });
                debug(`Get version medias`);
                await version.getMedia({
                    attributes: ['type', 'order', 'url', 'name'],
                    raw: true,
                    transaction,
                }).then(async (newMedias) => {
                    debug(`Creating content medias`);
                    await Promise.map(
                        newMedias,
                        (m) => {
                            return MediaModel.create(
                                {
                                    ...m,
                                    uuid: this.uuid,
                                    isDraft: false,
                                },
                                {transaction},
                            );
                        }
                    );
                });

                debug(`Deleting actual subtitles`);
                const actualSubtitles = await this.getSubtitle({
                    transaction,
                });
                actualSubtitles.map(async (subtitle) => {
                    await subtitle.destroy({transaction});
                });
                debug(`Get version subtitles`);
                await version.getSubtitle({
                    attributes: [
                        'lang', 'label', 'url',
                        'closeCaption', 'uuid',
                    ],
                    raw: true,
                    transaction,
                }).then(async (newSubtitles) => {
                    debug(`Creating content subtitles`);
                    await Promise.map(
                        newSubtitles,
                        (m) => {
                            return SubtitleModel.create(
                                {
                                    ...m,
                                    uuid: this.uuid,
                                    isDraft: false,
                                },
                                {transaction},
                            );
                        }
                    );
                });

                debug(`Deleting actual metadata`);
                const actualMetadata = await this.getMetadata({
                    transaction,
                });
                actualMetadata.map(async (meta) => {
                    await meta.destroy({transaction});
                });

                debug(`Creating metadata`);
                const versionMetadata = await version.getMetadata({raw: true});
                const metadataToCreate = versionMetadata.map((meta) => {
                    meta.entityId = this.uuid;
                    meta.entityType = MetadataModel.ENTITY_TYPE_CONTENT;
                    delete meta.createdAt;
                    delete meta.updatedAt;
                    delete meta.uuid;
                    return meta;
                });

                await MetadataModel.bulkCreate(metadataToCreate, {
                    validate: true,
                    transaction,
                });

                debug(`\tUpdating model`, plainVersion);
                await this.update(plainVersion, {transaction});

                debug(`\tSet as current version`);
                this.setCurrentVersion(version, {transaction});

                debug(`\tSetting status`);
                await version.setStatus(
                    ContentVersionModel.STATUS_ACCEPTED,
                    '',
                    transaction);
            } catch (e) {
                debug(`\tError saving`, e);
                if (!dbTransaction) {
                    debug(`\tRollbacking transaction`);
                    transaction.rollback();
                }
                throw e;
            }

            if (!dbTransaction) {
                debug(`\tCommiting transaction`);
                transaction.commit();
            }

            if (this.isIndexable) {
                await this.updateElastic();
            }

            return this;
        },

        /**
         * Get content seeder price at determinate moment
         * @param {number|Date} ts - Timestamp or Date instance
         * @return {Promise<Decimal>} - The price in that moment
         */
        async getSeederPriceAt(ts) {
            if (!this.seederFee) {
                throw new Error(`Content has not own seederFee`);
            }

            const onversions = await ContentVersionModel.getSeederFeeAt(
                this.uuid,
                ts
            );

            return new Decimal(onversions ? onversions : this.seederFee);
        },

    };

    const ContentModel = sequelize.define('Content', {

        uuid: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },

        hash: {
            type: DataTypes.STRING(40),
            allowNull: false,
            validate: {
                is: {
                    args: /^[A-F0-9]{40}$/i,
                    msg: 'Invalid SHA1 format',
                },
            },
            set(hash) {
                // Save hash in lower case
                this.setDataValue('hash', hash.toLowerCase());
            },
            unique: {
                args: true,
                msg: `This torrent is already added`,
            },
        },

        contentType: {
            type: DataTypes.INTEGER.UNSIGNED,
            defaultValue: TYPE_VIDEO,
            allowNull: false,
            validate: {
                isIn: [[
                    TYPE_VIDEO,
                    TYPE_SERIE,
                ]],
            },
        },

        title: {
            type: DataTypes.VIRTUAL(
                DataTypes.STRING(1024),
            ),
            get() {
                const meta = this.metadata;
                if (meta && meta.length) {
                    return meta[0].title;
                } else {
                    return '';
                }
            },
        },

        body: {
            type: DataTypes.VIRTUAL(
                DataTypes.TEXT,
            ),
            get() {
                const meta = this.metadata;
                if (meta && meta.length) {
                    return meta[0].body;
                } else {
                    return '';
                }
            },
        },

        bodyFormat: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: FORMAT_MARKDOWN,
            validate: {
                isIn: [[
                    FORMAT_MARKDOWN,
                ]],
            },
        },

        audioLang: {
            type: DataTypes.STRING(5),
            allowNull: false,
            defaultValue: 'en',
            validate: {
                is: {
                    args: /^[a-z]{2}(?:_[A-Z]{2})?/,
                    msg: 'Invalid lang format',
                },
            },
        },

        categoryId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
        },

        authorId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
        },

        payType: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: PAYTYPE_VIEW,
            validate: {
                isIn: [[
                    PAYTYPE_VIEW,
                ]],
            },
        },

        authorFee: {
            type: DataTypes.DECIMAL(36, 18),
            allowNull: false,
        },

        seederFee: {
            type: DataTypes.DECIMAL(36, 18),
            allowNull: false,
        },

        price: {
            type: DataTypes.VIRTUAL(
                DataTypes.DECIMAL(36, 18),
                ['authorFee', 'seederFee']
            ),
            get() {
                const sfee = this.get('seederFee');
                const afee = this.get('authorFee');
                if (!sfee && !afee) {
                    return undefined;
                } else if (!sfee) {
                    return new Decimal(afee).toFixed(18);
                } else if (!afee) {
                    return new Decimal(sfee).toFixed(18);
                } else {
                    return new Decimal(afee).plus(sfee).toFixed(18);
                }
            },
        },

        status: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: STATUS_NEW,
            validate: {
                isIn: {
                    args: [[
                        STATUS_NEW,
                        STATUS_CANCEL,
                        STATUS_PAUSED,
                        STATUS_ENABLED,
                    ]],
                    msg: 'Invalid status',
                },
            },
        },

        s3_available: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },

        priority: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },

        privacy: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: PRIVACY_PUBLIC,
        },

        releaseDate: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW,
        },

        finishDate: {
            type: DataTypes.DATE,
            allowNull: true,
        },

        cancelDate: {
            type: DataTypes.DATE,
            allowNull: true,
        },

        cancelComment: {
            type: DataTypes.TEXT,
            allowNull: true,
            get() {
                const {status} = this;

                if (status === STATUS_CANCEL || status === STATUS_PAUSED) {
                    return this.getDataValue('cancelComment');
                }

                return null;
            },
        },

        moderationStatus: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: MODSTATUS_NEW,
            validate: {
                isIn: [[
                    MODSTATUS_NEW,
                    MODSTATUS_ASSIGNED,
                    MODSTATUS_INPROGRESS,
                    MODSTATUS_ENABLED,
                    MODSTATUS_REJECT,
                    MODSTATUS_REQUEST_CHANGES,
                    MODSTATUS_CHANGES_REVIEW,
                ]],
            },
        },

        moderatedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },

        moderatorId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true,
        },

        moderationMessage: {
            type: DataTypes.TEXT,
            allowNull: true,
        },

        boost: {
            type: DataTypes.DECIMAL(36, 18),
            allowNull: true,
            defaultValue: 0,
        },

        rating: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: RATING_ALL,
            validate: {
                isIn: [[
                    RATING_ALL,
                    RATING_MILD,
                    RATING_MATURE,
                ]],
            },
        },

        credits: {
            type: DataTypes.TEXT,
            allowNull: true,
            validate: {
                is: /^(\n|.){5,}$/,
            },
        },

        tags: {
            type: DataTypes.VIRTUAL,
        },

        duration: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },

        downloadStatus: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },

        versionStatus: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: VERSION_STATUS_NONE,
            validate: {
                isIn: {
                    args: [[
                        VERSION_STATUS_NONE,
                        VERSION_STATUS_ACTIVE,
                    ]],
                    msg: 'Invalid version status',
                },
            },
        },

        isIndexable: {
            type: DataTypes.VIRTUAL(
                DataTypes.BOOLEAN,
                ['status', 'moderationStatus', 'contentType'],
            ),
            get() {
                return this.get('status') === STATUS_ENABLED &&
                       this.get('moderationStatus') === MODSTATUS_ENABLED &&
                       this.get('contentType') === 1;
            },
        },

        serieUUID: {
            type: DataTypes.UUID,
            index: true,
            allowNull: true,
        },

        seasonUUID: {
            type: DataTypes.UUID,
            index: true,
            allowNull: true,
        },

        order: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true,
            validate: {
                is: /^\d{1,3}$/,
            },
        },

    }, {
        timestamps: true,
        paranoid: true,

        validate: {
            statusFlow() {
                if (this.isNewRecord) {
                    return;
                }

                if (this.changed('status') &&
                    this.previous('status') === STATUS_CANCEL) {
                    throw new Error('A canceled content can\'t change status');
                }
            },

            canceledStatus() {
                if (this.isNewRecord) {
                    return;
                }

                if (this.status === STATUS_CANCEL &&
                    this.changed('moderationStatus')
                ) {
                    debug(`moderationStatus changhed in a canceled contents`);
                    throw new Error('A canceled content can\'t be moderated');
                }
            },

            requiredCancelComment() {
                let {status, cancelComment} = this;

                if (!cancelComment) {
                    cancelComment = '';
                }

                if (this.changed('status') &&
                    (status === STATUS_CANCEL || status === STATUS_PAUSED) &&
                    cancelComment.trim().length < 3
                ) {
                    throw new Error('You must to write a reason');
                }
            },
        },

        hooks:
            {
                afterDestroy(instance) {
                    // @see https://github.com/sequelize/sequelize/issues/6800
                    return Promise.resolve((async () => {
                        // If is indexable, remove from ES
                        if (instance.isIndexable) {
                            try {
                                await instance.removeFromElastic();
                            } catch (e) {
                                debug(`Error removing content from
                                        ES: ${JSON.stringify(e), null, 4}`);
                            }
                        }

                        if (instance.contentType === TYPE_SERIE) {
                            debug(`New serie chapter, running suggestion hook`);
                            const suggestions = await getServiceConnection(
                                'suggestions'
                            );
                            await suggestions.exec(
                                'hooks:changeSerieCaps',
                                instance.serieUUID
                            );
                        }
                    })());
                },

                afterCreate(instance, options) {
                    // @see https://github.com/sequelize/sequelize/issues/6800
                    return Promise.resolve((async () => {
                        if (instance.tags && instance.tags.length > 0) {
                            debug(`Adding content tags`);
                            await instance.findAndSetTags(
                                instance.tags,
                                options.transaction,
                            );
                        }

                        if (instance.contentType === TYPE_SERIE) {
                            debug(`New serie chapter, running suggestion hook`);
                            const suggestions = await getServiceConnection(
                                'suggestions'
                            );
                            await suggestions.exec(
                                'hooks:changeSerieCaps',
                                instance.serieUUID
                            );
                        }
                    })());
                },

                beforeUpdate(instance, options) {
                    // @see https://github.com/sequelize/sequelize/issues/6800
                    return Promise.resolve((async () => {
                        const {status} = instance;
                        const backStatus = instance.previous('status');
                        const hasNewStatus = instance.changed('status');

                        const supported = [
                            'status',
                            'moderationStatus',
                            'privacy',
                            'moderatorId',
                            'moderationMessage',
                            'cancelComment',
                        ];

                        if ( typeof instance.serieUUID === 'string' ) {
                            if ( instance.serieUUID !== '' ||
                                    instance.seasonUUID !== '' ) {
                                instance.contentType = TYPE_SERIE;
                            }
                        }

                        const change = supported.reduce((ch, k) => {
                            debug(`Check if ${k} change`);
                            return ch || instance.changed(k);
                        }, false);

                        if (change) {
                            const fields = supported.reduce((data, k) => {
                                if (instance.changed(k)) {
                                    debug(`Change ${k}`);
                                    data[k] = instance[k];
                                }

                                return data;
                            }, {uuid: instance.uuid});
                            debug(`Saving changed status data`, fields);

                            await ContentUpdateModel.create(fields);
                        }

                        if (hasNewStatus && (status === STATUS_CANCEL ||
                            status === STATUS_PAUSED) &&
                            (backStatus !== STATUS_CANCEL &&
                                backStatus !== STATUS_PAUSED)
                        ) {
                            debug(`Setting cancelDate`);
                            const delta = Math.floor(
                                Date.now() + (DELETE_DELTA * 60000));
                            instance.setDataValue('cancelDate', delta);
                        } else if (hasNewStatus &&
                                        status === STATUS_ENABLED &&
                                        backStatus === STATUS_PAUSED) {
                            debug(`Clean cancelDate`);
                            instance.setDataValue('cancelDate', null);
                        }

                        if (instance.tags) {
                            debug(`Setting content tags`);
                            await instance.findAndSetTags(
                                instance.tags,
                                options.transaction,
                            );
                        }
                    })());
                },

                afterUpdate(instance) {
                    // @see https://github.com/sequelize/sequelize/issues/6800
                    return Promise.resolve((async () => {
                        const {moderationStatus, status} = instance;
                        const moderationStatusOld = instance
                            .previous('moderationStatus');
                        const statusOld = instance
                            .previous('status');

                        if (
                            instance.contentType === TYPE_SERIE && (
                                instance.changed('status') ||
                                instance.changed('moderationStatus') ||
                                instance.changed('order') ||
                                instance.changed('seasonUUID')
                            )
                        ) {
                            debug(`Status change, running suggestion hook`);
                            const suggestions = await getServiceConnection(
                                'suggestions'
                            );
                            await suggestions.exec(
                                'hooks:changeSerieCaps',
                                instance.serieUUID
                            );
                        }

                        // If is accepted by moderation add to ES
                        if (instance.changed('moderationStatus') &&
                            instance.isIndexable &&
                            moderationStatus === MODSTATUS_ENABLED &&
                            instance.isIndexable ) {
                            await instance.addToElastic();
                        // If is de-accepted by moderator and was enabled,
                        // remove from ES @FUTURE I told u so Martin
                        } else if (instance.changed('moderationStatus') &&
                            instance.isIndexable &&
                            moderationStatusOld === MODSTATUS_ENABLED) {
                            await instance.removeFromElastic();
                        // Is status change and is accepted by modearation and
                        // old status was enabled, remove from ES
                        } else if (instance.changed('status') &&
                            moderationStatus === MODSTATUS_ENABLED &&
                            instance.isIndexable &&
                            statusOld === STATUS_ENABLED) {
                            await instance.removeFromElastic();
                        // If status changed and is currently acepted by
                        // moderator and status change to enabled, add to ES
                        } else if (instance.changed('status') &&
                            moderationStatus === MODSTATUS_ENABLED &&
                            instance.isIndexable &&
                            status === STATUS_ENABLED) {
                            await instance.addToElastic();
                        } else if (instance.status === STATUS_ENABLED &&
                            instance.moderationStatus === MODSTATUS_ENABLED) {

                            if (instance.isIndexable) {
                                await instance.updateElastic();
                            }
                        }
                    })());
                },
            },
        instanceMethods,
    });

    if (typeof ContentModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                ContentModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    /**
     * Type of content
     * @type {integer}
     */
    ContentModel.TYPE_VIDEO = TYPE_VIDEO;
    ContentModel.TYPE_SERIE = TYPE_SERIE;

    /**
     * Formats
     * @type {integer}
     */
    ContentModel.FORMAT_MARKDOWN = FORMAT_MARKDOWN;

    ContentModel.PAYTYPE_VIEW = PAYTYPE_VIEW;

    ContentModel.STATUS_NEW = STATUS_NEW;
    ContentModel.STATUS_ENABLED = STATUS_ENABLED;
    ContentModel.STATUS_CANCEL = STATUS_CANCEL;
    ContentModel.STATUS_PAUSED = STATUS_PAUSED;

    ContentModel.MODSTATUS_NEW = MODSTATUS_NEW;
    ContentModel.MODSTATUS_ASSIGNED = MODSTATUS_ASSIGNED;
    ContentModel.MODSTATUS_INPROGRESS = MODSTATUS_INPROGRESS;
    ContentModel.MODSTATUS_REJECT = MODSTATUS_REJECT;
    ContentModel.MODSTATUS_ENABLED = MODSTATUS_ENABLED;
    ContentModel.MODSTATUS_REQUEST_CHANGES = MODSTATUS_REQUEST_CHANGES;
    ContentModel.MODSTATUS_CHANGES_REVIEW = MODSTATUS_CHANGES_REVIEW;

    ContentModel.PRIVACY_LISTED = PRIVACY_LISTED;
    ContentModel.PRIVACY_PUBLIC_URL = PRIVACY_PUBLIC_URL;
    ContentModel.PRIVACY_PUBLIC = PRIVACY_PUBLIC;

    ContentModel.VERSION_STATUS_NONE = VERSION_STATUS_NONE;
    ContentModel.VERSION_STATUS_ACTIVE = VERSION_STATUS_ACTIVE;

    ContentModel.associate = function(models, db) {
        UsersModel = models.users;
        CategoryModel = models.categories;
        ContentUpdateModel = models.contentupdates;
        ContentVersionModel = models.contentversion;
        TagModel = models.tags;
        MediaModel = models.media;
        MetadataModel = models.metadata;
        TorrentFileModel = models.torrentfiles;
        SeasonModel = models.season;
        SerieModel = models.serie;
        SubtitleModel = models.subtitle;
        GeoBlockingModel = models.geoblocking;

        ContentModel.belongsTo(UsersModel, {
            as: 'author',
            foreignKey: 'authorId',
            targetKey: 'id',
            constraints: true,
        });

        ContentModel.belongsTo(UsersModel, {
            as: 'moderator',
            foreignKey: 'moderatorId',
            targetKey: 'id',
            constraints: true,
        });

        ContentModel.belongsTo(CategoryModel, {
            as: 'category',
            foreignKey: 'categoryId',
            targetKey: 'id',
            constraints: true,
        });

        ContentModel.hasMany(ContentUpdateModel, {
            as: 'updates',
            foreignKey: 'uuid',
            targetKey: 'uuid',
            constraints: true,
        });

        ContentModel.belongsTo(ContentVersionModel, {
            as: 'CurrentVersion',
            foreignKey: 'uuid',
            targetKey: 'contentUUID',
            constraints: false,
            allowNull: true,
        });

        ContentModel.hasMany(ContentVersionModel, {
            as: {
                singular: 'Version',
                plural: 'Versions',
            },
            foreignKey: 'contentUUID',
            targetKey: 'uuid',
            constraints: false,
        });

        ContentModel.belongsToMany(TagModel, {
            through: {
                model: 'ContentTags',
                attributes: [],
                uniq: true,
            },
            timestamps: false,
            paranoid: false,
        });

        ContentModel.hasMany(MediaModel, {
            as: 'media',
            foreignKey: 'uuid',
            targetKey: 'uuid',
            constraints: false,
            scope: {
                isDraft: false,
            },
        });

        MetadataModel.createAssociation(
            ContentModel,
            MetadataModel.ENTITY_TYPE_CONTENT,
            {as: 'metadata'}
        );

        ContentModel.hasOne(TorrentFileModel, {
            as: 'torrentFile',
            foreignKey: 'contentUUID',
            targetKey: 'uuid',
        });

        ContentModel.belongsTo(SerieModel, {
            as: 'serie',
            foreignKey: 'serieUUID',
            targetKey: 'uuid',
            constraints: false,
            allowNull: true,
        });

        ContentModel.belongsTo(SeasonModel, {
            as: 'season',
            foreignKey: 'seasonUUID',
            targetKey: 'uuid',
            constraints: false,
            allowNull: true,
        });

        ContentModel.hasMany(SubtitleModel, {
            as: 'subtitle',
            foreignKey: 'uuid',
            targetKey: 'uuid',
            constraints: false,
            scope: {
                isDraft: false,
            },
        });

        ContentModel.hasOne(GeoBlockingModel, {
            as: 'geoBlocking',
            foreignKey: 'targetUUID',
            targetKey: 'uuid',
            constraints: false,
            scope: {
                entityType: GeoBlockingModel.ETYPE_CONTENT,
            },
        });

        // Scopes
        ContentModel.addScope('enabled', {
            where: {
                // Use static method for normalization
                ...(ContentModel.enabledWhere()),
            },
        });

        ContentModel.addScope('filterCommunityContents', {
            where: {
                contentType: 1,
            },
        });

        ContentModel.addScope('filterSeriesContents', {
            where: {
                contentType: 2,
            },
        });

        ContentModel.addScope('byIds', (contentIds, forceOrder = false) => {
            return {
                where: {
                    uuid: {
                        [Op.in]: contentIds,
                    },
                },
                ...(forceOrder && contentIds.length?
                    {
                        order: multidialectCustomOrderItems(
                            'Content.uuid',
                            ...contentIds
                        ),
                    }: {}
                ),
            };
        });

        ContentModel.addScope('listing', (hasBody = true) => {
            const contentAttributes = [
                'uuid',
                'contentType',
                'title',
                'audioLang',
                'categoryId',
                'price',
                'releaseDate',
                'updatedAt',
                'createdAt',
                'duration',
                'downloadStatus',
                'rating',
            ];

            if (hasBody) {
                contentAttributes.push('body');
            }

            return {
                attributes: contentAttributes,
                order: [
                    ['releaseDate', 'DESC'],
                    ['updatedAt', 'DESC'],
                    ['createdAt', 'DESC'],
                ],
                include: [{
                    model: MediaModel,
                    as: 'media',
                    required: false,
                    separate: true,
                    attributes: [
                        'uuid',
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    required: false,
                    separate: true,
                    attributes: [
                        'uuid', 'label', 'lang',
                        'url', 'closeCaption',
                    ],
                }, {
                    model: UsersModel.scope('minimal'),
                    as: 'author',
                    required: true,
                }],
            };
        });

        ContentModel.addScope('excludeContents', (ids) => {
            return {
                where: {
                    uuid: {[Op.notIn]: ids},
                },
            };
        });

        ContentModel.addScope('listUnPurchaseds', (ids) => {
            return {
                attributes: [
                    'uuid',
                    'hash',
                    'contentType',
                    'title',
                    'body',
                    'authorFee',
                    'seederFee',
                    'audioLang',
                    'releaseDate',
                    'status',
                    'cancelDate',
                    'cancelComment',
                    'duration',
                    'rating',
                    'priority',
                    'authorId',
                ],
                where: {
                    uuid: {[Op.notIn]: ids},
                },
                order: [
                    ['priority', 'DESC'],
                    ['releaseDate', 'DESC'],
                ],
                include: [{
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'uuid',
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                    separate: true,
                }, {
                    model: UsersModel.scope('minimalProfile'),
                    as: 'author',
                    // required: true,
                }, {
                    model: CategoryModel.scope('public'),
                    as: 'category',
                    required: true,
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: [
                        'uuid', 'label', 'lang',
                        'url', 'closeCaption',
                    ],
                    separate: true,
                }, {
                    model: TorrentFileModel,
                    as: 'torrentFile',
                    attributes: [
                        'totalLength',
                        'pieceLength',
                        'lastPieceLength',
                        'torrentFile',
                        'torrentData',
                    ],
                    required: false,
                }],
            };
        });

        ContentModel.addScope('listPurchaseds', () => {
            return {
                attributes: [
                    'uuid',
                    'hash',
                    'contentType',
                    'title',
                    'body',
                    'seederFee',
                    'audioLang',
                    'releaseDate',
                    'status',
                    'cancelDate',
                    'cancelComment',
                    'updatedAt',
                    'createdAt',
                    'duration',
                    'rating',
                ],
                where: {
                    moderationStatus: MODSTATUS_ENABLED,
                    cancelDate: {
                        [Op.or]: {
                            [Op.eq]: null,
                            [Op.gte]: multidialectNow(),
                        },
                    },
                },
                order: [
                    ['media', 'order', 'ASC'],
                ],
                include: [{
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: UsersModel.scope('minimalProfile'),
                    as: 'author',
                    required: true,
                }, {
                    model: CategoryModel.scope('public'),
                    as: 'category',
                    required: true,
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: ['label', 'lang', 'url', 'closeCaption'],
                }, {
                    model: TorrentFileModel,
                    as: 'torrentFile',
                    attributes: [
                        'totalLength',
                        'pieceLength',
                        'lastPieceLength',
                        'torrentFile',
                        'torrentData',
                    ],
                    required: false,
                }, {
                    model: SerieModel,
                    as: 'serie',
                    attributes: ['title'],
                }],
            };
        });

        ContentModel.addScope('categoryJoin', () => {
            return {
                include: [{
                    model: CategoryModel.scope('public'),
                    as: 'category',
                    required: true,
                }],
            };
        });

        ContentModel.addScope('moderatorListing', () => {
            return {
                attributes: [
                    'uuid',
                    'contentType',
                    'status',
                    'moderationStatus',
                    'moderationMessage',
                    'moderatedAt',
                    'title',
                    'audioLang',
                    'categoryId',
                    'price',
                    'releaseDate',
                    'updatedAt',
                    'createdAt',
                    'cancelComment',
                    'duration',
                    'downloadStatus',
                ],
                order: [
                    ['releaseDate', 'DESC'],
                    ['updatedAt', 'DESC'],
                    ['createdAt', 'DESC'],
                    ['media', 'order', 'ASC'],
                ],
                include: [{
                    model: UsersModel.scope('minimal'),
                    as: 'author',
                    required: true,
                }, {
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                    where: {type: 2},
                }],
            };
        });

        ContentModel.addScope('authorListing', () => {
            return {
                attributes: [
                    'uuid',
                    'hash',
                    'contentType',
                    'status',
                    'cancelComment',
                    'cancelDate',
                    'moderationStatus',
                    'moderationMessage',
                    'moderatedAt',
                    'title',
                    'audioLang',
                    'categoryId',
                    'price',
                    'releaseDate',
                    'serieUUID',
                    'seasonUUID',
                    'updatedAt',
                    'createdAt',
                    'boost',
                    'duration',
                    'downloadStatus',
                    'deletedAt',
                ],
                order: [
                    ['releaseDate', 'DESC'],
                    ['updatedAt', 'DESC'],
                    ['createdAt', 'DESC'],
                    ['media', 'order', 'ASC'],
                ],
                include: [{
                    model: UsersModel.scope('minimal'),
                    as: 'author',
                    required: true,
                }, {
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: ['label', 'lang', 'url', 'closeCaption'],
                    required: false,
                }],
            };
        });

        ContentModel.addScope('authorDetailed', () => {
            return {
                attributes: [
                    'uuid',
                    'hash',
                    'contentType',
                    'title',
                    'bodyFormat',
                    'body',
                    'status',
                    'cancelComment',
                    'moderationStatus',
                    'moderationMessage',
                    'moderatedAt',
                    'audioLang',
                    'authorFee',
                    'seederFee',
                    'price',
                    'releaseDate',
                    'boost',
                    'credits',
                    'rating',
                    'updatedAt',
                    'createdAt',
                    'cancelDate',
                    'duration',
                    'downloadStatus',
                ],
                order: [
                    ['media', 'order', 'ASC'],
                ],
                include: [{
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'id',
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: CategoryModel.scope('public'),
                    as: 'category',
                    required: true,
                }, {
                    model: UsersModel.scope('minimalProfile'),
                    as: 'author',
                    required: true,
                }, {
                    model: TagModel,
                    attributes: ['tag'],
                    required: false,
                    through: {
                        attributes: [],
                    },
                }, {
                    model: ContentUpdateModel.scope('forAuthor'),
                    as: 'updates',
                    required: false,
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: ['id', 'label', 'lang', 'url', 'closeCaption'],
                    required: false,
                }],
            };
        });

        ContentModel.addScope('detailed', () => {
            return {
                attributes: [
                    'uuid',
                    'hash',
                    'contentType',
                    'title',
                    'bodyFormat',
                    'body',
                    'audioLang',
                    'authorFee',
                    'seederFee',
                    'price',
                    'releaseDate',
                    'boost',
                    'credits',
                    'rating',
                    'duration',
                    'downloadStatus',
                ],
                order: [
                    ['media', 'order', 'ASC'],
                ],
                include: [{
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: TorrentFileModel,
                    as: 'torrentFile',
                    attributes: [
                        'infoHash',
                        'totalLength',
                    ],
                }, {
                    model: CategoryModel.scope('public'),
                    as: 'category',
                    required: true,
                }, {
                    model: UsersModel.scope('minimalProfile'),
                    as: 'author',
                    required: true,
                }, {
                    model: TagModel,
                    attributes: ['tag'],
                    required: false,
                    through: {
                        attributes: [],
                    },
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: ['label', 'lang', 'url', 'closeCaption'],
                    required: false,
                }],
            };
        });

        ContentModel.addScope('moderatorDetailed', () => {
            return {
                attributes: [
                    'uuid',
                    'hash',
                    'contentType',
                    'serieUUID',
                    'seasonUUID',
                    'title',
                    'bodyFormat',
                    'body',
                    'status',
                    'cancelComment',
                    'moderationStatus',
                    'moderationMessage',
                    'moderatedAt',
                    'authorId',
                    'audioLang',
                    'authorFee',
                    'seederFee',
                    'price',
                    'releaseDate',
                    'boost',
                    'credits',
                    'rating',
                    'duration',
                    'downloadStatus',
                ],
                order: [
                    ['media', 'order', 'ASC'],
                ],
                include: [{
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: CategoryModel.scope('public'),
                    as: 'category',
                    required: true,
                }, {
                    model: UsersModel.scope('profile'),
                    as: 'author',
                    required: true,
                }, {
                    model: TagModel,
                    attributes: ['tag'],
                    required: false,
                    through: {
                        attributes: [],
                    },
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: ['label', 'lang', 'url', 'closeCaption'],
                    required: false,
                }],
            };
        });

        ContentModel.addScope('byCategory', (categoryId) => {
            return {
                attributes: [
                    'priority',
                ],
                where: {
                    categoryId,
                    contentType: TYPE_VIDEO,
                },
                order: [
                    ['priority', 'DESC'],
                    ['releaseDate', 'DESC'],
                    ['updatedAt', 'DESC'],
                    ['createdAt', 'DESC'],
                ],
            };
        });

        ContentModel.addScope('asModerator', (moderatorId) => {

            return {
                attributes: [

                ],
                where: {
                    moderatorId,
                    [Op.or]: [{
                        moderationStatus: [
                            MODSTATUS_ASSIGNED,
                            MODSTATUS_CHANGES_REVIEW,
                            MODSTATUS_REQUEST_CHANGES,
                        ],
                    }, {
                        versionStatus: VERSION_STATUS_ACTIVE,
                    }],
                    status: {[Op.not]: STATUS_CANCEL},
                    downloadStatus: {[Op.gte]: 1},
                },
                include: [{
                    model: ContentVersionModel,
                    as: 'Versions',
                    attributes: [
                        'uuid',
                        'status',
                        'createdAt',
                        'updatedAt',
                    ],
                    required: false,
                    where: {
                        status: ContentVersionModel.STATUS_REVIEW,
                    },
                }],
            };
        });

        ContentModel.addScope('moderatedBy', (moderatorId) => ({
            where: {moderatorId},
        }));

        ContentModel.addScope('titleOrSerieSearch', (param, seriesIds) => {
            return {
                where: {
                    [Op.or]: {
                        title: {
                            [Op.iLike]: `%${param}%`,
                        },
                        serieUUID: {
                            [Op.in]: seriesIds,
                        },
                    },
                },
            };
        });

        ContentModel.addScope('asAuthor', (authorId) => {
            return {
                where: {
                    authorId,
                },
                paranoid: true,
            };
        });

        ContentModel.addScope('requestChanges', (authorId) => {
            return {
                where: {
                    authorId,
                    moderationStatus: MODSTATUS_REQUEST_CHANGES,
                },
            };
        });

        ContentModel.addScope('seedbox', (date) => {
            return {
                attributes: [
                    'uuid',
                    'hash',
                    'title',
                    'updatedAt',
                    'createdAt',
                    'finishDate',
                    'cancelDate',
                    's3_available',
                    'contentType',
                ],
                where: {
                    [Op.or]: [
                        {
                            updatedAt: {
                                [Op.not]: null,
                                [Op.gt]: date,
                            },
                        }, {
                            updatedAt: null,
                            createdAt: {
                                [Op.gt]: date,
                            },
                        },
                    ],
                    moderationStatus: {
                        [Op.ne]: MODSTATUS_REJECT,
                    },
                },
                order: [
                    ['updatedAt', 'ASC'],
                    ['createdAt', 'ASC'],
                ],
                include: [{
                    model: TorrentFileModel,
                    as: 'torrentFile',
                    attributes: ['torrentFile'],
                }],
            };
        });

        ContentModel.addScope('information', () => {
            return {
                attributes: [
                    'uuid',
                    'hash',
                    'contentType',
                    'status',
                    'privacy',
                    'authorFee',
                    'seederFee',
                    'releaseDate',
                    'finishDate',
                    'moderationStatus',
                    'moderatedAt',
                    'createdAt',
                    'updatedAt',
                    'deletedAt',
                ],
                include: [{
                    model: CategoryModel.scope('public'),
                    as: 'category',
                    required: true,
                }, {
                    model: UsersModel.scope('information'),
                    as: 'author',
                    required: true,
                }, {
                    model: UsersModel.scope('information'),
                    as: 'moderator',
                    required: false,
                }],
            };
        });

        ContentModel.addScope(
            'byFolloweds',
            (followerId, userScope = 'profile' ) => {
                const followeds = db.dialect.QueryGenerator.selectQuery(
                    models.followers.tableName,
                    {
                        attributes: ['followedId'],
                        where: {followerId},
                    }
                ).slice(0, -1);

                return {
                    where: {
                        authorId: {
                            [Op.in]: db.literal(`(${followeds})`),
                        },
                    },
                };
            }
        );

        ContentModel.addScope('availableUpdates', (authorId) => {
            return {
                where: {
                    authorId,
                    moderationStatus: MODSTATUS_ENABLED,
                    status: [
                        STATUS_ENABLED,
                        STATUS_PAUSED,
                    ],
                },
                include: [{
                    model: MediaModel,
                    as: 'media',
                    attributes: [
                        'name',
                        'type',
                        'url',
                        'order',
                        'processed',
                        'sizes',
                    ],
                    required: false,
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: [
                        'label',
                        'lang',
                        'url',
                        'closeCaption',
                    ],
                    required: false,
                }, {
                    model: TagModel,
                    attributes: ['tag'],
                    required: false,
                    through: {
                        attributes: [],
                    },
                }],
            };
        });

        ContentModel.addScope('uploadSubtitlesAllowed', (authorId) => {
            return {
                where: {
                    authorId,
                    moderationStatus: [
                        MODSTATUS_REQUEST_CHANGES,
                        MODSTATUS_ASSIGNED,
                        MODSTATUS_NEW,
                    ],
                    status: [
                        STATUS_ENABLED,
                        STATUS_PAUSED,
                    ],
                },
            };
        });

        ContentModel.addScope('geoAvailable', (countryIso) => {
            const {ETYPE_CONTENT, ETYPE_SERIE_SEASON} = GeoBlockingModel;

            return GeoBlockingModel.extendScopeDefinition(
                countryIso,
                ETYPE_CONTENT,
                'Content.uuid',
            )({
                include: [
                    GeoBlockingModel.extendScopeDefinition(
                        countryIso,
                        ETYPE_SERIE_SEASON,
                        'season.uuid',
                        'geoBlocking',
                        '$season.geoBlocking.type$',
                    )({
                        model: SeasonModel,
                        as: 'season',
                        attributes: [
                            'uuid',
                        ],
                        required: false,
                    }),
                ],
            });
        });

        ContentModel.addScope('geoAvailableWOS', (countryIso, options = {}) => {
            const {ETYPE_CONTENT} = GeoBlockingModel;
            const {fieldName, geoTypeField} = options;

            return GeoBlockingModel.extendScopeDefinition(
                countryIso,
                ETYPE_CONTENT,
                fieldName || 'Content.uuid',
                'geoBlocking',
                geoTypeField || '$geoBlocking.type$',
            )({});
        });

        ContentModel.addScope(
            'i18n',
            MetadataModel.createI18nScope(null, {
                as: 'metadata',
            })
        );

        ContentModel.addScope('torrentInformation', {
            attributes: [
                'uuid',
                'hash',
            ],
            include: [{
                model: TorrentFileModel,
                as: 'torrentFile',
                attributes: [
                    'infoHash',
                    'totalLength',
                    'pieceLength',
                    'lastPieceLength',
                ],
            }],
        });

        ContentModel.addScope('toDenormalize', () => ({
            attributes: [
                'uuid',
                'contentType',
                'authorId',
                'seederFee',
                'authorFee',
                'status',
                'moderationStatus',
                'createdAt',
                'updatedAt',
                'boost',
                'releaseDate',
                'rating',
                'credits',
                'audioLang',
                'price',
                'hash',
                'duration',
                'downloadStatus',
                'priority',
            ],
            include: [{
                model: UsersModel.scope('profile'),
                as: 'author',
                required: true,
            }, {
                model: CategoryModel,
                as: 'category',
                attributes: ['name'],
                required: true,
            }, {
                model: MediaModel,
                as: 'media',
                attributes: [
                    'name',
                    'type',
                    'url',
                    'order',
                    'processed',
                    'sizes',
                ],
                required: false,
            }, {
                model: MetadataModel,
                as: 'metadata',
                attributes: [
                    'title',
                    'body',
                    'lang',
                ],
                required: false,
            }, {
                model: TagModel,
                attributes: ['tag'],
                required: false,
                through: {
                    attributes: [],
                },
            }, {
                model: GeoBlockingModel.scope('detailled'),
                as: 'geoBlocking',
                required: false,
            }],
        }));

        ContentModel.addScope('withSerieInfo', (ops = {}, required = true) => ({
            include: [{
                model: SerieModel,
                as: 'serie',
                attributes: ops.attributes || ['uuid'],
                required,
            }],
        }));

        ContentModel.addScope('toExport', () => {

            return {
                paranoid: false,
                attributes: {
                    exclude: [
                        'title',
                        'body',
                        'price',
                        'tags',
                        'isIndexable',
                    ],
                },
                include: [{
                    model: UsersModel.scope('toExport'),
                    as: 'author',
                    required: true,
                }, {
                    model: UsersModel.scope('toExport'),
                    as: 'moderator',
                    required: false,
                }, {
                    model: MetadataModel,
                    as: 'metadata',
                }, {
                    model: SerieModel.scope('toExport'),
                    as: 'serie',
                    required: false,
                }, {
                    model: MediaModel.scope('toExport'),
                    as: 'media',
                    required: false,
                }],
            };
        });

    };

    /**
     * Get where object for enable contents
     * @return {object}
     */
    ContentModel.enabledWhere = function() {
        return {
            status: STATUS_ENABLED,
            moderationStatus: MODSTATUS_ENABLED,
            downloadStatus: {
                [Op.gt]: 0,
            },
            releaseDate: {
                [Op.lte]: multidialectNow(),
            },
            finishDate: {
                [Op.or]: {
                    [Op.eq]: null,
                    [Op.gte]: multidialectNow(),
                },
            },
        };
    };

    /**
     * Search one record by hash
     * @param {string} hash
     * @param {object} [options]
     * @return {Promise<ContentModel|null>}
     */
    ContentModel.findByHash = function(hash, options = {}) {
        hash = hash.toLowerCase();
        return this.findOne({
            where: {hash},
            ...options,
        });
    };

    /**
     * Find by hash or uuid
     * @param {string} id
     * @param {object} [options]
     * @return {Promise<ContentModel|null>}
     */
    ContentModel.findByHashOrId = async function(id, options = {}) {
        id = id.toLowerCase().trim();

        if (id.match(/^[a-f0-9]{40}$/)) {
            return this.findByHash(id, options);
        } else if (id.match(/^[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}$/)) {
            return this.findOne({
                where: {
                    uuid: id,
                },
                ...options,
            });
        } else {
            throw new Error(`Invalid format`);
        }
    };

    /**
     * Check if content exists by infoHash
     * @param {string} hash
     * @param {bool} [paranoid=false]
     * @return {Promise<bool>}
     */
    ContentModel.existsHash = function(hash, paranoid = false) {
        hash = hash.toLowerCase();
        return this.count({
            paranoid,
            where: {hash},
        })
            .then((c) => !!c);
    };

    return ContentModel;
};
