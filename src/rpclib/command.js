
import Promise from 'bluebird';
import {v4 as uuid} from 'uuid';
import {createDebug} from '../logger';
import config from '../config';
import {RPCTimeOutError, RPCRemoteError} from './errors';

const debug = createDebug(__filename);

const RPC_TIMEOUT = config.get('rpc:timeout') || 2000;

/**
 * Remote command interface
 */
export default class Command {

    /**
     * Create new remote service client
     * @param {amqp.connection} connector
     * @param {string} service Service id, like com.flixxo.microservice.xxx
     * @param {object|Promise|function} [context] Context for instance
     */
    constructor(connector, service, context = {}) {
        this._connector = connector;
        this._channel = null;
        this._service = service;
        this._context = typeof context === 'function' ? context() : context;
        this._commands = [];
    }

    /**
     * Get context
     * @return {Promise<object>}
     */
    async getContext() {
        const context = await Promise.resolve(this._context);
        this._context = context;
        return context;
    }

    /**
     * Set context
     * @param {object|Pomise} context
     */
    setConext(context) {
        this._context = context;
    }

    /**
     * Get new instance of Command with new context
     * @param {object|Promise|function} context
     * @return {Command}
     */
    bindContext(context = {}) {
        return new Command(this._connector, this._service, context);
    }

    /**
     * Create a new channel if does not exists
     * @return {Promise<channel>}
     */
    async createChannel() {
        if (!this._channel) {
            debug(`Creating new channel`);

            this._channel = new Promise((resolve, reject) => {
                this._connector.createChannel((err, ch) => {
                    if (err) {
                        debug(`Error creating channel`, err);
                        return reject(err);
                    }

                    debug('channel created');
                    this._channel = ch;
                    return resolve(ch);
                });
            });

            await this._channel;
        }

        return this._channel;
    }

    /**
     * Create private queue
     * @return {object} Queue object
     */
    async createPrivateQueue() {
        const channel = await this.createChannel();

        const queue = await new Promise((resolve, reject) => {
            channel.assertQueue('', {
                exclusive: true,
                durable: false,
                autoDelete: true,
            }, (err, q) => {
                if (err) {
                    debug(`error asserting queue`, err);
                    return reject(err);
                }

                debug(`Created private queue ${q.queue}`);
                return resolve(q);
            });
        });

        return queue;
    }

    /**
     * Add new command to the pool
     * @param {object} pack Command package
     * @param {string} pack.uid Command UID
     * @param {string} pack.cmd Command name
     * @param {Array<mixed>} pack.params Params
     * @param {object} q Queue object
     * @param {object} [options] Options
     * @return {Promise}
     */
    async addCommand(pack, q, options = {}) {
        let resolve;
        let reject;

        const promise = new Promise((_resolve, _reject) => {
            resolve = _resolve;
            reject = _reject;
        });

        const command = {
            uid: pack.uid,
            q,
            topointer: setTimeout(() => {
                this.rejectCommand(pack.uid, new RPCTimeOutError(command));
            }, RPC_TIMEOUT),
            resolve,
            reject,
        };

        const plainPack = JSON.stringify(pack);

        const channel = await this.createChannel();

        await channel.consume(q.queue, (response) => {
            this.handleResponse(response);
        }, {
            noAck: true,
            // exclusive: true,
        });

        await channel.sendToQueue(this._service, Buffer.from(plainPack), {
            correlationId: pack.uid,
            replyTo: q.queue,
        });

        debug(`Command ${command.uid} sent`);
        this._commands.push(command);

        return promise;
    }

    /**
     * Clean active command
     * @param {object} command Command object
     * @return {object} Command object
     */
    async cleanCommand(command) {
        debug(`Cleaning command ${command.uid}`);
        this._commands = this._commands.filter((x) => x.uid !== command.uid);

        if (command.topointer) {
            clearTimeout(command.topointer);
        }

        const channel = await this.createChannel();
        debug(`delete queue ${command.q.queue}`);
        await channel.deleteQueue(command.q.queue);

        return command;
    }

    /**
     * Handle a remote response
     * @param {object} response RPC response object
     */
    handleResponse(response) {
        if (!response) {
            debug(`null response?`);
            return;
        }

        const uid = response.properties.correlationId;
        const command = this._commands
            .find((x) => x.uid === uid);

        if (!command) {
            debug(`Unknow uid command response: ${uid}`);
            return;
        }

        debug(`${uid}: response`);

        this.cleanCommand(command);

        const rpack = JSON.parse(response.content);

        if (rpack.error) {
            debug(`${uid}: has rejected`, rpack.error);
            command.reject(new RPCRemoteError(rpack.error));
        } else {
            command.resolve(rpack.result);
        }
    }

    /**
     * Reject active command
     * @param {string} uid
     * @param {Error} error
     */
    rejectCommand(uid, error) {
        const command = this._commands
            .find((x) => x.uid === uid);

        if (!command) {
            debug(`Unknow uid command ${uid} for reject`);
            throw new Error('Command does not exists');
        }

        debug(`${uid}: rejected`, error);

        this.cleanCommand(command);

        command.reject(error);
    }

    /**
     * Execute remote command
     * @param {string} cmd Command
     * @param {Array<mixed>} [...params] Params
     * @return {Promise<mixed|Error>}
     */
    async execute(cmd, ...params) {
        const uid = uuid();
        debug(`execute ${this._service}::${cmd} as ${uid}`);

        const context = await this.getContext();

        const pack = {
            uid,
            cmd,
            params,
            context,
        };

        const queue = await this.createPrivateQueue();

        return this.addCommand(pack, queue);
    }
}
