import {syncDb, loadData} from '../helpers/db';
import * as langs from '../../src/services/langs';

describe('#list', () => {
    beforeEach(async () => {
        await syncDb(true);
        await loadData('00_langs');
    });

    test(`Must to get langs`, async () => {
        let res = await langs.list();
        expect(Array.isArray(res)).toBeTruthy();
    });
});
