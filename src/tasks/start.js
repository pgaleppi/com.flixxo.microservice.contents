import bootstrapServer from '../services';
import {createServer} from '../rpclib';
import {StatusServer, setRPCServer} from '../status';
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

/**
 * Start microservice
 * @param {object} params
 * @return {bool}
 */
export default async function(params) {
    debug('Bootstraping server');

    debug(`Create RPC server`);
    const server = await createServer();

    debug(`Boostrap services`);
    await bootstrapServer(server);

    debug(`Setting status`);
    await setRPCServer(server);

    debug('Go to listen');
    await server.listen();

    logger.info('RPC listening');

    debug('Listen Helath server');
    await StatusServer.createServer();

    logger.info('App started!');

    return true;
}
