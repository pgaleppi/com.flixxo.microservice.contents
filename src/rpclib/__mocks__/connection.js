import Promise from 'bluebird';
import {v4 as uuid} from 'uuid';
import {createDebug} from '../../logger';

const debug = createDebug(__filename);

/**
 * RPClib Connection mock
 */
export class ConnectionMock {

    /**
     * Constructor
     */
    constructor() {
        this._rcb = {};
        this._onReplyCB;
    }

    /**
     * Set reply handler
     * @param {function} cb - callback
     */
    __onReply(cb) {
        this._onReplyCB = cb;
    }

    /**
     * @override
     */
    createChannel(cb) {
        const that = this;

        return cb(null, {
            sendToQueue(replyTo, pack, properties) {
                const dpack = JSON.parse(pack.toString('utf8'));
                debug(`Pack arrive for ${properties.replyTo}`);
                if (that._onReplyCB) {
                    debug(`__onReply defined`);
                    setTimeout(() => {
                        that._onReplyCB(
                            dpack,
                            properties,
                            (err, data) => {
                                that._rcb[properties.replyTo](
                                    err,
                                    data,
                                    properties
                                );
                            }
                        );
                    }, 4);
                }
            },

            ack() {
            },

            assertQueue(n, data, cb) {
                return cb(null, {
                    queue: `dummy-${uuid()}`,
                });
            },

            consume(q, cb) {
                that._rcb[q] = (err, data, properties) => {
                    if (err) {
                        return cb({
                            properties,
                            content: JSON.stringify({
                                success: false,
                                error: err,
                                result: null,
                            }),
                        });
                    }

                    cb({
                        properties,
                        content: JSON.stringify({
                            success: true,
                            error: null,
                            result: data,
                        }),
                    });
                };
            },

            deleteQueue(n) {
                delete that._rcb[n];
            },
        });
    }
}

/**
 * Get a RPC connection, if don't exists, create one
 * @return {Promise<amqp.connection>}
 */
export default function getConnection() {
    debug(`Getting mock connection`);
    return Promise.resolve(new ConnectionMock());
}
