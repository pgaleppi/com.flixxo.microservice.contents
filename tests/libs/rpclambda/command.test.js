import axios from 'axios';
import Promise from 'bluebird';
import MockAdapter from 'axios-mock-adapter';
import Command from '../../../src/rpclambda/command';
import config from '../../../src/config';

config.set('microservices:dummy:endpoint', 'http://dummy.com/cmd');

const mock = new MockAdapter(axios);

describe('Test', () => {

    describe('#getContext', () => {
        test(`Must to get plain context`, async () => {
            const service = new Command(null, 'dummy', {
                a: 1,
                b: [],
                c: 'text',
            });

            expect(await service.getContext()).toEqual({
                a: 1, b: [], c: 'text',
            });
        });

        test(`Must to get promised context`, async () => {
            const p = new Promise((r) => {
                setTimeout(() => {
                    r({
                        a: 1,
                        b: [],
                        c: 'text',
                    });
                }, 10);
            });

            const service = new Command(null, 'dummy', p);

            expect(await service.getContext()).toEqual({
                a: 1, b: [], c: 'text',
            });
        });

        test(`Must to get computed context`, async () => {
            const service = new Command(null, 'dummy', () => {
                return {a: 1, b: [], c: 'text'};
            });

            expect(await service.getContext()).toEqual({
                a: 1, b: [], c: 'text',
            });
        });

        test(`Must to get computed async context`, async () => {
            const service = new Command(null, 'dummy', async () => {
                return {a: 1, b: [], c: 'text'};
            });

            expect(await service.getContext()).toEqual({
                a: 1, b: [], c: 'text',
            });
        });
    });

    describe('#bindContext', () => {
        test(`Must to get a new instance with new context`, async () => {
            const service = new Command(null, 'dummy', {
                a: 1, b: 2, c: 'text',
            });

            const bindedService = service.bindContext({
                z: 9, y: 8, x: 'text',
            });

            expect(await service.getContext()).toEqual({
                a: 1, b: 2, c: 'text',
            });

            expect(await bindedService.getContext()).toEqual({
                z: 9, y: 8, x: 'text',
            });
        });
    });

    describe('#execute', () => {

        beforeEach(()=> {
            mock.onPost().reply(({data}) => {
                data = JSON.parse(data);
                return [200, {
                    success: true,
                    error: null,
                    result: {
                        pack: data,
                    },
                    uuid: data.uuid,
                }];
            });
        });

        afterEach(() => {
            mock.reset();
        });

        test('Send command', async () => {
            const command = new Command(null, 'dummy');
            let res = await command.execute('test', 1, 2, 'c');
            expect(res.pack).toMatchObject({
                cmd: 'test',
                params: [1, 2, 'c'],
                context: {},
            });
        });

        test('Send command with context', async () => {
            const command = new Command(null, 'dummy', {
                x: 99,
                y: 98,
                z: 'c',
            });
            let res = await command.execute('test', 1, 2, 'c');
            expect(res.pack).toMatchObject({
                cmd: 'test',
                params: [1, 2, 'c'],
            });
            expect(res.pack.context).toEqual({
                x: 99, y: 98, z: 'c',
            });
        });

        test('Send command with promised context', async () => {
            const contextPromise = new Promise((r) => setTimeout(() => {
                r({a: 1, b: [], c: 'text'});
            }, 10));

            const command = new Command(null, 'dummy', contextPromise);
            let res = await command.execute('test', 1, 2, 'c');
            expect(res.pack).toMatchObject({
                cmd: 'test',
                params: [1, 2, 'c'],
            });
            expect(res.pack.context).toEqual({a: 1, b: [], c: 'text'});
        });

        test('Send command with computed context', async () => {
            const command = new Command(null, 'dummy', () => {
                return {a: 1, b: [], c: 'text'};
            });
            let res = await command.execute('test', 1, 2, 'c');
            expect(res.pack).toMatchObject({
                cmd: 'test',
                params: [1, 2, 'c'],
            });
            expect(res.pack.context).toEqual({a: 1, b: [], c: 'text'});
        });

        test('Send command with computed async context', async () => {
            const command = new Command(null, 'dummy', () => {
                return new Promise((r) => {
                    setTimeout(() => {
                        r({a: 1, b: [], c: 'text'});
                    }, 10);
                });
            });
            let res = await command.execute('test', 1, 2, 'c');
            expect(res.pack).toMatchObject({
                cmd: 'test',
                params: [1, 2, 'c'],
            });
            expect(res.pack.context).toEqual({a: 1, b: [], c: 'text'});
        });
    });

});
