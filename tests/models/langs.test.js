
import {syncDb, loadData} from '../helpers/db';
import {matchers} from 'jest-json-schema';
import {ValidationError} from 'sequelize';
import models from '../../src/models';

expect.extend(matchers);

describe('Check validator', () => {
    test('Must to be ok with valid date', async () => {
        let newLang = models.langs.build({
            lang: 'es',
            nameEn: 'Spanish',
            nameNative: 'Español',
        });

        await newLang.validate();

        expect(newLang.enabled).toBe(true);
        expect(newLang.name).toBe('Spanish (Español)');
    });

    test('Must to fail with incorrect lang code', async () => {
        let newLang = models.langs.build({
            lang: 'esp',
            nameEn: 'Spanish',
            nameNative: 'Español',
        });

        try {
            await newLang.validate();
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.message).toBe(
                `Validation error: Invalid lang code,\n` +
                `Validation error: Language not enabled`);
            return;
        }

        throw new Error('Pass');
    });

    test('Must to fail with disabled lang code', async () => {
        let newLang = models.langs.build({
            lang: 'de',
            nameEn: 'German',
            nameNative: 'Deutsch',
        });

        try {
            await newLang.validate();
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.message).toBe(
                `Validation error: Language not enabled`);
            return;
        }

        throw new Error('Pass');
    });
});


describe('Check scopes', () => {
    beforeEach(async () => {
        await syncDb(true);
        await loadData('00_langs');
    });

    describe('simple', () => {
        test('Must to get langs in default scope', async () => {
            let res = await models.langs.findAll();
            let plain = res[0].get({pain: true});
            expect(Object.keys(plain).sort()).toEqual([
                'lang',
                'name',
                'nameEn',
                'nameNative',
            ]);
        });
    });
});
