
import models from '../models';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

/**
 * Set S3 Availability Task
 * @param {object} params
 */
export default async function({hash, available}) {
    debug(`Setting ${hash} availability to ${available}`);

    try {
        const content = await models.contents.findByHash(hash, {
            rejectOnEmpty: true,
        });

        await content.setS3Availability((available === 'true' ? true : false));
    } catch (e) {
        process.stderr.write(`${e.message}\n`);
    }

    process.stdout.write(`Done\n`);
}
