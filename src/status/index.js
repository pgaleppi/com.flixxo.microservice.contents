
import Promise from 'bluebird';
import nodestat from 'node-stat';
import StatusServer from './server';

let rpcServer;

/**
 * Set the RCP server to check data
 * @param {RPCServer} rpcinstance
 */
export function setRPCServer(rpcinstance) {
    rpcServer = rpcinstance;
}

/**
 * Get status
 * @return {object}
 */
export async function getStatus() {
    const now = Math.floor((new Date()).getTime() / 1000);
    const stats = await new Promise((resolve, reject) => {
        nodestat.get('stat', 'net', 'load', (err, data) => {
            if (err) {
                return reject(err);
            }

            return resolve(data);
        });
    });

    const rpcStats = rpcServer ? rpcServer.getStatus() : {};

    return {
        pong: true,
        timestamp: now,
        process: {
            pid: process.pid,
            uptime: process.uptime(),
            memoryUsage: process.memoryUsage(),
            usage: process.cpuUsage(),
            gid: process.getgid(),
            uid: process.getuid(),
        },
        rpc: rpcStats,
        server: stats,
    };
}

export {StatusServer};
