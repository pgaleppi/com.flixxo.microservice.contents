import Promise from 'bluebird';
import models from '../models';
import {Op, EmptyResultError} from 'sequelize';
import {serviceCacheable} from '../cache/decorators';
import {ValidationError} from '../errors';
import {createDebug} from '../logger';
import {getConnection} from '../storage';
import {getUser, createContent} from './contents';

const debug = createDebug(__filename);


/**
 * Create serie
 * @param {string} email
 * @param {string} moderatorEmail
 * @param {object} serie
 * @return {Promise}
 */
export async function createSerie(email, moderatorEmail, serie) {
    debug(`Creating new serie ${email}`);
    const user = await getUser(email);

    serie.authorId = user.id;
    serie.status = models.serie.STATUS_ENABLED;

    if (moderatorEmail) {
        const moderator = await getUser(moderatorEmail);
        if (moderator) {
            serie.moderatorId = moderator.id;
        }
    }

    /* @TODO moderation flow
    if (!serie.moderatorId) {
        serie.moderationStatus = models.serie.MODSTATUS_NEW;
    } else {
        serie.moderationStatus = models.serie.MODSTATUS_ASSIGNED;
    }
    */
    serie.moderationStatus = models.serie.MODSTATUS_ENABLED;

    let newSerie;
    let newSeason;
    const sequelize = getConnection();
    await sequelize.transaction( async (dbTxn) => {

        // Create serie on DB
        newSerie = await models.serie.create(
            serie,
            {transaction: dbTxn},
        );
        debug(`Serie created ${newSerie.uuid}`);

        // Create season
        newSeason = await models.season.create({
            serieUUID: newSerie.uuid,
            number: serie.seasonNumber,
            authorId: user.id,
            title: serie.seasonTitle,
        },
        {
            transaction: dbTxn,
        },
        );
        debug(`Season ${serie.seasonNumber} created`);
    });

    await seriesToElastic(newSerie.uuid, 'add');

    return {
        success: true,
        uuid: newSerie.uuid,
        seasonUUID: newSeason.uuid,
    };
}

/**
 * Create season
 * @param {string} email
 * @param {object} serieUUID
 * @param {object} season
 * @return {Promise}
 */
export async function createSeason(email, serieUUID, season) {
    debug(`Creating new season ${season.number}`);
    const user = await getUser(email);
    const geoBlocking = season.geoBlocking;

    const serie = await models.serie.scope(
        'statusEnabled',
        {method: ['asAuthor', user.id]},
        'authorDetailed',
    ).findByPk(serieUUID);

    if (!serie) {
        throw new ValidationError('Serie error', [{
            field: 'serieUUID',
            type: 'CreateSeasonError',
            msg: 'Invalid serie id',
        }]);
    }

    season.authorId = user.id;

    const result = await models.season.findOrCreate(
        {
            where: {
                serieUUID: serieUUID,
                number: season.number,
            },
            defaults: {
                authorId: user.id,
                title: season.title,
            },
        },
    ).then((result) => {
        debug(`Season fetched -> created=${result[1]}`);
        return result;
    });

    if (geoBlocking) {
        await setSeasonGeoblocking(
            email,
            result[0].uuid,
            geoBlocking.type,
            geoBlocking.countries,
        );
    }

    return {
        success: true,
        uuid: result[0].uuid,
        created: result[1],
    };
}

/**
 * Get a serie by author
 * @param {string} lang
 * @param {string} email
 * @param {string} uuid
 * @param {boolean} enabled
 * @return {Promise}
 */
export const getSerieByAuthor = ['lang',
    async (lang, email, uuid, enabled = false) => {
        const user = await getUser(email);

        debug(`Serie detail ${uuid} by author ${email}`);

        let serie;
        if (enabled) {
            serie = await models.serie.scope(
                'statusEnabled',
                {method: ['asAuthor', user.id]},
                {method: ['authorDetailed', lang]},
            ).findByPk(uuid, {
                rejectOnEmpty: false,
            });

            if (!serie) {
                throw new EmptyResultError();
            }
        } else {
            serie = await models.serie.scope(
                {method: ['asAuthor', user.id]},
                'authorDetailed'
            ).findByPk(uuid, {
                rejectOnEmpty: false,
            });

            if (!serie) {
                throw new EmptyResultError();
            }
            serie.categoryId = serie.category.id;
        }

        return {
            ...serie.get({plain: true}),
        };

    }];

/**
 * Create serie media
 * @param {string} serieUUID
 * @param {string} url
 * @param {string} name
 * @param {integer} type
 * @param {integer} order
 * @param {string} userEmail
 * @return {Promise}
 */
export async function createSerieMedia(
    serieUUID, url, name, type, order, userEmail,
) {
    debug(`Creating a serie media`);
    const user = await getUser(userEmail);

    const media = await models.media.create({
        type,
        name,
        url,
        order,
        userId: user.id,
        uuid: serieUUID,
    });
    debug(`Media created ${media.id}`);

    return {
        success: true,
        url: media.url,
        id: media.id,
    };
}

/**
 * Get a list of the top author series
 * @param {string} email
 * @return {Promise}
 */
export async function listTopByAuthor(email) {
    debug(`Series by author ${email}`);
    const user = await getUser(email);

    const series = await models.serie.scope(
        'authorListing',
        {method: ['asAuthor', user.id]},
    ).findAll({
        limit: 25,
    });
    debug(`Series by author ${series.length}`);
    const plain = series.map((serie) => serie.get({plain: true}));

    return {
        count: plain.length,
        series: plain,
    };

}

/**
 * Get series from list ids
 * @param {array<string>} seriesIds
 * @param {object} [options]
 * @return {array<object>} contents
 */
export async function listSeriesFromList(seriesIds, options = {}) {
    debug(`List contents from list`);

    const scopes = options.scopes || [
        {method: ['authorListing']},
        {method: ['categoryJoin']},
    ];

    const optionsWhere = options.where || {};
    const where = {
        ...optionsWhere,
    };

    if (seriesIds.length > 0) {
        where.uuid = {
            [Op.in]: seriesIds,
        };
    }

    delete options.where;
    delete options.scope;

    const series = await models.serie.scope(...scopes).findAll({
        where,
        ...options,
    });

    const plainContents = series.map((cont) => cont.get({plain: true}));

    return plainContents;
}


/**
 * List top series by category
 * @param {object} geo
 * @param {number} categoryId
 * @return {Promise}
 */
export const byCategory = [
    'geoIp',
    serviceCacheable(__filename),
    async function byCategory(geo, categoryId) {
        debug(`Listing by category ${categoryId}`);

        const series = await models.serie.scope(
            'enabled',
            'serieContentsList',
            {method: ['byCategory', categoryId]},
        ).findAll({
            limit: 25,
        });

        if (series.length === 0) {
            return {count: 0, series: []};
        }

        const serieIds = series.map((serie) => serie.uuid);

        debug(`GEO: ${JSON.stringify(geo.country, null, 4)}`);
        debug(`serieIds: ${JSON.stringify(serieIds, null, 4)}`);

        let countryId = await models.serie.getCountryId(geo.country);
        if (countryId) {
            countryId = countryId[0].id;
        }

        const avSeasons = await
        models.serie.readAvailableSeasons(countryId, serieIds);

        const realSeries = [];
        for ( let i = 0; i < series.length; i++ ) {
            let keepLooking = true;
            for ( let j = 0; j < avSeasons.length && keepLooking; j++ ) {
                if ( avSeasons[j].serieUUID === series[i].uuid ) {
                    realSeries.push(series[i]);
                    keepLooking = false;
                }
            }
        }

        // Fill data to display as a Content
        const filledSeries = realSeries.map((serie) => {
            return serie.fillDataToDisplay();
        });

        return {
            count: filledSeries.length,
            series: filledSeries,
        };
    },
];

/**
 * Create serie content
 * @param {string} email
 * @param {string} moderatorEmail
 * @param {string} serieUUID
 * @param {object} newContent
 * @param {string} warrantyAccountId
 * @return {Promise}
 */
export async function createSerieContent(email, moderatorEmail, serieUUID,
    newContent, warrantyAccountId) {
    debug(`Creating new content to serie ${email}`);
    // Validate serie to user and season
    const user = await getUser(email);
    const serie = await models.serie.scope(
        {method: ['contentRelation', user.id, newContent.seasonUUID]},
        'statusEnabledOrPaused',
    ).findByPk(serieUUID);
    if (!serie || !newContent.seasonUUID) {
        throw new ValidationError('Serie error', [{
            field: 'serieUUID',
            type: 'CreateSeasonError',
            msg: 'Invalid serie id',
        }]);
    }
    // Set content serie
    newContent.serieUUID = serieUUID;
    newContent.contentType = models.contents.TYPE_SERIE;

    const createdContent = await createContent(
        email,
        moderatorEmail,
        newContent,
        warrantyAccountId,
        true
    );

    await seriesToElastic(serieUUID, 'add');

    return createdContent;
}

/**
 * send series to elastic search
 * @param {string} uuid
 * @param {string} action
 */
export async function seriesToElastic(uuid, action) {
    const serieDB = await models.serie.scope(
        'statusEnabledOrPaused',
        {method: ['detailed']},
        {method: ['withSeasons', false]}
    )
        .findOne({
            where: {
                uuid: uuid,
            },
        });

    if (!serieDB) {
        debug(`${action} series to elastic. Serie: ${uuid} not found`);
        return;
    }

    let atLeastOneSeed = false;

    for (let i = 0; i < serieDB.season.length; i++) {
        for (let j = 0; j < serieDB.season[i].content.length; j++) {
            if (serieDB.season[i].content[j].downloadStatus > 0) {
                atLeastOneSeed = true;
                i = serieDB.season.length;
                break;
            }
        }
    }

    debug(`${action} series to elastic ${uuid} HasSeed:${atLeastOneSeed}`);

    switch (action) {
        case 'remove':
            if (!atLeastOneSeed) {
                await serieDB.removeFromElastic();
            }
            break;
        case 'add':
        case 'update':
            if (atLeastOneSeed) {
                await serieDB.addToElastic();
            }
            break;
    }
}

/**
 * Get an available serie by uuid
 * @param {object} geo
 * @param {string} lang
 * @param {string} uuid
 * @return {Promise}
 */
export const getAvailableSerie = [
    'geoIp',
    'lang',
    serviceCacheable(__filename),
    async function getAvailableSerie(geo, lang, uuid) {
        debug(`Get serie ${uuid}`);

        const serie = await models.serie.scope(
            'enabled',
            {method: ['detailed']},
            {method: ['withGeoAvailableSeasons', geo.country, lang]}
        ).findByPk(uuid, {
            rejectOnEmpty: false,
        });

        if (!serie) {
            throw new EmptyResultError();
        }

        serie.season.sort((s1, s2) => {
            if ( s1.number < s2.number) {
                return -1;
            } else if ( s1.number > s2.number ) {
                return 1;
            }
            return 0;
        });

        await serie.season.map((s) => {
            s.content.sort((c1, c2) => {
                if ( c1.order < c2.order) {
                    return -1;
                } else if ( c1.order > c2.order ) {
                    return 1;
                }
                return 0;
            });
        });

        return serie;
    },
];

/**
 * Update serie
 * @param {string} email
 * @param {object} serieUUID
 * @param {object} serie
 * @return {Promise}
 */
export async function updateSerie(email, serieUUID, serie) {
    debug(`Updating serie ${serieUUID}`);
    const user = await getUser(email);
    const serieDB = await models.serie.findOne({
        where: {
            uuid: serieUUID,
            authorId: user.id,
            status: models.serie.STATUS_ENABLED,
        },
    });

    if (!serieDB) {
        throw new ValidationError('Serie error', [{
            field: 'serieUUID',
            type: 'ForbiddenError',
            msg: 'Invalid user for serie id',
        }]);
    }
    await serieDB.update(
        serie,
        {
            'fields': [
                'title',
                'bodyFormat',
                'body',
                'categoryId',
                'audioLang',
                'credits',
                'rating',
                'tags',
            ],
        }
    );

    // to send to elastic
    await seriesToElastic(serieUUID, 'update');

    return {
        success: true,
    };
}

/**
 * Delete serie, seasons, contents and media related
 * @param {string} email
 * @param {object} serieUUID
 * @return {Promise}
 */
export async function deleteSerie(email, serieUUID) {
    debug(`Deleting serie ${serieUUID}`);
    const user = await getUser(email);
    const serieDB = await models.serie.scope(
        {method: ['asAuthor', user.id]},
        'authorDetailed',
    ).findByPk(serieUUID);

    debug(`Serie to delete: ${JSON.stringify(serieDB, null, 4)}`);

    // User does not have permissions to this serie or this does not exist
    if (!serieDB) {
        throw new ValidationError('Serie error', [{
            field: 'serieUUID',
            type: 'ForbiddenError',
            msg: 'Invalid user for serie id',
        }]);
    }

    let contents = [];
    if (serieDB.season) {
        await serieDB.season.map((season) => {
            contents = contents.concat(season.content);
        });
    }

    for (let i = 0; i < contents.length; i++) {
        await models.contents.destroy({where: {uuid: contents[i].uuid}});
    }

    try {
        serieDB.removeFromElastic();
    } catch (e) {
        debug(`Failed to remove from elastic: ${e}`);
    }

    await serieDB.destroy();

    return {
        success: true,
    };
}

/**
 * Get Season Geoblocking
 * @param {string} email
 * @param {uuid} seasonId
 * @return {Promise}
 */
export async function getSeasonGeoblocking(email, seasonId) {
    debug(`Get season geoblocking`);

    const user = await getUser(email);
    const season = await models.season.scope(
        {method: ['asAuthor', user.id]}
    ).findByPk(seasonId);

    if (!season) {
        throw new ValidationError('Geoblocking error', [{
            field: 'seasonUUID',
            type: 'ForbiddenError',
            msg: 'Invalid user or season uuid',
        }]);
    }

    let geoBlocking;
    geoBlocking = await season.getGeoBlocking({scope: 'detailled'});

    if (!geoBlocking) {
        geoBlocking = {
            type: 0,
            countries: [],
        };
    }

    return geoBlocking;
}

/**
 * Set Season Geoblocking
 * @param {string} email
 * @param {uuid} seasonId
 * @param {integer} type
 * @param {array<integer>} countries
 * @return {Promise}
 */
export async function setSeasonGeoblocking(email, seasonId, type, countries) {
    debug(`Set season geoblocking`);

    const user = await getUser(email);
    const season = await models.season.scope(
        {method: ['asAuthor', user.id]}
    ).findByPk(seasonId);

    if (!season) {
        throw new ValidationError('Geoblocking error', [{
            field: 'seasonUUID',
            type: 'ForbiddenError',
            msg: 'Invalid user or season uuid',
        }]);
    }

    let geoBlocking;
    geoBlocking = await season.getGeoBlocking();

    if (!geoBlocking) {
        geoBlocking = await models.geoblocking.create({
            targetUUID: seasonId,
            entityType: models.geoblocking.ETYPE_SERIE_SEASON,
            type,
        });
    } else {
        geoBlocking.update({type}, {fields: ['type']});
    }

    await geoBlocking.setCountries(countries);

    await seriesToElastic(season.serieUUID, 'update');

    return {
        success: true,
    };
}

/**
 * Delete Season Geoblocking
 * @param {string} email
 * @param {uuid} seasonId
 * @return {Promise}
 */
export async function deleteSeasonGeoblocking(email, seasonId) {
    debug(`Delete season geoblocking`);

    const user = await getUser(email);
    const season = await models.season.scope(
        {method: ['asAuthor', user.id]}
    ).findByPk(seasonId);

    if (!season) {
        throw new ValidationError('Geoblocking error', [{
            field: 'seasonUUID',
            type: 'ForbiddenError',
            msg: 'Invalid user or season uuid',
        }]);
    }

    const geoBlocking = await season.getGeoBlocking();

    if (geoBlocking) {
        await geoBlocking.destroy();
    }

    await seriesToElastic(season.serieUUID, 'update');

    return {
        success: true,
    };
}

/**
 * Delete serie content
 * @param {string} authorEmail
 * @param {string} serieId
 * @param {object} body
 * @return {Promise}
 */
export async function deleteSerieContent(authorEmail, serieId, body) {
    if (typeof body === 'string') {
        body = JSON.parse(body);
    }

    const contentIds = body.contentIds || [];
    const user = await getUser(authorEmail);
    const contents = await models.contents.scope(
        {method: ['asAuthor', user.id]},
    ).findAll({
        where: {
            serieUUID: serieId,
        },
    }, {
        rejectOnEmpty: true,
    });

    const toDeleteContent = [];
    const toKeepContent = [];
    await contents.map((content) => {
        const toDelete = contentIds.find((x) => x === content.uuid);
        if (toDelete) {
            toDeleteContent.push(content);
            return;
        }
        toKeepContent.push(content);
    });

    const toDeleteSeasons = body.seasonIds || [];
    await toDeleteContent.map(async (toDelete) => {
        const seasonHasContent =
                toKeepContent.find((x) => x.seasonUUID === toDelete.seasonUUID);
        if (!seasonHasContent) {
            if (toDeleteSeasons.indexOf(toDelete.seasonUUID) === -1 ) {
                toDeleteSeasons.push(toDelete.seasonUUID);
            }
        }
        toDelete.destroy();
    });

    // Remove seasons
    const seasons = await models.season.findAll({
        where: {
            uuid: {
                [Op.in]: toDeleteSeasons,
            },
            authorId: user.id,
        },
    });
    await Promise.map( seasons, async (s) => {
        await s.destroy();
    });

    // Check if all series' seasons were deleted,
    // if the case, then delete the serie itself
    if (toDeleteSeasons.length > 0) {
        const remainingSeasons = await models.season.count({
            where: {
                serieUUID: serieId,
            },
        });
        if (!remainingSeasons) {
            const serie = await models.serie.findByPk(serieId);
            serie.destroy();
            try {
                await seriesToElastic(serieId, 'remove');
            } catch (e) {
                debug(`Couldnt remove serie from elastic: ${e}`);
            }
        }
    }

    return {
        success: true,
    };
}

/**
 * Reorder serie content
 * @param {string} authorEmail
 * @param {object} newOrder
 * @return {Promise}
 */
export async function reorderEpisodes(authorEmail, newOrder) {
    const response = {success: false};

    if (!newOrder.reorder) {
        response.error = 'no reorder array provided';
        return response;
    }

    const contentIds = await newOrder.reorder.map((item) => {
        return item.uuid;
    });

    const user = await getUser(authorEmail);
    const contents = await models.contents.scope(
        {method: ['asAuthor', user.id]},
        {method: ['byIds', contentIds]},
    ).findAll();

    if (contents.length === 0) {
        response.error = 'No content found';
        return response;
    }

    await Promise.map(contents, async (content) => {
        const item = newOrder.reorder.find((x) => x.uuid === content.uuid);
        await content.update({order: item.order});
    });

    return {
        success: true,
    };
}

/**
 * Pause series
 * @param {string} authorEmail
 * @param {string} serieUUID
 * @param {Object} body
 * @return {Promise}
 */
export async function pauseSeries(authorEmail, serieUUID, body) {
    const user = await getUser(authorEmail);
    let serieDB;
    let seasons;
    let contents;

    let level = 'series';
    let seasonUUID = body.seasons || [];
    let contentUUID = body.contents || [];
    const message = body.message;

    if (message.length < 3) {
        throw new ValidationError('Serie pause error', [{
            field: 'message',
            type: 'Invalid message',
            msg: 'To pause contents, a message must be provided',
        }]);
    }

    if (seasonUUID.length > 0) level = 'season';
    if (contentUUID.length > 0) level = 'content';

    let newStatus;
    let elasticAction;
    if (body.pause && !body.reactivate) {
        newStatus = 'STATUS_PAUSED';
        elasticAction = 'remove';
    }
    if (!body.pause && body.reactivate) {
        newStatus = 'STATUS_ENABLED';
        elasticAction = 'add';
    }

    if (!newStatus) {
        throw new ValidationError('Serie pause error', [{
            field: 'pause',
            type: 'Invalid action',
            msg: 'Pause and reactivate fields must have different values',
        }]);
    }

    await models.sequelize.transaction(async (dbTx) => {

        switch (level) {
            case 'series':
                serieDB = await models.serie.scope(
                    {method: ['asAuthor', user.id]},
                    'authorDetailed',
                ).findByPk(serieUUID);

                await serieDB.update({
                    status: models.serie[newStatus],
                    cancelComment: message,
                }, {
                    transaction: dbTx,
                });

                seasonUUID = serieDB.season.map(({uuid}) => uuid);
                // falls through
            case 'season':
                seasons = await models.season.scope(
                    'authorDetailed',
                    {method: ['asAuthor', user.id]},
                ).findAll({
                    where: {
                        uuid: {
                            [Op.in]: seasonUUID,
                        },
                    },
                    transaction: dbTx,
                    lock: dbTx.LOCK.UPDATE,
                });

                contentUUID = await Promise.reduce(
                    seasons,
                    (contents, season) => season
                        .update({
                            status: models.season[newStatus],
                        }, {
                            transaction: dbTx,
                        })
                        .then(() => [
                            ...contents,
                            ...season.content.map(({uuid}) => uuid),
                        ])
                    ,
                    contentUUID
                );

                // falls through
            case 'content':
                contents = await models.contents.scope(
                    {method: ['asAuthor', user.id]},
                ).findAll({
                    where: {
                        uuid: {
                            [Op.in]: contentUUID,
                        },
                    },
                    transaction: dbTx,
                    lock: dbTx.LOCK.UPDATE,
                });

                await Promise.map(contents, (c) => c.update({
                    status: models.contents[newStatus],
                    cancelComment: message,
                }, {
                    transaction: dbTx,
                }));
        }

    });

    if (level === 'series' && elasticAction) {
        seriesToElastic( serieUUID, elasticAction );
    }

    return {
        success: true,
    };
}

/**
 * Replace cover form serie
 * @param {string} email
 * @param {string} uuid
 * @param {string} name
 * @param {string} url
 */
export async function replaceCover(email, uuid, name, url) {
    debug(`Replace cover of ${uuid} by ${url}`);

    const user = await getUser(email);

    const serie = await models.serie.scope({method: ['asAuthor', user.id]})
        .findOne({
            attributes: ['uuid'],
            where: {uuid},
            rejectOnEmpty: true,
        });

    await serie.replaceCover(name, url);

    debug(`\tsuccess`);
    return;
}

/**
 * Get serie capitules info
 * @param {string} uuid
 * @return {object}
 */
export async function getCapsInfo(uuid) {
    debug(`Get captiules info (order)`);
    const serie = await models.serie.scope({method: ['capsInfo']})
        .findOne({
            where: {uuid},
            rejectOnEmpty: true,
        });

    return serie.get({plain: true});
}

/**
 * Get series ids
 * @return {string[]}
 */
export async function listEnabledSeriesIds() {
    debug(`Listing all enabled series ids`);
    return models.serie.scope('enabled')
        .findAll({
            attributes: ['uuid'],
            raw: true,
        })
        .map((res) => res.uuid)
    ;
}
