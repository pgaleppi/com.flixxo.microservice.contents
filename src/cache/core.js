import {getStore} from './stores';
import {createKeyFor} from './utils';
import config from '../config';
import {createDebug} from '../logger';

const CACHE_ENABLED = config.get('cache:enabled');

const debug = createDebug(__filename);

const Store = getStore();

/**
 * Try to get from cache, or execute
 * @param {string} filename
 * @param {string} prefix
 * @param {any[]} toIdent
 * @param {object} [options]
 * @param {function} fn
 * @return {Promise<any>}
 */
export async function fromCache(filename, prefix, toIdent, options, fn) {

    if (typeof options === 'function') {
        fn = options;
        options = {};
    }

    if (fn.name === '' && !prefix) {
        throw new Error('You need to set a prefix for anonimous functions');
    }

    if (!CACHE_ENABLED) {
        debug(`Cache is disabled, pass`);
        return fn();
    }

    debug(`Apply cache to ${prefix}`);
    const store = options.store || new Store(filename, {prefix});

    const ckey = createKeyFor(...toIdent);
    debug(`Finding for cache for ${ckey}`);
    const cache = await store.get(ckey, {
        ...options,
    });

    if (cache) {
        debug(`\tThere are cache!`);
        return cache;
    }

    debug(`\tNot cached, continue`);

    const res = await fn();
    debug('\tSaved in cache, returning');
    store.set(ckey, res);
    return res;
}

/**
 * Touch flag
 * @param {string} flag
 * @param {object} [options]
 * @return {Promise}
 */
export async function touchFlag(flag, options = {}) {

    if (!CACHE_ENABLED) {
        debug(`[flag ${flag}] Cache is disabled, pass`);
        return;
    }

    debug(`Touching flag ${flag}`);
    const store = options.store || new Store(__filename);
    return store.touchFlag(flag);
}
