
import Promise from 'bluebird';
import {
    createNamespace as createCLNamespace,
    getNamespace as getCLNamespace,
} from 'cls-hooked';

const FOLLOW_CALL_CONTEXT = !!process.env['DEBUG_CONTEXT'];

/**
 * Class to manage NameSpace commands
 */
export class NSWrappedCommander {

    /**
     * Create new commander
     * @param {NameSpace|null} nameSpace - If is null, commander ignore commands
     */
    constructor(nameSpace) {
        this._ns = nameSpace;
    }

    /**
     * @type {NameSpace}
     */
    get nameSpace() {
        return this._ns;
    }

    /**
     * Add prefix to logs context
     * @param {stirng} key
     * @param {mixed} prefix
     */
    addPrefix(key, prefix) {
        if (!this._ns) {
            return;
        }

        this._ns.set('prefixes', [
            ...this._ns.get('prefixes') || [],
            {key: `${key}`, prefix},
        ]);
    }
}

export const createNamespace = (() => {
    let nameSpace;

    return function() {

        if (!FOLLOW_CALL_CONTEXT) {
            return null;
        }

        if (!nameSpace) {
            nameSpace = createCLNamespace('LogContext');
        }
        return nameSpace;
    };
})();


/**
 * Wrap a nameSpace in a async call stack and return it in a promise
 * @param {function} callback
 * @return {Promise}
 */
export function nsWrapper(callback) {
    if (!FOLLOW_CALL_CONTEXT) {
        return callback(new NSWrappedCommander(null));
    }

    const ns = createNamespace();
    return new Promise((resolve, reject) => {
        ns.run(() => {
            const commander = new NSWrappedCommander(ns);
            ns.set('commander', commander);

            Promise.resolve(callback(commander))
                .then((res) => {
                    resolve(res);
                })
                .catch((e) => {
                    reject(e);
                });
        });
    });
}

/**
 * Get the context name space
 * @return {NameSpace|null}
 */
export function getNamespace() {
    if (FOLLOW_CALL_CONTEXT) {
        return getCLNamespace('LogContext');
    } else {
        return null;
    }
}

/**
 * Get NSWrappedCommander from actual namespace
 * @return {NSWrappedCommander}
 */
export function getNSCommander() {
    if (FOLLOW_CALL_CONTEXT) {
        const ns = getNamespace();
        const commander = ns ? ns.get('commander') : null;
        return commander || new NSWrappedCommander(null);
    } else {
        return new NSWrappedCommander(null);
    }
}
