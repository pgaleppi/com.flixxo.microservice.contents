import {createDebug} from '../logger';

const debug = createDebug(__filename);

export default (sequelize, DataTypes) => {

    const FollowersModel = sequelize.define('UserFollowers', {
        followedId: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,
            unique: false,
        },

        followerId: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,
            unique: false,
        },
    }, {
        timestamps: true,
        updatAt: false,
        paranoid: false,
        indexes: [
            {
                name: 'followers_once',
                unique: true,
                fields: ['followedId', 'followerId'],
            },
        ],
        validate: {
            avoidAutoFollow() {
                if (this.followedId === this.followerId) {
                    debug(`User try to auto-follow`);
                    throw new Error(`An user can't follow to himself`);
                }
            },
        },
    });

    return FollowersModel;
};
