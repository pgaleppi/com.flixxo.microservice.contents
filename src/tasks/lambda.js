import Promise from 'bluebird';
import handler from '../lambda/handler';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

/**
 * Process call
 * @param {object} event
 * @param {object} context
 */
export async function processCall(event, context) {
    debug(`Processing`);
    const res = await handler(event, context);
    const body = JSON.parse(res.body);

    process.stdout.write(`Status code ${res.statusCode}\n`);

    process.stdout.write(JSON.stringify(body, null, 2));
    return;
}

/**
 * Process call from raw JSON
 * @param {Buffer} buff
 */
export async function processCallRaw(buff) {
    debug(`Running raw call`);
    const json = JSON.parse(buff.toString('utf8'));
    if (typeof json['event'] === 'undefined') {
        debug(`event is not present in data`);
        throw new Error('Data need to contain a `event` key');
    }

    return processCall(json.event, json.context || {});
}

/**
 * Simulate lambda call
 * @param {object} params
 * @return {bool}
 */
export default async function({event, context, params, cmd}) {
    const eventDefined = !!(event || context || params || cmd);

    event = {...event || {}};
    event = {
        ...event,
        body: event.body || '{}',
    };

    let body = JSON.parse(event.body);
    body = {
        ...body,
        cmd: cmd || body.cmd,
        params: params || body.params || [],
    };

    event.body = JSON.stringify(body);

    debug(`Running event ${event}`);

    if (eventDefined) {
        return processCall(event, context || {});
    } else {
        debug(`Running from stdin`);
        return new Promise((resolve, reject) => {
            let data = [];

            process.stdin.on('data', (chunk) => data = [...data, chunk]);
            process.stdin.on('error', (e) => reject(e));
            process.stdin.on('end', async () => {
                debug(`Data complete, call lambda`);
                await processCallRaw(Buffer.concat(data));
                return resolve();
            });
        });
    }
}
