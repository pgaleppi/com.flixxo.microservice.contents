import Promise from 'bluebird';
import {SQS} from 'aws-sdk';
import {loadService as loadTunneledService} from '../remoteservice';
import {createDebug} from '../logger';
import config from '../config';

const debug = createDebug(__filename);

const AWS_AUTH_KEY_ID = config.get('aws:auth:signatureAccessKeyId') || null;
const AWS_AUTH_KEY = config.get('aws:auth:signatureSecretKey') || null;
const SQS_SERVICES = config.get('aws:sqs:services');
const SQS_OPTIONS = config.get('aws:sqs:options');

/**
 * Service instance
 */
export class Service {

    /**
     * SQS connector instance
     * @private
     * @type {AWS.SQS}
     */
    #sqsClient = null;

    /**
     * Service tunnel
     * @private
     * @type {Service}
     */
    #serviceTunnel = null;

    /**
     * Create new service instance
     * @param {string} name - Get config from aws.sqs.services.[name]
     * @param {object|Promise|function} [context] Context for isntance
     */
    constructor(name, context = {}) {
        debug(`Creating service for ${name}`);

        if (typeof SQS_SERVICES[name] === 'undefined') {
            throw new Error(`Service ${name} not configured`);
        }

        this._serviceConfig = SQS_SERVICES[name];

        let opts = {
            sslEnabled: true,
            logger: (...p) => {
                debug(`[SQS:${name}]`, ...p);
            },
            ...SQS_OPTIONS || {},
            ...this._serviceConfig || {},
        };

        if (AWS_AUTH_KEY && AWS_AUTH_KEY_ID) {
            debug(`\tAWS AUTH provided, setting`);
            opts = {
                accessKeyId: AWS_AUTH_KEY_ID,
                secretAccessKey: AWS_AUTH_KEY,
                ...opts,
            };
        }

        debug(`\tStart ${name} with `, opts);
        this._name = name;
        this._context = typeof context === 'function' ? context() : context;
        this._sqsOps = opts;
    }

    /**
     * SQS client
     * @type {AWS.SQS}
     */
    get sqsClient() {
        if (this._serviceConfig.tunneled) {
            throw new Error(`Tunneled connection`);
        }

        if (!this.#sqsClient) {
            this.#sqsClient = new SQS(this._sqsOps);
        }

        return this.#sqsClient;
    }

    /**
     * Return tunnel connection
     * @return {Service}
     */
    async getServiceTunnel() {
        if (!this._serviceConfig.tunneled) {
            throw new Error(`Not tunneled connection`);
        }

        if (!this.#serviceTunnel) {
            this.#serviceTunnel = await loadTunneledService(
                this._serviceConfig.tunneled,
                this._context
            );
        }

        return this.#serviceTunnel;
    }

    /**
     * Get context
     * @return {Promise<object>}
     */
    async getContext() {
        const context = await Promise.resolve(this._context);
        this._context = context;
        return context;
    }

    /**
     * Set context
     * @param {object|Pomise} context
     */
    setConext(context) {
        this._context = context;
    }

    /**
     * Get new instance of Command with new context
     * @param {object|Promise|function} context
     * @return {Command}
     */
    bindContext(context = {}) {
        return new Service(this._name, context);
    }

    /**
     * Send RPC command to the pool
     * @param {string} cmd
     * @param {any[]} [...params]
     * @return {Promise}
     */
    async exec(cmd, ...params) {
        const context = await this.getContext();

        if (this._serviceConfig.tunneled) {
            const tunnel = await this.getServiceTunnel();
            return tunnel.execute(
                cmd,
                ...params
            );
        }

        const pack = {
            cmd,
            params,
            context,
        };

        return new Promise((resolve, reject) => {
            const {QueueUrl} = this._serviceConfig;
            const MessageBody = JSON.stringify(pack);

            this.sqsClient.sendMessage({
                MessageBody,
                QueueUrl,
                DelaySeconds: 0,
            }, (err, data) => {
                if (err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    }

}

export const getServiceConnection = (() => {
    const services = {};

    return function(name) {
        if (!services[name]) {
            services[name] = new Service(name);
        }

        return services[name];
    };

})();
