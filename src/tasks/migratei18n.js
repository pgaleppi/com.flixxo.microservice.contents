
import Promise from 'bluebird';
import models from '../models';
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

/**
 * Migrate DB task
 */
export default async function() {
    logger.info(`Getting all contents`);

    const contents = await models.contents.findAll({
        attributes: [
            'uuid',
            'audioLang',
            models.sequelize.col('Content.title'),
            models.sequelize.col('Content.body'),
        ],
        include: [{
            model: models.metadata,
            as: 'metadata',
            attributes: ['createdAt'],
            separate: false,
            required: false,
            scope: {
                lang: 'en',
            },
        }],
        raw: true,
        paranoid: false,
    })
        .map((i) => ({
            uuid: i.uuid,
            title: i.title,
            body: i.body,
            audioLang: i.audioLang,
            hasMeta: !!i['metadata.createdAt'],
        }));

    logger.info(`${contents.length} total contents`);

    const withoutMeta = contents.filter((c) => !c.hasMeta);

    logger.info(`${withoutMeta.length} hasn't metadata`);

    let createdCount = 0;

    logger.info(`Creating metadata`);

    await Promise.mapSeries(withoutMeta, async (data) => {
        const [md, created] = await models.metadata.upsert({
            entityType: models.metadata.ENTITY_TYPE_CONTENT,
            entityId: data.uuid,
            lang: data.audioLang,
            title: data.title,
            body: data.body,
        }, {
            returning: true,
        })
            .catch((e) => {
                logger.error(`Error saving name ${e.message}`);
                logger.error(e);
                return [null, false];
            });

        if (created) {
            createdCount++;
        }

        debug(typeof md);
    });


    logger.info(`Migrating versions`);

    const versions = await models.contentversion.findAll({
        attributes: [
            'uuid',
            'audioLang',
            models.sequelize.col('ContentVersion.title'),
            models.sequelize.col('ContentVersion.body'),
        ],
        include: [{
            model: models.metadata,
            as: 'metadata',
            attributes: ['createdAt'],
            separate: false,
            required: false,
            scope: {
                lang: 'en',
            },
        }],
        raw: true,
        paranoid: false,
    })
        .map((i) => ({
            uuid: i.uuid,
            title: i.title,
            body: i.body,
            audioLang: i.audioLang,
            hasMeta: !!i['metadata.createdAt'],
        }));

    logger.info(`${versions.length} total contents versions`);

    const versionsWithoutMeta = versions.filter((c) => !c.hasMeta);

    logger.info(`${versionsWithoutMeta.length} hasn't metadata`);

    let versionsCreatedCount = 0;

    logger.info(`Creating metadata`);

    await Promise.mapSeries(versionsWithoutMeta, async (data) => {
        const [md, created] = await models.metadata.upsert({
            entityType: models.metadata.ENTITY_TYPE_CONTENTVERSION,
            entityId: data.uuid,
            lang: data.audioLang,
            title: data.title,
            body: data.body,
        }, {
            returning: true,
        })
            .catch((e) => {
                logger.error(`Error saving name ${e.message}`);
                logger.error(e);
                return [null, false];
            });

        if (created) {
            versionsCreatedCount++;
        }

        debug(typeof md);
    });

    logger.info(`Migration completed`);
    logger.info(`${createdCount} contents names processed`);
    logger.info(`${versionsCreatedCount} versions names processed`);

    return;
}
