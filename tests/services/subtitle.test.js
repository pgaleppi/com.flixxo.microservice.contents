import {syncDb, loadFixture, loadData} from '../helpers/db';
import * as subtitlesService from '../../src/services/subtitles';
// import {SequelizeUniqueConstraintError} from 'sequelize';
import {ValidationError} from '../../src/errors';

describe('Testing subtitles service', () => {

    beforeEach(async (done) => {
        await syncDb();
        await loadData('categories');
        await loadData('00_countries');
        await loadFixture('users');
        await loadFixture('profiles');
        done();
    }, 10000);


    describe('#createSubtitle', () => {
        beforeEach(async (done) => {
            await loadFixture('subtitles-contents');
            await loadFixture('subtitles-for-get');
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        it(`Must create a new subtitle`, async () => {
            const newSubtitle = {
                label: 'Fake label',
                url: 'http://domain.com/spanish.srt',
                uuid: '7cdeff76-c4df-4bf2-a810-c0f054033a28',
                lang: 'es',
                closeCaption: true,
            };
            const result = await subtitlesService.createSubtitle(
                newSubtitle.uuid,
                newSubtitle.url,
                newSubtitle.lang,
                newSubtitle.label,
                newSubtitle.closeCaption,
            );

            expect(result.success).toBeTruthy();
            expect(result.url).toBeDefined();
            expect(result.id).toBeDefined();
        });

    });

    describe('#getContentByAuthor', () => {
        beforeEach(async (done) => {
            await loadFixture('subtitles-contents');
            await loadFixture('subtitles-for-get');
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        it(`Must get an available content for subtitle upload`, async () => {
            const lang = 'en';
            const result = await subtitlesService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                '7cdeff76-c4df-4bf2-a810-c0f054033a28',
            );

            expect(result).toBeDefined();
            expect(result.uuid).toBe('7cdeff76-c4df-4bf2-a810-c0f054033a28');
        });

        it(`Must fail by user not allowed`, async () => {
            const lang = 'en';
            await expect(subtitlesService.
                getContentByAuthor[1](
                    lang,
                    'user1@fakemail.com',
                    '7cdeff76-c4df-4bf2-a810-c0f054033a28',
                )
            ).rejects.toThrow(ValidationError);
        });

    });
});
