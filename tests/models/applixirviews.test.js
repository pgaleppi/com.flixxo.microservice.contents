
import {ValidationError} from 'sequelize';
import config from '../../src/config';
config.reset('ads:applixir:secret', '00000000');
import models from '../../src/models';
import {validateCheckSum} from '../../src/models/applixirviews';

describe('@validators', () => {

    test(`Must to validate correct body`, async () => {
        const res = models.applixirviews.build({
            uuid: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
            userId: '00000000-b5aa-4507-913a-a387419b6ef8',
            status: models.applixirviews.STATUS_NEW,
            rewardedAt: new Date('2020-05-04T18:31:16.226Z'),
            checksum: '40808e07900088f8aba112035e6a7aab',
            rawData: {
                account_id: '123',
                game_id: '345',
                custom2: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
                timestamp: '1588617076226',
                custom1: '00000000-b5aa-4507-913a-a387419b6ef8',
                checksum: '40808e07900088f8aba112035e6a7aab',
            },
        });

        await res.validate();
    });

    test(`Must to throw error with invalid checksum`, async () => {
        const res = models.applixirviews.build({
            uuid: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
            userId: '00000000-b5aa-4507-913a-a387419b6ef8',
            status: models.applixirviews.STATUS_NEW,
            rewardedAt: new Date('2020-05-04T18:31:16.226Z'),
            checksum: 'Hello world',
            rawData: {
                account_id: '123',
                game_id: '345',
                custom2: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
                timestamp: '1588617076226',
                custom1: '00000000-b5aa-4507-913a-a387419b6ef8',
                checksum: 'Hello world',
            },
        });

        try {
            await res.validate();
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.errors.map(({path, message}) => ({path, message}))).toEqual([
                {
                    path: 'checksum',
                    message: 'Invalid checksum format',
                },
                {
                    path: 'validChecksum',
                    message: 'Invalid checksum',
                },
            ]);
            return;
        }

        throw new Error('Pass');
    });
});

describe('#validateCheckSum', () => {

    /**
     * WARNING: This test DOES NOT validate REAL cases
     * need confirmation with REAL examples
     */

    test('Must to be true with valid checksum', () => {
        const res = validateCheckSum({
            account_id: '123',
            game_id: '345',
            custom2: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
            timestamp: '1588617076226',
            custom1: '00000000-b5aa-4507-913a-a387419b6ef8',
            checksum: '40808e07900088f8aba112035e6a7aab',
        });

        expect(res).toBeTruthy();
    });

    test('Must to false with invalid checksum', () => {
        const res = validateCheckSum({
            account_id: '123',
            game_id: '345',
            custom2: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
            timestamp: '1588617076226',
            custom1: '00000000-b5aa-4507-913a-a387419b6ef8',
            checksum: '2d893ae152497ebfe49e73ebb5f73ab6',
        });

        expect(res).toBeFalsy();
    });
});

describe('.fromRaw', () => {

    test(`Must to create valid view`, async () => {
        const view = models.applixirviews.fromRaw({
            account_id: '123',
            game_id: '345',
            custom2: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
            timestamp: '1588617076226',
            custom1: '00000000-b5aa-4507-913a-a387419b6ef8',
            checksum: '40808e07900088f8aba112035e6a7aab',
        });

        expect(view.get({plain: true})).toEqual({
            uuid: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
            userId: '00000000-b5aa-4507-913a-a387419b6ef8',
            status: models.applixirviews.STATUS_NEW,
            rewardedAt: new Date('2020-05-04T18:31:16.226Z'),
            checksum: '40808e07900088f8aba112035e6a7aab',
            rawData: {
                account_id: '123',
                game_id: '345',
                custom2: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
                timestamp: '1588617076226',
                custom1: '00000000-b5aa-4507-913a-a387419b6ef8',
                checksum: '40808e07900088f8aba112035e6a7aab',
            },
        });

        await view.validate();
    });

    test(`Must to throw error when invalid checksum`, async () => {
        const view = models.applixirviews.fromRaw({
            account_id: '123',
            game_id: '345',
            custom2: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
            timestamp: '1588617076226',
            custom1: '00000000-b5aa-4507-913a-a387419b6ef8',
            checksum: '40808e07900088f8aba112035e6a7aa0',
        });

        expect(view.get({plain: true})).toEqual({
            uuid: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
            userId: '00000000-b5aa-4507-913a-a387419b6ef8',
            status: models.applixirviews.STATUS_NEW,
            rewardedAt: new Date('2020-05-04T18:31:16.226Z'),
            checksum: '40808e07900088f8aba112035e6a7aa0',
            rawData: {
                account_id: '123',
                game_id: '345',
                custom2: 'AAB49D3D-38D5-4336-AC71-80BE4057C024',
                timestamp: '1588617076226',
                custom1: '00000000-b5aa-4507-913a-a387419b6ef8',
                checksum: '40808e07900088f8aba112035e6a7aa0',
            },
        });

        try {
            await view.validate();
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.message).toBe('Validation error: Invalid checksum');
            return;
        }

        throw new Error('Pass');
    });

});
