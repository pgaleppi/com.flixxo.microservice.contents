{
  "name": "com.flixxo.microservice.contents",
  "version": "0.50.0",
  "description": "",
  "main": "lib/main.js",
  "scripts": {
    "test": "npx jest",
    "test:watch": "npx jest --coverage=false --watch",
    "test:debug": "node --inspect-brk=0.0.0.0:9229 ./node_modules/.bin/jest --runInBand --testTimeout=60000",
    "test:dc": "docker-compose run --rm test",
    "test:dc:watch": "docker-compose run --rm test --coverage=false --watch",
    "test:dc:debug": "docker-compose run --rm -p 0.0.0.0:9229:9229 --entrypoint=\"yarn test:debug\" test",
    "start-local": "npx babel-node src/index.js start",
    "start-local:debug": "npx babel-node --inspect-brk=0.0.0.0:9229 src/index.js start",
    "start-local:profile": "npx babel-node --prof src/index.js start",
    "start-dev": "node lib/index.js start",
    "start-qa": "node lib/index.js start",
    "start-stg": "node lib/index.js start",
    "start-prod": "node lib/index.js start",
    "start-watch": "npm run watch",
    "start-serverless": "npm run build && npx sls offline start -s",
    "build": "rm -rf lib/**; npx babel src --out-dir lib --copy-files",
    "deploy": "npm run build && npx sls deploy -v --stage",
    "sls": "npx sls",
    "sls:prune": "npx sls prune -n 3 --stage",
    "deploy:hook:custom": "cd infra && npx sls",
    "deploy:hook:create": "npm run deploy:hook:custom -- deploy --stage $(git symbolic-ref HEAD | sed 's/^.*\\///g') --prodbranch $(git config gitflow.branch.master || echo -n master) --name $(basename $(pwd | sed 's/\\/infra$//g') | sed 's/^.*\\.//g')",
    "deploy:hook:remove": "npm run deploy:hook:custom -- remove --stage $(git symbolic-ref HEAD | sed 's/^.*\\///g') --prodbranch $(git config gitflow.branch.master || echo -n master) --name $(basename $(pwd | sed 's/\\/infra$//g') | sed 's/^.*\\.//g')",
    "s3deploy": "npx sls s3deploy -v --stage",
    "prepare": "npm run build",
    "lint": "npx eslint --fix src",
    "watch": "npx babel-watch src/index.js start",
    "task": "node lib/index.js",
    "dev-task": "npx babel-node src/index.js",
    "dev-task:debug": "npx babel-node --inspect-brk=0.0.0.0:9229 src/index.js",
    "dev-task:profile": "npx babel-node --prof src/index.js",
    "ncu": "npx ncu",
    "ncu:list": "npx ncu",
    "ncu:upgrade": "npx ncu -u",
    "syncdb": "npm run task -- syncdb",
    "loaddata": "npm run task -- loaddata --path",
    "dev-syncdb": "npm run dev-task -- syncdb",
    "dev-loaddata": "npm run dev-task -- loaddata --path",
    "loadfixtures": "npm run task -- loaddata --path ./fixtures/\\*",
    "dev-loadfixtures": "npm run dev-task -- loaddata --path ./fixtures/\\*",
    "bulk-es-index": "npm run task -- bulkESIndex",
    "dev-bulk-es-index": "npm run dev-task -- bulkESIndex",
    "clean-temp-media": "npm run task -- cleanTempMedia",
    "dev-clean-temp-media": "npm run dev-task -- cleanTempMedia",
    "process-ads-rewards": "npm run task -- adsrewards",
    "dev-process-ads-rewards": "npm run dev-task -- adsrewards",
    "pre-deploy": "npm run task -- syncdb --force && npm run task -- loaddata --path ./fixtures/\\*"
  },
  "husky": {
    "hooks": {
      "pre-commit": "./hooks/pre-commit.sh"
    }
  },
  "author": "Flixxo Development Team",
  "contributors": [
    {
      "name": "Oscar J. Gentilezza",
      "email": "exos@flixxo.com"
    },
    {
      "name": "Martín Pielvitori",
      "email": "mpielvitori@edrans.com"
    },
    {
      "name": "Jonatan Bravo",
      "email": "jbravo@edrans.com"
    },
    {
      "name": "Christian Herlein",
      "email": "cherlein@edrans.com"
    }
  ],
  "license": "private",
  "etc": {
    "logging": {
      "cli": "info"
    },
    "storage": {
      "dialect": "sqlite",
      "logging": true,
      "pool": {
        "min": 5,
        "max": 15
      }
    },
    "healthcheck": {
      "port": 9045
    },
    "ads": {
      "accountId": "11111111-6418-4203-bc65-8e0976fd64cb",
      "incrementAmount": 10,
      "incentive": "1.02",
      "CPM": 1,
      "maxRegulatedFund": "10000",
      "admob": {
        "checkViews": true
      },
      "applixir": {
        "checkViews": true
      }
    },
    "warranty": {
      "returnPeriod": 604800
    },
    "serverless": {
      "host": "https://46q1xy3eme.execute-api.us-east-1.amazonaws.com/dev",
      "contents": "/contents",
      "series": "/series",
      "contentsMedia": "/contents/media"
    },
    "torrent": {
      "pieceSizeExponent": 21
    },
    "microservices": {},
    "aws": {
      "auth": {},
      "sqs": {
        "services": {}
      },
      "s3": {
        "host": "https://s3.amazonaws.com/",
        "bucket": "setBucketName",
        "uploadBucket": "setBucketName",
        "contentmedia": {
          "folder": "content-media/",
          "processedFolder": "content-media-processed",
          "acl": "public-read",
          "sizesSet": "contents"
        },
        "seriemedia": {
          "folder": "serie-media/",
          "processedFolder": "serie-media-processed",
          "acl": "public-read",
          "sizesSet": "contents"
        },
        "avatarmedia": {
          "folder": "profile/",
          "processedFolder": "profile-processed",
          "acl": "public-read",
          "sizesSet": "profile"
        }
      }
    },
    "media": {
      "expirationTime": 1800,
      "s3": {
        "cacheControl": "max-age=31536000"
      },
      "sizes": {
        "contents": {
          "hero": {
            "width": 1360,
            "height": 765
          },
          "mainscreen": {
            "width": 900,
            "height": 510
          },
          "cover": {
            "width": 445,
            "height": 250
          },
          "xcover": {
            "width": 300,
            "height": 170
          }
        },
        "profile": {
          "h32": {
            "width": 32,
            "height": 32
          },
          "h164": {
            "width": 164,
            "height": 164
          },
          "h328": {
            "width": 328,
            "height": 328
          }
        }
      }
    },
    "publication": {
      "deleteTime": 1440
    },
    "cache": {
      "enabled": false,
      "prefix": "CACHE",
      "expiration": 900,
      "type": "memory",
      "redis": {
        "endpoint": "localhost",
        "port": 6379
      }
    },
    "profiles": {
      "minAge": 13
    },
    "weblinks": {
      "expiration": 3600
    }
  },
  "devDependencies": {
    "@babel/cli": "^7.12.1",
    "@babel/core": "^7.12.3",
    "@babel/node": "^7.12.6",
    "@babel/plugin-proposal-class-properties": "^7.12.1",
    "@babel/plugin-proposal-decorators": "^7.12.1",
    "@babel/plugin-transform-runtime": "^7.12.1",
    "@babel/preset-env": "^7.12.1",
    "axios-mock-adapter": "^1.19.0",
    "babel-eslint": "^10.1.0",
    "eslint": "^7.13.0",
    "eslint-config-google": "^0.14.0",
    "eslint-config-recommended": "^4.1.0",
    "eslint-plugin-jest": "^24.1.3",
    "husky": "^4.3.0",
    "ioredis-mock": "^5.1.0",
    "jest": "^26.6.3",
    "jest-cli": "^26.6.3",
    "jest-json-schema": "^2.1.0",
    "jest-silent-reporter": "^0.3.0",
    "jest-spec-reporter": "^1.0.14",
    "npm-check-updates": "^10.1.1",
    "sequelize-mock": "https://github.com/exos/sequelize-mock#exos/testing",
    "serverless": "^2.11.1",
    "serverless-consul-variables": "=1.0.6",
    "serverless-offline": "^6.8.0",
    "serverless-plugin-existing-s3": "^2.4.0",
    "serverless-plugin-tracing": "^2.0.0",
    "serverless-prune-plugin": "^1.4.3",
    "serverless-vpc-discovery": "^2.0.0",
    "sqlite3": "^5.0.0"
  },
  "dependencies": {
    "@babel/runtime": "^7.12.5",
    "@exoshtw/admob-ssv": "^0.1.0-rc1",
    "amqplib": "^0.6.0",
    "aws-sdk": "^2.793.0",
    "axios": "^0.21.0",
    "bluebird": "^3.7.2",
    "cls-hooked": "^4.2.2",
    "commander": "^6.2.0",
    "debug": "^4.2.0",
    "decimal.js": "^10.2.1",
    "etc": "^1.1.0",
    "etc-yaml": "^0.0.5",
    "extendable-error": "^0.1.7",
    "ioredis": "^4.19.2",
    "node-stat": "^0.1.4",
    "parse-torrent": "^9.1.0",
    "pg": "^8.5.1",
    "request": "^2.88.2",
    "request-promise": "^4.2.6",
    "sequelize": "^5.21.12",
    "sequelize-fixtures": "=1.0.2",
    "sequelize-import": "^1.2.1",
    "sharp": "^0.26.3",
    "strip-bom": "^4.0.0",
    "subsrt": "^1.1.1",
    "uuid": "^8.3.1",
    "winston": "^3.3.3"
  },
  "jest": {
    "verbose": true,
    "collectCoverage": true,
    "testEnvironment": "node",
    "coveragePathIgnorePatterns": [
      "/node_modules/",
      "/tests/"
    ]
  },
  "engines": {
    "node": ">=12.0.0 <13"
  }
}
