# Flixxo Microservice

## Development commands

### test

Run unit-tests:

    $ yarn test

Tests uses [jest](https://jestjs.io/), you can to use the jest params to
improve testing, for example:

    $ yarn test --watch

See the [command line documentation](https://jestjs.io/docs/en/cli) for more
options.

### build

Use this command to generate the final _transpile_ scripts with babel.

    $ yarn build

### deploy

This command build and deploy the microservice in serverless, using the rules
configured in the _serverless file_.

    $ yarn deploy dev

### sls

Run custom Serverless command

    $ yarn sls deploy -v --stage dev

See the [Serverless CLI
reference](https://serverless.com/framework/docs/providers/aws/cli-reference/)
to see all availables commands.

> You need to be authentified with AWS before run this commands.

### sls:prune

Run Serverless prune task with default params

    $ yarn sls:prune dev

### deploy:hook:create

Task to create the **Codebuild** hook for continuos delivery, 
    $ yarn deploy:hook:create

### deploy:hook:remove

Remove the created Codebuild hook.

    $ yarn deploy:hook:remove

### deploy:hook:custom

Execute Serverless task for _infra definition_ to create hook.

### ncu

Execute [npm-check-updates](https://www.npmjs.com/package/npm-check-updates)
for packages upgrades.

### ncu:list

Same as single ncu, list package ready for upgrdade.

### ncu:upgrade

Upgrade packages.

### lint

Run the linter.

### watch

Run the code in dev mode, watching for changes

## Production commands

### task

Run microservice task in CLI

    $ yarn task start

### dev-task

Run microservice task in development mode, using on fly tranpile from sources.

### dev-task:debug

Like `dev-task` but open the inspect debugging port `9229` to inspect the code.

### dev-task:profile

Like `dev-task` but generate a profile repport.

### start-{env}

Use to start the microservice in the especific enviroment:

    $ yarn start-stg

As extra stages for dev are:

 * start-local
 * start-local:debug
 * start-local:profile
 * start-watch

## Microservice tasks

### start

Start the microservice. (no params)
