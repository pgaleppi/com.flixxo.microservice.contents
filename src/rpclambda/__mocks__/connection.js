
import Promise from 'bluebird';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

/**
 * Get a RPC connection, if don't exists, create one
 * @return {Promise<amqp.connection>}
 */
export default function getConnection() {
    debug(`Getting mock connection`);
    return Promise.resolve({});
}
