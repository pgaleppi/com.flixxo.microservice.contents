
import {syncDb, loadFixture, loadData} from '../helpers/db';

jest.mock('../../src/sqsrpc');
jest.mock('../../src/rpclib');

import {ValidationError as SequelizeValidationError} from 'sequelize';
import {ValidationError} from '../../src/errors';
import * as adsService from '../../src/services/advertisements';
import {fakeResponse, clear as clearRpc} from '../../src/rpclib';
import models from '../../src/models';
import config from '../../src/config';

describe('Testing ads service', () => {

    beforeEach(async (done) => {
        await syncDb(true);
        await loadFixture('users');
        await loadFixture('ads');
        await loadFixture('metters');
        done();
    }, 10000);

    describe('#get', () => {
        it(`Must to get a random ad`, async () => {
            let result = await adsService.getRandomAd('user1@fakemail.com');
            expect(result.id).toBeDefined();
        });
    });

    describe.skip('#adWatched', () => {
        it(`Must create a new user ad record`, async () => {
            const result = await adsService.watchedAdvertisement(
                'user1@fakemail.com',
                1,
            );
            expect(result).toBe(2);
        });
    });

    describe('#getUserAdRewards', () => {
        it(`Must to get user/ad reward`, async () => {
            let result = await adsService.getUserAdReward(
                'user1@fakemail.com'
            );
            expect(result).toBe('1.020000000000000000');
        });
    });

    describe('#createAdvertisement', () => {
        it(`Must create an advertisement`, async () => {
            let result = await adsService.createAdvertisement(
                'My advertisement',
                'http://myadurl.com/myad',
                '00000000-b5aa-4507-913a-a387419b6ef8',
            );
            expect(result.success).toBeTruthy();
        });
    });

    describe('#authorizeAdMobView', () => {

        const data = {
            'ad_network': '5450213213286189855',
            'ad_unit': '1809337431',
            'custom_data': '54716A59-7F79-4F6D-9C2A-0CF161E75FC1',
            'reward_amount': '1',
            'reward_item': 'Reward',
            'timestamp': '1588561213637',
            'transaction_id': '25bcd7138aa5d024279ae34635133027',
            'user_id': '00000000-b5aa-4507-913a-a387419b6ef8',
            'signature': 'MEUCICzWuOjBrU4JFWZ0TnGgwDzXO8PmGAe5Fq-cLHpuCCVBAiEAyI45z9isWowELtWiYk2jWXhJui_M3s9rqcu1mmmvJ6c',
            'key_id': '3335741209',
        };

        const baddata = {
            'ad_network': '5450213213286189855',
            'ad_unit': '1809337431',
            'custom_data': '66927EF6-A062-43DA-A84D-79A88D4815A2',
            'reward_amount': '1',
            'reward_item': 'Reward',
            'timestamp': '1588357637324',
            'transaction_id': '0330d5abe974cafe2f9590bd4a4d8362',
            'user_id': '00000000-b5aa-4507-913a-a387419b6ef8',
            'signature': 'MEUCIQCVs5p24DkICIOnzIyVMHq9oD0XTKXklonVPiFlyBjf6wIgOmTZUGo5o1EdCLlLIUiOyX2bJ7K21x5w3AS2oNHd8Pw',
            'key_id': '3335741209',
        };

        const baduser = {
            'ad_network': '5450213213286189855',
            'ad_unit': '1809337431',
            'custom_data': 'B4CC4ED1-EFED-4876-B225-8184AF8814B5',
            'reward_amount': '1',
            'reward_item': 'Reward',
            'timestamp': '1588561469175',
            'transaction_id': '1e925d7c3c261c7f2b7bcb236abc92a9',
            'user_id': '00000000-0000-0000-0000-a387419b6ef8',
            'signature': 'MEQCIBDA5uFZi-iS30r6rfz6g1QQyWRlJAgrsaGlf9H5_oKkAiAbgj90Kpb0QPVT-DXzIiAAEqbUIXbyLd0HVvTHx35-rA',
            'key_id': '3335741209',
        };

        test(`Must to generate one valid`, async () => {
            const result = await adsService.authorizeAdMobView(data);
            expect(result).toEqual({success: true});

            const reg = await models.admobviews.findByPk(data['custom_data']);
            expect(reg);
        });

        test(`Must to get error with invalid signature`, async () => {
            try {
                await adsService.authorizeAdMobView(baddata);
            } catch (e) {
                expect(e).toBeInstanceOf(SequelizeValidationError);
                expect(e.message).toBe('Validation error: Not valid signature');
                expect(e.errors).toHaveLength(1);
                expect(e.errors[0].path).toBe('validSSVSignature');
                return;
            }

            throw new Error('Pass');
        });

        test(`Must to get error with invalid keyId`, async () => {
            const adata = {...baddata};
            adata['key_id'] = 1234567;
            try {
                await adsService.authorizeAdMobView(adata);
            } catch (e) {
                expect(e).toBeInstanceOf(SequelizeValidationError);
                expect(e.message).toBe('Validation error: Not valid signature');
                expect(e.errors).toHaveLength(1);
                expect(e.errors[0].path).toBe('validSSVSignature');
                return;
            }

            throw new Error('Pass');
        });

        test(`Must to get error with invalid user`, async () => {
            try {
                await adsService.authorizeAdMobView(baduser);
            } catch (e) {
                expect(e).toBeInstanceOf(ValidationError);
                expect(e.message).toBe('Invalid user');
                expect(e.errors).toHaveLength(1);
                expect(e.errors[0]).toEqual({
                    field: 'user_id',
                    type: 'Not exists',
                    message: 'User does not exists',
                });
                return;
            }

            throw new Error('Pass');
        });
    });

    describe('#authorizeAppLixirView', () => {

        const data = {
            account_id: '123',
            game_id: '345',
            custom2: '54716A59-7F79-4F6D-9C2A-0CF161E75FC1',
            timestamp: '1588561213637',
            custom1: '00000000-b5aa-4507-913a-a387419b6ef8',
            checksum: '44218a0f55f2c3dad6e1b8752e5ff748',
        };

        const baddata = {
            account_id: '123',
            game_id: '345',
            custom2: '54716A59-7F79-4F6D-9C2A-0CF161E75FC1',
            timestamp: '1588561213637',
            custom1: '00000000-b5aa-4507-913a-a387419b6ef8',
            checksum: 'bc020c4ba5b131589c77a9bc9bd2f400',
        };

        const baduser = {
            account_id: '123',
            game_id: '345',
            custom2: '54716A59-7F79-4F6D-9C2A-0CF161E75FC1',
            timestamp: '1588561213637',
            custom1: '00000000-b5aa-4507-913a-a387419b6e00',
            checksum: '18bbcfa883333733af414309e3996dd0',
        };

        test(`Must to generate one valid`, async () => {
            const result = await adsService.authorizeAppLixirView(data);
            expect(result).toEqual({success: true});

            const reg = await models.applixirviews.findByPk(data['custom2']);
            expect(reg);
            expect(reg.status).toBe(models.applixirviews.STATUS_NEW);
        });

        test(`Must to get error with invalid signature`, async () => {
            try {
                await adsService.authorizeAppLixirView(baddata);
            } catch (e) {
                expect(e).toBeInstanceOf(SequelizeValidationError);
                expect(e.message).toBe('Validation error: Invalid checksum');
                expect(e.errors).toHaveLength(1);
                expect(e.errors[0].path).toBe('validChecksum');
                return;
            }

            throw new Error('Pass');
        });

        test(`Must to get error with invalid user`, async () => {
            try {
                await adsService.authorizeAppLixirView(baduser);
            } catch (e) {
                expect(e).toBeInstanceOf(ValidationError);
                expect(e.message).toBe('Invalid user');
                expect(e.errors).toHaveLength(1);
                expect(e.errors[0]).toEqual({
                    field: 'user_id',
                    type: 'Not exists',
                    message: 'User does not exists',
                });
                return;
            }

            throw new Error('Pass');
        });
    });

    describe('#watchedAdvertisement', () => {

        beforeEach(async () => {
            fakeResponse(
                'com.flixxo.microservice.payments',
                'users:incrementUserBalance',
                (uuid, adu, rew, title) => rew,
            );
        });
        
        afterEach(() => clearRpc());

        describe('AdMob', () => {
            beforeEach(async () => {
                await loadFixture('admobviews-for-watch-advertisement');
            });

            test('Must to save valid view', async () => {
                const res = await adsService.watchedAdvertisement(
                    'user1@fakemail.com',
                    '54716A59-7F79-4F6D-9C2A-0CF161E75FC1',
                    'admob'
                );

                expect(res).toEqual({
                    success: true,
                    newBalance: '1.020000000000000000',
                    rewards: '1.020000000000000000',
                });

                const reg = await models.admobviews.findByPk(
                    '54716A59-7F79-4F6D-9C2A-0CF161E75FC1',
                );

                expect(reg.status).toBe(models.admobviews.STATUS_PAYED);

            });

            test('Must to fail for already payed', async () => {
                return expect(adsService.watchedAdvertisement(
                    'user1@fakemail.com',
                    'AAB49D3D-38D5-4336-AC71-80BE4057C024',
                    'admob'
                ))
                    .rejects.toBeInstanceOf(ValidationError);
            });

            test('Must to fail for not existing view', async () => {
                return expect(adsService.watchedAdvertisement(
                    'user1@fakemail.com',
                    'AAB49D3D-38D5-4336-AC71-000000000000',
                    'admob'
                ))
                    .rejects.toBeInstanceOf(ValidationError);
            });
        });

        describe('AppLixir', () => {
            beforeEach(async () => {
                await loadFixture('applixir-for-watch-advertisement');
                config.reset('ads:applixir:secret', '00000000');
            });

            test('Must to save valid view', async () => {
                const res = await adsService.watchedAdvertisement(
                    'user1@fakemail.com',
                    '54716A59-7F79-4F6D-9C2A-0CF161E75FC1',
                    'applixir'
                );

                expect(res).toEqual({
                    success: true,
                    newBalance: '1.020000000000000000',
                    rewards: '1.020000000000000000',
                });

                const reg = await models.applixirviews.findByPk(
                    '54716A59-7F79-4F6D-9C2A-0CF161E75FC1',
                );

                expect(reg.status).toBe(models.applixirviews.STATUS_PAYED);

            });

            test('Must to fail for already payed', async () => {
                return expect(adsService.watchedAdvertisement(
                    'user1@fakemail.com',
                    'AAB49D3D-38D5-4336-AC71-80BE4057C024',
                    'applixir'
                ))
                    .rejects.toBeInstanceOf(ValidationError);
            });

            test('Must to fail for not existing view', async () => {
                return expect(adsService.watchedAdvertisement(
                    'user1@fakemail.com',
                    'AAB49D3D-38D5-4336-AC71-000000000000',
                    'applixir'
                ))
                    .rejects.toBeInstanceOf(ValidationError);
            });
        });
    });

    describe('#resetMetter', () => {
        test('Must to reset user', async () => {
            const user = await models.users.scope('withAdMetter').findOne({
                where: {uuid: '00000000-4453-4b7a-9687-05ea2d1759cd'},
                rejectOnEmpty: true,
            });

            const metter = await user.resolveAdMetter();

            expect(metter.metter.toFixed(18)).toBe('13.000000000000000000');
            expect(metter.cicle.toFixed(18)).toBe('0.014277286179788351');
            expect(metter.lastCicle.toFixed(18)).toBe('0.000000000000000000');

            await adsService.resetMetter(
                '00000000-4453-4b7a-9687-05ea2d1759cd',
                '1.475232490823423903',
            );

            await metter.reload();

            expect(metter.metter.toFixed(18)).toBe('0.000000000000000000');
            expect(metter.cicle.toFixed(18)).toBe('0.000000000000000000')
            expect(metter.lastCicle.toFixed(18)).toBe('-1.461235151431474500');

        });
    });
});
