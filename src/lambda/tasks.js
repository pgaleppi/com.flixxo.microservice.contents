import * as tasks from '../tasks';
import createLogger from '../logger';
import {nsWrapper} from '../logger/context';

const logger = createLogger(__filename);

/**
 * Lambda Tasks Handler
 * for HTTP triggering (API Gateway)
 * @param {object} event
 * @param {object} context
 * @param {function} callback
 * @return {object} apigw_result
 */
export async function apigw(event, context, callback) {
    return nsWrapper(async (callContext) => {
        let response;
        const {awsRequestId} = context;

        context.callbackWaitsForEmptyEventLoop = false;
        callContext.addPrefix('RequestId', awsRequestId);

        try {
            const body = JSON.parse(event.body);
            if (!body.task) {
                return {
                    statusCode: 400,
                    body: JSON.stringify({
                        message: 'Bad Request',
                    }),
                };
            } else if (!tasks[body.task]) {
                return {
                    statusCode: 404,
                    body: JSON.stringify({
                        message: `Task ${body.task} not found.`,
                    }),
                };
            }

            const params = body.params || [];
            response = await tasks[body.task](...params);
            return {
                statusCode: 200,
                body: JSON.stringify(response),
            };
        } catch (err) {
            logger.error(
                `Error when calling task. Body: ${event.body}.`, err
            );

            return {
                statusCode: 500,
                body: JSON.stringify({
                    message: 'Internal server error',
                    error: err,
                }),
            };
        }
    });
}

/**
 * Lambda Tasks Handler
 * for scheduled events
 * @param {object} event
 * @param {object} context
 * @param {function} callback
 * @return {object} apigw_result
 */
export async function scheduled(event, context, callback) {
    return nsWrapper(async (callContext) => {
        context.callbackWaitsForEmptyEventLoop = false;
        const {awsRequestId} = context;

        callContext.addPrefix('RequestId', awsRequestId);

        try {
            if (!event.task) {
                return {error: 'Missing task.task'};
            } else if (!tasks[event.task]) {
                return {error: 'Task not found'};
            }

            const params = event.params || [];
            return await tasks[event.task](...params);
        } catch (err) {
            const strEvent = JSON.stringify(event);
            logger.error(
                `Error when calling task. Event: ${strEvent}.`, err
            );

            return {error: err};
        }
    });
}

