
import {syncDb, loadFixture} from '../helpers/db';
import {
    ValidationError,
} from 'sequelize';
import models from '../../src/models';
import config from '../../src/config';

const MIN_AGE = config.get('profiles:minAge');

describe('ageRestriction', () => {

    test('Must to accept null values', async () => {
        const prof = models.profile.build({
            userId: 1,
        });

        await prof.validate();
    });

    test('Must to accept date of 13 years', async () => {
        const now = (new Date());
        const year = now.getUTCFullYear();
        const valid = new Date(
            year - MIN_AGE,
            now.getUTCMonth(),
            now.getUTCDate()
        );

        const prof = models.profile.build({
            userId: 1,
            // minus one day
            birthDate: new Date(valid.getTime() - 24 * 3600000),
        });

        try {
            await prof.validate();
        } catch (e) {
            throw e;
        }
    });

    test('Must to accept date of 13 years on ts format', async () => {
        const now = (new Date());
        const year = now.getUTCFullYear();
        const valid = new Date(
            year - MIN_AGE,
            now.getUTCMonth(),
            now.getUTCDate()
        );

        const prof = models.profile.build({
            userId: 1,
            // minus one day
            birthDate: valid.getTime() - 24 * 3600000,
        });

        try {
            await prof.validate();
        } catch (e) {
            throw e;
        }
    });


    test('Must to reject date of exact 13 years (no day)', async () => {
        const now = (new Date());
        const year = now.getUTCFullYear();
        const valid = new Date(
            year - MIN_AGE,
            now.getUTCMonth(),
            now.getUTCDate()
        );

        const prof = models.profile.build({
            userId: 1,
            birthDate: valid,
        });

        try {
            await prof.validate();
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.errors).toHaveLength(1);
            expect(e.errors[0].path).toBe('birthDate');
            expect(e.errors[0].validatorKey).toBe('ageRestriction');
            return;
        }

        throw new Error('pass');
    });

    test('Must to reject date of less 13 years', async () => {
        const now = (new Date());
        const year = now.getUTCFullYear();
        const valid = new Date(
            year - MIN_AGE + 1,
            now.getUTCMonth(),
            now.getUTCDate()
        );

        const prof = models.profile.build({
            userId: 1,
            birthDate: valid,
        });

        try {
            await prof.validate();
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.errors).toHaveLength(1);
            expect(e.errors[0].path).toBe('birthDate');
            expect(e.errors[0].validatorKey).toBe('ageRestriction');
            return;
        }

        throw new Error('pass');
    });

    test('Must to reject date of less 13 years by day', async () => {
        const now = (new Date());
        const year = now.getUTCFullYear();
        const valid = new Date(
            year - MIN_AGE,
            now.getUTCMonth(),
            now.getUTCDate()
        );
        // 1 day before birthdate
        const invalid = new Date(valid.getTime() + (24 * 3600 * 1000));

        const prof = models.profile.build({
            userId: 1,
            birthDate: invalid,
        });

        try {
            await prof.validate();
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.errors).toHaveLength(1);
            expect(e.errors[0].path).toBe('birthDate');
            expect(e.errors[0].validatorKey).toBe('ageRestriction');
            return;
        }

        throw new Error('pass');
    });

});
