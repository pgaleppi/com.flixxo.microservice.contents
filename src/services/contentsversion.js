import models from '../models';
import {EmptyResultError} from 'sequelize';
import {
    deleteMedia as deleteContentMedias,
    createMedias,
    deleteSubtitles as deleteContentSubtitles,
    createSubtitles,
    getTempContentMedia, getTempContentSubtitles,
} from './contents';
import {createDebug} from '../logger';
import {ValidationError} from '../errors';
import {getUser} from './contents';
import {getConnection} from '../storage';

const debug = createDebug(__filename);

/**
 * List version from a content
 * @param {string} lang
 * @param {string} contentUUID
 * @return {Promise<object[]>}
 */
export const listByContent = ['lang', async (lang, contentUUID) => {
    debug(`List versions from content ${contentUUID}`);

    await models.contents.findByPk(contentUUID, {
        attributes: ['uuid'],
        rejectOnEmpty: true,
    });

    debug(`\tReturning versions`);

    const versions = await models.contentversion
        .scope(
            {method: ['i18n', lang]},
            'listing',
        )
        .findAll({
            where: {
                contentUUID,
            },
        });

    return versions.map((x) => x.get({plain: true}));
}];

/**
 * List version from a content of user
 * @param {string} lang
 * @param {string} email
 * @param {string} contentUUID
 * @return {Promise<object[]>}
 */
export const listByUserContent = ['lang', async (lang, email, contentUUID) => {
    debug(`List versions from content ${contentUUID} of user ${email}`);

    debug(`\tFind user ${email}`);
    const user = await models.users.findByEmail(email);

    debug(`\tFind conent`);
    await models.contents.scope(
        {method: ['asAuthor', user.id]}
    ).findByPk(contentUUID, {
        attributes: ['uuid'],
        rejectOnEmpty: true,
    });

    let versions = await models.contentversion
        .scope(
            {method: ['i18n', lang]},
            'listing',
        )
        .findAll({
            where: {
                contentUUID,
            },
        });

    versions = versions.slice(0, 10);

    return versions.map((x) => x.get({plain: true}));
}];

/**
 * List versions for moderator
 * @param {string} lang
 * @param {string} email
 * @param {string} contentUUID
 * @return {Promise<object[]>}
 */
export const listForModerator = ['lang', async (lang, email, contentUUID) => {
    debug(`List version for content ${contentUUID} to moderator ${email}`);

    const user = await getUser(email);

    debug(`\tFind conent`);
    await models.contents.scope(
        {method: ['moderatedBy', user.id]}
    )
        .findByPk(contentUUID, {
            attributes: ['uuid'],
            rejectOnEmpty: true,
        });

    debug(`\tReturning versions`);

    const versions = await models.contentversion
        .scope(
            {method: ['i18n', lang]},
            'listing',
        )
        .findAll({
            where: {
                contentUUID,
            },
        });


    return versions.map((x) => x.get({plain: true}));
}];

/**
 * Get version for moderator
 * @param {string} lang
 * @param {string} email
 * @param {string} contentUUID
 * @param {string} uuid
 * @return {Promise<object[]>}
 */
export const getForModerator = [
    'lang',
    async (lang, email, contentUUID, uuid) => {
        debug(
            `Get version ${uuid} content ${contentUUID} to moderator ${email}`
        );

        const user = await getUser(email);

        const version = await models.contentversion.scope([
            'detailled',
            {method: ['i18n', lang]},
            {method: ['byModerator', user.id]},
        ])
            .findByPk(uuid, {
                rejectOnEmpty: false,
            });

        if (!version) {
            throw new EmptyResultError();
        }

        const clear = version.get({plain: true});
        delete clear.Content;

        return clear;
    },
];

/**
 * Set status of user content
 * @param {string} email
 * @param {string} contentUUID
 * @param {integer} status
 * @return {Promise<boolean>}
 */
export async function setStatusOfUserContent(email, contentUUID, status) {
    debug(`Setting status of revision of ${contentUUID} of user ${email}`);

    const {
        STATUS_CANCELED,
    } = models.contentversion;

    if ([STATUS_CANCELED].indexOf(status) === -1) {
        throw new ValidationError('Invalid status',
            [{
                field: 'status',
                type: 'invalid',
                message: 'Invalid status',
            }]
        );
    }

    const user = await getUser(email);

    // Validate if content is enabled
    const content = await models.contents.scope(
        {method: ['availableUpdates', user.id]},
    )
        .findByPk(contentUUID, {
            rejectOnEmpty: true,
        });

    // Find active content version
    debug(`\tFind previous content version for uuid ${content.uuid}`);
    const actualVersion = await models.contentversion.scope(
        {method: ['actualByContent', content.uuid]},
    )
        .findOne({
            rejectOnEmpty: true,
        });

    await actualVersion.setStatus(status);
    return true;
}

/**
 * Apply version to content
 * @param {ContentVersionModel} version
 * @param {string} [message]
 * @return {Promise}
 */
async function applyVersion(version, message = '') {
    debug(`Apply version ${version.uuid} to content ${version.contentUUID}`);

    const content = await models.contents.findByPk(version.contentUUID, {
        rejectOnEmpty: true,
    });

    return content.applyVersion(version.uuid, message);
}

/**
 * Set status as moderator
 * @param {string} email - Email of moderator
 * @param {string} contentUUID - Content uuid to moderate active version
 * @param {integer} status - New status
 * @param {string} [message] New Message
 * @return {bool}
 */
export async function moderateStatus(email, contentUUID, status, message) {
    debug(`Setting status of active versioin for content ${contentUUID}`);

    const {
        STATUS_ACCEPTED,
        STATUS_REJECTED,
        STATUS_REQUEST_CHANGES,
    } = models.contentversion;

    const availableStatus = [
        STATUS_ACCEPTED,
        STATUS_REJECTED,
        STATUS_REQUEST_CHANGES,
    ];

    if (availableStatus.indexOf(status) === -1) {
        debug(`\tSend status is not enabled for moderation acction`, status);
        throw new ValidationError(`Invalid status`, [{
            type: 'Invalid',
            field: 'status',
            message: `Invalid status`,
        }]);
    }

    debug(`\tGet moderator`);
    const user = await getUser(email);

    debug(`\tFind active revision by contentUUID for moderator`);
    const version = await models.contentversion.scope([
        {method: ['activeForModeration', user.id]},
    ])
        .findOne({
            where: {contentUUID},
            rejectOnEmpty: true,
        });

    switch (status) {
        case STATUS_ACCEPTED:
            await applyVersion(version, message);
            break;
        default:
            await version.setStatus(status, message);
    }

    return true;
}

/**
 * Get or create version
 * @param {string} email
 * @param {string} uuid
 * @param {boolean} create
 * @return {Promise}
 */
export async function getVersionForUpdate(email, uuid, create = false) {
    debug(`Get or create version from ${uuid} by author ${email}`);
    const user = await getUser(email);

    // Validate if content is enabled
    const content = await models.contents.scope(
        {method: ['availableUpdates', user.id]},
    ).findByPk(uuid);
    if (!content) {
        throw new ValidationError('Content not available for updates',
            [{
                field: 'contentUUID',
                type: 'Content error',
                message: 'Content not available for updates',
            }]
        );
    }

    // Find active content version
    debug(`Find previous content version for uuid ${content.uuid}`);
    const actualVersion = await models.contentversion.scope(
        {method: ['actualByContent', content.uuid]},
    ).findOne();

    // If there is a previous version, update its status
    if (actualVersion) {
        if (actualVersion.status === models.contentversion.STATUS_REVIEW) {
            throw new ValidationError('Version not available for updates',
                [{
                    field: 'contentUUID',
                    type: 'Version error',
                    message: 'Version not available for updates',
                }]
            );
        } else {
            debug(`Returns actual version`);
            return actualVersion;
        }
    }
    if (!create) {
        throw new ValidationError('Version non-exists',
            [{
                field: 'contentUUID',
                type: 'Version error',
                message: 'Version non-exists',
            }]
        );
    } else {
        // Create new content version
        // return await createVersionFromContent(content.get({plain: true}));
        await createVersionFromContent(content.get({plain: true}));
        return await models.contentversion.scope(
            {method: ['actualByContent', content.uuid]},
        ).findOne();
    }
}

/**
 * Create content version
 * @param {object} content
 * @return {Promise}
 */
async function createVersionFromContent(content) {
    debug(`Creating new version for content ${content.uuid}`);
    // Set tags as string array on virtual field
    const plainTags = content.Tags.map((tagModel) => tagModel.tag);
    content.tags = plainTags;

    content.contentUUID = content.uuid;
    delete content.createdAt;
    delete content.updatedAt;

    const newVersion = {
        ...content,
        status: models.contentversion.STATUS_DRAFT,
    };
    delete newVersion.uuid;

    const sequelize = getConnection();
    return await sequelize.transaction( async (dbTxn) => {
        const createdVersion = await models.contentversion.create(
            newVersion,
            {
                transaction: dbTxn,
            }
        );

        if (content.media && content.media.length) {
            const newMedias = content.media.map((media) => {
                return {
                    isDraft: true,
                    uuid: createdVersion.uuid,
                    name: media.name,
                    type: media.type,
                    url: media.url,
                    order: media.order,
                };
            });
            await models.media.bulkCreate(newMedias, {
                validate: true,
                transaction: dbTxn,
            });
        }

        if (content.subtitle && content.subtitle.length) {
            const newSubtitles = content.subtitle.map((subtitle) => {
                return {
                    isDraft: true,
                    lang: subtitle.lang,
                    label: subtitle.label,
                    url: subtitle.url,
                    uuid: createdVersion.uuid,
                    closeCaption: subtitle.closeCaption,
                };
            });
            await models.subtitle.bulkCreate(newSubtitles, {
                validate: true,
                transaction: dbTxn,
            });
        }

        const contentMetadata = await models.metadata.findAll({
            where: {
                entityId: content.uuid,
            },
        });

        if (contentMetadata && contentMetadata.length) {
            const newMetadata = contentMetadata.map((meta) => {
                return {
                    entityId: createdVersion.uuid,
                    entityType: models.metadata.ENTITY_TYPE_CONTENTVERSION,
                    lang: meta.lang,
                    title: meta.title,
                    body: meta.body,
                };
            });
            await models.metadata.bulkCreate(newMetadata, {
                validate: true,
                transaction: dbTxn,
            });
        }

        const actualVersion = await models.contentversion.scope(
            {method: ['actualByContent', content.uuid]},
        ).findOne();
        return actualVersion;
    });
}

/**
 * Update active version
 * @param {string} email
 * @param {string} uuid
 * @param {object} version
 * @return {Promise}
 */
export async function updateActiveVersion(email, uuid, version) {
    const user = await getUser(email);
    debug(`Updating active version ${uuid} by author ${email}`);
    // Validate version status to set
    if (version.status !== models.contentversion.STATUS_REVIEW &&
        version.status !== models.contentversion.STATUS_DRAFT) {
        debug(`Invalid version status ${version.status}`);
        throw new ValidationError('Invalid version status',
            [{
                field: 'status',
                type: 'Version error',
                message: 'Invalid version status',
            }]
        );
    }
    // Parse media ids
    let newMediaIds = version.newMediaIds || [];
    newMediaIds = newMediaIds
        .filter((x) => x !== null)
        .map((i) => parseInt(i, 10));
    let deleteMediaIds = version.deleteMediaIds || [];
    deleteMediaIds = deleteMediaIds.
        filter((x) => x !== null)
        .map((i) => parseInt(i, 10));

    // Parse subtitle ids
    let newSubtitleIds = version.newSubtitleIds || [];
    newSubtitleIds = newSubtitleIds
        .filter((x) => x !== null)
        .map((i) => parseInt(i, 10));
    let deleteSubtitleIds = version.deleteSubtitleIds || [];
    deleteSubtitleIds = deleteSubtitleIds.
        filter((x) => x !== null)
        .map((i) => parseInt(i, 10));

    // Get active version
    const activeVersion = await getVersionForUpdate(email, uuid);
    const baseContent = await models.contents.scope(
        {method: ['availableUpdates', user.id]},
    ).findByPk(uuid);

    debug(`\tFetching new medias`);
    const newMedia = await getTempContentMedia(
        {uuid: activeVersion.uuid},
        newMediaIds,
    );

    debug(`\tFetching new subtitles`);
    const newSubtitles = await getTempContentSubtitles(
        {uuid: activeVersion.uuid},
        newSubtitleIds,
    );

    let resp;
    if (activeVersion.status === models.contentversion.STATUS_REQUEST_CHANGES) {
        resp = await updateActiveVersionHistoric(
            activeVersion, version,
            newMedia, deleteMediaIds,
            newSubtitles, deleteSubtitleIds);
    } else {
        resp = await updateActiveVersionCurrent(
            activeVersion, version,
            newMedia, deleteMediaIds,
            newSubtitles, deleteSubtitleIds);
    }

    if (baseContent.contentType === models.contents.TYPE_SERIE) {
        debug('Content belongs to SERIE, accepting automatically...');
        await applyVersion(
            resp,
            'Content Type: SERIE. Automatically approved'
        );
    } else {
        // If nothing changed but price
        if ((newSubtitleIds.length === 0) &&
            (deleteSubtitleIds.length === 0) &&
            (newMediaIds.length === 0) &&
            (deleteMediaIds.length === 0) &&
            (await resp.onlyPriceHasChanged(baseContent.get({plain: true})))
        ) {
            debug('Only the price has changed, applying version...');
            await applyVersion(
                resp,
                'Content automatically approved'
            );
        }
    }

    return resp;
}

/**
 * Create new historic version and update from old one
 * @param {object} activeVersion
 * @param {object} version
 * @param {array} newMedia
 * @param {array} deleteMediaIds
 * @param {array} newSubtitles
 * @param {array} deleteSubtitleIds
 * @return {Promise}
 *
 **/
async function updateActiveVersionHistoric(
    activeVersion, version,
    newMedia, deleteMediaIds,
    newSubtitles, deleteSubtitleIds
) {
    // Make historic version
    debug(`\tMake historic version`);
    const plainTags = activeVersion.Tags.map((tagModel) => tagModel.tag);
    activeVersion.tags = plainTags;

    debug(`\tMerge new version with user request version and update`);
    const sequelize = getConnection();
    return await sequelize.transaction(async (dbTxn) => {
        const newVersion = await activeVersion.clone(true, dbTxn);
        let deleteMedias = [];
        let deleteSubtitles = [];

        if (newMedia.length) {
            debug(`\tGet actual (cloned) medias`);
            const medias = await newVersion.getMedia({
                attributes: ['id', 'type', 'order', 'url', 'name', 'uuid'],
                raw: true,
                transaction: dbTxn,
            });

            debug(`\tGet previous medias`);
            const previousMedias = await activeVersion.getMedia({
                attributes: ['id', 'type', 'order', 'url', 'name', 'uuid'],
                raw: true,
            });

            // Update medias id from the new version
            deleteMediaIds = deleteMediaIds.map((i) => {
                const histMedia = previousMedias.find(
                    (media) => media.id === i
                );
                return medias.find(
                    (media) => media.url === histMedia.url
                ).id;
            });
            newMedia.map((m) => {
                m.uuid = newVersion.uuid;
                return m;
            });
            // Media types validation
            validateMedias(deleteMediaIds, newMedia, medias);

            debug(`\tMapping deleteMedias with actual medias`);
            deleteMedias = deleteMediaIds.map((id) => {
                const media = medias.find((x) => x.id === id);
                if (!media) {
                    debug(deleteMediaIds);
                    debug(`${id} does not exists in`, medias);
                    throw new ValidationError(
                        'Non-existent media to delete',
                        [{
                            field: 'images',
                            type: 'Delete images failed',
                            message: 'Non-existent media to delete',
                        }]
                    );
                }
                return media;
            });

        }

        if (newSubtitles.length) {
            debug(`\tGet actual (cloned) subtitles`);
            const subtitles = await newVersion.getSubtitle({
                attributes: [
                    'id', 'lang', 'label',
                    'url', 'uuid', 'closeCaption',
                ],
                raw: true,
                transaction: dbTxn,
            });

            debug(`\tGet previous subtitles`);
            const previousSubtitles = await activeVersion.getSubtitle({
                attributes: [
                    'id', 'lang', 'label',
                    'url', 'uuid', 'closeCaption',
                ],
                raw: true,
            });

            // Update subtitle id from the new version
            deleteSubtitleIds = deleteSubtitleIds.map((i) => {
                const histSubtitle = previousSubtitles.find(
                    (subtitle) => subtitle.id === i
                );
                return subtitles.find(
                    (subtitle) => subtitle.url === histSubtitle.url
                ).id;
            });
            newSubtitles.map((s) => {
                s.uuid = newVersion.uuid;
                return s;
            });

            debug(`\tMapping deleteSubtitles with actual subtitles`);
            deleteSubtitles = deleteSubtitleIds.map((id) => {
                const subtitle = subtitles.find((x) => x.id === id);
                if (!subtitle) {
                    debug(deleteSubtitleIds);
                    debug(`${id} does not exists in`, subtitles);
                    throw new ValidationError(
                        'Non-existent subtitle to delete',
                        [{
                            field: 'subtitles',
                            type: 'Delete subtitles failed',
                            message: 'Non-existent subtitle to delete',
                        }]
                    );
                }
                return subtitle;
            });
        }

        debug(`Updating historic version ${activeVersion.uuid}`);
        await activeVersion.update(
            {
                status:
                    models.contentversion.STATUS_REQUEST_CHANGES_HISTORIC,
            },
            {
                transaction: dbTxn,
            }
        );

        debug(`Persist new version ${newVersion.uuid}`);
        const resp = await newVersion.update(
            {
                ...version,
            },
            {
                transaction: dbTxn,
            }
        );

        // Persist media(version.newMediaIds) to newVersion.medias DB
        if (newMedia.length) {
            debug(`\tCreating new medias`);
            await createMedias(newMedia, newVersion.uuid, dbTxn, true);
            debug(`\t\tNew media created: ${newMedia.length}`);

            // Delete temp media from DB
            const tmpMediaIds = newMedia.map((tmpMedia) => tmpMedia.id);
            await models.tempmedia.scope(
                {method: ['queryList', tmpMediaIds]},
            ).destroy({transaction: dbTxn}).then((deletedCount) => {
                debug(`${deletedCount} temp media destroyed`);
            });
        }

        // Delete medias(version.deletedMediaIds) only from DB
        if (deleteMedias.length) {
            debug(`\tDeleting media`);
            await deleteContentMedias(deleteMedias, dbTxn, false);
            debug(`\t\tDeleted media: ${deleteMedias.length}`);
        }

        // Persist subtitle(version.newSubtitleIds) to newVersion.subtitles DB
        if (newSubtitles.length) {
            debug(`\tCreating new subtitles`);
            await createSubtitles(newSubtitles, newVersion.uuid, dbTxn, true);
            debug(`\t\tNew subtitle created: ${newSubtitles.length}`);

            // Delete temp subtitle from DB
            const tmpSubtitleIds = newSubtitles.map(
                (tmpSubtitle) => tmpSubtitle.id
            );
            await models.tempsubtitle.scope(
                {method: ['queryList', tmpSubtitleIds]},
            ).destroy({transaction: dbTxn}).then((deletedCount) => {
                debug(`${deletedCount} temp subtitles destroyed`);
            });
        }

        // Delete subtitles(version.deletedSubtitleIds) only from DB
        if (deleteSubtitles.length) {
            debug(`\tDeleting subtitles`);
            await deleteContentSubtitles(deleteSubtitles, dbTxn, false);
            debug(`\t\tDeleted subtitles: ${deleteSubtitles.length}`);
        }

        await models.metadata.destroy(
            {
                where: {'entityId': newVersion.uuid}, transaction: dbTxn}
        ).then((deletedCount) => {
            debug(`${deletedCount} metadata destroyed`);
        });

        if (version.metadata && version.metadata.length) {
            const newMetadata = version.metadata.map((meta) => {
                return {
                    entityId: newVersion.uuid,
                    entityType: models.metadata.ENTITY_TYPE_CONTENTVERSION,
                    lang: meta.lang,
                    title: meta.title,
                    body: meta.body,
                };
            });

            await models.metadata.bulkCreate(newMetadata, {
                validate: true,
                transaction: dbTxn,
            });
        }

        return resp;
    });

}

/**
 * Update current active version
 * @param {object} activeVersion
 * @param {object} version
 * @param {array} newMedia
 * @param {array} deleteMediaIds
 * @param {array} newSubtitles
 * @param {array} deleteSubtitleIds
 * @return {Promise}
 *
 **/
async function updateActiveVersionCurrent(
    activeVersion, version,
    newMedia, deleteMediaIds,
    newSubtitles, deleteSubtitleIds
) {
    debug(`\tUpdate active version with user request version`);
    let deleteMedias = [];
    let deleteSubtitles = [];

    if (newMedia.length) {
        debug(`\tGet actual medias`);
        const medias = await activeVersion.getMedia({
            attributes: ['id', 'type', 'order', 'url'],
            raw: true,
        });
        // Media types validation
        validateMedias(deleteMediaIds, newMedia, medias);

        debug(`\tMapping deleteMedias with actual medias`);
        deleteMedias = deleteMediaIds.map((id) => {
            const media = medias.find((x) => x.id === id);
            if (!media) {
                debug(deleteMediaIds);
                debug(`${id} does not exists in`, medias);
                throw new ValidationError('Non-existent media to delete',
                    [{
                        field: 'images',
                        type: 'Delete images failed',
                        message: 'Non-existent media to delete',
                    }]
                );
            }
            return media;
        });
    }

    debug(`\tGet actual subtitles`);
    const subtitles = await activeVersion.getSubtitle({
        attributes: [
            'id', 'lang', 'label',
            'url', 'uuid', 'closeCaption',
        ],
        raw: true,
    });

    debug(`\tMapping deleteSubtitles with actual subtitles`);
    deleteSubtitles = deleteSubtitleIds.map((id) => {
        const subtitle = subtitles.find((x) => x.id === id);
        if (!subtitle) {
            debug(deleteSubtitleIds);
            debug(`${id} does not exists in`, subtitles);
            throw new ValidationError('Non-existent subtitle to delete',
                [{
                    field: 'subtitles',
                    type: 'Delete subtitles failed',
                    message: 'Non-existent subtitles to delete',
                }]
            );
        }
        return subtitle;
    });

    const sequelize = getConnection();
    return await sequelize.transaction( async (dbTxn) => {
        debug(`Updating version ${activeVersion.uuid}`);
        const resp = await activeVersion.update(
            {
                ...version,
            },
            {
                transaction: dbTxn,
            }
        );
        // Persist media(version.newMediaIds) to activeVersion.medias DB
        if (newMedia.length) {
            debug(`\tCreating new medias`);
            await createMedias(newMedia, activeVersion.uuid, dbTxn, true);
            debug(`\t\tNew media created: ${newMedia.length}`);

            // Delete temp media from DB
            const tmpMediaIds = newMedia.map((tmpMedia) => tmpMedia.id);
            await models.tempmedia.scope(
                {method: ['queryList', tmpMediaIds]},
            ).destroy({transaction: dbTxn}).then((deletedCount) => {
                debug(`${deletedCount} temp media destroyed`);
            });
        }

        // Delete medias from activeVersion.medias DB and from S3
        if (deleteMedias.length) {
            debug(`\tDeleting media`);
            await deleteContentMedias(deleteMedias, dbTxn, true);
            debug(`\t\tDeleted media: ${deleteMedias.length}`);
        }

        // Persist subtitle(version.newSubtitleIds) to
        // activeVersion.subtitles DB
        if (newSubtitles.length) {
            debug(`\tCreating new subtitles`);
            await createSubtitles(
                newSubtitles,
                activeVersion.uuid,
                dbTxn, true);
            debug(`\t\tNew subtitles created: ${newSubtitles.length}`);

            // Delete temp subtitles from DB
            const tmpSubtitleIds = newSubtitles.map(
                (tmpSubtitle) => tmpSubtitle.id);

            await models.tempsubtitle.scope(
                {method: ['queryList', tmpSubtitleIds]},
            ).destroy({transaction: dbTxn}).then((deletedCount) => {
                debug(`${deletedCount} temp subtitles destroyed`);
            });
        }

        // Delete subtitles from activeVersion.subtitles DB and from S3
        if (deleteSubtitles.length) {
            debug(`\tDeleting subtitles`);
            await deleteContentSubtitles(deleteSubtitles, dbTxn, true);
            debug(`\t\tDeleted subtitles: ${deleteSubtitles.length}`);
        }

        await models.metadata.destroy(
            {
                where: {'entityId': activeVersion.uuid}, transaction: dbTxn}
        ).then((deletedCount) => {
            debug(`${deletedCount} metadata destroyed`);
        });

        if (version.metadata && version.metadata.length) {
            const newMetadata = version.metadata.map((meta) => {
                return {
                    entityId: activeVersion.uuid,
                    entityType: models.metadata.ENTITY_TYPE_CONTENTVERSION,
                    lang: meta.lang,
                    title: meta.title,
                    body: meta.body,
                };
            });

            await models.metadata.bulkCreate(newMetadata, {
                validate: true,
                transaction: dbTxn,
            });
        }

        return resp;
    });
}

/**
 * Return Validate version medias
 * @param {array<integer>} deleteMediaIds
 * @param {array<Object>} newMedia
 * @param {array<Object>} medias
 */
async function validateMedias(deleteMediaIds, newMedia, medias) {
    const finalMedias = [
        ...medias.filter((x) => deleteMediaIds.indexOf(x.id) === -1),
        ...newMedia,
    ];
    debug(`\tFinal medias: `, finalMedias);

    const counts = finalMedias.reduce((r, m) => {
        r[m.type]++;
        return r;
    }, {
        [models.media.TYPE_COVER]: 0,
        [models.media.TYPE_THUMB]: 0,
    });

    debug(`\tMedias by type`, counts);

    if (counts[models.media.TYPE_COVER] !== 1) {
        throw new ValidationError(`Must to be one cover`);
    }

    debug(counts[models.media.TYPE_THUMB], '>3 <1');
    if (counts[models.media.TYPE_THUMB] > 3 ||
        counts[models.media.TYPE_THUMB] < 1
    ) {
        throw new ValidationError(`Invalid number of thumbs`);
    }
}

