
import models from '../models';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

/**
 * Follow user
 * @param {string} followerEmail
 * @param {integer} followedId
 * @return {bool}
 */
export async function follow(followerEmail, followedId) {
    debug(`${followerEmail} - follows -> ${followedId}`);
    const follower = await models.users.findByEmail(followerEmail);
    await follower.addFollowed(followedId);

    return true;
}

/**
 * Unfollow user
 * @param {string} followerEmail
 * @param {integer} followedId
 * @return {bool}
 */
export async function unfollow(followerEmail, followedId) {
    debug(`${followerEmail} - unfollows -> ${followedId}`);
    const follower = await models.users.findByEmail(followerEmail);
    await follower.removeFollowed(followedId);

    return true;
}

/**
 * Get follower of an user
 * @param {string} email
 * @return {Promise<object[]>}
 */
export async function getFollowers(email) {
    debug(`Getting followers of ${email}`);
    const user = await models.users.findByEmail(email);
    const followers = await user.getFollowers({
        scope: 'minimalProfile',
    });

    return followers.map((x) => {
        const user = x.get({plain: true});
        delete user.UserFollowers;
        return user;
    });
}

/**
 * Get followeds of an user
 * @param {string} email
 * @return {Promise<object[]>}
 */
export async function getFolloweds(email) {
    debug(`Getting followers of ${email}`);
    const user = await models.users.findByEmail(email);
    const followers = await user.getFolloweds({
        scope: 'minimalProfile',
    });

    return followers.map((x) => {
        const user = x.get({plain: true});
        delete user.UserFollowers;
        return user;
    });
}
