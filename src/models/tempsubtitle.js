import Sequelize from 'sequelize';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

export default (sequelize, DataTypes) => {

    const instanceMethods = {};

    const TempSubtitle = sequelize.define('TempSubtitle', {

        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },

        lang: {
            type: DataTypes.STRING(5),
            allowNull: false,
            defaultValue: 'en',
            validate: {
                is: {
                    args: /^[a-z]{2}(?:_[A-Z]{2})?/,
                    msg: 'Invalid lang format',
                },
            },
        },

        label: {
            type: DataTypes.STRING(2014),
            allowNull: true,
            validate: {
                is: /^.{3,}$/,
            },
        },

        url: {
            type: DataTypes.STRING(1024),
            allowNull: false,
            validate: {
                isUrl: true,
            },
        },

        hash: {
            type: DataTypes.STRING(40),
            allowNull: true,
            validate: {
                is: {
                    args: /^[A-F0-9]{40}$/i,
                    msg: 'Invalid SHA1 format',
                },
            },
            set(hash) {
                if (hash) {
                    this.setDataValue('hash', hash.toLowerCase());
                }
            },
            index: true,
        },

        uuid: {
            type: DataTypes.UUID,
            allowNull: true,
        },

        userId: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,
            index: true,
        },

        closeCaption: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },

    }, {
        timestamps: true,
        paranoid: false,
        instanceMethods,
        validate: {
            mustBeHashOrUUID() {
                if (!this.hash && !this.uuid) {
                    throw new Error(
                        'Temporary subtitle must have hash or uuid'
                    );
                }
            },
        },
    });

    if (typeof TempSubtitle.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                TempSubtitle.prototype[m] = instanceMethods[m];
            }
        }
    }

    TempSubtitle.associate = function(models) {

        TempSubtitle.belongsTo(models.users, {
            foreignKey: 'userId',
            targetKey: 'id',
        });

        // Scopes

        TempSubtitle.addScope('byUser', (user) => {
            let userId;

            if (typeof user === 'object') {
                userId = user.id;
            } else {
                userId = parseInt(user, 10);
            }

            return {
                where: {userId},
            };
        });

    };

    TempSubtitle.addScope('expired', (expirationDate) => {
        return {
            where: {
                createdAt: {
                    [Sequelize.Op.lte]: expirationDate,
                },
            },
        };
    });

    TempSubtitle.addScope('queryList', (tempSubtitleIds) => {
        return {
            where: {
                id: {
                    [Sequelize.Op.in]: tempSubtitleIds,
                },
            },
        };
    });

    return TempSubtitle;
};

