
import path from 'path';
import sequelizeFixtures from 'sequelize-fixtures';
import models from '../models';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

/**
 * Syncdb task
 * @param {object} params
 */
export default async function({force = false, nodata = false}) {
    try {
        await models.sequelize.sync({force});
    } catch (e) {
        debug(`Error syncdb`, e);
        process.stderr.write(`${e.message}\n`);
    }

    process.stdout.write(`db synced\n`);

    if (!nodata) {
        process.stdout.write(`Loading data\n`);
        try {
            await sequelizeFixtures.loadFile(
                path.resolve(__dirname, '../../data/**/*.json'),
                models
            );
        } catch (e) {

            if (e.message.match(/^No files matching/i)) {
                process.stderr.write(`No data to load\n`);
            } else {
                process.stderr.write(`${e.message}\n`);
            }

        }

        process.stdout.write(`Data loaded\n`);
    }
}
