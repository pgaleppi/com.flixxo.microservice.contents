
import {Decimal} from 'decimal.js';
import {createDebug} from '../logger';

const debug = createDebug(__filename);
const ADS_METER_MIN_ERROR = 'Min validation failed';

export const TYPE_USER = 0x01;
export const TYPE_PROVIDER = 0x02;

/**
 * MettersModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {
    let UserModel;

    const decimalField = (name, options = {}) => ({
        type: DataTypes.DECIMAL(36, 18),
        get() {
            const val = this.getDataValue(name);
            if (typeof val === 'undefined') {
                return undefined;
            }
            return new Decimal(val);
        },
        set(val) {
            if (!Decimal.isDecimal(val)) {
                val = new Decimal(val);
            }

            this.setDataValue(name, val.toFixed(18));
        },
        ...options,
    });

    const instanceMethods = {

        async incrementMetter(rewarded, dbTx = null) {
            debug(
                `Increment metter and sum ${rewarded.toFixed(18)} to cicle ` +
                `of ${this.userUUID}`
            );
            await this.increment({
                metter: 1,
                cicle: rewarded.toFixed(18),
            }, {
                ...(dbTx ? {transaction: dbTx} : {}),
            });

            await this.reload({
                transaction: dbTx,
            });
        },

        async decrementMetter(rewarded, dbTx) {
            debug(
                `Decrement metter in ${rewarded.toFixed(18)} ` +
                `of ${this.userUUID}`
            );

            // Do it for atomic transaction!!!
            const {metter} = await MettersModel.findOne({
                attributes: [
                    'metter',
                ],
                where: {
                    userUUID: this.userUUID,
                    type: this.type,
                },
                rejectOnEmpty: true,
                transaction: dbTx,
                lock: dbTx.LOCK.UPDATE,
            });

            const newMetter = metter
                .minus(rewarded)
            ;

            await this.update({
                metter: newMetter.gt(0) ? newMetter : 0,
            }, {
                transaction: dbTx,
            });
        },

        async resetMetter(spended, incentive, dbTx) {
            debug(
                `Reset metter of ${this.userUUID}, spend ${spended.toFixed(18)}`
            );

            // Do it for atomic transaction!!!
            const {cicle} = await MettersModel.findOne({
                attributes: [
                    'cicle',
                ],
                where: {
                    userUUID: this.userUUID,
                    type: this.type,
                },
                rejectOnEmpty: true,
                transaction: dbTx,
                lock: dbTx.LOCK.UPDATE,
            });

            const lastCicle = cicle
                .div(incentive)
                .minus(spended)
            ;

            const updatedCicle = Decimal.max( lastCicle, 0 );

            /* const lastCicleFormula = sequelize.fn(
                'subtraction',
                sequelize.fn(
                    'division',
                    sequelize.col('cicle'),
                    incentive.toFixed(18),
                ),
                spended.toFixed(18)
            );*/

            await this.update({
                lastCicle,
                cicle: updatedCicle,
                metter: 0,
            }, {
                transaction: dbTx,
            });
        },

    };

    const MettersModel = sequelize.define('Metter', {
        userUUID: {
            type: DataTypes.UUID,
            primaryKey: true,
        },

        type: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            validate: {
                isIn: {
                    args: [[
                        TYPE_USER,
                        TYPE_PROVIDER,
                    ]],
                    msg: `Invalid metter type`,
                },
            },
        },

        metter: decimalField('metter', {
            allowNull: false,
            defaultValue: 0,
        }),

        cicle: decimalField('cicle', {
            allowNull: false,
            defaultValue: 0,
        }),

        lastCicle: decimalField('lastCicle', {
            allowNull: false,
            defaultValue: 0,
        }),

    }, {
        instanceMethods: instanceMethods,

        indexes: [
            {
                name: 'by_user_type',
                unique: true,
                fields: ['userUUID', 'type'],
            },
            {
                name: 'by_type',
                fields: ['type'],
            },
        ],

        scopes: {
            users: {
                where: {
                    type: TYPE_USER,
                },
            },

            providers: {
                where: {
                    type: TYPE_PROVIDER,
                },
            },
        },

    });

    if (typeof MettersModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                MettersModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    MettersModel.TYPE_USER = TYPE_USER;
    MettersModel.TYPE_PROVIDER = TYPE_PROVIDER;

    MettersModel.associate = function(models) {
        UserModel = models.users;

        MettersModel.belongsTo(UserModel, {
            as: 'Owner',
            foreignKey: 'userUUID',
            targetKey: 'uuid',
            constraints: true,
        });
    };

    /**
     * Find user by email
     * @param {string} email
     * @param {boolean} [rejectOnEmpty=true]
     * @return {Promise<MettersModel|null>}
     */
    MettersModel.findByEmail = async function(email, rejectOnEmpty = true) {
        debug(`Finding user ${email}`);
        const user = await this.findOne({
            where: {email},
            rejectOnEmpty,
        });

        return user;
    };

    MettersModel.ADS_METER_MIN_ERROR = ADS_METER_MIN_ERROR;

    return MettersModel;
};
