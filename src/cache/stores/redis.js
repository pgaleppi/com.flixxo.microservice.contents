import {getConnection} from '../rediscon';
import {createDebug} from '../../logger';
import Store from './base';

const debug = createDebug(__filename);

/**
 * Store cache
 */
export default class RedisStore extends Store {

    /**
     * Get redis connection
     */
    get connection() {
        return getConnection();
    }

    /**
     * Get value
     * @param {string} rkey
     * @param {object} [options]
     * @return {any}
     */
    async get(rkey, options = {}) {
        const key = `${this._prefix}:${rkey}`;
        debug(`Getting ${key}`);

        const {flags} = options;

        if (flags && flags.length) {
            debug(`\tFlags requests`);
            const multi = this.connection.multi();
            multi.get(key);

            for (const k of flags) {
                debug(`\tGetting to ${k} as ${this._flagPrefix}:${k}`);
                multi.get(`${this._flagPrefix}:${k}`);
            }

            let lres = await multi.exec();

            let [err, res] = lres.shift();

            if (err) {
                throw err;
            }

            res = this._decode(res);

            lres = lres
                .filter(([e, d]) => !e && d)
                .map(([e, d]) => this._decode(d));

            if (!res) {
                debug(`\tNo data`);
                return undefined;
            }

            if (lres.length) {
                for (const f of lres) {
                    if (res.timestamp <= f.timestamp) {
                        debug(`\tFlag ${f.flag} is newer than cache`);
                        debug(`\tcached data ignored`);
                        return undefined;
                    }
                }
            }

            debug(`\tReturn data`);
            return res.data;
        } else {
            const res = await this.connection.get(key);
            return res ? this._decode(res).data : undefined;
        }
    }

    /**
     * Set value
     * @param {string} rkey
     * @param {any} val
     * @param {object} [options]
     * @param {integer} [options.expiration]
     * @return {Promise};
     */
    async set(rkey, val, options = {}) {
        const exp = options.expiration || this._options.expiration;
        const key = `${this._prefix}:${rkey}`;
        debug(`Saving ${key} with ${exp} expiration`);
        const pack = this._encode({
            timestamp: Date.now(),
            data: val,
        });
        return this.connection.setex(key, exp, pack);
    }

    /**
     * Touch flah
     * @param {string} flag
     * @param {object} [options]
     * @return {Promise}
     */
    async touchFlag(flag, options = {}) {
        const exp = options.expiration || this._options.expiration;
        const key = `${this._flagPrefix}:${flag}`;
        debug(`Touching flag as ${key}`);
        const pack = this._encode({
            flag,
            timestamp: Date.now(),
        });

        return this.connection.setex(key, exp, pack);
    }
}
