import models from '../models';

/**
 * List countries
 * @param {string} lang
 * @return {Promise<object[]>}
 */
export const list = [
    'lang',
    async function list(lang) {
        const countries = await models.countries.scope([
            'simple',
            {method: ['byLang', lang]},
        ]).findAll();

        return countries.map((c) => ({
            ...c.get({plain: true}),
            name: undefined,
            Name: {
                value: c.name[0].name,
            },
        }));
    },
];
