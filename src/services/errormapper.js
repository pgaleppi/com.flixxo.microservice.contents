
import ErrorMapper from '../middleware/errormapping';
import * as errors from '../errors';

const errorMapper = new ErrorMapper();

/*
errorMapper.use('ExampleError', (err) => {
    return new errors.ShowableError(err.message);
});
*/

errorMapper.use('SequelizeEmptyResultError', (err) => {
    return new errors.NotFoundError(err.message);
});

errorMapper.use('SequelizeValidationError', (err) => {
    return errors.ValidationError.fromSequelizeValidationError(err);
});

errorMapper.use('SequelizeUniqueConstraintError', (err) => {
    return errors.UniqueConstraintError.fromSequelizeValidationError(err);
});

errorMapper.use('SequelizeForeignKeyConstraintError', (err) => {
    return new errors.NotFoundError(`Subitem does not exists`);
});

export default errorMapper;
