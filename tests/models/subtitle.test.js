jest.mock('axios');

import {syncDb, loadFixture, loadData} from '../helpers/db';
import models from '../../src/models';

describe('Check default values', () => {

    beforeEach(async () => {
        await syncDb();
        await loadFixture('users');
        await loadData('categories');
    });

    it('Must return def value', async () => {
        let subtitle = await models.subtitle.create({
            uuid: '7cdeff76-c4df-4bf2-a810-c0f054033a28',
            url: 'https://yts.am/subtitle',
        });

        expect(subtitle.closeCaption).toBeFalsy();
        expect(subtitle.lang).toBe('en');
        expect(subtitle.url).toBeDefined();
        expect(subtitle.uuid).toBeDefined();
        expect(subtitle.label).toBeUndefined();
    });

});
