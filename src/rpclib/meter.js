
/**
 * Meter for count operation by a time period
 */
export default class Meter {

    /**
     * Create new meter
     * @param {object} [options]
     * @param {integer} [options.interval]
     * @param {integer} [options.logs]
     */
    constructor(options = {}) {
        this._interval = options.interval || 60;
        this._logsCount = options.logs || 5;
        this._log = {};
    }

    /**
     * Get period
     * @private
     * @param {integer} [bn]
     * @return {Meter}
     */
    _getPeriod(bn = 0) {
        let now = Math.floor((new Date()).getTime() / 1000);
        now -= this._interval * bn;
        return Math.floor(now / this._interval);
    }

    /**
     * Get periods map
     * @private
     * @return {integer[]}
     */
    _getPeriodsMap() {
        const periods = [];

        for (let i = 0; i < this._logsCount; i++) {
            const period = this._getPeriod(i);
            periods.push(period);
        }

        return periods;
    }

    /**
     * Check logs to delete old records
     * @private
     */
    _checkLogs() {
        const periods = this._getPeriodsMap().map((x) => `period${x}`);
        for (const i in this._log) {
            if (!i.match(/^period/)) {
                continue;
            }

            if (periods.indexOf(i) === -1) {
                delete this._log[i];
            }
        }
    }

    /**
     * Get status of a period
     * @param {integer} period
     * @return {integer}
     */
    getPeriodStatus(period) {
        const key = `period${period}`;
        if (typeof this._log[key] === 'undefined') {
            return 0;
        }

        return this._log[key];
    }

    /**
     * Push new operation
     * @param {integer} [c]
     * @return {Meter}
     */
    push(c = 1) {
        const period = this._getPeriod();
        const key = `period${period}`;
        if (typeof this._log[key] === 'undefined') {
            this._log[key] = 0;
        }

        this._log[key] += c;

        this._checkLogs();

        return this;
    }

    /**
     * Get status of the last periods
     * @return {integer[]}
     */
    getStatus() {
        return this._getPeriodsMap().map((x) => this.getPeriodStatus(x));
    }

}
