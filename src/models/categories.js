
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

logger.info('dummy');

/**
 * CategoriesModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {

    const instanceMethods = {};

    const CategoriesModel = sequelize.define('Category', {

        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },

        name: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },

        thumb: {
            type: DataTypes.STRING(1024),
            allowNull: false,
            // @TODO replace
            defaultValue: 'https://dummyimage.com/120',
            validate: {
                isUrl: true,
            },
        },

    }, {
        name: {
            singular: 'Category',
            plural: 'Categories',
        },
        instanceMethods: instanceMethods,
    });

    if (typeof CategoriesModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                CategoriesModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    CategoriesModel.associate = function(models) {

        CategoriesModel.belongsToMany(models.users, {
            as: 'UsersFollowers',
            through: 'UserCategories',
            foreignKey: {
                name: 'categoryId',
                allowNull: false,
            },
            otherKey: {
                name: 'userId',
                allowNull: false,
            },
            constraints: true,
        });

        CategoriesModel.addScope('public', {
            attributes: [
                'id',
                'name',
                'thumb',
            ],
        });
    };

    return CategoriesModel;
};
