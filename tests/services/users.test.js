import {syncDb, loadFixture, loadData} from '../helpers/db';
import {NotFoundError} from '../../src/errors';
jest.mock('../../src/rpclib');
import models from '../../src/models';
import * as usersService from '../../src/services/users';
import * as rpcMock from '../../src/rpclib';
import {RPCRemoteError} from '../../src/rpclib/errors' 
import { EmptyError } from 'rxjs';

jest.mock('../../src/sqsrpc');

describe('Testing users version service', () => {

    beforeAll(() => {
        jest.mock('../../src/sqsrpc');
    });

    beforeEach(async (done) => {
        await syncDb(true);
        await loadData('00_countries');
        await loadFixture('users');
        await loadFixture('profiles');
        done();
    }, 10000);

    afterAll(() => {
        jest.unmock('../../src/sqsrpc');
    });

    describe('#setProfileAvatar', () => {

        test('Must set profile avatar', async () => {
            const url = 'http://domain.com/image';
            let res = await usersService.setProfileAvatar(
                'user1@fakemail.com', url
            );

            expect(res.profile.avatar).toBe(url);
        });
    });

    describe('#removeProfileAvatar', () => {
        test.skip('Must remove profile avatar', async () => {
            let res = await usersService.removeProfileAvatar(
                'user2@fakemail.com',
            );
            expect(res.success).toBeTruthy();
        });

        test('Avatar not found', async () => {
            let res = await usersService.removeProfileAvatar(
                'user1@fakemail.com',
            );
            expect(res.success).toBeFalsy();
        });
    });

    describe('#cancel', () => {

        test('Must to delete user', async() => {
            await usersService.cancel(
                'user2@fakemail.com'
            );

            const c = await models.users.count({
                where: {email: 'user2@fakemail.com'},
                paranoid: false,
            });

            expect(c).toBe(0);
        });
        
    });

});

describe.skip('Test download img', () => {
    test('Check img download', async () => {
        const res = await usersService.downloadProfileImg(
            'test@flixxo.com',
            'https://www.heart.org/-/media/images/health-topics/congenital-heart-defects/50_1683_44a_asd.jpg'
        );
    });
});

describe('#findUser', () => {

    beforeAll(async () => {
        await syncDb(true);
        await loadData('categories');
        await loadFixture('users');
        await loadFixture('followers-for-with-followers-metter-scope');
        await loadFixture('content-for-with-content-metters');
        rpcMock.fakeResponse(
            'com.flixxo.microservice.auth',
            'auth:findUser',
            (query) => {
                switch (query) {
                    case 'user1@fakemail.com':
                        return {
                            uuid: '00000000-b5aa-4507-913a-a387419b6ef8',
                        };
                    case 'user9@fakemail.com':
                        return null;
                    default:
                        throw new Error('?');
                }
            },
        );
        rpcMock.fakeResponse(
            'com.flixxo.microservice.wallets',
            'wallets:getByUser',
            (uuid) => {
                switch (uuid) {
                    case '00000000-b5aa-4507-913a-a387419b6ef8':
                        return '0x71C7656EC7ab88b098defB751B7401B5f6d8976F';
                    default:
                        throw new Error('?');
                }
            },
        );
    });

    afterAll(() => rpcMock.clear());

    test(`Must to return existing user`, async () => {
        const res = await usersService.findUser[1](
            {},
            '00000000-dd08-42c5-a8ac-a99258689b5b',
            'user1@fakemail.com',
        );

        expect(res).toMatchSnapshot();
    });

    test(`Must to throw erro when from is invalid`, async () => {
        try {
            await usersService.findUser[1](
                {},
                '00000000-0000-42c5-a8ac-a99258689b5b',
                'user1@fakemail.com',
            );
        } catch (e) {
            expect(e).toBeInstanceOf(Error);
            expect(e.message).toBe('From user not found');
            return;
        }

        throw new Error('Pass');
    });

    test(`Must to return NotFoundError with user does not exists`, async () => {
        try {
            await usersService.findUser[1](
                {},
                '00000000-dd08-42c5-a8ac-a99258689b5b',
                'user9@fakemail.com',
            );
        } catch (e) {
            expect(e).toBeInstanceOf(NotFoundError);
            expect(e.message).toBe('User not found');
            return;
        }

        throw new Error('Pass');
    });
});

describe('#fillUserInformation', () => {

    beforeAll(async () => {
        await syncDb(true);
        await loadFixture('users');
    });

    test(`Must to return existing user`, async () => {
        const res = await usersService.fillUserInformation(['00000000-c1eb-41b5-85e7-256b0568538b'], {attributes: ['uuid', 'nickname']});
        expect(res).toMatchSnapshot();
    });
});

describe('#findUserByWallet', () => {

    beforeAll(async () => {
        await syncDb(true);
        await loadData('categories');
        await loadFixture('users');
        await loadFixture('followers-for-with-followers-metter-scope');
        await loadFixture('content-for-with-content-metters');
        rpcMock.fakeResponse(
            'com.flixxo.microservice.auth',
            'auth:findUser',
            (query) => {
                switch (query) {
                    case '00000000-b5aa-4507-913a-a387419b6ef8':
                        return {
                            uuid: '00000000-b5aa-4507-913a-a387419b6ef8',
                        };
                    case 'user9@fakemail.com':
                        return null;
                    default:
                        throw new Error('?');
                }
            },
        );
        rpcMock.fakeResponse(
            'com.flixxo.microservice.wallets',
            'wallets:getByUser',
            (uuid) => {
                switch (uuid) {
                    case '00000000-b5aa-4507-913a-a387419b6ef8':
                        return '0x71C7656EC7ab88b098defB751B7401B5f6d8976F';
                    default:
                        throw new Error('?');
                }
            },
        );
        rpcMock.fakeResponse(
            'com.flixxo.microservice.wallets',
            'wallets:getWalletUser',
            (address) => {
                switch (address) {
                    case '0x71C7656EC7ab88b098defB751B7401B5f6d8976F':
                        return '00000000-b5aa-4507-913a-a387419b6ef8';
                    default:
                        throw new NotFoundError();  
                }
            },
        );
    });

    afterAll(() => rpcMock.clear());

    test(`Must to return existing user`, async () => {
        const res = await usersService.findUserByWallet(
            '00000000-dd08-42c5-a8ac-a99258689b5b',
            '0x71C7656EC7ab88b098defB751B7401B5f6d8976F',
        );

        expect(res).toMatchSnapshot();
    });

    test(`Must to return empty error`, async () => {
        return expect(usersService.findUserByWallet(
            '00000000-dd08-42c5-a8ac-a99258689b5b',
            '0x71C7656EC7ab88b097defB751B7001B5f6d8966Y',
        )).rejects.toBeInstanceOf(NotFoundError);
    });
});
