import {createHash, timingSafeEqual} from 'crypto';
import config from '../config';
import {createDebug} from '../logger';

const APP_LIXIR_SECRET = config.get('ads:applixir:secret');

const debug = createDebug(__filename);

const SSV_PICK_KEYS = [
    'account_id',
    'game_id',
    'custom1',
    'custom2',
    'timestamp',
    'checksum',
];

const STATUS_NEW = 0x01;
const STATUS_PAYED = 0x02;

/**
 * Validate data checksum
 * @param {object} data
 * @return {bool}
 */
export function validateCheckSum(data) {
    debug(`Validate AppLixir checksum`);
    let res;
    const hasher = createHash('md5');
    const {checksum} = data;

    const clean = `${data.account_id}${data.game_id}${data.custom1}` +
                  `${data.custom2}${data.timestamp}${APP_LIXIR_SECRET}`
    ;

    debug(`clean: ${clean}`);

    const ownsum = hasher.update(clean).digest();

    debug(`own: ${ownsum.toString('hex')}, d: ${data.checksum}`);

    try {
        res = timingSafeEqual(
            ownsum,
            Buffer.from(checksum, 'hex'),
        );
    } catch (e) {
        debug(`Error`, e);
        if (e.name === 'RangeError') {
            res = false;
        } else {
            throw e;
        }
    }

    return res;
}

/**
 * AppLixirViewsModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {
    let UsersModel;

    const instanceMethods = {};

    const AppLixirViewsModel = sequelize.define('AppLixirView', {

        uuid: {
            type: DataTypes.UUID,
            primaryKey: true,
        },

        checksum: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                is: {
                    args: [/^[a-f0-9]{32}$/i],
                    msg: 'Invalid checksum format',
                },
            },
        },

        rewardedAt: {
            type: DataTypes.DATE,
            allowNull: false,
        },

        userId: {
            type: DataTypes.UUID,
            allowNull: false,
        },

        rawData: {
            type: DataTypes.TEXT,
            allowNull: false,
            get() {
                const val = this.getDataValue('rawData');
                return val ? JSON.parse(val) : null;
            },
            set(val) {
                if (typeof val === 'string') {
                    val = JSON.parse(val);
                }

                const picked = Object.fromEntries(
                    Object.entries(val)
                        .filter(([key]) => SSV_PICK_KEYS.includes(key))
                );

                this.setDataValue('rawData', JSON.stringify(picked));
            },
        },

        status: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: STATUS_NEW,
            validate: {
                isIn: [[
                    STATUS_NEW,
                    STATUS_PAYED,
                ]],
            },
        },

    }, {
        name: {
            singular: 'AppLixirView',
            plural: 'AppLixirViews',
        },
        timestamps: true,
        instanceMethods: instanceMethods,
        validate: {
            async validChecksum() {
                const {rawData} = this;
                if (!validateCheckSum(rawData)) {
                    throw new Error('Invalid checksum');
                }
            },
        },
    });

    if (typeof AppLixirViewsModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                AppLixirViewsModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    AppLixirViewsModel.STATUS_NEW = STATUS_NEW;
    AppLixirViewsModel.STATUS_PAYED = STATUS_PAYED;

    AppLixirViewsModel.associate = function(models) {
        UsersModel = models.users;

        AppLixirViewsModel.belongsTo(UsersModel, {
            as: 'Viewer',
            targetKey: 'uuid',
            foreignKey: 'userId',
            constraints: true,
        });

        // Scopes
        AppLixirViewsModel.addScope('availables', {
            where: {
                status: STATUS_NEW,
            },
        });

        AppLixirViewsModel.addScope('byUser', (userId) => ({
            where: {userId},
        }));
    };

    /**
     * Get new AppLixirViewsModel from rawData
     * @param {object} rawData
     * @return {AppLixirViewsModel}
     */
    AppLixirViewsModel.fromRaw = function(rawData) {
        return this.build({
            rawData,
            uuid: rawData['custom2'],
            checksum: rawData['checksum'],
            rewardedAt: parseInt(rawData['timestamp'], 10),
            userId: rawData['custom1'],
        });
    };

    return AppLixirViewsModel;
};
