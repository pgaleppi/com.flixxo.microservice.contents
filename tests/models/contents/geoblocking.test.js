jest.mock('axios');

// import {EmptyResultError} from 'sequelize';
import {syncDb, loadFixture, loadData} from '../../helpers/db';
import models from '../../../src/models';

jest.mock('../../../src/sqsrpc');

const lang = 'en';

describe('ContentsModel: GeoBlocking methods', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadData('00_countries');
        done();
    }, 30000);

    describe('#getGeoBlocking', () => {
        beforeEach(async () => {
            await loadFixture('contents-for-geoblocking-getters');
        });

        it(`Must to return null, if is not setted`, async () => {
            const content = await models.contents.findByPk(
                '7cdeff76-abcd-4bf2-a810-c0f054033a28');
            const geoBlockingInfo = await content.getGeoBlocking();
            expect(geoBlockingInfo).toBeNull();
        });

        it(`Must to return geoblocking data, is setted`, async () => {
            const content = await models.contents.findByPk(
                '7cdeff76-c4df-4bf2-a810-c0f054033a28');
            const geoBlockingInfo = await content.getGeoBlocking({
                scope: ['detailled'],
            });
            expect(geoBlockingInfo.get({plain: true}).countries).toEqual([
                {id: 10, iso: 'AR'},
                {id: 41, iso: 'CL'},
            ]);
        });

        it(`Must include geoblocking data when denormalizing for ElasticSearch`, async () => {
            const content = await models.contents.findByPk(
                '7cdeff76-c4df-4bf2-a810-c0f054033a28');
            let denormalizedContent = await content.denormalize();
            expect(denormalizedContent.blockedCountries).toEqual(
                ['AR', 'CL']
            );
            expect(denormalizedContent.availableCountries).toEqual(
                []
            );
            expect(denormalizedContent.geoBlockingType).toEqual(
                models.geoblocking.TYPE_BLACK
            );
        });
    });
});

describe('ContentsModel: GeoBlocking scope', () => {

    beforeAll(async (done) => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadData('00_countries');
        await loadFixture('contents-for-geoblocking-scope');
        done();
    }, 30000);

    test(`Must to show contents only available in AR`, async () => {

        const res = await models.contents.scope(
            {method: ['i18n', lang]},
            {method: ['geoAvailable', 'AR']}
        ).findAll({
            attributes: ['title', 'uuid'],
        });

        const plain = res
                .map(({title, uuid, geoBlocking}) => (
                    {
                        title,
                        uuid,
                        geoBlocking: geoBlocking ? {
                            ...geoBlocking.get({plain: true}),
                        } : 'nop',
                    }
                ));

        expect(plain).toEqual([
            {
                title: 'Enbled',
                uuid: '7cdeff76-abcd-4bf2-a810-c0f054030002',
                geoBlocking: {blockType: 32},
            },
            {
                title: 'No rulez',
                uuid: '7cdeff76-abcd-4bf2-a810-c0f054030003',
                geoBlocking: 'nop',
            },
            {
                title: 'Season blacklist',
                geoBlocking: 'nop',
                uuid: '7cdeff76-abcd-4bf2-a810-c0f054030011',
            },
            {
                title: 'Content not moderated',
                geoBlocking: {blockType: 32},
                uuid: '7cdeff76-c4df-4bf2-a815-c0f054030001',
            },
        ]);

    });

    test(`Must to show contents bloqued AR and CL`, async () => {

        const res = await models.contents.scope(
            {method: ['i18n', lang]},
            {method: ['geoAvailable', 'CN']}
        ).findAll({
            attributes: ['title', 'uuid'],
        });

        const plain = res
                .map(({title, uuid, geoBlocking}) => (
                    {
                        title,
                        uuid,
                        geoBlocking: geoBlocking ? {
                            ...geoBlocking.get({plain: true}),
                        } : 'nop',
                    }
                ));

        expect(plain).toEqual([
            {
                title: 'Blocked',
                uuid: '7cdeff76-c4df-4bf2-a810-c0f054030001',
                geoBlocking: {blockType: 16},
            },
            {
                title: 'No rulez',
                uuid: '7cdeff76-abcd-4bf2-a810-c0f054030003',
                geoBlocking: 'nop',
            },
            {
                title: 'Season whitelist',
                geoBlocking: 'nop',
                uuid: '7cdeff76-abcd-4bf2-a810-c0f054030007',
            },
            {
                title: 'Content not enabled by user',
                geoBlocking: {blockType: 16},
                uuid: '7cdeff76-c4df-4bf2-a816-c0f054030001',
            },
        ]);

    });

    test(`Must to show contents bloqued AR and CL when iso is null`, async () => {

        const res = await models.contents.scope(
            {method: ['i18n', lang]},
            {method: ['geoAvailable', null]}
        ).findAll({
            attributes: ['title', 'uuid'],
        });

        const plain = res
                .map(({title, uuid, geoBlocking}) => (
                    {
                        title,
                        uuid,
                        geoBlocking: geoBlocking ? {
                            ...geoBlocking.get({plain: true}),
                        } : 'nop',
                    }
                ));

        expect(plain).toEqual([
            {
                title: 'Blocked',
                uuid: '7cdeff76-c4df-4bf2-a810-c0f054030001',
                geoBlocking: {blockType: 16},
            },
            {
                title: 'No rulez',
                uuid: '7cdeff76-abcd-4bf2-a810-c0f054030003',
                geoBlocking: 'nop',
            },
            {
                title: 'Season whitelist',
                geoBlocking: 'nop',
                uuid: '7cdeff76-abcd-4bf2-a810-c0f054030007',
            },
            {
                title: 'Content not enabled by user',
                geoBlocking: {blockType: 16},
                uuid: '7cdeff76-c4df-4bf2-a816-c0f054030001',
            },
        ]);

    });

    test(`Must to show enabled contents only available in AR`, async () => {

        const res = await models.contents.scope(
            'enabled',
            {method: ['i18n', lang]},
            {method: ['geoAvailable', 'AR']}
        ).findAll({
            attributes: ['title', 'uuid'],
        });

        const plain = res
                .map(({title, uuid, geoBlocking}) => (
                    {
                        title,
                        uuid,
                        geoBlocking: geoBlocking ? {
                            ...geoBlocking.get({plain: true}),
                        } : 'nop',
                    }
                ));

        expect(plain).toEqual([
            {
                title: 'Enbled',
                uuid: '7cdeff76-abcd-4bf2-a810-c0f054030002',
                geoBlocking: {blockType: 32},
            },
        ]);

    });

    test(`Must to show enabled contents bloqued AR and CL`, async () => {

        const res = await models.contents.scope(
            'enabled',
            {method: ['i18n', lang]},
            {method: ['geoAvailable', 'CN']}
        ).findAll({
            attributes: ['title', 'uuid'],
        });

        const plain = res
                .map(({title, uuid, geoBlocking}) => (
                    {
                        title,
                        uuid,
                        geoBlocking: geoBlocking ? {
                            ...geoBlocking.get({plain: true}),
                        } : 'nop',
                    }
                ));

        expect(plain).toEqual([
            {
                title: 'Season whitelist',
                geoBlocking: 'nop',
                uuid: '7cdeff76-abcd-4bf2-a810-c0f054030007',
            },
        ]);

    });

    test(`Must to show enabled contents bloqued AR and CL when iso is null`, async () => {

        const res = await models.contents.scope(
            'enabled',
            {method: ['i18n', lang]},
            {method: ['geoAvailable', null]}
        ).findAll({
            attributes: ['title', 'uuid'],
        });

        const plain = res
                .map(({title, uuid, geoBlocking}) => (
                    {
                        title,
                        uuid,
                        geoBlocking: geoBlocking ? {
                            ...geoBlocking.get({plain: true}),
                        } : 'nop',
                    }
                ));

        expect(plain).toEqual([
            {
                title: 'Season whitelist',
                geoBlocking: 'nop',
                uuid: '7cdeff76-abcd-4bf2-a810-c0f054030007',
            },
        ]);

    });

    test('Must to throw error with invalid type ISO code', async () => {

        try {
            await models.contents.scope(
                {method: ['i18n', lang]},
                {method: ['geoAvailable', 324]}
            ).findAll({
                attributes: ['title', 'uuid'],
            });
        } catch (e) {
            expect(e).toBeInstanceOf(TypeError);
            expect(e.message).toBe('countryIso must to be a string');
            return;
        }

        throw new Error('Pass');
    });

    test('Must to throw error with invalid ISO code', async () => {

        try {
            await models.contents.scope(
                {method: ['i18n', lang]},
                {method: ['geoAvailable', 'ARG']}
            ).findAll({
                attributes: ['title', 'uuid'],
            });
        } catch (e) {
            expect(e).toBeInstanceOf(Error);
            expect(e.message).toBe('Invalid ISO code');
            return;
        }

        throw new Error('Pass');
    });
});

