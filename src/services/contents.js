import Promise from 'bluebird';
import models from '../models';
import createLogger, {createDebug} from '../logger';
import {serviceCacheable} from '../cache/decorators';
import {ValidationError} from '../errors';
import {getConnection} from '../storage';
import {loadService} from '../remoteservice';
import {byCategory as seriesByCategory, seriesToElastic} from './series';
import {
    Op,
    EmptyResultError,
    ValidationError as SequelizeValidationError,
} from 'sequelize';
import axios from 'axios';
import {S3} from 'aws-sdk';
import config from '../config';
import subsrt from 'subsrt';
import stripBom from 'strip-bom';

const BUCKET_NAME = config.get('aws:s3:bucket');
const BUCKET_HOST = config.get('aws:s3:host');
const CONTENT_SUBTITLE_FOLDER = config.get('aws:s3:contentsubtitle:folder');
const CONTENT_SUBTITLE_ACL = config.get('aws:s3:contentsubtitle:acl');

const UPLOAD_BUCKET_NAME = config.get('aws:s3:uploadBucket');

const WEBLINKS_EXPIRATION = parseInt(config.get('weblinks:expiration'), 10);

const FILTER_TYPE_ALL = 'all';
const FILTER_TYPE_COMMUNITY = 'community';
const FILTER_TYPE_SERIES = 'series';

const debug = createDebug(__filename);
const logger = createLogger(__filename);

const SLS_HOST = config.get('serverless:host');
const SLS_CONTENTS = config.get('serverless:contents');
// @TODO uncomment post beta release
// const WARRANTY_MULTIPLIER = 5;
// @TODO uncomment post beta release
// const DEFAULT_WARRANTY_PERIOD = config.get('warranty:returnPeriod');

/**
 * Get a list of the top contents
 * @param {object} geo
 * @param {lang} lang
 * @return {Promise}
 */
export const listTop = [
    'geoIp',
    'lang',
    serviceCacheable(__filename),
    async function listTop(geo, lang) {
        debug(`List top contents for ${geo.country}`);
        // @TODO: Parametrize!
        const content = await models.contents.scope(
            'enabled',
            {method: ['i18n', lang]},
            {method: ['listing']},
            {method: ['geoAvailable', geo.country]},
        ).findAll({
            limit: 25,
        });

        const plain = content.map((cont) => cont.get({plain: true}));

        return {
            count: plain.length,
            contents: plain,
        };
    },
];

/**
 * Get contents from list ids
 * @param {string} lang
 * @param {array<string>} contentsIds
 * @param {object} [options]
 * @return {array<object>} contents
 */
export const listContentsFromList = [
    'lang',
    serviceCacheable(__filename),
    async (lang, contentsIds, options = {}) => {
        debug(`List contents from list`);

        const scopes = options.scopes || [
            {method: ['i18n', lang]},
            {method: ['listing']},
            {method: ['categoryJoin']},
        ];

        const optionsWhere = options.where || {};
        const where = {
            ...optionsWhere,
        };

        if (contentsIds.length > 0) {
            where.uuid = {
                [Op.in]: contentsIds,
            };
        }

        delete options.where;
        delete options.scope;

        const contents = await models.contents.scope(...scopes).findAll({
            where,
            ...options,
        });

        const plainContents = contents.map((cont) => cont.get({plain: true}));

        return plainContents;
    }];

/**
 * Get a content available by uuid
 * @param {object} geo
 * @param {string} lang
 * @param {string} uuid
 * @param {bool} [serieInfo=false]
 * @return {Promise}
 */
export const getAvailableContent = [
    'geoIp',
    'lang',
    serviceCacheable(__filename),
    async function(geo, lang, uuid, serieInfo = false) {
        debug(`Get content ${uuid}, from ${geo.country}, lang ${lang}`);

        const scopes= [
            'enabled',
            {method: ['i18n', lang]},
            {method: ['detailed']},
            {method: ['geoAvailable', geo.country]},
        ];

        if (serieInfo) {
            scopes.push({method: ['withSerieInfo', {}, false]});
        }

        const content = await models.contents.scope(...scopes).findByPk(uuid, {
            rejectOnEmpty: false,
        });

        if (!content) {
            throw new EmptyResultError();
        }

        return {
            ...content.get({plain: true}),
        };
    }];

/**
 * Get a list of the top author contents
 * @DEPRECATED
 *
 * @param {string} lang
 * @param {string} email
 * @param {date} from
 * @param {date} to
 * @return {Promise}
 */
export const listTopByAuthor = ['lang', async (lang, email, from, to) => {
    const user = await getUser(email);

    logger.warn('WARNING: contents:listTopByAuthor is deprecated');

    debug(`Contents by author ${email}`);

    const contents = await models.contents.scope(
        {method: ['i18n', lang]},
        {method: ['asAuthor', user.id]},
        {method: ['authorListing']}
    ).findAll({
        limit: 25,
    });

    const plainContents = contents.map((cont) => cont.get({plain: true}));

    debug(`Creating payments service connection`);
    const paymentsService = await loadService('@payments');

    debug(`Calling payments service`);
    const filledContents = await paymentsService.execute(
        'users:fillUserContentsEarnings',
        user.uuid, plainContents, from, to
    );

    return {
        count: filledContents.length,
        contents: filledContents,
    };

}];

/**
 * Get a list of the author contents
 * @param {string} lang
 * @param {string} email
 * @param {date} from
 * @param {date} to
 * @param {integer} limit
 * @return {Promise}
 */
export const listByAuthor = ['lang', async (lang, email, from, to, limit) => {
    debug(`Exec listByAuthor for ${email}, between ${from} and ${to}`);
    const user = await getUser(email);
    let opts = {};

    if (limit) {
        opts = {
            limit,
        };
    }
    if (from && to) {
        opts.where = {
            releaseDate: {
                [Op.gte]: from,
                [Op.lte]: to,
            },
        };
    }

    debug(`Contents by author ${email}`);

    const contents = await models.contents.scope(
        {method: ['i18n', lang]},
        {method: ['asAuthor', user.id]},
        {method: ['authorListing']}
    ).findAll(opts);

    const plainContents = contents.map((cont) => cont.get({plain: true}));

    debug(`Creating payments service connection`);
    const paymentsService = await loadService('@payments');

    debug(`Calling payments service`);
    const filledContents = await paymentsService.execute(
        'users:fillUserContentsEarnings',
        user.uuid, plainContents, from, to
    );

    return {
        count: filledContents.length,
        contents: filledContents,
    };

}];

/**
 * Get a content by author
 * @param {string} lang
 * @param {string} email
 * @param {string} uuid
 * @param {object} [options]
 * @return {Promise}
 */
export const getContentByAuthor = [
    'lang',
    async function getContentByAuthor(lang, email, uuid, options = {}) {
        const user = await getUser(email);

        debug(`Content detail ${uuid} by author ${email}`);

        const content = await models.contents.scope(
            {method: ['asAuthor', user.id]},
            {method: ['authorDetailed']},
            {method: ['i18n', lang]},
        ).findByPk(uuid, {
            ...options,
            rejectOnEmpty: false,
        });

        if (!content) {
            throw new EmptyResultError();
        }

        return {
            ...content.get({plain: true}),
        };
    },
];

/**
 * Check if contents is of authory of an user
 * @param {string} email
 * @param {string} uuid
 * @return {Promise<bool>}
 */
export async function isAuthor(email, uuid) {
    const user = await getUser(email);

    debug(`Check if content ${uuid} is by author ${email}`);

    const content = await models.contents.scope(
        {method: ['asAuthor', user.id]},
    ).findByPk(uuid, {
        attributes: ['uuid'],
        rejectOnEmpty: false,
    });

    return !!content;
}

/**
 * List top contents by category
 * @param {object} geo
 * @param {object} lang
 * @param {number} categoryId
 * @param {string} contentType
 * @return {Promise}
 */
export const byCategory = [
    'geoIp',
    'lang',
    serviceCacheable(__filename),
    async function byCategory(
        geo, lang, categoryId, contentType = FILTER_TYPE_ALL) {
        debug(`Listing by category ${categoryId} from ${geo.country}`);
        // @TODO: Parametrize!
        let content = [];
        if (contentType !== FILTER_TYPE_SERIES) {
            content = await models.contents.scope(
                'enabled',
                {method: ['i18n', lang]},
                {method: ['byCategory', categoryId]},
                {method: ['listing']},
                {method: ['geoAvailable', geo.country]}
            ).findAll({
                limit: 25,
            });
        }

        let plain = content.map((cont) => {
            const p = cont.get({plain: true});
            delete p.priority;
            return p;
        });

        // Search for series
        if (contentType !== FILTER_TYPE_COMMUNITY) {
            const seriesResult = await seriesByCategory[2](geo, categoryId);
            plain = plain.concat(seriesResult.series);
        }

        return {
            count: plain.length,
            contents: plain,
        };
    },
];

/**
 * Get contents by user
 * @param {UsersModel} user
 * @param {string[]} purchased
 * @param {object} geo
 * @param {string} lang
 * @return {Promise}
 */
const byUserContents = async function(user, purchased, geo, lang) {
    debug(`\tFilter by community`);

    return models.contents.scope(
        'enabled',
        'filterCommunityContents',
        {method: ['i18n', lang]},
        {method: ['listUnPurchaseds', purchased]},
        {method: ['geoAvailable', geo.country]},
    )
        .findAll({
            limit: 25,
        })
        .then((contents) => contents.map((cont) => {
            const p = cont.get({plain: true});
            delete p.priority;
            return p;
        }))
    ;
};

/**
 * Get contents series by user
 * @param {UsersModel} user
 * @param {string[]} purchased
 * @param {object} geo
 * @param {string} lang
 * @return {Promise}
 */
const byUserSeries = async function(user, purchased, geo, lang) {
    debug(`\tFilter by series`);

    /*
     let seriesIds = await models.contents.scope(
        'enabled',
        'filterSeriesContents',
        {method: ['geoAvailable', geo.country]},
    )
        .findAll({
            attributes: ['serieUUID'],
            group: ['serieUUID'],
        })
        .then((results) => {

        });
    ;

    seriesIds = await seriesIds.map((serie) => {
        return serie.serieUUID;
    });

    content = await models.serie.scope(
        'enabled',
        {method: ['detailed']},
        {method: ['withGeoAvailableSeasons',
            geo.country,
            true,
            lang,
        ]},
    ).findAll({
        where: {
            uuid: {
                [Op.notIn]: seriesIds,
            },
        },
    }, {limit: 25});

    seriesIds = content.map((series) => {
        return series.uuid;
    });

    // Fill data to display as a Content
    plain = content.map((serie) => {
        return serie.fillDataToDisplay();
    });*/

    /*   return models.serie.scope(
        'enabled',
        {method: ['detailed']},
        {method: ['withGeoAvailableSeasons',
            geo.country,
            true,
            lang,
        ]},
    )
        .findAll({limit: 25})
        .then((rs) => rs.map((s) => s.fillDataToDisplay()))
    ;*/
    const series = await models.serie.scope(
        'enabled',
        {method: ['serieContentsList', true]},
    ).findAll({
        limit: 25,
    });

    if (series.length === 0) {
        return {count: 0, series: []};
    }

    const serieIds = series.map((serie) => serie.uuid);

    let countryId = await models.serie.getCountryId(geo.country || 'XX');
    if (countryId) {
        countryId = countryId[0].id;
    }

    const avSeasons = await models
        .serie.readAvailableSeasons(countryId, serieIds);

    const realSeries = [];
    for ( let i = 0; i < series.length; i++ ) {
        let keepLooking = true;
        for ( let j = 0; j < avSeasons.length && keepLooking; j++ ) {
            if ( avSeasons[j].serieUUID === series[i].uuid ) {
                realSeries.push(series[i]);
                keepLooking = false;
            }
        }
    }

    // Fill data to display as a Content
    return realSeries.map((serie) => {
        const plain = serie.fillDataToDisplay();
        // @TODO Please killme
        plain.bodyFormat = 1;
        return plain;
    });
};

/**
 * List filtering by the categories followed by the user
 * @param {object} geo
 * @param {string} lang
 * @param {string} email
 * @param {string} contentType
 * @return {Promise}
 */
export const byUser = [
    'geoIp',
    'lang',
    serviceCacheable(__filename),
    async function byUser(geo, lang, email, contentType) {
        const paymentsService = await loadService('@payments');
        if (!contentType) {
            contentType = FILTER_TYPE_COMMUNITY;
        }

        debug(`Listing by user for ${email} from ${geo.country}`);
        debug(`Read contentType: ${JSON.stringify(contentType, null, 4)}`);

        let result;

        if (contentType === FILTER_TYPE_COMMUNITY) {
            // @TODO move block to super scope
            const user = await models.users.findByEmail(email);

            if (!user) {
                throw new Error('User not exist');
            }

            const purchased = await paymentsService.execute(
                'contents:getMinPurchasedContents',
                user.uuid,
            );

            const ids = purchased.contents || [];

            result = await byUserContents(user, ids, geo, lang);
        } else if (contentType === FILTER_TYPE_SERIES) {
            const user = {};
            const ids = [];
            result = await byUserSeries(user, ids, geo, lang);
        } else {
            throw new ValidationError(`Unknow content type ${contentType}`, [{
                field: 'contentType',
                type: 'unknow',
                message: `Unknow content type ${contentType}`,
            }]);
        }

        return {
            count: result ? result.length : 0,
            contents: result || [],
        };
    }];

/**
 * List contents of authors followed by user
 * @param {object} geo
 * @param {string} lang
 * @param {string} email - Email of follower
 * @param {number} [offset=0] - Offset
 * @return {Object[]}
 */
export const byFolloweds = [
    'geoIp', 'lang', async function(geo, lang, email, offset = 0) {
        debug(`Listinng by followeds user ${email} in ${geo.country}`);

        const user = await models.users.findByEmail(email, {
            attributes: ['id'],
        });

        const contents = await models.contents.scope(
            'listing',
            {method: ['i18n', lang]},
            {method: ['byFolloweds', user.id]},
            {metgod: ['geoAvailable', geo.country]},
        )
            .findAll({
            // @TODO parametrizar
                limit: 25,
                offset,
            });

        return contents.map((c) =>{
            return c.get({plain: true});
        });
    }];

/**
 * List contents filtering by moderator user
 * @param {string} lang
 * @param {string} email
 * @return {Promise}
 */
export const byModerator = ['lang', async function byModerator(lang, email) {
    debug(`List contents filtering by moderator user ${email}`);

    const user = await getUser(email);

    debug(`Listing by moderator ${email}`);

    const contents = await models.contents.scope(
        {method: ['i18n', lang]},
        {method: ['asModerator', user.id]},
        {method: ['moderatorListing']},
    ).findAll();

    return {
        contents,
    };
}];

/**
 * Show detailed information filtering by moderator user
 * @param {string} lang
 * @param {string} email
 * @param {string} uuid
 * @return {Promise}
 */
export const detailByModerator = [
    'lang',
    async function detailByModerator(lang, email, uuid) {
        debug(`Content detail filtering by moderator user ${email}`);

        const content = await getContentByModerator(email, uuid, lang);

        return {
            ...content.get({plain: true}),
        };
    }];

/**
 * Check if a content is moderated by user
 * @param {string} uuid
 * @param {string} email
 * @return {Promise<bool>}
 */
export async function isModeratedBy(uuid, email) {
    debug(`Check if ${email} is moderator of ${uuid}`);
    const user = await getUser(email);

    const content = await models.contents.scope(
        {method: ['asModerator', user.id]},
    )
        .findByPk(uuid, {
            attributes: ['uuid'],
            rejectOnEmpty: false,
        });

    return !!content;
}

/**
 * List contents approved and updated later to
 * @param {number} [ts]
 * @return {Promise}
 */
export async function seedboxList(ts = null) {
    debug(`List seedbox contents ${ts}`);
    let date;

    if (ts) {
        date = new Date(parseInt(ts, 10));
    } else {
        date = new Date();
    }

    debug(`\tdate: ${date}`);

    const contents = await models.contents.scope(
        {method: ['seedbox', date]},
    ).findAll({
        limit: 100,
    });

    const plain = contents.map((cont) => cont.get({plain: true}));

    return {
        contents: plain,
    };
}

/**
 * Update moderation status by moderator
 * @param {string} email
 * @param {string} uuid
 * @param {integer} status
 * @param {string} message
 * @param {string} warrantyAccountId
 * @return {Promise}
 */
export async function updateModerationStatus(
    email, uuid, status, message, warrantyAccountId
) {

    debug(`Updating moderation status by moderator user ${email}`);

    const content = await getContentByModerator(email, uuid);
    if (content.moderationStatus === models.contents.MODSTATUS_REJECT ||
        (content.moderationStatus === models.contents.MODSTATUS_ENABLED &&
         status !== models.contents.MODSTATUS_REJECT) ) {
        debug(`Content moderation status ${content.moderationStatus} ` +
              `cannot be changed`);
        throw new ValidationError('Content moderation status cannot be changed',
            [{
                field: 'moderationStatus',
                type: 'Update moderation status failed',
                message: 'Content moderation status cannot be changed',
            }]
        );
    }
    // @TODO uncomment post beta release
    // const oldModerationStatus = content.moderationStatus;
    debug(`Updating moderation content ${uuid} by moderator ${email}`);

    const sequelize = getConnection();
    await sequelize.transaction( async (dbTxn) => {
        if (content.contentType === models.contents.TYPE_VIDEO) {
            // Update content on DB
            await content.updateModerationStatus(status, message, dbTxn);
        } else if (content.contentType === models.contents.TYPE_SERIE) {
            // Update all of the serie's content on DB
            await content.updateModerationStatus(status, message, dbTxn);

            const author = await models.users.findByPk(content.authorId);
            const serie = await models.serie.scope(
                'statusEnabled',
                {method: ['asAuthor', author.id]},
                'listing',
            ).findByPk(content.serieUUID);

            await serie.updateAllContentsModerationStatus(
                status, message, dbTxn
            );
        }

        /* @TODO uncomment post beta release
        if ((status === models.contents.MODSTATUS_ENABLED ||
            status === models.contents.MODSTATUS_REJECT) &&
            oldModerationStatus !== models.contents.MODSTATUS_ENABLED ) {
            // Create warranty return transaction
            const warrantyAmount = content.price * WARRANTY_MULTIPLIER;
            let applyDate = new Date();
            if (status === models.contents.MODSTATUS_ENABLED) {
                debug('Create pending warranty return');
                if (content.moderatedAt.getTime() >
                    content.releaseDate.getTime()) {
                    applyDate = new Date(
                        content.moderatedAt.getTime() +
                        DEFAULT_WARRANTY_PERIOD * 1000
                    );
                } else {
                    applyDate = new Date(
                        content.releaseDate.getTime() +
                        DEFAULT_WARRANTY_PERIOD * 1000
                    );
                }
            }
            debug(`Creating payments service connection`);
            let paymentsService = await getServiceConnection(
                'com.flixxo.microservice.payments'
            );
            debug(`Calling payments service`);
            await paymentsService.execute(
                'contents:returnContentWarranty',
                content.author.uuid, content.uuid,
                warrantyAccountId, warrantyAmount,
                applyDate,
            );
        }*/
    });


    debug(`Content updated ${uuid}`);

    return {
        success: true,
    };
}

/**
 * Get content filtering by moderator user
 * @param {string} email
 * @param {string} uuid
 * @param {string} lang
 * @return {Promise}
 */
async function getContentByModerator(email, uuid, lang = 'en') {
    const user = await getUser(email);

    debug(`Content detail ${uuid} by moderator ${email}`);

    const content = await models.contents.scope(
        {method: ['i18n', lang]},
        {method: ['asModerator', user.id]},
        {method: ['moderatorDetailed']},
    ).findByPk(uuid, {
        rejectOnEmpty: false,
    });

    if (!content) {
        throw new EmptyResultError();
    }

    return content;
}

/**
 * Get and validate existent user
 * @param {string} email
 * @return {Promise}
 */
export async function getUser(email) {
    const user = await models.users.findByEmail(email);

    if (!user) {
        debug(`User ${email} does not exists`);
        throw new ValidationError('Non-registered user', [{
            field: 'email',
            type: 'Get user by email failed',
            message: 'Non-registered user',
        }]);
    }

    return user;
}

/**
 * Unassign content moderator
 * @param {string} email
 * @param {string} uuid
 * @return {Promise}
 */
export async function unassignModerator(email, uuid) {
    debug(`Unassigning moderator ${email}`);

    const content = await getContentByModerator(email, uuid);

    debug(`Unassigning moderator to content ${uuid}`);

    await content.unassignModerator();

    debug(`Moderator unnassigned ${uuid}`);

    return {
        success: true,
    };
}

/**
 * Get a content status
 * @param {string} hash
 * @return {Promise<object>}
 */
export async function getContentStatusByHash(hash) {
    debug(`Getting information by hash of ${hash}`);
    const content = await models.contents.scope('information').findByHash(
        hash,
        {rejectOnEmpty: true}
    );

    return content.get({plain: true});
}

/**
 * Create content
 * @param {string} email
 * @param {string} moderatorEmail
 * @param {object} newContent
 * @param {string} warrantyAccountId
 * @param {boolean} forceModEnabled
 * @return {Promise}
 */
export async function createContent(email, moderatorEmail, newContent,
    warrantyAccountId, forceModEnabled = false) {
    /* eslint require-atomic-updates: 0 */
    debug(`Creating new content ${email}`);
    const user = await getUser(email);
    const mediaIds = newContent.mediaIds;
    const subtitleIds = newContent.subtitleIds || [];
    const geoBlocking = newContent.geoBlocking;
    const metadata = newContent.metadata;

    delete newContent.mediaIds;
    delete newContent.subtitleIds;
    delete newContent.metadata;

    if (!mediaIds || mediaIds.length <= 0) {
        throw new ValidationError('Empty images list', [{
            field: 'mediaIds',
            type: 'Create content failed',
            msg: 'Images list is empty or undefined',
        }]);
    }

    const tempMedias = await getTempContentMedia(
        {hash: newContent.hash, userId: user.id},
        mediaIds,
    );

    const tempSubtitles = await getTempContentSubtitles(
        {hash: newContent.hash, userId: user.id},
        subtitleIds,
    );

    newContent.authorId = user.id;
    newContent.status = models.contents.STATUS_ENABLED;

    if (moderatorEmail) {
        const moderator = await getUser(moderatorEmail);
        if (moderator) {
            newContent.moderatorId = moderator.id;
        }
    }

    if (forceModEnabled) {
        newContent.moderationStatus = models.contents.MODSTATUS_ENABLED;
    } else {
        if (!newContent.moderatorId) {
            newContent.moderationStatus = models.contents.MODSTATUS_NEW;
        } else {
            newContent.moderationStatus = models.contents.MODSTATUS_ASSIGNED;
        }
    }

    if (newContent.torrentFile) {
        try {
            newContent.torrentFile = Buffer.from(
                newContent.torrentFile, 'base64');
        } catch (e) {
            throw new ValidationError('Invalid encoding', [{
                field: 'torrentFile',
                msg: 'Torrentfile must to be encoded in base64',
                type: 'Encoding',
            }]);
        }
    }

    const torrentFile = newContent.torrentFile;
    delete newContent.torrentFile;

    let content;
    const sequelize = getConnection();
    await sequelize.transaction( async (dbTxn) => {
        // Create content on DB
        content = await models.contents.create(
            newContent,
            {transaction: dbTxn},
        );
        debug(`Content created ${content.uuid}`);

        debug(`Create metadata for ${content.uuid}`);
        await createContentMetadata(metadata, content.uuid, dbTxn);

        debug(`Create medias`);
        await createMedias(tempMedias, content.uuid, dbTxn);
        // Delete temp media from DB
        const tmpMediaIds = tempMedias.map((tmpMedia) => tmpMedia.id);
        await models.tempmedia.scope(
            {method: ['queryList', tmpMediaIds]},
        ).destroy({transaction: dbTxn}).then((deletedCount) => {
            debug(`${deletedCount} temp media destroyed`);
        });

        debug(`Create subtitles`);
        await createSubtitles(tempSubtitles, content.uuid, dbTxn);
        // Delete temp subtitles from DB
        const tmpSubtitleIds = tempSubtitles.map(
            (tmpSubtitle) => tmpSubtitle.id
        );
        await models.tempsubtitle.scope(
            {method: ['queryList', tmpSubtitleIds]},
        ).destroy({transaction: dbTxn}).then((deletedCount) => {
            debug(`${deletedCount} temp subtitles destroyed`);
        });

        let tf;
        try {
            tf = models.torrentfiles.fromTorrentFile(torrentFile);
        } catch (e) {
            if (e instanceof SequelizeValidationError) {
                throw new ValidationError(`Error parsing torrentFile`, [{
                    type: 'Invalid',
                    field: 'torrentFile',
                    message: e.message,
                }]);
            }

            throw e;
        }

        tf.contentUUID = content.uuid;

        if (tf.infoHash !== content.hash) {
            throw new ValidationError(`torrent is invalid`, [{
                field: 'torrentFile',
                type: 'Invalid',
                message: `Torrentfile infoHash does not match with content one`,
            }]);
        }

        if (forceModEnabled &&
            newContent.contentType === models.contents.TYPE_VIDEO
        ) {
            await content.addToElastic();
        }

        if (geoBlocking) {
            await setContentGeoblocking(
                email,
                content.uuid,
                geoBlocking.type,
                geoBlocking.countries,
            );
        }

        await tf.save({
            transaction: dbTxn,
        });

        /* @TODO uncomment post beta release
        // Create content warranty
        const warrantyAmount = content.price * WARRANTY_MULTIPLIER;
        debug(`Creating payments service connection`);
        let paymentsService = await getServiceConnection(
            'com.flixxo.microservice.payments'
        );
        debug(`Calling payments service`);
        await paymentsService.execute(
            'contents:contentWarranty',
            user.uuid, content.uuid, warrantyAccountId, warrantyAmount);
        */
    });

    return {
        success: true,
        uuid: content.uuid,
        torrentFile,
    };

}

/**
 * Create temporary content media
 * @param {string} hash
 * @param {string} url
 * @param {string} name
 * @param {integer} type
 * @param {integer} order
 * @param {string} userEmail
 * @param {string} uuid
 * @return {Promise}
 */
export async function createTempContentMedia(
    hash, url, name, type, order, userEmail, uuid
) {
    debug(`Creating a temporary contentMedia`);
    const user = await getUser(userEmail);
    const tempMedia = await models.tempmedia.create({
        type,
        name,
        url,
        hash,
        order,
        userId: user.id,
        uuid,
    });
    debug(`Temporary media created ${tempMedia.id}`);

    return {
        success: true,
        url: tempMedia.url,
        id: tempMedia.id,
    };
}

/**
 * Create temporary content subtitle
 * @param {string} userEmail
 * @param {string} hash
 * @param {string} url
 * @param {string} lang
 * @param {string} label
 * @param {boolean} closeCaption
 * @param {string} uuid
 * @return {Promise}
 */
export async function createTempContentSubtitle(
    userEmail, hash, url, lang, label, closeCaption, uuid
) {
    debug(`Creating temp content subtitle`);

    const user = await getUser(userEmail);

    let format = '';
    let fileContent = '';

    await axios.get(url).then( (content) => {
        content = content.data;
        const val = validate(content);
        fileContent = content; // to transform later if vtt
        format = val.format;
    });

    debug(`format: ${format}`);

    if ( format.includes('invalid') ) {
        return {
            success: false,
            error: 'Unknown subtitle format.',
            url: '',
            id: '',
        };
    }

    // Convert srt to vtt
    if ( format !== 'vtt' ) {
        let toVttFile = await convert(fileContent, 'vtt');
        format = validate(toVttFile).format;
        if ( format !== 'vtt' ) {
            return {
                success: false,
                error: 'Cannot convert subtitle file to VTT file format',
                url: '',
                id: '',
            };
        }
        toVttFile = toValidMilisecond(toVttFile);
        url = await uploadToS3(toVttFile);
        debug(`converted url: ${JSON.stringify(url, null, 4)}`);
    }

    const subtitle = await models.tempsubtitle.create({
        hash,
        label,
        url,
        lang,
        closeCaption,
        userId: user.id,
        uuid,
    });
    debug(`TempSubtitle created ${subtitle.id}`);

    return {
        success: true,
        url: url,
        id: subtitle.id,
    };
}

/**
 * Upload new subtitles file to s3
 * @param {string} content
 * @return {string}
 */
export async function uploadToS3(content) {
    const s3 = new S3();
    const filename = CONTENT_SUBTITLE_FOLDER + Date.now().toString();
    const params = {
        ACL: CONTENT_SUBTITLE_ACL,
        Body: content,
        Bucket: BUCKET_NAME,
        Key: filename,
    };
    s3.upload(params, async (err, data) => {
        debug(`Uploaded subtitle to S3 error: ${err}, data: ${data}`);
    });
    return BUCKET_HOST + BUCKET_NAME + '/' + filename;
}

/**
 * Check if subtitle format is vtt, rts or invalid
 * @param {string} content
 * @return {object}
 */
export function validate( content ) {
    let valid = false;
    let format = subsrt.detect(content);
    const formats = ['vtt', 'srt', 'sub', 'sbv', 'ssa', 'smi', 'lrc', 'json'];
    for ( let i = 0; i < formats.length; i++ ) {
        if ( format === formats[i] ) {
            valid = true;
            break;
        }
    }
    if ( !valid ) {
        format = 'invalid: ' + format;
    }
    return {format: format};
}

/**
 * To valid milisecond change , by . on vtt file
 * @param {string} content
 * @return {string}
 */
export function toValidMilisecond( content ) {
    const lines = content.split('\r\n');
    let newContent = '';

    for ( let i = 0; i < lines.length; i++ ) {
        if ( lines[i].includes('-->') ) {
            lines[i] = lines[i].replace(',', '.');
            lines[i] = lines[i].replace(',', '.');
        }
        newContent += lines[i] + '\r\n';
    }
    return newContent;
}

/**
 * Convert vtt subtitle to srt
 * @param {string} content
 * @param {string} to
 * @return {string}
 */
export function convert( content, to ) {
    content = stripBom(content);
    const srt = subsrt.convert(content, {format: to});
    return srt;
}


/**
 * Update content boost
 * @param {string} contentId
 * @param {number} amount
 * @param {boolean} decrement
 * @return {Promise}
 */
export async function updateContentBoost(contentId, amount, decrement = false) {
    debug(`Updating content boost ${contentId}`);

    const content = await models.contents.scope('enabled').findByPk(contentId, {
        rejectOnEmpty: true,
    });
    if (!decrement) {
        await content.increment(
            'boost',
            {
                by: amount,
            }
        ).then((model) => {
            return model.reload();
        });
    } else {
        await content.decrement(
            'boost',
            {
                by: amount,
            }
        ).then((model) => {
            return model.reload();
        });
    }
    const url = SLS_HOST + SLS_CONTENTS;
    debug(`Calling to Serverless ${url}`);
    await axios.put(url, {uuid: content.uuid, boost: content.boost});

    debug(`Content boost updated ${content.boost}`);

    return {
        success: true,
        boost: content.boost,
    };
}

/**
 * Get all contents by author
 * @param {string} lang
 * @param {string} email
 * @return {Promise}
 */
export const listAllContentsByAuthor = ['lang', async (lang, email) => {
    debug(`All contents by author ${email}`);
    const user = await getUser(email);
    return await models.contents.scope(
        {method: ['i18n', lang]},
        {method: ['asAuthor', user.id]},
        {method: ['listing']},
    ).findAll({
        raw: true,
    });
}];

/**
 * Delete content
 * @param {string} authorEmail
 * @param {string} contentUUID
 * @return {Promise}
 */
export async function deleteContent(authorEmail, contentUUID) {
    debug(`Delete content ${contentUUID} by author ${authorEmail}`);
    const user = await getUser(authorEmail);

    const content = await models.contents.scope(
        {method: ['asAuthor', user.id]},
    ).findByPk(contentUUID, {
        rejectOnEmpty: true,
    });
    await content.destroy();
    debug(`Content ${contentUUID} deleted`);

    return {
        success: true,
    };
}

/**
 * Update content download status
 * @param {string} hash
 * @param {number} quantity
 * @param {boolean} decrement
 * @return {Promise}
 */
export async function updateContentDownloadStatus(
    hash, quantity = 1, decrement = false
) {
    debug(`Updating content download status ${hash}`);

    const content = await models.contents.findByHash(hash, {
        rejectOnEmpty: true,
    });
    if (!decrement) {
        await content.increment(
            'downloadStatus',
            {
                by: quantity,
            }
        ).then((model) => {
            return model.reload();
        });
    } else {
        await content.decrement(
            'downloadStatus',
            {
                by: quantity,
            }
        ).then((model) => {
            return model.reload();
        });
    }
    debug(`Content download status updated ${content.downloadStatus}`);

    if (content.contentType === models.contents.TYPE_SERIE ) {
        if (content.downloadStatus > 0) {
            await seriesToElastic(content.serieUUID, 'add');
        } else {
            await seriesToElastic(content.serieUUID, 'remove');
        }
    }

    return {
        success: true,
        downloadStatus: content.downloadStatus,
    };
}

/**
 * Get torrent information by hash
 * @param {string} id - uuid or hash
 * @return {Promise<object>}
 */
export async function getTorrentInformation(id) {
    debug(`Getting torrent information about ${id}`);

    const content = await models.contents.scope(
        'enabled',
        'torrentInformation',
    )
        .findByHashOrId(id, {
            rejectOnEmpty: true,
        });

    return content.torrentFile.get({plain: true});
}

/**
 * Get torrentFile
 * @param {string} hash
 * @return {Promise<object>}
 */
export async function getTorrentFile(hash) {
    debug(`Getting torrentFile for ${hash}`);

    const torrentFile = await models.torrentfiles.findByHash(hash, {
        attributes: [
            'infoHash',
            'totalLength',
            'pieceLength',
            'lastPieceLength',
            'torrentData',
        ],
        rejectOnEmpty: true,
    });

    return torrentFile.get({json: true});
}

/**
 * Return temporary content media
 * @param {object} filters
 * @param {array<integer>} tempMediaIds
 * @return {Promise}
 */
export async function getTempContentMedia(filters, tempMediaIds) {
    debug(`Getting temp medias`);
    const tempMedias = await models.tempmedia.findAll({
        where: {
            ...filters,
            id: {
                [Op.in]: tempMediaIds,
            },
        },
        raw: true,
    });
    if (tempMedias.length !== tempMediaIds.length) {
        throw new ValidationError('Any of the images sent does not exist',
            [{
                field: 'images',
                type: 'Create images failed',
                message: 'Any of the images sent does not exist',
            }]
        );
    }

    return tempMedias;
}

/**
 * Return temporary version subtitles
 * @param {object} filters
 * @param {array<integer>} tempSubtitleIds
 * @return {Promise}
 */
export async function getTempContentSubtitles(filters, tempSubtitleIds) {
    debug(`Getting temp subtitles`);
    const tempSubtitles = await models.tempsubtitle.findAll({
        where: {
            ...filters,
            id: {
                [Op.in]: tempSubtitleIds,
            },
        },
        raw: true,
    });
    if (tempSubtitles.length !== tempSubtitleIds.length) {
        throw new ValidationError('Any of the subtitles sent does not exist',
            [{
                field: 'subtitles',
                type: 'Create subtitles failed',
                message: 'Any of the subtitles sent does not exist',
            }]
        );
    }

    return tempSubtitles;
}

/**
 * Create content media
 * @param {Array<Object>} tempMedias
 * @param {string} contentId
 * @param {object} transaction
 * @param {boolean} isDraft
 * @return {Promise}
 */
export async function createMedias(
    tempMedias, contentId, transaction, isDraft = false
) {
    debug(`Creating medias`);
    const mediasToCreate = tempMedias.map((otempMedia) => {
        const tempMedia = {...otempMedia};
        tempMedia.uuid = contentId;
        tempMedia.isDraft = isDraft;
        delete tempMedia.hash;
        delete tempMedia.updatedAt;
        delete tempMedia.createdAt;
        delete tempMedia.id;
        return tempMedia;
    });
    await models.media.bulkCreate(mediasToCreate, {
        validate: true,
        transaction,
    });

    return {
        success: true,
    };
}

/**
 * Create content subtitles
 * @param {Array<Object>} tempSubtitles
 * @param {string} contentId
 * @param {object} transaction
 * @param {boolean} isDraft
 * @return {Promise}
 */
export async function createSubtitles(
    tempSubtitles, contentId, transaction, isDraft = false
) {
    debug(`Creating subtitles`);
    const subtitlesToCreate = tempSubtitles.map((otempSubtitle) => {
        const tempSubtitle = {...otempSubtitle};
        tempSubtitle.uuid = contentId;
        tempSubtitle.isDraft = isDraft;
        delete tempSubtitle.hash;
        delete tempSubtitle.userId;
        delete tempSubtitle.updatedAt;
        delete tempSubtitle.createdAt;
        delete tempSubtitle.id;
        return tempSubtitle;
    });
    await models.subtitle.bulkCreate(subtitlesToCreate, {
        validate: true,
        transaction,
    });

    return {
        success: true,
    };
}

/**
 * Create content metadata
 * @param {Array<Object>} metadata
 * @param {string} contentId
 * @param {object} transaction
 * @return {Promise}
 */
export async function createContentMetadata(
    metadata, contentId, transaction,
) {
    debug(`Creating metadata`);
    const metadataToCreate = metadata.map((meta) => {
        meta.entityId = contentId;
        return meta;
    });

    await models.metadata.bulkCreate(metadataToCreate, {
        validate: true,
        transaction,
    });

    return {
        success: true,
    };
}


/**
 * Set status
 * @param {string} uuid
 * @param {integer} status
 * @param {string} [comment]
 */
export async function setStatus(uuid, status, comment = null) {
    debug(`Set status of`, uuid, 'to', status);

    const content = await models.contents.findByPk(uuid, {
        rejectOnEmpty: true,
    });

    await content.setStatus(status, comment);
}

/**
 * Set status
 * @param {string} email
 * @param {string} uuid
 * @param {integer} status
 * @param {string} [comment]
 */
export async function setStatusByAuthor(email, uuid, status, comment = null) {
    debug(`Set status of`, uuid, 'to', status, `of user ${email}`);

    const user = await models.users.findByEmail(email);

    const content = await models.contents.scope({method: ['asAuthor', user.id]})
        .findByPk(uuid, {
            rejectOnEmpty: true,
        });

    await content.setStatus(status, comment);
}

/**
 * Update new content unnaproved
 * @param {string} email
 * @param {string} uuid
 * @param {object} updatedContent
 * @return {Promise}
 */
export async function updateContent(email, uuid, updatedContent) {
    debug(`Updating content ${uuid} by author ${email}`);
    let newMediaIds = updatedContent.newMediaIds || [];
    let deleteMediaIds = updatedContent.deleteMediaIds || [];

    const user = await getUser(email);

    newMediaIds = newMediaIds
        .filter((x) => x !== null)
        .map((i) => parseInt(i, 10));

    deleteMediaIds = deleteMediaIds.
        filter((x) => x !== null)
        .map((i) => parseInt(i, 10));

    const content = await models.contents.scope(
        {method: ['requestChanges', user.id]},
    ).findByPk(uuid);

    if (!content) {
        throw new ValidationError('Content not available for updates',
            [{
                field: 'uuid',
                type: 'Content error',
                message: 'Content not available for updates',
            }]
        );
    }

    debug(`\tGet actual medias`);
    const medias = await content.getMedia({
        attributes: ['id', 'type', 'order', 'url'],
        raw: true,
    });

    debug(`\tUpdating content ${content.uuid}`);

    updatedContent.moderationStatus = models.contents.MODSTATUS_CHANGES_REVIEW;
    updatedContent.moderatedAt = new Date();

    const sequelize = getConnection();

    debug(`\tMapping deleteMedias with actual medias`);
    const deleteMedias = deleteMediaIds.map((id) => {
        const media = medias.find((x) => x.id === id);
        if (!media) {
            debug(deleteMediaIds);
            debug(`${id} does not exists in`, medias);
            throw new ValidationError('Non-existent media to delete',
                [{
                    field: 'images',
                    type: 'Delete images failed',
                    message: 'Non-existent media to delete',
                }]
            );
        }
        return media;
    });

    debug(`\tFetching new medias`);
    const newMedia = await getTempContentMedia(
        {hash: content.hash, userId: user.id},
        newMediaIds,
    );

    const finalMedias = [
        ...medias.filter((x) => deleteMediaIds.indexOf(x.id) === -1),
        ...newMedia,
    ];

    debug(`\tFinal medias: `, finalMedias);

    const counts = finalMedias.reduce((r, m) => {
        r[m.type]++;
        return r;
    }, {
        [models.media.TYPE_COVER]: 0,
        [models.media.TYPE_THUMB]: 0,
    });

    debug(`\tMedias by type`, counts);

    if (counts[models.media.TYPE_COVER] !== 1) {
        throw new ValidationError(`Must to be one cover`);
    }

    debug(counts[models.media.TYPE_THUMB], '>3 <1');
    if (counts[models.media.TYPE_THUMB] > 3 ||
        counts[models.media.TYPE_THUMB] < 1
    ) {
        throw new ValidationError(`Invalid number of thumbs`);
    }

    debug(`\tStart transaction`);
    await sequelize.transaction( async (dbTxn) => {
        debug(`\tSaving model`);
        await content.update(
            updatedContent,
            {
                'fields': [
                    'moderationMessage',
                    'moderationStatus',
                    'moderatedAt',
                    'credits',
                    'rating',
                    'releaseDate',
                    'finishDate',
                    'privacy',
                    'seederFee',
                    'authorFee',
                    'payType',
                    'categoryId',
                    'audioLang',
                    'bodyFormat',
                    'tags',
                ],
                'transaction': dbTxn,
            }
        );
        debug(`\t\tContent updated ${content.uuid}`);

        debug(`\tSaving metadata`);
        if (updatedContent.metadata && updatedContent.metadata.length) {
            await models.metadata.destroy(
                {
                    where: {'entityId': content.uuid}, transaction: dbTxn}
            ).then((deletedCount) => {
                debug(`${deletedCount} metadata destroyed`);
            });

            await createContentMetadata(
                updatedContent.metadata,
                content.uuid,
                dbTxn);
            debug(`Metadata created.`);
        }

        if (newMedia.length) {
            debug(`\tCreating new medias`);
            await createMedias(newMedia, content.uuid, dbTxn);
            debug(`\t\tNew media created: ${newMedia.length}`);
        }

        if (deleteMedias.length) {
            debug(`\tDeleting media`);
            await deleteMedia(deleteMedias, dbTxn);
            debug(`\t\tDeleted media: ${deleteMedias.length}`);
        }

    });

    return {
        success: true,
    };
}

/**
 * Delete media
 * @param {array<integer>} medias
 * @param {object} transaction
 * @param {boolean} deleteFile
 */
export async function deleteMedia(medias, transaction, deleteFile = true) {
    // Delete media from DB
    const mediaIds = medias.map((media) => media.id);
    await models.media.scope(
        {method: ['queryList', mediaIds]},
    ).destroy({transaction}).then((deletedCount) => {
        debug(`${deletedCount} media destroyed`);
    });

    if (deleteFile) {
        // Get media URLs
        const mediaURLs = medias.map((media) => {
            const s3Object = media.url.replace(
                BUCKET_HOST + BUCKET_NAME + '/', ''
            );
            return {Key: s3Object};
        });
        const s3 = new S3();
        const params = {
            Bucket: BUCKET_NAME,
            Delete: {
                Objects: mediaURLs,
            },
        };
        // Delete media from S3
        const deletedDocs = await new Promise((resolve, reject) => {
            s3.deleteObjects(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data.Deleted);
                }
            });
        });
        debug(`Deleted documents from S3: ${deletedDocs.length}`);
    }
}

/**
 * Delete subtitles
 * @param {array<object>} subtitles
 * @param {object} transaction
 * @param {boolean} deleteFile
 */
export async function deleteSubtitles(
    subtitles, transaction, deleteFile = true
) {
    // Delete subtitles from DB
    const subtitleIds = subtitles.map((subtitle) => subtitle.id);
    await models.subtitle.scope(
        {method: ['queryList', subtitleIds]},
    ).destroy({transaction}).then((deletedCount) => {
        debug(`${deletedCount} subtitles destroyed`);
    });

    if (deleteFile) {
        // Get subtitle URLs
        const subtitleURLs = subtitles.map((subtitle) => {
            const s3Object = subtitle.url.replace(
                BUCKET_HOST + BUCKET_NAME + '/', ''
            );
            return {Key: s3Object};
        });
        const s3 = new S3();
        const params = {
            Bucket: BUCKET_NAME,
            Delete: {
                Objects: subtitleURLs,
            },
        };
        // Delete subtitle from S3
        const deletedDocs = await new Promise((resolve, reject) => {
            s3.deleteObjects(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data.Deleted);
                }
            });
        });
        debug(`Deleted documents from S3: ${deletedDocs.length}`);
    }
}

/**
 * Set S3 Availability
 * @param {string} hash
 * @param {string} available
 */
export async function setS3Availability(hash, available) {
    debug(`Set S3 availability of`, hash, 'to', available);

    const content = await models.contents.findByHash(hash, {
        rejectOnEmpty: true,
    });

    await content.setS3Availability(available);

    if (content.contentType === models.contents.TYPE_SERIE) {
        await seriesToElastic(content.serieUUID, 'update');
    }
}

/**
 * Check if hash alrady exists
 * @param {string} hash
 * @return {object}
 */
export async function existsHash(hash) {
    debug(`Check if hash exists ${hash}`);

    hash = hash.toLowerCase();

    if (!hash.match(/^[0-9a-f]{40}$/)) {
        throw new ValidationError(
            'Invalid hash',
            [{
                field: 'hash',
                type: 'invalid',
                message: 'Invalid hash format',
            }]
        );
    }

    const result = await models.contents.existsHash(hash);
    return {
        hash,
        exists: result,
    };
}

/**
 * Get Content Geoblocking
 * @param {string} email
 * @param {uuid} contentId
 * @return {Promise}
 */
export async function getContentGeoblocking(email, contentId) {
    debug(`Get content geoblocking`);

    const user = await getUser(email);
    const content = await models.contents.scope(
        {method: ['asAuthor', user.id]},
    ).findByPk(contentId);

    if (!content) {
        throw new ValidationError('Geoblocking error', [{
            field: 'uuid',
            type: 'ForbiddenError',
            msg: 'Invalid user or content uuid',
        }]);
    }

    let geoBlocking;
    geoBlocking = await content.getGeoBlocking({scope: 'detailled'});

    if (!geoBlocking) {
        geoBlocking = {
            type: 0,
            countries: [],
        };
    }

    return geoBlocking;
}

/**
 * Set Content Geoblocking
 * @param {string} email
 * @param {uuid} contentId
 * @param {integer} type
 * @param {array<integer>} countries
 * @return {Promise}
 */
export async function setContentGeoblocking(email, contentId, type, countries) {
    debug(`Set content geoblocking`);

    const user = await getUser(email);
    const content = await models.contents.scope(
        {method: ['asAuthor', user.id]},
    ).findByPk(contentId);

    if (!content) {
        throw new ValidationError('Geoblocking error', [{
            field: 'uuid',
            type: 'ForbiddenError',
            msg: 'Invalid user or content uuid',
        }]);
    }

    let geoBlocking;
    geoBlocking = await content.getGeoBlocking();

    if (!geoBlocking) {
        geoBlocking = await models.geoblocking.create({
            targetUUID: contentId,
            entityType: models.geoblocking.ETYPE_CONTENT,
            type,
        });
    } else {
        geoBlocking.update({type}, {fields: ['type']});
    }

    await geoBlocking.setCountries(countries);

    await content.updateElastic();

    return {
        success: true,
    };
}

/**
 * Delete Content Geoblocking
 * @param {string} email
 * @param {uuid} contentId
 * @return {Promise}
 */
export async function deleteContentGeoblocking(email, contentId) {
    debug(`Delete content geoblocking`);

    const user = await getUser(email);
    const content = await models.contents.scope(
        {method: ['asAuthor', user.id]},
    ).findByPk(contentId);

    if (!content) {
        throw new ValidationError('Geoblocking error', [{
            field: 'uuid',
            type: 'ForbiddenError',
            msg: 'Invalid user or content uuid',
        }]);
    }

    const geoBlocking = await content.getGeoBlocking();

    if (geoBlocking) {
        await geoBlocking.destroy();
    }

    return {
        success: true,
    };
}

/**
 * Get a temporary web link
 * @param {string} uuid
 * @param {string} [keyId] - Amazon accessKeyId
 * @param {string} [accessKey] - Amazon secretAccessKey
 * @return {Promise<String>}
 */
export async function getWebLink(uuid, keyId = null, accessKey = null) {
    debug(`Generate temporary url for content ${uuid}`);

    const ops = {};

    if (keyId) ops.accessKeyId = keyId;
    if (accessKey) ops.secretAccessKey = accessKey;

    const s3 = new S3(ops);
    const {hash} = await models.contents.findOne({
        attributes: ['hash'],
        plain: true,
        where: {uuid},
        rejectOnEmpty: true,
        paranoid: false,
    });

    return s3.getSignedUrl('getObject', {
        Bucket: UPLOAD_BUCKET_NAME,
        Key: `${hash}.mp4`,
        Expires: WEBLINKS_EXPIRATION,
    });
}

/**
 * Get the seederFee price in at deteminate moment
 * @param {string} uuid - Content UUID
 * @param {number} ts - Timestamp
 * @return {Promise<string>} - Price formated in string
 */
export async function getSeederPriceAt(uuid, ts) {
    debug(`Get seederFee price for ${uuid}, on ${ts}`);
    const content = await models.contents.findByPk(uuid, {
        attributes: ['uuid', 'seederFee'],
        paranoid: false,
        rejectOnEmpty: true,
    });

    debug(`\tGetting price`);
    const price = await content.getSeederPriceAt(ts);

    return price.toFixed(18);
}

/**
 * Check if content has metadata
 * @param {string} hash
 * @return {bool}
 */
export async function hasmeta(hash) {
    debug(`Checking if content ${hash} has metadata`);
    const content = await models.contents.findByHash(hash, {
        attributes: ['hash', 'uuid'],
        include: [{
            model: models.torrentfiles,
            as: 'torrentFile',
            attributes: ['createdAt'],
            require: false,
        }],
        paranoid: false,
        rejectOnEmpty: true,
    });

    return {
        has: !!content.torrentFile,
    };
}


/**
 * Set content metadata
 * @param {string} hash
 * @param {string} data
 * @return {bool}
 */
export async function setmeta(hash, data) {
    debug(`Set metadata of content ${hash} has metadata`);

    try {
        data = Buffer.from(data, 'base64');
    } catch (e) {
        throw new ValidationError(`Data must be base64`, [{
            type: 'invalid',
            field: 'data',
            message: 'Invalid data format',
        }]);
    }

    const content = await models.contents.findByHash(hash, {
        attributes: ['hash', 'uuid'],
        include: [{
            model: models.torrentfiles,
            as: 'torrentFile',
            attributes: ['createdAt'],
            require: false,
        }],
        paranoid: false,
        rejectOnEmpty: true,
    });

    if (content.torrentFile) {
        throw new ValidationError(`Content already has metadata`, [{
            type: 'invalid',
            field: 'data',
            message: 'Already created',
        }]);
    }

    let tf;

    try {
        tf = models.torrentfiles.fromTorrentFile(data);
    } catch (e) {
        throw new ValidationError(`Error parsing torrentFile`, [{
            type: 'invalid',
            field: 'data',
            message: e.message,
        }]);
    }

    tf.setContent(content);

    if (tf.infoHash !== content.hash) {
        throw new ValidationError(`torrent is invalid`, [{
            field: 'torrentFile',
            type: 'invalid',
            message: `Torrentfile infoHash does not match with content one`,
        }]);
    }

    await tf.save();

    return true;
}
