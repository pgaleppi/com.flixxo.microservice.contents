
import Promise from 'bluebird';
import models from '../models';
import {S3} from 'aws-sdk';
import {createDebug} from '../logger';
import config from '../config';
const TEMP_MEDIA_EXPIRE_AT = config.get('media:expirationTime');
const BUCKET_NAME = config.get('aws:s3:bucket');
const BUCKET_HOST = config.get('aws:s3:host');

const debug = createDebug(__filename);

/**
 * Clean temporary media
 */
export default async () => {
    try {
        debug(`Looking for temp media`);
        const nowTime = new Date().getTime();
        const expireTime = nowTime - TEMP_MEDIA_EXPIRE_AT * 1000;
        const expiredDate = new Date(expireTime);
        const expiredTmpMedias = await models.tempmedia.scope(
            {method: ['expired', expiredDate]}
        ).findAll();
        if (expiredTmpMedias.length > 0) {
            const tempMediaURL = expiredTmpMedias.map((tmpMedia) => {
                const s3Object = tmpMedia.url.replace(
                    BUCKET_HOST + BUCKET_NAME + '/', ''
                );
                return {Key: s3Object};
            });
            const s3 = new S3();
            const params = {
                Bucket: BUCKET_NAME,
                Delete: {
                    Objects: tempMediaURL,
                },
            };
            const deletedDocs = await new Promise((resolve, reject) => {
                s3.deleteObjects(params, (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data.Deleted);
                    }
                });
            });
            debug(`Deleted documents: ${deletedDocs.length}`);
            const tmpMediaIds = expiredTmpMedias.map((tmpMedia) => tmpMedia.id);
            await models.tempmedia.scope(
                {method: ['queryList', tmpMediaIds]},
            ).destroy().then((deletedCount) => {
                debug(`${deletedCount} temp media destroyed`);
            });
        } else {
            debug(`There is no temp media to destroy`);
        }

    } catch (e) {
        debug(`Error cleaning temp media ${e}`);
        process.stderr.write(`${e.message}\n`);
    }
};
