
import Promise from 'bluebird';
import {v4 as uuid} from 'uuid';
import {createDebug} from '../../logger';
import config from '../../config';
import {RPCTimeOutError, RPCRemoteError} from '../errors';

const debug = createDebug(__filename);

const RPC_TIMEOUT = config.get('rpc:timeout') || 2000;

/**
 * Remote command interface
 */
export default class Command {

    /**
     * Create new remote service client
     * @param {amqp.connection} connector
     * @param {string} service Service id, like com.flixxo.microservice.xxx
     */
    constructor(connector, service) {
        debug(`Created fake command interface for ${service}`);
        this._connector = connector;
        this._channel = null;
        this._service = service;
        this._commands = [];
    }

    /**
     * @override
     */
    async getContext() {
        return {};
    }

    /**
     * @override
     */
    async setConext() {}

    /**
     * @override
     */
    async bindContext() {
        return this;
    }

    /**
     * Add new command to the pool
     * @param {object} pack Command package
     * @param {string} pack.uid Command UID
     * @param {string} pack.cmd Command name
     * @param {Array<mixed>} pack.params Params
     * @param {object} q Queue object
     * @param {object} [options] Options
     * @return {Promise}
     */
    async addCommand(pack, q, options = {}) {
        let resolve;
        let reject;

        const promise = new Promise((_resolve, _reject) => {
            resolve = _resolve;
            reject = _reject;
        });

        const command = {
            uid: pack.uid,
            q,
            topointer: setTimeout(() => {
                this.rejectCommand(pack.uid, new RPCTimeOutError(command));
            }, RPC_TIMEOUT),
            resolve,
            reject,
        };

        debug(`Command ${command.uid} sent`);
        this._commands.push(command);

        this._connector.exec(this, this._service, pack);

        return promise;
    }

    /**
     * Clean active command
     * @param {object} command Command object
     * @return {object} Command object
     */
    async cleanCommand(command) {
        debug(`Cleaning command ${command.uid}`);
        this._commands = this._commands.filter((x) => x.uid !== command.uid);

        if (command.topointer) {
            clearTimeout(command.topointer);
        }

        return command;
    }

    /**
     * Handle a remote response
     * @param {object} response RPC response object
     */
    handleResponse(response) {
        debug(`Reponse!`, response);
        if (!response) {
            debug(`null response?`);
            return;
        }

        const uid = response.uid;
        const command = this._commands
            .find((x) => x.uid === uid);

        if (!command) {
            debug(`Unknow uid command response: ${uid}`);
            return;
        }

        debug(`${uid}: response`);

        this.cleanCommand(command);

        const rpack = response;

        if (rpack.error) {
            debug(`${uid}: has rejected`, rpack.error);
            command.reject(new RPCRemoteError(rpack.error));
        } else {
            command.resolve(rpack.result);
        }
    }

    /**
     * Reject active command
     * @param {string} uid
     * @param {Error} error
     */
    rejectCommand(uid, error) {
        debug(`Remote command reject`, error);
        const command = this._commands
            .find((x) => x.uid === uid);

        if (!command) {
            debug(`Unknow uid command ${uid} for reject`);
            throw new Error('Command does not exists');
        }

        debug(`${uid}: rejected`, error);

        this.cleanCommand(command);

        command.reject(error);
    }

    /**
     * Execute remote command
     * @param {string} cmd Command
     * @param {Array<mixed>} [...params] Params
     * @return {Promise<mixed|Error>}
     */
    async execute(cmd, ...params) {
        const uid = uuid();
        debug(`execute ${this._service}::${cmd} as ${uid}`);

        const pack = {
            uid,
            cmd,
            params,
        };

        return this.addCommand(pack);
    }
}
