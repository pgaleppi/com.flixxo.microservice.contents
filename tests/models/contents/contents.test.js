jest.mock('axios');

import {EmptyResultError} from 'sequelize';
import {syncDb, loadFixture, loadData} from '../../helpers/db';
import models from '../../../src/models';
import Decimal from 'decimal.js';

jest.mock('../../../src/sqsrpc');

describe('Check default values', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
    });

    it('Must return def value', async () => {
        let content = await models.contents.create({
            categoryId: 1,
            hash: 'ffffa3ee5e6b4b0d3255bfef95601890afd80709',
            authorId: 100,
            seederFee: 10,
            authorFee: 10,
            thumb: 'https://dummyimage.com/600x400/000/fff',
        });

        expect(content.contentType).toBe(models.contents.TYPE_VIDEO);
        expect(content.bodyFormat).toBe(models.contents.FORMAT_MARKDOWN);
        expect(content.audioLang).toBe('en');
        expect(content.payType).toBe(models.contents.PAYTYPE_VIEW);
        expect(content.status).toBe(models.contents.STATUS_NEW);
        expect(content.privacy).toBe(models.contents.PRIVACY_PUBLIC);
        expect(content.moderationStatus).toBe(models.contents.MODSTATUS_NEW);
        expect(content.priority).toBe(0);
    });

});

describe('Check scopes', () => {

    describe('Default', () => {

        beforeEach(async (done) => {
            await syncDb();
            await loadFixture('users');
            await loadData('categories');
            await loadFixture('contents-for-scope-check');
            await loadFixture('metadata-for-scope-check');
            setTimeout(done, 1000); // for filters
        }, 0);


        test('Must to list one in enabled scope', async () => {
            let list = await models.contents
                .scope(['enabled', {method: ['i18n', 'en']}])
                .findAll({
                    sql: true,
                });
            expect(list).toHaveLength(1);
            expect(list[0].title).toBe('Title en 1');
        });

    });

    describe('i18n', () => {

        beforeEach(async (done) => {
            await syncDb();
            await loadFixture('users');
            await loadData('categories');
            await loadFixture('contents-for-scope-check');
            await loadFixture('metadata-for-scope-check');
            setTimeout(done, 1000); // for filters
        }, 0);


        test('Must to list one in EN i18n scope', async () => {
            let list = await models.contents.scope([
                'enabled',
                {method: ['i18n', 'en']},
            ]).findAll();
            expect(list).toHaveLength(1);
            expect(list[0].title).toBe('Title en 1');
            expect(list[0].body).toBe('Body en 1');
        });

        test('Must to list one in ES i18n scope', async () => {
            let list = await models.contents.scope([
                'enabled',
                {method: ['i18n', 'es']},
            ]).findAll();
            expect(list).toHaveLength(1);
            expect(list[0].title).toBe('Titulo es 1');
            expect(list[0].body).toBe('Body es 1');
        });

        test('Must to fallback to audioLang if no metadata present for the given language', async () => {
            let list = await models.contents.scope([
                'enabled',
                {method: ['i18n', 'pt']},
            ]).findAll();
            expect(list).toHaveLength(1);
            expect(list[0].title).toBe('Title en 1');
            expect(list[0].body).toBe('Body en 1');
        });

        test('Must denormalize including available languages', async () => {
            let list = await models.contents.scope([
                'enabled',
                {method: ['i18n', 'en']},
            ]).findAll();
            expect(list).toHaveLength(1);
            const content = list[0];
            const denormalizedContent = await content.denormalize();
            expect(denormalizedContent.title).toMatchObject({es: 'Titulo es 1', en: 'Title en 1'});
            expect(denormalizedContent.body).toMatchObject({es: 'Body es 1', en: 'Body en 1'});
        });
    });

    describe('asModerator', () => {

        beforeEach(async (done) => {
            await syncDb();
            await loadFixture('users');
            await loadData('categories');
            await loadFixture('contents-for-as-moderator-scope');
            return done();
        });

        test(`Must to get contents to moderate by moderator`, async () => {
            let res = await models.contents.scope([
                'listing',
                {method: ['asModerator', 101]},
            ]).findAll();

            expect(res).toHaveLength(2);
        });

    });

});

describe('Check hooks', () => {

    beforeEach(async (done) => {
        await syncDb();
        await loadFixture('users');
        await loadData('categories');
        setTimeout(done, 1000); // for filters
    }, 0);

    it('Must to create status change register', async () => {
        let m = await models.contents.create({
            title: 'Dummy title',
            hash: 'da39a3ee5e6b4b0d3255bfef95601890afd81709',
            body: 'Dummy body',
            categoryId: 1,
            authorId: 100,
            seederFee: 10,
            authorFee: 10,
            thumb: 'https://dummyimage.com/600x400/000/fff',
        });

        await m.setStatus(0x30, 'No reason');

        let updates = await models.contentupdates.findByContent(m.uuid, {
            raw: true,
        });

        expect(updates.length).toBe(1);
        expect(updates[0].uuid).toBe(m.uuid);
        expect(updates[0].status).toBe(0x30);
        expect(updates[0].privacy).toBeNull();
        expect(updates[0].moderationStatus).toBeNull();
        expect(updates[0].moderatorId).toBeNull();
    });

});

describe.skip('Check virtuals', () => {
    beforeEach(async (done) => {
        await syncDb();
        await loadFixture('users');
        await loadData('categories');
        done();
    }, 0);

    describe('isIndexable', () => {
        test('Must to change isIndexable when change status', async () => {
            let content = models.contents.build({
                title: 'Dummy title',
                body: 'Dummy body',
                categoryId: 1,
                authorId: 100,
                seederFee: 10,
                authorFee: 10,
                status: models.contents.STATUS_ENABLED,
                moderationStatus: models.contents.MODSTATUS_ENABLED,
            });

            expect(content.isIndexable).toBeTruthy();

            content.status = models.contents.STATUS_PAUSED;

            expect(content.isIndexable).toBeFalsy();
            expect(content.changed('isIndexable')).toBeTruthy();
            expect(content.previous('isIndexable')).toBeTruthy();
        });
    });

});

describe('#denormalize', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contents-for-denormalize');
    });

    test('Must to return demormalized content', async () => {
        const content = await models.contents.findByPk(
            '3000000-c3d1-0000-0000-000000000001',
            {
                rejectOnEmpty: true,
            }
        );

        const res = await content.denormalize();

        expect(res).toMatchSnapshot();
    });

});

describe('Tags', () => {

    beforeEach(async (done) => {
        await syncDb();
        await loadFixture('users');
        await loadData('categories');
        setTimeout(done, 1000); // for filters
    }, 0);

    it('Must be create a new Content  with tags', async () => {
        let m = await models.contents.create({
            hash: 'da39a3ee5e6b4b0d3255bfef95601890afd81709',
            categoryId: 1,
            authorId: 100,
            seederFee: 10,
            authorFee: 10,
            Tags: [
                {tag: 'tag1'},
                {tag: 'tag2'},
                {tag: 'tag3'},
            ],
        }, {
            include: models.tags,
        });

        let tags = await m.getTags({raw: true});

        expect(tags.length).toBe(3);
    });

    it('Must to not repeat tags', async () => {
        await loadFixture('tags');
        await loadFixture('contents-for-tags-check');

        let contents = await models.contents.findAll();

        await contents[0].findAndSetTags([
            'tag10',
            'tag11',
        ]);

        await contents[1].findAndSetTags([
            'tag11',
            'tag12',
        ]);

        let tagsa = await contents[0].getTags();

        let tagsb = await contents[1].getTags();

        expect(tagsa.map((x) => x.tag)).toEqual(['tag10', 'tag11']);
        expect(tagsb.map((x) => x.tag)).toEqual(['tag11', 'tag12']);
    });

    it('Must to create new tags', async () => {
        await loadFixture('tags');
        await loadFixture('contents-for-tags-check');

        let contents = await models.contents.findAll();

        await contents[0].findAndSetTags([
            'tag10',
            'tag11',
            'newtag1',
        ]);

        await contents[1].findAndSetTags([
            'tag11',
            'tag12',
            'newtag1',
            'newtag2',
        ]);

        let tagsa = await contents[0].getTags();

        let tagsb = await contents[1].getTags();

        expect(tagsa.map((x) => x.tag)).toEqual(['tag10', 'tag11', 'newtag1']);
        expect(tagsb.map((x) => x.tag)).toEqual(
            ['tag11', 'tag12', 'newtag1', 'newtag2']);
    });

    it('Must to create all tags', async () => {
        await loadFixture('contents-for-tags-check');

        let content = await models.contents.findOne();

        await content.findAndSetTags([
            'newtag1',
            'newtag2',
            'newtag3',
            'newtag4',
        ]);

        let tags = await content.getTags();
        expect(tags.map((x) => x.tag)).toEqual(
            ['newtag1', 'newtag2', 'newtag3', 'newtag4']
        );
    });

});

describe('ByFolloweds scope', () => {
    beforeEach(async () => {
        await syncDb(true);
        await loadData('categories');
        await loadFixture('contents-for-followers-scope');
    });

    test(`Must to get contents from followeds users`, async () => {
        let contents = await models.contents.scope({
            method: ['byFolloweds', 103],
        }).findAll();

        const expected = [
            {uuid: '7cdeff76-c4df-4bf2-a810-c0f054033a28', authorId: 100},
            {uuid: '7cdeff76-c4df-4bf2-a810-c0f054033a29', authorId: 100},
            {uuid: '7cdeff76-c4df-4bf2-a810-c0f054033a30', authorId: 101},
        ];

        expect(contents).toHaveLength(3);
        expect(
            contents.map(({uuid, authorId}) => ({uuid, authorId}))
        )
            .toEqual(expected);
    });

    test(`Must to get empty for user without followeds`, async () => {
        let contents = await models.contents.scope({
            method: ['byFolloweds', 104],
        }).findAll();

        expect(contents).toHaveLength(0);
    });
});

describe('#setStatus', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contents-for-set-status');
    });

    test(`Must to auto set cancelDate on pause`, async () => {
        let content = await models.contents.findByPk(
            '41a0ff76-a4df-2bf2-a810-c0f054033a28');

        expect(content.status).toBe(models.contents.STATUS_ENABLED);
        await content.setStatus(models.contents.STATUS_PAUSED, 'abcdef');
        expect(content.status).toBe(models.contents.STATUS_PAUSED);
        expect(content.cancelDate).not.toBeNull();
    });

    test(`Must to auto set cancelDate on cancel`, async () => {
        let content = await models.contents.findByPk(
            '41a0ff76-a4df-2bf2-a810-c0f054033a28');

        expect(content.status).toBe(models.contents.STATUS_ENABLED);
        await content.setStatus(models.contents.STATUS_PAUSED, 'abcde');
        expect(content.status).toBe(models.contents.STATUS_PAUSED);
        expect(content.cancelDate).not.toBeNull();
    });

    test(`Must to keep cancelDate on paused -> cancel`, async () => {
        let lastCancelDate = null;
        let content = await models.contents.findByPk(
            '41a0ff76-c4df-4bf2-a810-c0f054033a29');

        expect(content.status).toBe(models.contents.STATUS_PAUSED);
        expect(content.cancelDate).not.toBeNull();
        lastCancelDate = content.cancelDate;
        await content.setStatus(models.contents.STATUS_CANCEL, 'I die');
        expect(content.status).toBe(models.contents.STATUS_CANCEL);
        expect(content.cancelDate).toBe(lastCancelDate);
    });

    test(`Must to clear cancelDate on re-enable`, async () => {
        let content = await models.contents.findByPk(
            '41a0ff76-c4df-4bf2-a810-c0f054033a29');

        expect(content.status).toBe(models.contents.STATUS_PAUSED);
        expect(content.cancelDate.getTime()).toBe(1534478719143);
        await content.setStatus(models.contents.STATUS_ENABLED);
        expect(content.status).toBe(models.contents.STATUS_ENABLED);
        expect(content.cancelDate).toBeNull();
    });

    test(`Must to throw error if try change status on cenceled`, async () => {
        let content = await models.contents.findByPk(
            '41a0ff76-c4df-4bf2-a810-c0f054033a30');

        expect(content.status).toBe(models.contents.STATUS_CANCEL);
        try {
            await content.setStatus(models.contents.STATUS_ENABLED);
        } catch (err) {
            expect(err.message).toBe(
                'Validation error: A canceled content can\'t change status');
            return;
        }

        throw new Error('Pass?');
    });

    test(`Must to throw error if comment is not set on pause`, async () => {
        let content = await models.contents.findByPk(
            '41a0ff76-a4df-2bf2-a810-c0f054033a28');

        try {
            await content.setStatus(models.contents.STATUS_PAUSED);
        } catch (err) {
            expect(err.message).toBe(
                'Validation error: You must to write a reason');
            return;
        }

        throw new Error('Pass?');
    });

    test(`Must to throw error if comment is not set on cancel`, async () => {
        let content = await models.contents.findByPk(
            '41a0ff76-a4df-2bf2-a810-c0f054033a28');

        try {
            await content.setStatus(models.contents.STATUS_CANCEL);
        } catch (err) {
            expect(err.message).toBe(
                'Validation error: You must to write a reason');
            return;
        }

        throw new Error('Pass?');
    });
});

describe.skip('#prepareVersion', () => {
    // TODO, check i18n logic

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contents-for-add-version');
    });

    test('Must to create a new version from a valid content', async () => {
        let content = await models.contents.findByPk(
            '41a0ff76-0001-2bf2-a810-c0f054033a28', {
                rejectOnEmpty: true,
            }
        );

        let version = content.prepareVersion({
            title: 'This is a new title',
        });

        expect(version);
        expect(version).toBeInstanceOf(models.contentversion);
        expect(version.title).toBe('This is a new title');
        expect(version.contentUUID).toBe(content.uuid);
    });

});

describe('#applyVersion', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contents-for-apply-version');
    });

    test(`Must to apply valid new version`, async () => {
        const content = await models.contents
            .scope({method: ['i18n', 'en']})
            .findByPk(
            '00000000-0019-0000-0000-000000000000'
            , {include: ['media', 'subtitle']}
        );

        await content.applyVersion('00000000-0020-0000-0000-000000000000');
        await content.reload();

        expect(content.title).toBe('Content version title');
        expect(content.body).toBe('Content version body');
        expect(content.audioLang).toBe('en');
        expect(content.categoryId).toBe(5);
        expect(content.rating).toBe(3);
        expect(content.duration).toBe(120);
        expect(content.media.length).toBe(3);
        expect(content.subtitle.length).toBe(2);

        let version = await content.getCurrentVersion();
        expect(version.uuid).toBe('00000000-0020-0000-0000-000000000000');
        expect(version.status).toBe(models.contentversion.STATUS_ACCEPTED);
    });

    test(`Must to fail without valid version`, async () => {
        const content = await models.contents.findByPk(
            '00000000-0019-0000-0000-000000000000'
        );

        try {
            await content.applyVersion('00000000-0030-0000-0000-000000000000');
        } catch (e) {
            expect(e).toBeInstanceOf(EmptyResultError);
            return;
        }

        throw new Error('Pass');
    });
});

describe('#getSeederPriceAt', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contentversion-for-get-last-seederfee-at');
    });

    test(`Must to take valid price from versions (last)`, async () => {
        const content = await models.contents.findByPk(
            '41a0ff76-faf0-2bf2-a810-c0f054033a28'
        );
        const last = await content.getSeederPriceAt(1563303500000);
        expect(last).toBeInstanceOf(Decimal);
        expect(last.toNumber()).toEqual(2);
    });

    test(`Must to take own price if has not version yet`, async () => {
        const content = await models.contents.findByPk(
            '41a0ff76-faf0-2bf2-a810-c0f054033a28'
        );
        const last = await content.getSeederPriceAt(1563300000000);
        expect(last).toBeInstanceOf(Decimal);
        expect(last.toFixed()).toEqual('0.000000123000012309');
    });

    test(`Must to take own price if has not versions`, async () => {
        const content = await models.contents.findByPk(
            '41a0ff76-faf0-2bf2-a810-c0f054033a29'
        );
        const last = await content.getSeederPriceAt(1563390000000);
        expect(last).toBeInstanceOf(Decimal);
        expect(last.toFixed()).toEqual('123');
    });

    test(`Must to throw error if does not has seederFee`, async () => {
        const content = await models.contents.findByPk(
            '41a0ff76-faf0-2bf2-a810-c0f054033a29',
            {attributes: ['uuid']}
        );
        try {
            await content.getSeederPriceAt(1563390000000);
        } catch (e) {
            expect(e.message).toBe('Content has not own seederFee');
            return;
        }

        throw new Error('Pass');
    });
});

describe('.existsHash', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contents-for-exists-hash');
    });

    test(`Must to be true on existing hash`, async () => {
        const res = await models.contents.existsHash(
            'ebff5c06d2b76d524833329c39ffaf4847957ee4');
        expect(res).toBeTruthy();
    });

    test(`Must to be false on unexisting hash`, async () => {
        const res = await models.contents.existsHash(
            'ebff5c06d2b76d524830009c39ffaf4847957ee4');
        expect(res).toBeFalsy();
    });

    test(`Must to be true on existing hash (unavailable)`, async () => {
        const res = await models.contents.existsHash(
            'ebff5c06d2b76d524833329c39ffaf4847957ee2');
        expect(res).toBeTruthy();
    });

    test(`Must to be true on existing hash (deleted)`, async () => {
        const res = await models.contents.existsHash(
            'ebff5c06d2b76d524833329c39ffaf4847957ee2');
        expect(res).toBeTruthy();
    });
});
