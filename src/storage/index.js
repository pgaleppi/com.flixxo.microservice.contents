
import Promise from 'bluebird';
import Sequelize from 'sequelize';
import sqlString from 'sequelize/lib/sql-string';
import config from '../config';
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

let connection;

const DB_URI = config.get('storage:uri') || null;
const DB_DIALECT = config.get('storage:dialect') || null;
const DB_HOST = config.get('storage:host') || 'localhost';
const DB_HOST_PORT = config.get('storage:port') || null;
const DB_HOST_PROTOCOL = config.get('storage:protocol') || null;
const DB_USERNAME = config.get('storage:username') || '';
const DB_PASSWORD = config.get('storage:password') || '';
const DB_DATABASE = config.get('storage:schema') || null;
const DB_LOGGING = config.get('storage:logging') || false;
const DB_POOL = config.get('storage:pool') || {};
const DB_SSL = config.get('storage:usessl') || false;

const defOptions = {
    logging(msg) {
        if (DB_LOGGING) {
            debug(msg);
        }
    },
    define: {
        underscored: false,
        freezeTableName: false,
        charset: 'utf8',
        timestamps: true,
    },
    pool: {
        min: 1,
        max: 15,
        idle: 30000,
        acquire: 10000,
        ...DB_POOL,
    },
};

/**
 * Dirty hack to forcefully disable connection pooling
 **/
Sequelize.addHook('afterInit', function(sequelize) {
    if (sequelize.options.dialect !== 'sqlite') {
        sequelize.options.handleDisconnects = false;
        sequelize.connectionManager.pool.destroy();
        sequelize.connectionManager.pool = null;
        sequelize.connectionManager.getConnection = function getConnection() {
            return this._connect(sequelize.config);
        };
        sequelize.connectionManager.releaseConnection =
            function releaseConnection(connection) {
                return this._disconnect(connection);
            };
    }
}
);

/**
 * Get a connection
 * @return {sequelize.connection}
 */
export function getConnection() {
    if (!connection) {
        let params;

        if (DB_URI) {
            params = DB_URI;
        } else {
            params = {
                dialect: DB_DIALECT,
                database: DB_DATABASE,
                username: DB_USERNAME,
                password: DB_PASSWORD,
            };

            if (DB_DIALECT === 'sqlite') {
                params.storage = DB_HOST;
            } else {
                params.host = DB_HOST;
            }

            if (DB_HOST_PORT) {
                params.port = DB_HOST_PORT;
            }

            if (DB_HOST_PROTOCOL) {
                params.protocol = DB_HOST_PROTOCOL;
            }
        }

        if (DB_SSL) {
            params.dialectOptions = {
                ssl: true,
            };
        }

        connection = new Sequelize({
            ...defOptions,
            ...params,
        });

        logger.info('DB connection created');
    }

    return connection;
}

/**
 * Get multidialect now operator
 * @return {symbol}
 */
export function multidialectNow() {
    let key;

    switch (DB_DIALECT) {
        case 'sqlite':
            key = `DATETIME('NOW')`;
            break;
        case 'postgres':
            key = 'NOW()';
            break;
        default:
            throw new Error(`Dialect ${DB_DIALECT} not supported`);
    }

    return getConnection().literal(key);
}

/**
 * Get multidialect custom order
 * @param {string} field
 * @param {string} firstItem
 * @param {array} defaultColumn
 * @return {symbol}
 */
export function multidialectCustomOrder(field, firstItem, defaultColumn) {
    const escapedFirstItem = sqlString.escape(firstItem);
    let key;

    switch (DB_DIALECT) {
        case 'sqlite':
            key = `case ${field} `;
            key += `when ${escapedFirstItem} then 0 `;
            key += `when ${defaultColumn} then 1 `;
            key += 'end';
            break;
        case 'postgres':
            key = 'array_position(array[' +
                [escapedFirstItem, `"${defaultColumn}"`]
                    .join(',') + '], "' +
                field + '")';
            break;
        default:
            throw new Error(`Dialect ${DB_DIALECT} not supported`);
    }

    return getConnection().literal(key);
}

/**
 * Get multidialect custom order of items
 * @param {string} field
 * @param {any[]} items
 * @return {symbol}
 */
export function multidialectCustomOrderItems(field, ...items) {
    let key;
    let efield;
    const escapedItems = items.map((i) => sqlString.escape(i));

    switch (DB_DIALECT) {
        case 'sqlite':
            key = `case `;
            key += escapedItems
                .reduce(
                    (t, i, ix) => `${t} when ${field} = ${i} then ${ix} `,
                    ''
                );
            key += `else ${escapedItems.length} end`;
            break;
        case 'postgres':
            efield = field.split(/\./g).map((f) => `"${f}"`).join('.');
            key = `case ${efield} `;
            key += escapedItems
                .reduce(
                    (t, i, ix) => `${t} when ${i} then ${ix}::smallint `,
                    ''
                );
            key += `else ${escapedItems.length}::smallint end`;
            break;
        default:
            throw new Error(`Dialect ${DB_DIALECT} not supported`);
    }

    return getConnection().literal(key);
}

/**
 * Return a sequelize where object to compare values forcing lower (for
 * case-insensitive comparations)
 * @param {string} colName
 * @param {any} val
 * @return {Sequelize.Where}
 */
export function compareLower(colName, val) {
    const {fn, col, where} = getConnection();

    return where(
        fn('lower', col(colName)),
        '=',
        (`${val}`).toLowerCase(),
    );
}

/**
 * Check if a string is a uuid valid
 * @param {string} str
 * @return {bool}
 */
export function isUUID(str) {
    const pattern = /^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/;
    return pattern.test(str);
}

/**
 * Use transaction or use new
 * @param {null|sequelize.Transaction} dbTx
 * @param {function} cb
 * @param {bool} [autoCommit=true] - Autocommit transaction
 * @return {Promise}
 */
export async function useOrCreateTransaction(dbTx, cb, autoCommit = true) {
    const sequelize = getConnection();
    if (dbTx) {
        return Promise.resolve(cb(dbTx));
    } else if (autoCommit) {
        return sequelize.transaction(async (dbTx) => cb(dbTx));
    } else {
        dbTx = new sequelize.Transaction();
        await Promise.resolve(cb(dbTx));
        return dbTx;
    }
}
