
import {multidialectCustomOrderItems} from '../storage';
import {createDebug} from '../logger';
import langs from '../langs.json';

const debug = createDebug(__filename);

const ENTITY_TYPE_CONTENT = 0x02;
const ENTITY_TYPE_CONTENTVERSION= 0x03;
const ENTITY_TYPE_CARRUSEL = 0xA0;

const DEFAULT_LANG = langs.def;

export default (sequelize, DataTypes) => {

    let ContentModel;
    let ContentVersionModel;
    const instanceMethods = {};

    const MetadataModel = sequelize.define('Metadata', {
        uuid: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },

        entityType: {
            type: DataTypes.INTEGER.UNSIGNED,
            defaultValue: ENTITY_TYPE_CONTENT,
            allowNull: false,
            validate: {
                isIn: [[
                    ENTITY_TYPE_CONTENT,
                    ENTITY_TYPE_CONTENTVERSION,
                    ENTITY_TYPE_CARRUSEL,
                    // ... extend as needed
                ]],
            },
        },

        entityId: {
            type: DataTypes.UUID,
            index: true,
            allowNull: false,
        },

        lang: {
            type: DataTypes.STRING(5),
            allowNull: false,
            defaultValue: 'en',
            validate: {
                is: {
                    args: /^[a-z]{2}(?:_[A-Z]{2})?/,
                    msg: 'Invalid lang format',
                },
            },
        },

        title: {
            type: DataTypes.STRING(1024),
            allowNull: true,
        },

        body: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
    }, {
        timestamps: true,
        paranoid: false,
        instanceMethods,

        indexes: [
            {
                name: 'metadata_search_by_entity',
                fields: ['entityId', 'entityType'],
            },
            {
                name: 'metadata_unique_meta_by_lang',
                unique: true,
                fields: ['entityId', 'entityType', 'lang'],
            },
        ],

        hooks: {},
    });

    if (typeof MetadataModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                MetadataModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    /**
     * Entity types
     * @type {integer}
     */
    MetadataModel.ENTITY_TYPE_CONTENT = ENTITY_TYPE_CONTENT;
    MetadataModel.ENTITY_TYPE_CONTENTVERSION = ENTITY_TYPE_CONTENTVERSION;
    MetadataModel.ENTITY_TYPE_CARRUSEL = ENTITY_TYPE_CARRUSEL;

    MetadataModel.associate = function(models) {
        ContentModel = models.contents;
        ContentVersionModel = models.contentversion;

        MetadataModel.belongsTo(ContentModel, {
            as: 'content',
            foreignKey: 'entityId',
            targetKey: 'uuid',
            constraints: false,
        });

        MetadataModel.belongsTo(ContentVersionModel, {
            as: 'contentversion',
            foreignKey: 'entityId',
            targetKey: 'uuid',
            constraints: false,
        });
    };

    /**
     * Create metadata association is custom model
     * @param {Sequelize.Model} model
     * @param {integer} entityType
     * @param {object} [options]
     * @param {string} [options.as="Metadata"] - Association name
     * @param {string} [options.targetKey="uuid"] - Target key name
     * @return {model}
     */
    MetadataModel.createAssociation =
    function(model, entityType, options = {}) {

        const {as, targetKey} = {
            as: 'Metadata',
            targetKey: 'uuid',
            ...options,
        };

        return model.hasMany(MetadataModel, {
            as,
            foreignKey: 'entityId',
            targetKey,
            constraints: false,
            scope: {
                entityType: entityType,
            },
        });
    };

    /** Create Scope on custom model
     * @param {string} [defaultLang]
     * @param {object} [options]
     * @param {string} [options.as="Metadata"] - Association name
     * @return {function}
     */
    MetadataModel.createI18nScope =
    function(defaultLang = null, options = {}) {

        if (!defaultLang) {
            defaultLang = DEFAULT_LANG;
        }

        const {as} = {
            as: 'Metadata',
            ...options,
        };

        return (lang = DEFAULT_LANG) => {
            lang = lang.split('_')[0].toLowerCase();

            return {
                include: [{
                    model: MetadataModel,
                    as,
                    attributes: [
                        'lang',
                        'title',
                        'body',
                    ],
                    required: false,
                    separate: true,
                    order: multidialectCustomOrderItems(
                        'lang',
                        ...[
                            lang,
                            ...(lang !== defaultLang ? [defaultLang] : []),
                        ],
                    ),
                    limit: 1,
                }],
            };
        };
    };

    /** Create Scope on custom model
     * @param {object} [options]
     * @param {string} [options.as="Metadata"] - Association name
     * @return {function}
     */
    MetadataModel.createMetadataScope =
    function(options = {}) {

        const {as} = {
            as: 'Metadata',
            ...options,
        };

        return {
            include: [{
                model: MetadataModel,
                as,
                attributes: [
                    'lang',
                    'title',
                    'body',
                ],
                required: false,
                separate: true,
            }],
        };
    };

    return MetadataModel;
};

