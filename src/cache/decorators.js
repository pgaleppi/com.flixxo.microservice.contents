import Promise from 'bluebird';
import {getStore} from './stores';
import {createKeyFor} from './utils';
import config from '../config';
import createLogger, {createDebug} from '../logger';

const CACHE_ENABLED = config.get('cache:enabled');

const logger = createLogger(__filename);
const debug = createDebug(__filename);

/**
 * Apply cache to function
 * @param {string} filename
 * @param {string} [prefix]
 * @param {string} [vname]
 * @param {function} fn
 * @param {object} [options]
 * @return {function}
 */
export function applyCacheable(filename, prefix, vname, fn, options = {}) {
    if (typeof prefix === 'function') {
        options = vname || {};
        fn = prefix;
        prefix = undefined;
        vname = undefined;
    }

    if (typeof vname === 'function') {
        options = fn || {};
        fn = vname;
        vname = undefined;
    }

    if (!vname) {
        vname = fn.name;
    }

    options = options || {};

    const StoreObject = options.store || getStore();
    let store;

    /**
     * Get store
     * @return {Store}
     */
    function getStoreObject() {
        if (!store) {
            store = new StoreObject(filename, {prefix});
        }

        return store;
    }

    return async function(...params) {
        const ts = Date.now();
        const foptions = {
            ...options,
        };

        if (!CACHE_ENABLED) {
            debug(`Cache is disabled, pass`);
            // eslint-disable-next-line no-invalid-this
            return Promise.resolve(fn.apply(this, params));
        }

        if (typeof foptions.flags === 'function') {
            debug(`\tResolving flags from function`);
            foptions.flags = foptions.flags(...params);
        }

        const ckey = createKeyFor(vname, ...params);
        debug(`Finding for cache for ${ckey}`);
        const cache = await getStoreObject().get(ckey, foptions);

        if (cache) {
            debug(`\tThere are cache!`);
            const td = Date.now() - ts;
            logger.info(`Return data from cache in ${td}ms`);
            return cache;
        }

        debug(`\tNot cached, continue`);

        // eslint-disable-next-line no-invalid-this
        const res = await Promise.resolve(fn.apply(this, params));
        debug('\tSaved in cache, returning');
        getStoreObject().set(ckey, res, foptions);
        const td = Date.now() - ts;
        logger.info(`Saving response in cache in ${td}ms`);
        return res;
    };
}

/**
 * Decorator for cacheable
 * @param {string} filename
 * @param {string} [prefix]
 * @param {object} [options]
 * @param {Storage} [options.storage] Inject own storage
 * @return {function}
 */
export function cacheable(filename, prefix = null, options = {}) {

    return function(target, key, descriptor) {
        debug(
            `Apply cacheable decordator to ${key}` +
            ` (in ...${filename.substr(-20)})`);

        const value = descriptor.value;

        descriptor.value = applyCacheable(
            filename,
            prefix,
            key,
            value,
            options
        );

        return descriptor;
    };
}

/**
 * Add cache to service method
 * @param {string} filename
 * @param {string} [prefix]
 * @param {object} [options]
 * @return {function}
 */
export function serviceCacheable(filename, prefix = null, options = {}) {

    if (typeof prefix === 'object') {
        options = prefix;
        prefix = null;
    }

    return function(func, ns, cmd) {
        debug(
            `Apply cacheable decordator to service metod ${ns}:${cmd}`);

        return applyCacheable(
            filename,
            prefix,
            `${ns}:${cmd}`,
            func,
            options
        );
    };
}
