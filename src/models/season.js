import {createDebug} from '../logger';

const STATUS_NEW = 0x10;
const STATUS_ENABLED = 0x20;
const STATUS_CANCEL = 0x30;
const STATUS_PAUSED = 0x40;

const debug = createDebug(__filename);

export default (sequelize, DataTypes) => {
    let SerieModel;
    let ContentModel;
    let UsersModel;
    let GeoBlockingModel;
    let MediaModel;
    let SubtitleModel;

    const instanceMethods = {};

    const SeasonModel = sequelize.define('Season', {

        uuid: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },

        serieUUID: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false,
            index: true,
        },

        title: {
            type: DataTypes.STRING(2014),
            allowNull: true,
            validate: {
                is: /^.{1,}$/,
            },
        },

        number: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            validate: {
                is: /^\d{1,3}$/,
            },
        },

        status: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: STATUS_ENABLED,
            validate: {
                isIn: {
                    args: [[
                        STATUS_NEW,
                        STATUS_CANCEL,
                        STATUS_PAUSED,
                        STATUS_ENABLED,
                    ]],
                    msg: 'Invalid status',
                },
            },
        },

        authorId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
        },

    }, {
        timestamps: true,
        paranoid: true,
        instanceMethods,

        indexes: [
            {
                unique: true,
                fields: ['serieUUID', 'number'],
            },
        ],
    });

    if (typeof SeasonModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                SeasonModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    SeasonModel.STATUS_NEW = STATUS_NEW;
    SeasonModel.STATUS_ENABLED = STATUS_ENABLED;
    SeasonModel.STATUS_CANCEL = STATUS_CANCEL;
    SeasonModel.STATUS_PAUSED = STATUS_PAUSED;

    SeasonModel.associate = function(models, db) {
        UsersModel = models.users;
        ContentModel = models.contents;
        SerieModel = models.serie;
        MediaModel = models.media;
        GeoBlockingModel = models.geoblocking;
        SubtitleModel = models.subtitle;

        SeasonModel.belongsTo(UsersModel, {
            as: 'author',
            foreignKey: 'authorId',
            targetKey: 'id',
            constraints: true,
        });

        SeasonModel.belongsTo(SerieModel, {
            as: 'serie',
            foreignKey: 'serieUUID',
            targetKey: 'uuid',
            constraints: true,
        });

        SeasonModel.hasMany(ContentModel, {
            as: 'content',
            foreignKey: 'seasonUUID',
            sourceKey: 'uuid',
            constraints: false,
        });

        SeasonModel.hasOne(GeoBlockingModel, {
            as: 'geoBlocking',
            foreignKey: 'targetUUID',
            targetKey: 'uuid',
            constraints: false,
            scope: {
                entityType: GeoBlockingModel.ETYPE_SERIE_SEASON,
            },
        });

        // Scopes

        SeasonModel.addScope('enabled', {
            where: {
                status: STATUS_ENABLED,
            },
        });

        SeasonModel.addScope('detailed', {
            attributes: ['uuid', 'title', 'number'],
            include: [{
                model: ContentModel,
                as: 'content',
                attributes: [
                    'uuid', 'title', 'order', 'status',
                    'contentType', 'price', 'releaseDate',
                    'audioLang', 'duration', 'downloadStatus',
                    'hash', 'body', 'boost',
                ],
                include: [{
                    model: MediaModel,
                    as: 'media',
                    attributes: ['id', 'name', 'type', 'url', 'order'],
                    required: false,
                    order: [
                        ['order', 'ASC'],
                    ],
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: ['id', 'lang', 'label', 'url'],
                    required: false,
                }],
                required: true,
            }],
        });

        SeasonModel.addScope('authorDetailed', {
            attributes: ['uuid', 'title', 'number'],
            include: [{
                model: ContentModel,
                as: 'content',
                attributes: [
                    'uuid', 'title', 'order', 'status',
                    'contentType', 'price', 'releaseDate',
                    'audioLang', 'duration', 'downloadStatus',
                    'hash', 'body', 'boost',
                ],
                include: [{
                    model: MediaModel,
                    as: 'media',
                    attributes: ['id', 'name', 'type', 'url', 'order'],
                    required: false,
                    order: [
                        ['order', 'ASC'],
                    ],
                }, {
                    model: SubtitleModel,
                    as: 'subtitle',
                    attributes: ['id', 'lang', 'label', 'url'],
                    required: false,
                }],
                required: false,
            }],
        });

        SeasonModel.addScope('asAuthor', (authorId) => {
            return {
                where: {
                    authorId,
                },
                paranoid: true,
            };
        });

        SeasonModel.addScope('geoAvailable', (countryIso) => {
            return GeoBlockingModel.extendScopeDefinition(
                countryIso,
                GeoBlockingModel.ETYPE_SERIE_SEASON,
                'season.uuid',
            )({});
        });

        SeasonModel.addScope('toExport', {
            paranoid: false,
        });
    };

    /**
     * Search one record by hash
     * @param {string} hash
     * @param {object} [options]
     * @return {Promise<SeasonModel|null>}
     */
    SeasonModel.findByHash = function(hash, options = {}) {
        hash = hash.toLowerCase();
        return this.findOne({
            where: {hash},
            ...options,
        });
    };

    return SeasonModel;
};
