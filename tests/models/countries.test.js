import {syncDb, loadData} from '../helpers/db';
import models from '../../src/models';

describe('Check scopes', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadData('00_countries');
        await loadData('10_countries-names-en');
        await loadData('10_countries-names-es');
        done();
    }, 30000);

    describe('byLang', () => {

        test('Must to get countries in english', async () => {
            let res = await models.countries.scope([
                {method: ['byLang', 'en']},
            ]).findByPk(224);
            expect(res.name[0].name).toBe('United States of America');
        });

        test('Must to get countries in spanish', async () => {
            let res = await models.countries.scope([
                {method: ['byLang', 'es']},
            ]).findByPk(224);
            expect(res.name[0].name).toBe('Estados Unidos de América');
        });
    });
});
