
import program from 'commander';
import packageData from '../package.json';
import * as tasks from './tasks';
import * as parsers from './tasks/parsers';
import {createDebug} from './logger';

const debug = createDebug(__filename);

program
    .version(packageData.version)
;

program
    .command('start')
    .description('Start microservices')
    .action((args) => {
        debug('Go to start task');
        tasks.start(args);
        return false;
    })
;

program
    .command('lambda')
    .description('Emulate lambda call, if no one parameter is passed, excute' +
        ' from stdin as body in JSON')
    .option(
        '-c, --cmd <cmd>',
        'Command',
    )
    .option(
        '-e, --event <body>',
        'Body of event, in JSON format',
        (p) => parsers.parseJSON(p),
    )
    .option(
        '--context <body>',
        'Context body ins JSON format',
        (p) => parsers.parseJSON(p),
    )
    .option(
        '--params <body>',
        'Params body ins JSON format',
        (p) => parsers.parseJSON(p),
    )
    .action((args) => {
        debug('Go to emulate lambda call');
        tasks.lambda(args);
        return false;
    })
;

program
    .command('syncdb')
    .description('Sync database')
    .option('--force', 'Force sync')
    .option('--nodata', 'No load data')
    .action((args) => {
        debug('Go to syncdb task');
        tasks.syncdb(args);
        return false;
    })
;

program
    .command('loaddata')
    .description('Load fixtures')
    .option('--path <path>', 'Path from data')
    .action((args) => {
        debug('Go to loaddata task');
        tasks.loaddata(args);
        return false;
    })
;

program
    .command('adsrewards')
    .description('Process ads rewards')
    .action((args) => {
        debug('Go to adsrewards task');
        tasks.adsrewards(args);
        return false;
    })
;

program
    .command('bulkESIndex')
    .description('Process bulk ES Index')
    .action((args) => {
        debug('Go to bulkESIndex task');
        tasks.bulkESIndex(args);
        return false;
    })
;

program
    .command('cleanTempMedia')
    .description('Clean temp media')
    .action((args) => {
        debug('Go to cleanTempMedia task');
        tasks.cleanTempMedia(args);
        return false;
    })
;

program
    .command('setS3Availability')
    .description('Set content s3 availability')
    .option('--hash <hash>', 'Content hash')
    .option('--available <available>', 'Content avilable or not: true/false')
    .action((args) => {
        debug('Go to setS3Availability Task');
        tasks.setS3Availability(args);
        return false;
    })
;

program
    .command('imageResize')
    .description('Resize image')
    .option('--ids <id1,id2,idN,...>', 'Media ids')
    .action((args) => {
        debug('Go to imageResize Task');
        tasks.imageResize(args.ids.split(','));
        return false;
    })
;

program
    .command('migratei18n')
    .description('Migrate I18N')
    .action((args) => {
        debug('Go to migrate I18N Task');
        tasks.migratei18n(args);
        return false;
    })
;

program
    .command('exportconents')
    .description('Export contents')
    .option('-p, --path <path>', 'Path to output')
    .option('-a, --authors <authors>', 'Authors uuid separate by ,')
    .option('--ids <ids>', 'UUID separate by ,')
    .action((args) => {
        debug('Go to exportcontents task');
        tasks.exportcontents(args);
        return false;
    })
;


debug('Parsing options');
program.parse(process.argv);
