
import {getNSCommander} from '../../logger/context';
import {createDebug} from '../../logger';

const debug = createDebug(__filename);

/**
 * Add API reqId to logger context
 * @return {function}
 */
export default function apiReqIdMiddleware() {
    return ['apiReqIdMiddleware', 'pre', (body) => {
        const {context} = body;

        if (!context || typeof context !== 'object') {
            debug(`Context not present, ignoring`);
            return body;
        }

        if (typeof context['$reqId'] !== 'undefined') {
            debug(`Call with $reqId context, adding to logging`);
            const callContext = getNSCommander();
            callContext.addPrefix('ApiReqId', context.$reqId);
        }

        return body;
    }];
}
