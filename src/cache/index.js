
import {getStore} from './stores';
import {fromCache, touchFlag} from './core';

const Store = getStore();

export {Store};
export {fromCache};
export {touchFlag};
