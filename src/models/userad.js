
import {createDebug} from '../logger';

const debug = createDebug(__filename);


export default (sequelize, DataTypes) => {

    const instanceMethods = {};

    const UserWatchedAd = sequelize.define('UserWatchedAd', {
        id: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
    }, {
        timestamps: true,
        paranoid: true,
        scopes: {
            historical: {
                paranoid: false,
            },
        },
    });

    UserWatchedAd.associate = function(models) {

    };

    if (typeof UserWatchedAd.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                UserWatchedAd.prototype[m] = instanceMethods[m];
            }
        }
    }

    return UserWatchedAd;
};
