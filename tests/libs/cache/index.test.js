jest.mock('../../../src/config/env', () => {
    return {
        cache: {
            enabled: true,
        },
    };
});

import Promise from 'bluebird';
import Store, {clearCache, isSaved, getFlag} from '../../../src/cache/stores/memory';
import {fromCache, touchFlag} from '../../../src/cache';
import {createPrefix} from '../../../src/logger/utils';

const MICROSERVICE_PREFIX = createPrefix(__filename);

describe('#fromCache', () => {
    afterEach(() => clearCache());

    test(`Must to exec once and get cache`, async () => {
        const first = await fromCache(
            __filename,
            'lol',
            [1, 2, {}],
            {store: new Store(__filename)},
            () => {
                return new Promise((r) => {
                    setTimeout(() => {
                        r({a: 1, b: '2', c: {A: [2, 5]}});
                    }, 10);
                });
            }
        );

        const k = `CACHE:${MICROSERVICE_PREFIX}:1_c4e5d69d24382ffec006be4b76736f94e20df80a`;

        expect(first).toEqual({a: 1, b: '2', c: {A: [2, 5]}});
        expect(isSaved(k, first)).toBeTruthy();

        const second = await fromCache(__filename, 'lol', [1, 2, {}], {store: new Store(__filename)}, () => {
            throw new Error('Pass?');
        });

        expect(second).toEqual({a: 1, b: '2', c: {A: [2, 5]}});
    });

});

describe('#touchFlag', () => {
    afterEach(() => clearCache());

    test(`Must to set flag`, async () => {
        let now = Date.now();

        await touchFlag('test', {
            store: new Store(__filename),
        });

        expect(getFlag('test')).not.toBeNull();
        expect(Math.floor(getFlag('test') / 10000)).toBe(Math.floor(now/10000));
    });
});
