
import config from '../config';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

const MIN_AGE = config.get('profiles:minAge');

/**
 * ProfileModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */

const GENDER_MALE = 0x01;
const GENDER_FEMALE = 0x02;
const GENDER_OTHER = 0xff;

const GENDER_STRING_MAP = {
    'm': GENDER_MALE,
    'f': GENDER_FEMALE,
    'o': GENDER_OTHER,
};

export default (sequelize, DataTypes) => {
    // let MediaModel;

    const instanceMethods = {};

    const ProfileModel = sequelize.define('Profile', {

        // uuid: {
        //     type: DataTypes.UUID,
        //     defaultValue: DataTypes.UUIDV4,
        //     allowNull: false,
        //     unique: true,
        //     index: true,
        //     primaryKey: true,
        // },

        userId: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            autoIncrement: false,
        },

        realName: {
            type: DataTypes.STRING(1024),
            allowNull: true,
        },

        birthDate: {
            type: DataTypes.DATE,
            allowNull: true,
            validate: {
                ageRestriction(value) {
                    if (!value) return;

                    if (typeof value !== 'number') {
                        value = value.getTime();
                    }

                    const now = new Date();
                    const validDate = new Date(
                        now.getUTCFullYear() - MIN_AGE,
                        now.getUTCMonth(),
                        now.getUTCDate()
                    );

                    if (value > (validDate.getTime() - 24 * 3600000)) {
                        debug(`Age is minnor of ${MIN_AGE}`);
                        throw new Error(
                            `User is not old enough to use Flixxo`);
                    }
                },
            },
        },

        lang: {
            type: DataTypes.STRING(5),
            allowNull: false,
            defaultValue: 'en',
            validate: {
                is: /^[a-z]{2}(?:_[A-Z]{2})?/,
            },
        },

        avatar: {
            type: DataTypes.STRING(1024),
            allowNull: true,
            validate: {
                isUrl: true,
            },
        },

        gender: {
            type: DataTypes.INTEGER,
            allowNull: true,
            isIn: [[
                GENDER_MALE,
                GENDER_FEMALE,
                GENDER_OTHER,
            ]],
            set(val) {
                if (typeof val === 'string') {
                    val = GENDER_STRING_MAP[val] || null;
                }

                if (val) {
                    this.setDataValue('gender', val);
                }
            },
        },

    }, {
        instanceMethods: instanceMethods,
    });

    if (typeof ProfileModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                ProfileModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    ProfileModel.associate = function(models) {
        // MediaModel = models.media;

        // ProfileModel.hasMany(MediaModel, {
        //     as: 'media',
        //     foreignKey: 'uuid',
        //     targetKey: 'uuid',
        //     scope: {
        //         isDraft: false,
        //     },
        // });

        ProfileModel.belongsTo(models.users, {
            foreignKey: 'userId',
            targetKey: 'id',
            constraints: true,
            onDelete: 'CASCADE',
        });

        ProfileModel.belongsTo(models.countries, {
            foreignKey: {
                name: 'countryId',
                allowNull: true,
            },
            targetKey: 'id',
            constraints: true,
        });

        ProfileModel.addScope('public', {
            attributes: [
                // 'uuid',
                'userId',
                'realName',
                'birthDate',
                'lang',
                'gender',
                'countryId',
                'avatar',
            ],
            // include: [{
            //     model: MediaModel,
            //     as: 'media',
            //     required: false,
            //     attributes: [
            //         'uuid',
            //         'name',
            //         'type',
            //         'url',
            //         'order',
            //         'processed',
            //         'sizes',
            //     ],
            // }],
        });
    };

    return ProfileModel;
};
