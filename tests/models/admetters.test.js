
import {Decimal} from 'decimal.js';
import {syncDb, loadFixture} from '../helpers/db';
import models from '../../src/models';

describe('#incrementMetter', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadFixture('admetters-for-increment-metter');
    });

    test('Must to update empty metter', async () => {
        const metter = await models.admetters.findOne({
            where: {userUUID: '00000000-b5aa-4507-913a-a387419b6ef8'},
            rejectOnEmpty: true,
        });

        expect(metter.metter.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.cicle.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.lastCicle.toFixed(18)).toBe('0.000000000000000000');

        await metter.incrementMetter(new Decimal('0.123456789012345678'));

        expect(metter.metter.toFixed(18)).toBe('1.000000000000000000');
        
        expect(
            metter.cicle
                .times('1e14')
                .toNumber()
        ).toBeCloseTo(12345678901234.5678); // Not exact precission in sqlite -_o_-

        expect(metter.lastCicle.toFixed(18)).toBe('0.000000000000000000');
    });

    test('Must to update already metter', async () => {
        const metter = await models.admetters.findOne({
            where: {userUUID: '00000000-c1eb-41b5-85e7-256b0568538b'},
            rejectOnEmpty: true,
        });

        expect(metter.metter.toFixed(18)).toBe('66.000000000000000000');
        expect(metter.cicle.toFixed(18)).toBe('123.456789123456790000');
        expect(metter.lastCicle.toFixed(18)).toBe('0.000000000000000000');

        await metter.incrementMetter(new Decimal('3.123456789012345678'));

        expect(metter.metter.toFixed(18)).toBe('67.000000000000000000');
        
        expect(
            metter.cicle
                .toFixed(14) // Not exact precission in sqlite -_o_-
        ).toBe('126.58024591246914');
        expect(metter.lastCicle.toFixed(18)).toBe('0.000000000000000000');
    });

});

describe('#resetMetter', () => {
    
    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadFixture('admetters-for-increment-metter');
    });

    test('Must to reset empty metter', async () => {
        const metter = await models.admetters.findOne({
            where: {userUUID: '00000000-b5aa-4507-913a-a387419b6ef8'},
            rejectOnEmpty: true,
        });

        expect(metter.metter.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.cicle.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.lastCicle.toFixed(18)).toBe('0.000000000000000000');


        await models.sequelize.transaction(async (dbTx) => {

            await metter.resetMetter(
                new Decimal('0.123456789012345678'),
                new Decimal('1.2'),
                dbTx,
            );
        });

        expect(metter.metter.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.cicle.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.lastCicle.toFixed(18)).toBe('-0.123456789012345678');
    });

    test('Must to reset metter with cicle', async () => {
        const metter = await models.admetters.findOne({
            where: {userUUID: '00000000-c1eb-41b5-85e7-256b0568538b'},
            rejectOnEmpty: true,
        });

        expect(metter.metter.toFixed(18)).toBe('66.000000000000000000');
        expect(metter.cicle.toFixed(18)).toBe('123.456789123456790000');
        expect(metter.lastCicle.toFixed(18)).toBe('0.000000000000000000');

        await models.sequelize.transaction(async (dbTx) => {

            await metter.resetMetter(
                new Decimal('10.123456789012345678'),
                new Decimal('1.2'),
                dbTx,
            );
        });

        // 123.456789123456790000
        //   /
        //   1.200000000000000000
        //   -
        //  10.123456789012345678

        expect(metter.metter.toFixed(18)).toBe('0.000000000000000000');
        expect(metter.cicle.toFixed(18)).toBe('92.757200813868312652');
        expect(metter.lastCicle.toFixed(18)).toBe('92.757200813868312652');
    });
});
