jest.mock('axios');

import {EmptyResultError} from 'sequelize';
import {syncDb, loadFixture, loadData} from '../helpers/db';
import models from '../../src/models';

describe('Check season creation', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('serie-for-season-creation');
    }, 10000);

    it('Must fail for existent season on same serie', async () => {
        await models.season.create({
            number: 1,
            serieUUID: '0000000-0000-0000-0000-000000000001',
            authorId: 100,
        });
        await models.season.create({
            number: 1,
            serieUUID: '0000000-0000-0000-0000-000000000002',
            authorId: 100,
        });

        try {
            await models.season.create({
                number: 1,
                serieUUID: '0000000-0000-0000-0000-000000000001',
                authorId: 100,
            });
        } catch (err) {
            expect(err.message).toBe(
                'Validation error');
            return;
        }
    });

});

describe('scopes', () => {
    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadData('00_countries');
    }, 10000);

    describe('geoAvailable', () => {
        beforeEach(async () => {
            await loadFixture('seasons-for-geoblocking-scope');
        });

        test(`Must to show seasons only available in AR`, async () => {

            const res = await models.season.scope(
                {method: ['geoAvailable', 'AR']}
            ).findAll({
                attributes: ['title', 'uuid'],
            });

            const plain = res
                    .map(({title, uuid, geoBlocking}) => (
                        {
                            title,
                            uuid,
                            geoBlocking: geoBlocking ? {
                                ...geoBlocking.get({plain: true}),
                            } : 'nop',
                        }
                    ));

            expect(plain).toEqual([
                {
                    title: 'Season 1',
                    uuid: '1000000-0000-0000-0000-000000000001',
                    geoBlocking: {blockType: models.geoblocking.TYPE_WHITE},
                },
                {
                    title: 'Season 3',
                    uuid: '1000000-0000-0000-0000-000000000003',
                    geoBlocking: 'nop',
                },
            ]);

        });

        test(`Must to show seasons bloqued AR and CL`, async () => {

            const res = await models.season.scope(
                {method: ['geoAvailable', 'CN']}
            ).findAll({
                attributes: ['title', 'uuid'],
            });

            const plain = res
                    .map(({title, uuid, geoBlocking}) => (
                        {
                            title,
                            uuid,
                            geoBlocking: geoBlocking ? {
                                ...geoBlocking.get({plain: true}),
                            } : 'nop',
                        }
                    ));

            expect(plain).toEqual([
                {
                    title: 'Season 2',
                    uuid: '1000000-0000-0000-0000-000000000002',
                    geoBlocking: {blockType: models.geoblocking.TYPE_BLACK},
                },
                {
                    title: 'Season 3',
                    uuid: '1000000-0000-0000-0000-000000000003',
                    geoBlocking: 'nop',
                },
            ]);

        });

        test(`Must to show seasons bloqued AR and CL when iso is null`, async () => {

            const res = await models.season.scope(
                {method: ['geoAvailable', null]}
            ).findAll({
                attributes: ['title', 'uuid'],
            });

            const plain = res
                    .map(({title, uuid, geoBlocking}) => (
                        {
                            title,
                            uuid,
                            geoBlocking: geoBlocking ? {
                                ...geoBlocking.get({plain: true}),
                            } : 'nop',
                        }
                    ));

            expect(plain).toEqual([
                {
                    title: 'Season 2',
                    uuid: '1000000-0000-0000-0000-000000000002',
                    geoBlocking: {blockType: models.geoblocking.TYPE_BLACK},
                },
                {
                    title: 'Season 3',
                    uuid: '1000000-0000-0000-0000-000000000003',
                    geoBlocking: 'nop',
                },
            ]);

        });

        test('Must to throw error with invalid type ISO code', async () => {

            try {
                await models.season.scope(
                    {method: ['geoAvailable', 324]}
                ).findAll({
                    attributes: ['title', 'uuid'],
                });
            } catch (e) {
                expect(e).toBeInstanceOf(TypeError);
                expect(e.message).toBe('countryIso must to be a string');
                return;
            }

            throw new Error('Pass');
        });

        test('Must to throw error with invalid ISO code', async () => {

            try {
                await models.season.scope(
                    {method: ['geoAvailable', 'ARG']}
                ).findAll({
                    attributes: ['title', 'uuid'],
                });
            } catch (e) {
                expect(e).toBeInstanceOf(Error);
                expect(e.message).toBe('Invalid ISO code');
                return;
            }

            throw new Error('Pass');
        });
    });

});
