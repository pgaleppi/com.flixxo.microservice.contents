import MemStore from './memory';
import RedisStore from './redis';
import config from '../../config';
import createLogger from '../../logger';

const logger = createLogger(__filename);

const CACHE_TYPE = config.get('cache:type');

let cacheClass;

/**
 * Return a Store backend
 * @return {Store}
 */
export function getStore() {

    if (cacheClass) {
        return cacheClass;
    }

    switch (CACHE_TYPE) {
        case 'redis':
            logger.info(`Using redis storage for cache`);
            cacheClass = RedisStore;
            return RedisStore;
        case 'memory':
            logger.info(`Using in-memory storage for cache`);
            cacheClass = MemStore;
            return MemStore;
        default:
            logger.error(`Error with cache backend ${CACHE_TYPE}`);
            throw new Error('Unknow cache backend');
    }
}
