import {Op} from 'sequelize';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

export default (sequelize, DataTypes) => {
    let UsersModel;
    let ContentModel;

    const instanceMethods = {};

    const ContentUpdateModel = sequelize.define('ContentUpdate', {

        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true,
        },

        uuid: {
            type: DataTypes.UUID,
            allowNull: false,
        },

        when: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW,
        },

        status: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true,
        },

        privacy: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true,
        },

        moderationStatus: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true,
        },

        moderatorId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true,
        },

        moderationMessage: {
            type: DataTypes.TEXT,
            allowNull: true,
        },

        cancelComment: {
            type: DataTypes.TEXT,
            allowNull: true,
        },

    }, {
        timestamps: false,
        paranoid: true,
        instanceMethods,
    });

    if (typeof ContentUpdateModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                ContentUpdateModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    ContentUpdateModel.associate = function(models) {
        ContentModel = models.contents;
        UsersModel = models.users;

        ContentUpdateModel.belongsTo(ContentModel, {
            as: 'content',
            foreignKey: 'uuid',
            targetKey: 'uuid',
            constraints: true,
        });

        ContentUpdateModel.belongsTo(UsersModel, {
            as: 'moderator',
            foreignKey: 'moderatorId',
            targetKey: 'id',
            constraints: true,
        });

        ContentUpdateModel.addScope('forAuthor', {
            attributes: [
                'when',
                'status',
                'cancelComment',
                'moderationStatus',
                'moderationMessage',
            ],
            where: {
                [Op.or]: [
                    {
                        moderationStatus: {
                            [Op.not]: null,
                        },
                    },
                    {
                        status: {
                            [Op.not]: null,
                        },
                    },
                ],
            },
        });
    };

    ContentUpdateModel.findByContent = function(uuid, options = {}) {
        return ContentUpdateModel.findAll({
            ...options,
            where: {
                ...options.where || {},
                uuid: uuid,
            },
        });
    };

    return ContentUpdateModel;
};
