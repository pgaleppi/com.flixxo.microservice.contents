import {createPrefix} from '../../logger/utils';
import config from '../../config';
import {createDebug} from '../../logger';

const debug = createDebug(__filename);

const CACHE_PREFIX = config.get('cache:prefix');
const CACHE_EXPIRATION = parseInt(config.get('cache:expiration'), 10);

/**
 * Store cache
 */
export default class Store {

    /**
     * Create new Store
     * @param {string} file
     * @param {object} [options]
     */
    constructor(file, options = {}) {
        const {prefix} = options;
        this._prefix = Store.getPrefix(file, prefix);
        this._flagPrefix = Store.getFlagPrefix();
        this._options = {
            expiration: CACHE_EXPIRATION,
        };
        debug(`Store create for ${this._prefix}`);
    }

    /**
     * Encode data
     * @protected
     * @param {object} data
     * @return {string}
     */
    _encode(data) {
        return JSON.stringify(data);
    }

    /**
     * Decode data
     * @protected
     * @param {string} data
     * @return {objcet}
     */
    _decode(data) {
        return JSON.parse(data);
    }

    /**
     * Get value
     * @abstract
     * @param {string} rkey
     * @param {object} [options]
     * @return {any}
     */
    async get(rkey, options) {
        throw new Error('Not implemented');
    }

    /**
     * Set value
     * @abstract
     * @param {string} rkey
     * @param {any} val
     * @param {object} [options]
     * @param {integer} [options.expiration]
     * @return {Promise};
     */
    async set(rkey, val, options = {}) {
        throw new Error('Not implemented');
    }

    /**
     * Touch a flag
     * @abstract
     * @param {string} flag
     */
    async touchFlag(flag) {
        throw new Error('Not implemented');
    }

    /**
     * Return prefix
     * @param {string} file
     * @param {string} [pref=null]
     * @return {string}
     */
    static getPrefix(file, pref = null) {
        let pre = `${CACHE_PREFIX}:${createPrefix(file)}`;
        if (pref) {
            pre += `:${pref}`;
        }

        return pre;
    }

    /**
     * Return prefix for flags
     * @return {string}
     */
    static getFlagPrefix() {
        const pre = `${CACHE_PREFIX}:flags`;
        return pre;
    }

}
