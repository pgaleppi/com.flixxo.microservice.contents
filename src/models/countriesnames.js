import {createDebug} from '../logger';

const debug = createDebug(__filename);

/**
 * CountriesCountryNamesModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {

    const instanceMethods = {};

    const CountryNamesModel = sequelize.define('CountryName', {

        countryId: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
        },

        lang: {
            type: DataTypes.STRING(5),
            primaryKey: true,
        },

        name: {
            type: DataTypes.STRING,
            allowNull: true,
        },

    }, {
        name: {
            singular: 'CountryName',
            plural: 'CountryNames',
        },

        instanceMethods: instanceMethods,
    });

    if (typeof CountryNamesModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                CountryNamesModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    CountryNamesModel.associate = function(models) {
    };

    return CountryNamesModel;
};
