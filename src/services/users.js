import Promise from 'bluebird';
import {EmptyResultError} from 'sequelize';
import models from '../models';
import {getConnection} from '../storage';
import {serviceCacheable} from '../cache/decorators';
import {ValidationError, NotFoundError} from '../errors';
import createLogger, {createDebug} from '../logger';
import {loadService} from '../remoteservice';
import {S3} from 'aws-sdk';
import config from '../config';
import axios from 'axios';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

const BUCKET_NAME = config.get('aws:s3:bucket');
const BUCKET_HOST = config.get('aws:s3:host');
const PROFILE_FOLDER = config.get('s3:avatarmedia:folder');
const PROFILE_ACL = config.get('s3:avatarmedia:acl');
const PROFILE_URL = `${BUCKET_HOST}${BUCKET_NAME}/`;

/**
 * Get user by email
 * @param {string} email
 * @return {Promise<object|null>}
 */
export async function getByEmail(email) {

    debug('Getting user by email');
    // @TODO association queryng fail
    const user = await models.users.scope('profile').findByEmail(email);

    if (!user) {
        debug(`User ${email} not found`);
        return null;
    }

    debug(`User found, getting profile`);
    const profile = await user.getProfile({
        scope: ['public'],
        logging: (...p) => debug(...p),
    });

    const userData = {
        ...user.get({plain: true}),
        profile: {},
    };

    if (profile) {
        userData.profile = profile.get({plain: true});
    }

    return userData;
}

export const findUser = [
    serviceCacheable(__filename),
    async function findUser($, from, query) {
        debug(`Find user ${query}`);
        const fromUser = await models.users.findOne({
            attributes: ['id'],
            where: {uuid: from},
            raw: true,
            rejectOnEmpty: true,
        })
            .catch((e) => {
                if (e instanceof EmptyResultError) {
                    logger.error(`User ${from} not found`);
                    throw new Error('From user not found');
                } else {
                    throw e;
                }
            });

        const authService = await loadService('@auth');

        const remoteUser = await authService.execute(
            'auth:findUser',
            query,
            {
                attributes: ['uuid'],
            }
        );
        if (!remoteUser) {
            debug(`User not found`);
            throw new NotFoundError(`User not found`);
        }

        debug(`\tGetting user information`);
        const data = await models.users.scope([
            'txInfo',
            {method: ['withFollowersMetters']},
            {method: ['withContentMetters']},
            {method: ['isFollowing', fromUser.id]},
        ])
            .findOne({
                where: {uuid: remoteUser.uuid},
                rejectOnEmpty: true,
            })
            .then((user) => user.get({plain: true}))
        ;

        debug(`\tGetting wallet`);
        const wallet = await loadService('@wallets')
            .then((wallets) => wallets.execute(
                'wallets:getByUser',
                data.uuid
            ))
            .catch((e) => {
                if (e.name === 'RemoteNotFoundError') {
                    debug(`\t\tWallet not found`);
                    return null;
                } else {
                    logger.error(
                        `Unknow error getting wallet ${e.message} (` +
                        `${e.name})`
                    );
                    throw e;
                }
            })
        ;

        data.wallet = wallet;

        debug(`Send user data`);
        return {
            uuid: data.uuid,
            displayName: data.displayName,
            photo: data.Profile ? data.Profile.avatar : null,
            nick: data.nickname,
            address: wallet,
            followers: parseInt(data.followers, 10),
            following: parseInt(data.following, 10),
            numContents: parseInt(data.numContents, 10),
            isFollowing: !!data.isFollowing,
            isFollower: !!data.isFollower,
        };
    },
];

/**
 * Create user
 * @param {string} email
 * @param {string} uuid
 * @param {string} nickname
 * @param {string} mobileNumber
 * @return {Promise<UsersModel>}
 */
export async function create(email, uuid, nickname, mobileNumber) {
    debug(`Creating user for ${email} with number ${mobileNumber}`);
    const user = await models.users.create({
        email,
        uuid,
        nickname,
        mobileNumber,
    });

    return user.get({plain: true});
}

/**
 * Save user profile
 * @param {string} email
 * @param {object} data
 * @param {string} [data.realName]
 * @param {string} [data.birthDate]
 * @param {string} [data.lang]
 * @param {string} [data.gender]
 * @param {bool} callAuth
 * @return {Promise<object>}
 */
export async function saveProfile(email, data, callAuth = true) {
    debug(`Save profile`);

    const {
        realName,
        birthDate,
        lang,
        gender,
        mobileNumber,
        countryId,
    } = data;

    // Check if there was some data sent
    if ( !realName && !birthDate &&
         !lang && !gender &&
         !countryId && !mobileNumber ) {
        debug(`No information provided`);
        throw new ValidationError(`No information provided`);
    }

    const user = await models.users.scope('profile').findByEmail(email);

    const fields = {
        realName,
        birthDate,
        lang,
        gender,
        countryId,
    };

    if (fields.countryId) {
        try {
            await models.countries.findByPk(fields.countryId, {
                attributes: ['id'],
                rejectOnEmpty: true,
            });
        } catch (e) {

            if (e instanceof models.sequelize.EmptyResultError) {
                throw new ValidationError(`Country is not a valid one`, [{
                    field: 'countryId',
                    message: `Country ${fields.countryId} does not exists`,
                }]);
            } else {
                throw e;
            }
        }
    }

    const attributes = Object.keys(fields).reduce((atrs, key) => {
        if (fields[key]) {
            atrs.push(key);
        }

        return atrs;
    }, []);

    debug(`Saving profile`, attributes);
    let profile = await user.saveProfile(fields, {
        fields: attributes,
    });

    const usr = await models.users.scope('minimal').findByPk(profile.userId);
    if (mobileNumber && usr.mobileNumber !== mobileNumber) {
        const authService = await loadService('@auth');
        try {
            await authService.execute(
                'auth:setMobileNumber',
                email, mobileNumber,
            );
        } catch (e) {
            debug(`Failed on update user:`, e);
        }
        usr.mobileNumber = mobileNumber;
        await usr.save();
    }

    debug(`Reload profile with public scope`);
    profile = await models.profile.scope('public').findByPk(profile.userId);
    profile = profile.get({plain: true});
    profile.mobileNumber = usr.mobileNumber;

    return {
        ...user.get({plain: true}),
        profile: profile,
    };
}

/**
 * Get following categories
 * @param {string} email
 * @return {Promise<object[]>}
 */
export async function getFollowedCategories(email) {
    debug(`Getting followed categories for ${email}`);
    const user = await models.users.findByEmail(email);

    debug(`Get categories`);
    const categories = await user.getFollowedCategories({
        attributes: ['id', 'name', 'thumb'],
    });

    // @TODO Refactor (Do it better)
    return categories.map((category) => {
        const res = category.get({plain: true});
        delete res.UserCategories;
        return res;
    });
}

/**
 * Set followed categories
 * @param {string} email
 * @param {number[]} ids
 * @return {Promise<object[]>}
 */
export async function setFollowedCategories(email, ids) {
    debug(`Set followed categories to user ${email}`, ids);

    if (!Array.isArray(ids)) {
        throw new Error(`Ids must to be an array of cateogires IDs`);
    }

    if (ids.length !== [...new Set(ids)].length) {
        throw new ValidationError(
            'Repeated categories');
    }

    ids = [...new Set(ids)];

    // @TODO parametrize
    if (ids.length < 5) {
        throw new ValidationError(
            'Followed categories must be bigger than 4');
    }

    ids = ids.map((x) => parseInt(x));

    const user = await models.users.findByEmail(email);

    const categories = await models.categories.scope('public').findAll({
        where: {
            id: ids,
        },
    });

    if (!categories || categories.length !== ids.length) {
        const notFound = ids
            .filter((id) => !categories.find((c) => c.id === id));
        throw new NotFoundError(`Some categories not found`, notFound);
    }

    await user.setFollowedCategories(categories);

    return categories.map((c) => c.get({plain: true}));
}


/**
 * Set profile avatar
 * @param {string} email
 * @param {string} url
 * @param {bool} remote
 * @return {Promise<object>}
 */
export async function setProfileAvatar(email, url, remote = false) {
    debug(`Setting profile avatar to ${email}`);

    const user = await models.users.scope('profile').findByEmail(email);
    let profile;
    const avatarURL = user.Profile.avatar;

    // Check if need to download img and upload to s3
    if ( remote ) {
        url = await downloadProfileImg(email, url);
        debug(`newPic:`, url);
    }

    const sequelize = getConnection();
    await sequelize.transaction( async (dbTxn) => {
        debug(`Saving profile avatar`);
        profile = await user.saveProfile(
            {avatar: url},
            {transaction: dbTxn}
        );
        await models.media.create({
            type: models.media.TYPE_AVATAR,
            name: `${user.uuid}.avatar`,
            url: url,
            isDraft: false,
            uuid: user.uuid,
        });
        if (avatarURL) {
            debug(`Remove previous avatar from S3`);
            // Remove from S3
            await removeDocument(avatarURL);
        }
    });

    return {
        profile: profile.get({plain: true}),
    };
}

/**
 * Download remote profile img
 * @param {string} email
 * @param {string} url
 * @return {string}
 */
export async function downloadProfileImg(email, url) {
    debug(`To download: ${url} for ${email}`);
    let s3file = PROFILE_FOLDER + Date.now().toString();

    const result = await uploadReadableStream(s3file, url);

    debug(`Results: ${result}`);
    s3file = `${PROFILE_URL}${s3file}`;

    debug(`Sent: ${s3file}`);
    return s3file;
}

/**
 * Upload to S3 from stream
 * @param {string} s3file
 * @param {string} url
 * @return {Object}
 */
export async function uploadReadableStream(s3file, url) {
    const s3 = new S3();

    const {data} = await axios({
        url,
        method: 'GET',
        responseType: 'stream',
    });

    const params = {
        ACL: PROFILE_ACL,
        Bucket: BUCKET_NAME,
        Key: s3file,
        ContentType: 'image/jpeg',
        Body: data,
    };
    return s3.upload(params).promise();
}

/**
 * Remove profile avatar
 * @param {string} email
 * @return {Promise<object>}
 */
export async function removeProfileAvatar(email) {
    debug(`Removing profile avatar to ${email}`);

    const user = await models.users.scope('profile').findByEmail(email);
    if (!user.Profile.avatar) {
        // No avatar found
        debug(`Non-existent profile avatar`);
        return {
            success: false,
        };
    } else {
        debug(`Removing profile avatar`);
        const sequelize = getConnection();
        return await sequelize.transaction( async (dbTxn) => {
            await user.saveProfile({avatar: null}, {transaction: dbTxn});
            // Remove from S3
            return await removeDocument(user.Profile.avatar);
        });
    }

}


/**
 * Remove document from S3
 * @param {string} url
 * @return {Promise<object>}
 */
async function removeDocument(url) {
    // Remove from S3
    const s3 = new S3();
    const params = {
        Bucket: BUCKET_NAME,
        Key: url.replace(
            BUCKET_HOST + BUCKET_NAME + '/', ''
        ),
    };
    // Delete doc from S3
    await new Promise((resolve, reject) => {
        s3.deleteObject(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
    debug(`Document deleted from S3`);

    return {
        success: true,
    };
}

/**
 * Cancel user created recently
 * @param {string} email
 * @return {Promise}
 */
export async function cancel(email) {
    const user = await models.users.findByEmail(email, false);
    if (user) {
        await user.cancel();
        logger.info(`User ${email} canceled`);
    } else {
        logger.info(`Signal for cancel user ${email} ignored`);
    }
}

/**
 * Update user nickname
 * @param {string} uuid
 * @param {string} nickname
 * @return {Promise<bool>}
 */
export async function updateNickname(uuid, nickname) {
    debug(`Updating user ${uuid}`);
    await models.users.update({nickname}, {
        where: {uuid},
    });

    return true;
}

/**
 * Fill user information
 * @param {string[]} uuids
 * @param {object} [options]
 * @return {Promise<object[]>}
 */
export async function fillUserInformation(uuids, options = {}) {
    const scopes = options.scopes || [];
    delete options.scopes;
    const users = await models.users.scope(...scopes).findAll({
        where: {uuid: uuids},
        ...options,
    });

    return users.map((u) => u.get({plain: true}));
}

/**
 * Find user by wallet address
 * @param {string} from
 * @param {string} address
 * @return {Object}
 */
export async function findUserByWallet(from, address) {
    const authService = await loadService('@wallets');

    const userUUID = await authService.execute(
        'wallets:getWalletUser',
        address
    )
        .catch((e) => {
            if (e.name === 'RemoteNotFoundError') {
                debug(`Wallet not found`);
                throw new NotFoundError(`Wallet not found`, 'address');
            }

            throw e;
        });

    const userData = await findUser[1]({}, from, userUUID);

    return userData;
}
