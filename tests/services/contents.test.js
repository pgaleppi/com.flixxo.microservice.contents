import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

// This sets the mock adapter on the default instance
const mockAxios = new MockAdapter(axios);

jest.mock('../../src/sqsrpc');
jest.mock('../../src/rpclib');

import {
    EmptyResultError,
    ValidationError as SequelizeValidationError,
} from 'sequelize';
import {matchers} from 'jest-json-schema';
import {syncDb, loadFixture, loadData} from '../helpers/db';

import * as contentsService from '../../src/services/contents';
import models from '../../src/models';
import {ValidationError} from '../../src/errors';
import {fakeResponse, clear as clearRpc} from '../../src/rpclib';
import request from 'request';

expect.extend(matchers);

const listItemSchema = {
    type: 'object',
    properties: {
        price: {type: 'string'},
        uuid: {type: 'string'},
        contentType: {type: 'integer'},
        title: {type: 'string'},
        audioLang: {type: 'string'},
        categoryId: {type: 'integer'},
        authorFee: {type: 'number'},
        seederFee: {type: 'number'},
        author: {
            type: 'object',
            properties: {
                id: {type: 'integer'},
                Profile: {
                    realName: {type: 'string'},
                },
            },
        },
    },
};

const DetailedItemSchema = {
    type: 'object',
    properties: {
        ...listItemSchema.properties,
        bodyFormat: {type: 'integer'},
        body: {type: 'string'},
        author: {
            type: 'object',
            properties: {
                id: {type: 'integer'},
                Profile: {
                    realName: {type: 'string'},
                    gender: {type: 'integer'},
                    lang: {type: 'string'},
                },
            },
        },
        tags: {
            type: 'array',
            items: {type: 'string'},
        },
    },
};

const lang = 'en';

describe('Testing contents service', () => {
    const geo = {country: 'AR'};

    beforeEach(async (done) => {
        await syncDb();
        await loadData('categories');
        await loadData('00_countries');
        await loadFixture('users');
        await loadFixture('profiles');
        done();
    }, 10000);

    describe('#listTop', () => {

        beforeEach(async (done) => {
            await loadFixture('contents_for_service_check');
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        it(`Must to get a list of contents`, async () => {
            let result = await contentsService.listTop[3](geo, lang);
            expect(result.contents.length).toBe(25);
            expect(result.count).toBe(result.contents.length);
            expect(result.contents[0]).toMatchSchema(listItemSchema);
        });
    });

    describe('#get', () => {
        beforeEach(async (done) => {
            await loadFixture('contents-for-service-get');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must to get a single content reg`, async () => {
            const UUID = '7cdeff76-c4df-4bf2-a810-c0f054033a28';
            let result = await contentsService.getAvailableContent[3](
                geo, lang, UUID);
            expect(result).toMatchSchema(DetailedItemSchema);
            expect(result.uuid).toBe(UUID);
        });
    });

    describe.skip('#byCategory', () => {

        beforeEach(async (done) => {
            await loadFixture('contents_for_service_check');
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        it(`Must to get a list of contents by category`, async () => {
            let result = await contentsService.byCategory[3](geo, lang, 5);

            expect(!!result.contents.length).toBeTruthy();
            expect(result.count).toBe(result.contents.length);
            expect(result.count).toBe(3);

            result.contents.forEach((res) => {
                expect(res).toMatchSchema(listItemSchema);
                expect(typeof res.priority).toBe('undefined');
                expect(res.categoryId).toBe(5);
            });

            let uuids = result.contents.map((x) => x.uuid);

            expect(uuids[0]).toBe('faf29bda-e00b-42d1-a230-43911a57f1fb');
            expect(uuids[1]).toBe('3f568efa-c987-4186-b72c-d9a448757fd4');
            expect(uuids[2]).toBe('01c45832-4c6d-412e-b5ff-8e89fe8385dc');
        });
    });

    describe.skip('Get contents and series', () => {

        beforeEach(async (done) => {
            await loadFixture('contents-by-category');
            await loadFixture('serie-contents');
            await loadFixture('series-for-get');
            await loadFixture('season-for-get');
            await loadFixture('medias-for-series');
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        it(`Must to get a list of contents and series by category`,
                async () => {
            let result = await contentsService.byCategory[3](geo, lang, 5);

            expect(!!result.contents.length).toBeTruthy();
            expect(result.count).toBe(result.contents.length);
            expect(result.count).toBe(4);

            result.contents.forEach((res) => {
                expect(res).toMatchSchema(listItemSchema);
                expect(res.categoryId).toBe(5);
            });
        });
    });


    describe('#byUser', () => {
        beforeEach(async (done) => {
            await loadFixture('contents-for-scope-byuser');

            fakeResponse(
                'com.flixxo.microservice.payments',
                'contents:getMinPurchasedContents',
                () => [],
            );

            setTimeout(done, 1000); // For datetime filters
        });

        afterEach(() => clearRpc());

        it(`Must get top 5 priorized content that user hasnt bought`,
                async () => {
            let result = await contentsService
                                .byUser[3](geo, lang, 'user1@fakemail.com');

            expect(!!result.contents.length).toBeTruthy();
            expect(result.count).toBe(result.contents.length);

            result.contents.forEach((res) => {
                expect(res).toMatchSchema(listItemSchema);
            });

            return;
        });

    });

    describe('#byModerator', () => {
        beforeEach(async (done) => {
            await loadFixture('contents-for-scope-bymoderator');
            await loadFixture('users');
            setTimeout(done, 1000); // For datetime filters
        });

        it(`Must get only 4 moderator contents`, async () => {
            let result = await contentsService.byModerator[1](
                lang,
                'user1@fakemail.com');
            expect(result.contents.length).toBe(3);
        });

        it(`Must to get a single moderator content reg`, async () => {
            const UUID = '0000000-0000-0000-0000-000000000013';
            const result = await contentsService.detailByModerator[1](
                lang, 'user1@fakemail.com', UUID);
            expect(result).toMatchSchema(DetailedItemSchema);
            expect(result.uuid).toBe(UUID);
            expect(result.status).toBeDefined();
            expect(result.body).toBeDefined();
        });

        it(`Must not to get a single moderator content reg`, async () => {
            const UUID = '7cdeff76-c4df-4bf2-a810-c0f054033a28';
            try {
              await contentsService.detailByModerator[1](
                  lang, 'user3@fakemail.com', UUID);
            } catch (e) {
                expect(e.name).toBe('SequelizeEmptyResultError');
            }
        });

        it(`Must update moderation status for content TYPE_VIDEO`, async () => {
            const UUID = '0000000-0000-0000-0000-000000000011';
            const email = 'user2@fakemail.com';

            const newStatus = models.contents.MODSTATUS_REQUEST_CHANGES;
            const message = 'Change the title description please';

            const res = await contentsService.updateModerationStatus(
                email, UUID, newStatus, message);

            expect(res).toBeTruthy();
        });

        it(`Must update moderation status for content TYPE_SERIE`, async () => {
            const UUID = '2000000-0000-0000-0000-000000000001';
            const serieUUID = '1234567-0000-0000-0000-000000000001';
            const email = 'user1@fakemail.com';

            // TODO: check why fails when changing to MODSTATUS_ACCEPTED
            const newStatus = models.contents.MODSTATUS_REQUEST_CHANGES;
            const message = 'Change the title description please';

            const res = await contentsService.updateModerationStatus(
                email, UUID, newStatus, message);

            let serie = await models.serie.findByPk(serieUUID);
            let allSerieContents = await serie.getContent({raw: true});
            for (let content of allSerieContents) {
                expect(content.moderationStatus)
                    .toBe(models.contents.MODSTATUS_REQUEST_CHANGES);
            }

            expect(res).toBeTruthy();
        });


        it(`Must remove moderator`, async () => {
            const UUID = '0000000-0000-0000-0000-000000000013';
            const email = 'user1@fakemail.com';

            const result = await contentsService.unassignModerator(email, UUID);
            try {
              await contentsService.detailByModerator[1](
                  lang, 'user1@fakemail.com', UUID);
            } catch (e) {
                expect(e.name).toBe('SequelizeEmptyResultError');
            }
            expect(result).toBeTruthy();
        });
    });

    // @TODO fix test
    describe.skip('#seedbox', () => {

        beforeEach(async (done) => {
            await loadFixture('seedbox_contents');
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        it(`Must to get a list of contents`, async () => {
            const updatedDate = new Date(new Date().getTime() - 3600000);
            const result = await contentsService.seedboxList(updatedDate);
            expect(result.contents.length).toBe(1);
            expect(result.contents[0]).toMatchSchema(listItemSchema);
        });
    });

    // @TODO validate response schema and add tests
    describe('#getContentStatusByHash', () => {
        beforeEach(async (done) => {
            await loadFixture('users');
            await loadFixture('contents-for-get-by-hash');
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        test('Must to get a content by hash', async () => {
            await contentsService.getContentStatusByHash(
                 'ebff5c06d2b76d524833329c39ffaf4847957ee4');
        });

        test('Must to get a content if this is new', async () => {
            await contentsService.getContentStatusByHash(
                 'ebff5c06d2b76d524833329c39ffaf4847957ee5');
        });

        test('Must to get a content if this is unmoderated', async () => {
            await contentsService.getContentStatusByHash(
                 'ebff5c06d2b76d524833329c39ffaf4847957ee6');
        });

        test('Must to get a error if this is deleted', async () => {
            try {
                await contentsService.getContentStatusByHash(
                                'ebff5c06d2b76d524833329c39ffaf4847957ee7');
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('pass');
        });
    });

    describe('#create', () => {
        beforeEach(async (done) => {
            await loadData('categories');
            await loadFixture('temp-medias');
            await loadFixture('temp-subtitles-for-contentsversion');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must create a new content`, async () => {
            const hash = 'b2177406fc4d44036822a908494b15c2e3ee9027';
            const newContent = {
                'metadata': [
                    {
                        'lang': 'en',
                        'title': 'Fullmetal Alchemist (2017)',
                        'body': `The plot takes place at the beginning of the
                            20th century, in a reality where alchemy is real,
                            extremely developed and respected. The plot
                            features brothers Edward and Alphonse Elric who,
                            after attempting the forbidden technique of human
                            transmutation, suffer the consequences. Alphonse
                            loses his entire body while Edward loses his left
                            leg. Edward then sacrifices his right arm to save
                            his brother's soul by attaching it to a metal suit
                            of armor. Edward is then given mechanical
                            prosthetics known as "automail" in place of his
                            missing arm and leg. Upon acceptance into the
                            State Alchemists, he is given the nickname
                            "Fullmetal Alchemist", all while searching with
                            Alphonse for the legendary philosopher's stone,
                            that will repair their bodies.`,
                    },
                ],
                'categoryId': 2,
                'seederFee': 10,
                'authorFee': 60,
                hash,
                'mediaIds': [1, 2, 3, 4],
                'subtitleIds': [2, 3, 4],
                'torrentFile': `ZDg6YW5ub3VuY2UzOTpodHRwczovL3RyYWNrZ
                    XIucHJvLmZsaXh4by5jb20vYW5ub3VuY2UxMzphbm5vdW5jZS
                    1saXN0bGwzOTpodHRwczovL3RyYWNrZXIucHJvLmZsaXh4by5
                    jb20vYW5ub3VuY2VlZTEwOmNyZWF0ZWQgYnkyODo5MzhlNjEw
                    OThmNDU1YjgzYWQyN2M1ZmJkZmE3MTM6Y3JlYXRpb24gZGF0Z
                    WkxNTI5NjA1MzgzZTg6ZW5jb2Rpbmc1OlVURi04NDppbmZvZD
                    Y6bGVuZ3RoaTczMjc5MjAxZTQ6bmFtZTM3OkppbWkgSGVuZHJ
                    peCAtIFZhbGxleXMgT2YgTmVwdHVuZS5tcDQxMjpwaWVjZSBs
                    ZW5ndGhpMjA5NzE1MmU2OnBpZWNlczcwMDovnjjtWEKUvuI1o
                    L/PdVwNryjk0qd5QpOpkximfjjcTt2pxV3tbC/3JaOU2OV1TD
                    UJFN0PwzElWmMHgGPw8Jkvwl1kWKKp8mOXfJbrXn2IJOjKh/T
                    g2x9kBhN5jjpHxK1y8Mg7kGKxRmIBHz4Ketn9Lwzt7UdPj+w0
                    lQmLRoGtCzyflcKNEAT7j58n7HJoQJ6Q9+xd1Eeooe3DrGPf2
                    FTta+NyddJCLWh9sAb6ymipQdP0ZT812UiSHzVUpt2texxjcD
                    oS0aKWnQdTPfGkV4k7QScGUAipeRXGUbVDI82yD8JW9h5qtM7
                    EHw5HfYvBwDAlxDDoVuKZuvVIi3ByVIezCyYRX9Egf6wkYxfT
                    bXmo815D4AskuORB4y1RtK0IZxbcub5Tb2Q/cPEVHM8PrFSFG
                    hTohKkyNC9nhLIyJElkRj0OMCsQbaVlLlxeOAYmp7nla7kr99
                    +mLERVrK/oIpxYEA/0BFfZCrisfsmGnI0cqwO9QfJxH16IeFh
                    wpt/o0M4cHoBkAiz2TtF/nZeW3nqMmVLBonGLOMRjZspLlTSF
                    A/jyAOKtA6lMAgp8wZ7PjvzwFn9EomPUm3kNYrYHhaVkicWzO
                    agWjFMk8W1xhMe1D8VHSbnSA+K8SMxUrz39fXh3HPIelm2Fdy
                    fv3LWUW+PL+s+NATmjes/rI4nuRq5vJTrObRkf9nLedcguv2p
                    OOCNT4+08nKKjkmahRll/pRASj70eZVHGdgmn+nGvfLzCoZLF
                    pRQtOhEep61DojTqdtffmYDuZ5Vn8bEvp8jfCJO3RtFppBLL/
                    8pJ1fd8C3rV8mcfUBmm4JRujZWF9EYoGYNb21QCHFXCV7qynC
                    0/8cMd5NjaTwvS56bR3k/RRxJ1yWVEhwQzjlyd5z+581lKCWq
                    y3yxbgz/i+JimZ3+JUMBkHZDqzlXcsmrkPZuDNzpwcml2YXRl
                    aTFlZWU=%`,
                'geoBlocking': {
                    'type': 16,
                    'countries': [10, 11, 12],
                },
            };

            mockAxios.reset();
            mockAxios.onPost().reply(200, {});
            mockAxios.onPut().reply(200, {});

            const result = await contentsService.createContent(
                    'user1@fakemail.com', 'user2@fakemail.com', newContent);
            expect(result.success).toBeTruthy();
            expect(result.uuid).toBeDefined();
            expect(result.torrentFile).toBeDefined();

            const contentModel = models.contents;
            const content = await contentModel
                .scope([{method: ['i18n', 'es']}])
                .findOne({
                where: {uuid: result.uuid},
                include: ['media', 'subtitle'],
            });
            expect(content.media.length).toBe(4);
            expect(content.subtitle.length).toBe(3);

            expect(content.title).toBe('Fullmetal Alchemist (2017)');
            expect(content.body).toBeTruthy();

            const geoBlock = await models.geoblocking.findOne({
                where: {targetUUID: result.uuid},
            });
            expect(geoBlock).toBeTruthy();

            const countries = await geoBlock.getCountries();
            expect(geoBlock.type).toBe(16);
            expect(countries.length).toBe(3);
            expect(countries.map((country) => country.id))
                .toEqual([10, 11, 12]);

            const metadata = await models.metadata.findAll({
                where: {entityId: result.uuid},
            });
            expect(metadata).toBeTruthy();
            expect(metadata.length).toBe(1);
        });

        it(`Must reject bad torrentfile`, async () => {
            const hash = 'b2177406fc4d44036822a908494b15c2e3ee9027';
            const newContent = {
                'metadata': [
                    {
                        'lang': 'en',
                        'title': 'Fullmetal Alchemist (2017)',
                        'body': `The plot takes place at the beginning of the
                            20th century, in a reality where alchemy is real,
                            extremely developed and respected. The plot
                            features brothers Edward and Alphonse Elric who,
                            after attempting the forbidden technique of human
                            transmutation, suffer the consequences. Alphonse
                            loses his entire body while Edward loses his left
                            leg. Edward then sacrifices his right arm to save
                            his brother's soul by attaching it to a metal suit
                            of armor. Edward is then given mechanical
                            prosthetics known as "automail" in place of his
                            missing arm and leg. Upon acceptance into the
                            State Alchemists, he is given the nickname
                            "Fullmetal Alchemist", all while searching with
                            Alphonse for the legendary philosopher's stone,
                            that will repair their bodies.`,
                    },
                ],
                'categoryId': 2,
                'seederFee': 10,
                'authorFee': 60,
                hash,
                'mediaIds': [1, 2, 3, 4],
                'torrentFile': `ZDg6YW5ub3VuY2UzOTpodHRwczovL3RyYWNrZXIucHJvLmZ
                    saXh4by5jb20vYW5ub3VuY2UxMzphbm5vdW5jZS1saXN0bGwzOTpodHRwcz
                    ovL3RyYWNrZXIucHJvLmZsaXh4by5jb20vYW5ub3VuY2VlZTEwOmNyZWF0Z
                    WQgYnkyODo5MzhlNjEwOThmNDU1YjgzYWQyN2M1ZmJkZmE3MTM6Y3JlYXRp
                    b24gZGF0ZWkxNTI5NjA1MzgzZTg6ZW5jb2Rpbmc1OlVURi04NDppbmZvZDY
                    6bGVuZ3RoaTczMjc5MjAxZTQ6bmFtZTM3OkppbWkgSGVuZHJpeCAtIFZhbG
                    xleXMgT2YgTmVwdHVuZS5tcDQxMjpwaWVjZSBsZW5ndGhpMjA5NzE1MmU2O
                    nBpZWNlczcwMDovnjjtWEKUvuI1oL
                    /PdVwNryjk0qd5QpOpkximfjjcTt2pxV3tbC
                    /3JaOU2OV1TDUJFN0PwzElWmMHgGPw8Jkvwl1kWKKp8mOXfJbrXn2IJOjKh
                    /Tg2x9kBhN5jjpHxK1y8Mg7kGKxRmIBHz4Ketn9Lwzt7UdPj
                    +w0lQmLRoGtCzyflcKNEAT7j58n7HJoQJ6Q9
                    +xd1Eeooe3DrGPf2FTta
                    +NyddJCLWh9sAb6ymipQdP0ZT812UiSHzVUpt2texxjcDoS0aKWnQdTPfGk
                    V4k7QScGUAipeRXGUbVDI82yD8JW9h5qtM7EHw5HfYvBwDAlxDDoVuKZuvV
                    Ii3ByVIezCyYRX9Egf6wkYxfTbXmo815D4AskuORB4y1RtK0IZxbcub5Tb2Q
                    /cPEVHM8PrFSFGhTohKkyNC9nhLIyJElkRj0OMCsQba
                    VlLlxeOAYmp7nla7kr99
                    +mLERVrK
                    /oIpxYEA
                    /0BFfZCrisfsmGnI0cqwO9QfJxH16IeFhwpt
                    /o0M4cHoBkAiz2TtF
                    /nZeW3nqMmVLBonGLOMRjZspLlTSFA
                    /jyAOKtA6lMAgp8wZ7PjvzwFn9EomPUm3kNYrYHhaVkic
                    WzOagWvvvk8W1xhMe1D8VHSbnSA
                    +K8SMxUrz39fXh3HPIelm2Fdyfv3LWUW+PL+s+NATmjes
                    /rI4nuRq5vJTrObRkf9nLedcguv2pOOCNT4
                    +08nKKjkmahRll
                    /pRASj70eZVHGdgmn
                    +nGvfLzCoZLFpRQtOhEep61DojTqdtffmYDuZ5Vn8bEvp8jfCJO3RtFppBLL
                    /8pJ1fd8C3rV8mcfUBmm4JRujZWF9EYoGYNb21QCHFXCV7qynC0
                    /8cMd5NjaTwvS56bR3k/RRxJ1yWVEhwQzjlyd5z
                    +581lKCWqy3yxbgz/i+JimZ3
                    +JUMBkHZDqzlXcsmrkPZuDNzpwcml2YXRlaTFlZWU=%`,
            };
            return expect(contentsService.createContent(
                'user1@fakemail.com', 'user2@fakemail.com', newContent))
                .rejects.toThrow(ValidationError);
        });

    });

    describe('#contentMedia', () => {
        beforeEach(async (done) => {
            await loadFixture('temp-medias');
            await loadFixture('contents-for-service-get');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must create temporary media`, async () => {
            const result = await contentsService.createTempContentMedia(
                    'b2177406fc4d44036822a908494b15c2e3ee9027',
                    'https://yts.am/assets/images/movies/fullmetal_alchemist_2017/medium-cover.jpg',
                    'Cover image',
                    1,
                    1,
                    'user1@fakemail.com',
                );
            expect(result.success).toBeTruthy();
            expect(result.url).toBeDefined();
            expect(result.id).toBeDefined();
        });

        it(`Return tmp medias`, async () => {
            const result = await contentsService.getTempContentMedia(
                    {
                        hash: 'b2177406fc4d44036822a908494b15c2e3ee9027',
                        userId: 100,
                    },
                    [1, 2, 3, 4],
                );
            expect(result.length).toBe(4);
        });

        it(`Reject to return tmp medias`, async () => {
            expect(contentsService.getTempContentMedia(
                    {
                        hash: 'b2177406fc4d44036822a908494b15c2e3ee9027',
                        userId: 100,
                    },
                    [1, 2, 3, 5],
                )).rejects.toThrow(ValidationError);
        });

        it(`Create medias`, async () => {
            const tempMedias = await contentsService.getTempContentMedia(
                    {
                        hash: 'b2177406fc4d44036822a908494b15c2e3ee9027',
                        userId: 100,
                    },
                    [1, 2, 3, 4],
                );
            const result = await contentsService.createMedias(
                    tempMedias,
                    '7cdeff76-c4df-4bf2-a810-c0f054033a28',
                    null,
                );
            expect(result.success).toBeTruthy();
        });
    });

    describe('#contentSubtitle', () => {
        beforeEach(async (done) => {
            await loadFixture('temp-subtitles-for-contentsversion');
            await loadFixture('contents-for-service-get');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must create temporary subtitle`, async () => {
            mockAxios.onGet().reply(200,
                    `WEBVTT

                    00:01.000 --> 00:04.000
                    Never drink liquid nitrogen.

                    00:05.000 --> 00:09.000
                    - It will perforate your stomach.
                    - You could die.`
            );
            const result = await contentsService.createTempContentSubtitle(
                    'user1@fakemail.com',
                    'b2177406fc4d44036822a908494b15c2e3ee9027',
                    'https://s3.amazonaws.com/flixxo-local-mp/content-subtitle/1549903216741',
                    'en',
                    false,
                    1,
                );
            expect(result.success).toBeTruthy();
            expect(result.url).toBeDefined();
            expect(result.id).toBeDefined();
        });

        it(`Return tmp subtitles`, async () => {
            const result = await contentsService.getTempContentSubtitles(
                    {
                        hash: 'b2177406fc4d44036822a908494b15c2e3ee9027',
                        userId: 100,
                    },
                    [1, 2, 3, 4],
                );
            expect(result.length).toBe(4);
        });

        it(`Reject to return tmp subtitles`, async () => {
            expect(contentsService.getTempContentSubtitles(
                    {
                        hash: 'b2177406fc4d44036822a908494b15c2e3ee9027',
                        userId: 100,
                    },
                    [1, 2, 3, 5],
                )).rejects.toThrow(ValidationError);
        });

        it(`Create subtitles`, async () => {
            const tempSubtitles = await contentsService.getTempContentSubtitles(
                    {
                        hash: 'b2177406fc4d44036822a908494b15c2e3ee9027',
                        userId: 100,
                    },
                    [1, 2, 3, 4],
                );
            const result = await contentsService.createSubtitles(
                    tempSubtitles,
                    '7cdeff76-c4df-4bf2-a810-c0f054033a28',
                    null,
                );
            expect(result.success).toBeTruthy();
        });
    });

    describe('#contentMetadata', () => {
        beforeEach(async (done) => {
            await loadFixture('contents-for-service-get');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must create content metadata`, async () => {
            const CONTENT_UUID = 'b2177406fc4d44036822a908494b15c2e3ee9027';
            const metadata = [
              {
                lang: 'en',
                title: 'Dummy Title',
                body: 'Dummy Body',
              },
            ];

            const result = await contentsService.createContentMetadata(
                metadata,
                CONTENT_UUID,
                false,
            );
            expect(result.success).toBeTruthy();

            const dbMetadata = await models.metadata.findAll({
                where: {
                    entityId: CONTENT_UUID,
                    entityType: models.metadata.ENTITY_TYPE_CONTENT,
                },
            });

            expect(dbMetadata).toBeTruthy();
            expect(dbMetadata.length).toBe(1);
            expect(dbMetadata[0].lang).toBe('en');
            expect(dbMetadata[0].title).toBe('Dummy Title');
            expect(dbMetadata[0].body).toBe('Dummy Body');
        });
    });

    describe('#byFolloweds', () => {

        beforeEach(async (done) => {
            await syncDb(true);
            await loadData('categories');
            await loadFixture('contents-for-followers-scope');
            done();
        });

        test(`Must to get videos`, async () => {
            const contents = await contentsService.byFolloweds[2](
                lang, 'AR', 'user@4.com');
            expect(contents.length).toBe(3);
        });

    });

    /**
     * @TODO
     *
     * The method listTopByAuthor uses microservices calls, and it mocks are
     * not implemented.
     */
    describe.skip('#listTopByAuthor', () => {
        beforeEach(async (done) => {
            await loadFixture('contents-for-scope-bymoderator');
            setTimeout(done, 1000); // For datetime filters
        });

        it(`Must get only author contents`, async () => {
            let result = await contentsService.listTopByAuthor[1](
                lang,
                'user1@fakemail.com');

            expect(result.count).toBe(result.contents.length);
            expect(result.count).toBe(11);
        });

    });

    describe('#delete', () => {
        beforeEach(async (done) => {
            await loadFixture('contents-for-service-get');
            mockAxios.onDelete().reply(200, {});
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        afterEach(() => {
            mockAxios.reset();
        });

        it(`Must delete a content`, async () => {
            const contentId = '7cdeff76-c4df-4bf2-a810-c0f054033a28';
            const content = await contentsService.getAvailableContent[3](
                geo, lang, contentId);

            const result = await contentsService
                .deleteContent('user4@fakemail.com', content.uuid);

            expect(result.success).toBeTruthy();
            await expect(contentsService.getAvailableContent[3](geo, lang, contentId))
                .rejects.toThrow(EmptyResultError);
        });

        it(`Must not delete a content`, async () => {
            const contentId = '7cdeff76-c4df-4bf2-a810-c0f054033a28';

            await expect(contentsService
                            .deleteContent('user3@fakemail.com',
                                '7cdeff76-c4df-4bf2-a810-c0f054033a28')
                )
                .rejects.toThrow(EmptyResultError);
            const content = await contentsService
                                    .getAvailableContent[2](geo, lang, contentId);
            expect(content).toBeDefined();
        });
    });

    describe('#update download status', () => {
        beforeEach(async (done) => {
            await loadFixture('contents-for-service-get');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must increment download status by 1`, async () => {
            const hash = 'ebff5c06d2b76d524833329c39ffaf4847957ee4';

            const result = await contentsService
                                    .updateContentDownloadStatus(hash);

            expect(result.success).toBeTruthy();
            expect(result.downloadStatus).toBe(2);
        });

        it(`Must decrement download status by 4`, async () => {
            const hash = 'ebff5c06d2b76d524833329c39ffaf4847957ee2';

            const result = await contentsService
                                    .updateContentDownloadStatus(hash, 4, true);

            expect(result.success).toBeTruthy();
            expect(result.downloadStatus).toBe(6);
        });
    });

    describe('#getTorrentInformation', () => {
        beforeEach(async () => {
            await syncDb();
            await loadFixture('contents-get-torrent-information');
        });

        test(`Must to return valid torrentFile`, async () => {
            let res = await contentsService.getTorrentInformation(
                'b2177406fc4d44036822a908494b15c2e3ee9027');

            expect(res).toEqual({
                infoHash: 'b2177406fc4d44036822a908494b15c2e3ee9027',
                lastPieceLength: 1976033,
                pieceLength: 2097152,
                totalLength: 73279201,
            });
        });

        test(`Must to throw not found`, async () => {
            return expect(contentsService.getTorrentInformation(
                'b2177406fccd440c6822a908494b15c2e3ee9027'))
                .rejects.toThrow(EmptyResultError);
        });
    });

    describe('#setStatus', () => {
        beforeEach(async () => {
            await syncDb(true);
            await loadFixture('users');
            await loadData('categories');
            await loadFixture('contents-for-set-status');
        });

        afterEach(() => {
            mockAxios.reset();
        });

        test(`Must to exec with valid status`, async () => {
            mockAxios.onDelete().reply(200, {});
            await contentsService.setStatus(
                '41a0ff76-a4df-2bf2-a810-c0f054033a28',
                0x30,
                'I left my religion'
            );
        });

        test(`Must to throw not found error`, async () => {
            try {
                await contentsService.setStatus(
                    '41a0ff76-a4df-2bf2-a800-c0f054033a28',
                    0x30
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });

        test(`Must to throw not invalid status`, async () => {
            try {
                await contentsService.setStatus(
                    '41a0ff76-a4df-2bf2-a810-c0f054033a28',
                    0x99
                );
            } catch (e) {
                expect(e).toBeInstanceOf(SequelizeValidationError);
                expect(e.message).toBe('Validation error: Invalid status');
                return;
            }

            throw new Error('Pass');
        });

    });

    describe('#setStatusByAuthor', () => {
        beforeEach(async () => {
            await syncDb(true);
            await loadFixture('users');
            await loadData('categories');
            await loadFixture('contents-for-set-status');
        });

        afterEach(() => {
            mockAxios.reset();
        });

        test(`Must to exec with valid status`, async () => {
            mockAxios.onDelete().reply(200, {});

            await contentsService.setStatusByAuthor(
                'user1@fakemail.com',
                '41a0ff76-a4df-2bf2-a810-c0f054033a28',
                0x30,
                'There are a new version'
            );
        });

        test(`Must to throw not found content error`, async () => {
            try {
                await contentsService.setStatusByAuthor(
                    'user1@fakemail.com',
                    '41a0ff76-a4df-2bf2-a800-c0f054033a28',
                    0x30
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                expect(`${e}`).toBe('SequelizeEmptyResultError');
                return;
            }

            throw new Error('Pass');
        });

        test(`Must to throw not found user error`, async () => {
            try {
                await contentsService.setStatusByAuthor(
                    'user99@fakemail.com',
                    '41a0ff76-a4df-2bf2-a810-c0f054033a28',
                    0x30
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                expect(e.message).toBe('');
                return;
            }

            throw new Error('Pass');
        });


        test(`Must to throw not invalid status`, async () => {
            try {
                await contentsService.setStatusByAuthor(
                    'user1@fakemail.com',
                    '41a0ff76-a4df-2bf2-a810-c0f054033a28',
                    0x99
                );
            } catch (e) {
                expect(e).toBeInstanceOf(SequelizeValidationError);
                expect(e.message).toBe('Validation error: Invalid status');
                return;
            }

            throw new Error('Pass');
        });

        test(`Must to throw found error with no-owned content`, async () => {
            try {
                await contentsService.setStatusByAuthor(
                    'user1@fakemail.com',
                    '41a0ff76-a4df-2bf2-a810-c0f054033a31',
                    0x30
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });

    });

    describe('#updateContent', () => {

        beforeAll(() => {
            jest.mock('aws-sdk');
            jest.mock('../../src/sqsrpc');
        });

        beforeEach(async (done) => {
            await syncDb(true);
            await loadData('categories');
            await loadFixture('users');
            await loadFixture('contents-for-update-service');
            done();
        });

        afterAll(() => {
            jest.unmock('aws-sdk');
            jest.unmock('../../src/sqsrpc');
        });

        it(`Must update content title`, async () => {
            const UUID = '1234ff76-1010-4bf2-a810-c0f054033a28';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );
            originalContent.metadata = [
                {
                    'lang': 'en',
                    'title': 'New title',
                    'body': 'New body',
                },
            ];

            const result = await contentsService.updateContent(
                'user4@fakemail.com', UUID, originalContent
            );
            let updatedContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );

            expect(result.success).toBeTruthy();
            expect(updatedContent.title).toBe('New title');
            expect(updatedContent.body).toBe('New body');
            expect(updatedContent.moderationStatus)
                .toBe(models.contents.MODSTATUS_CHANGES_REVIEW);
        });

        test(`Must accept empty arrays`, async () => {
            const UUID = '1234ff76-1010-4bf2-a810-c0f054033a28';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );
            originalContent.newMediaIds = [];
            originalContent.deleteMediaIds = [];

            const result = await contentsService.updateContent(
                'user4@fakemail.com', UUID, originalContent
            );
            let updatedContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );

            expect(result.success).toBeTruthy();
            expect(updatedContent.moderationStatus)
                .toBe(models.contents.MODSTATUS_CHANGES_REVIEW);
        });

        it(`Must delete medias`, async () => {
            const UUID = '1234ff76-1010-4bf2-a810-c0f054033a28';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );
            originalContent.newMediaIds = [];
            originalContent.deleteMediaIds = [2];

            const result = await contentsService.updateContent(
                'user4@fakemail.com', UUID, originalContent
            );
            let updatedContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );

            expect(result.success).toBeTruthy();
            expect(updatedContent.moderationStatus)
                .toBe(models.contents.MODSTATUS_CHANGES_REVIEW);
        });

        it(`Must delete subtitles`, async () => {
            const UUID = '1234ff76-1010-4bf2-a810-c0f054033a28';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );
            originalContent.newSubtitleIds = [];
            originalContent.deleteSubtitleIds = [2];

            const result = await contentsService.updateContent(
                'user4@fakemail.com', UUID, originalContent
            );
            let updatedContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );

            expect(result.success).toBeTruthy();
            expect(updatedContent.moderationStatus)
                .toBe(models.contents.MODSTATUS_CHANGES_REVIEW);
        });

        test(`Must to avoid delete the only cover`, async () => {
            const UUID = '1234ff76-1010-4bf2-a810-c0f054033a28';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );
            originalContent.newMediaIds = [];
            originalContent.deleteMediaIds = [1];


            try {
                await contentsService.updateContent(
                'user4@fakemail.com', UUID, originalContent
                );
            } catch (e) {
                expect(e).toBeInstanceOf(ValidationError);
                expect(e.message).toBe('Must to be one cover');
                return;
            }

            throw new Error('Pass');
        });

        test(`Must to avoid delete all thumbs`, async () => {
            const UUID = '1234ff76-1010-4bf2-a810-c0f054033a28';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );
            originalContent.newMediaIds = [];
            originalContent.deleteMediaIds = [2, 3];


            try {
                await contentsService.updateContent(
                    'user4@fakemail.com', UUID, originalContent
                );
            } catch (e) {
                expect(e).toBeInstanceOf(ValidationError);
                expect(e.message).toBe('Invalid number of thumbs');
                return;
            }

            throw new Error('Pass');
        });

        test(`Must to create new medias`, async () => {
            const UUID = '1234ff76-1010-4bf2-a810-c0f054033a29';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );
            originalContent.newMediaIds = [1];
            originalContent.deleteMediaIds = [];

            const result = await contentsService.updateContent(
                'user4@fakemail.com', UUID, originalContent
            );
            let updatedContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );

            expect(result.success).toBeTruthy();
            expect(updatedContent.moderationStatus)
                .toBe(models.contents.MODSTATUS_CHANGES_REVIEW);
        });

        test(`Must to create new subtitles`, async () => {
            const UUID = '1234ff76-1010-4bf2-a810-c0f054033a29';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );
            originalContent.newSubtitleIds = [1];
            originalContent.deleteSubtitleIds = [];

            const result = await contentsService.updateContent(
                'user4@fakemail.com', UUID, originalContent
            );
            let updatedContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );

            expect(result.success).toBeTruthy();
            expect(updatedContent.moderationStatus)
                .toBe(models.contents.MODSTATUS_CHANGES_REVIEW);
        });


        test(`Must to replace cover`, async () => {
            const UUID = '1234ff76-1010-4bf2-a810-c0f054033a29';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );
            originalContent.newMediaIds = [2];
            originalContent.deleteMediaIds = [10];

            const result = await contentsService.updateContent(
                'user4@fakemail.com', UUID, originalContent
            );
            let updatedContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );

            expect(result.success).toBeTruthy();
            expect(updatedContent.moderationStatus)
                .toBe(models.contents.MODSTATUS_CHANGES_REVIEW);
        });

        test(`Must to avoid add two covers`, async () => {
            const UUID = '1234ff76-1010-4bf2-a810-c0f054033a29';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user4@fakemail.com',
                UUID,
            );
            originalContent.newMediaIds = [2];
            originalContent.deleteMediaIds = [];

            try {
                await contentsService.updateContent(
                    'user4@fakemail.com', UUID, originalContent
                );
            } catch (e) {
                expect(e).toBeInstanceOf(ValidationError);
                expect(e.message).toBe('Must to be one cover');
                return;
            }

            throw new Error('Pass');
        });
    });

    describe('#existsHash', () => {
        const T_RESULT = {exists: true};
        const F_RESULT = {exists: false};

        beforeEach(async () => {
            await syncDb(true);
            await loadFixture('users');
            await loadData('categories');
            await loadFixture('contents-for-exists-hash');
        });

        test(`Must to be true on existing hash`, async () => {
            const res = await contentsService.existsHash(
                'ebff5c06d2b76d524833329c39ffaf4847957ee4');
            expect(res).toMatchObject(T_RESULT);
        });

        test(`Must to be false on unexisting hash`, async () => {
            const res = await contentsService.existsHash(
                'ebff5c06d2b76d524830009c39ffaf4847957ee4');
            expect(res).toMatchObject(F_RESULT);
        });

        test(`Must to be true on existing hash (unavailable)`, async () => {
            const res = await contentsService.existsHash(
                'ebff5c06d2b76d524833329c39ffaf4847957ee2');
            expect(res).toMatchObject(T_RESULT);
        });

        test(`Must to be true on existing hash (deleted)`, async () => {
            const res = await contentsService.existsHash(
                'ebff5c06d2b76d524833329c39ffaf4847957ee2');
            expect(res).toMatchObject(T_RESULT);
        });

        test(`Must to fail if hash is invalid`, async () => {
            try {
                await contentsService.existsHash(
                    'ebff5c06d2b76d524833329c39ffaf4847957ezz');
            } catch (e) {
                expect(e).toBeInstanceOf(ValidationError);
                expect(e.message).toBe('Invalid hash');
                return;
            }

            throw new Error('Pass');
        });
    });

    describe('#ContentGeoblocking', () => {
        const T_RESULT = {success: true};

        beforeEach(async () => {
            await syncDb(true);
            await loadFixture('users');
            await loadData('categories');
            await loadFixture('contents-for-geoblocking-check');
            await loadFixture('countries');

            mockAxios.reset();
            mockAxios.onPost().reply(200, {});
            mockAxios.onPut().reply(200, {});
        });

        test(`Must to set content geoblocking`, async () => {
            const CONTENT_UUID = '7cdeff76-c4df-4bf2-a810-c0f054033a28';
            const AUTHOR_EMAIL = 'user1@fakemail.com';
            const res = await contentsService.setContentGeoblocking(
                AUTHOR_EMAIL, CONTENT_UUID, 16, [10, 11, 12]);
            const geoBlock = await contentsService.getContentGeoblocking(
                AUTHOR_EMAIL, CONTENT_UUID,
            );
            expect(geoBlock).toBeTruthy();
            expect(geoBlock.type).toBe(16);
            expect(geoBlock.countries.length).toBe(3);
            expect(geoBlock.countries.map((country) => country.id))
                .toEqual([10, 11, 12]);
            expect(res).toMatchObject(T_RESULT);
        });

        test(`Must to fail to set content geoblocking`, async () => {
            const CONTENT_UUID = '00000000-c4df-4bf2-a810-c0f054033a28';
            const AUTHOR_EMAIL = 'user1@fakemail.com';
            const geoBlock = await models.geoblocking.findOne({
                where: {targetUUID: CONTENT_UUID},
            });
            expect(geoBlock).toBeFalsy();
            expect(contentsService.setContentGeoblocking(
                AUTHOR_EMAIL, CONTENT_UUID, 16, [10, 11, 12]))
                .rejects.toThrow(ValidationError);
        });

        test(`Must to replace content geoblocking`, async () => {
            const CONTENT_UUID = '7cdeff76-c4df-4bf2-a810-c0f054033a28';
            const AUTHOR_EMAIL = 'user1@fakemail.com';

            // First update
            const res = await contentsService.setContentGeoblocking(
                AUTHOR_EMAIL, CONTENT_UUID, 16, [10]);
            const geoBlock = await models.geoblocking.findOne({
                where: {targetUUID: CONTENT_UUID},
            });
            const countries = await geoBlock.getCountries();
            expect(geoBlock).toBeTruthy();
            expect(geoBlock.type).toBe(16);
            expect(countries.length).toBe(1);
            expect(countries.map((country) => country.id))
                .toEqual([10]);
            expect(res).toMatchObject(T_RESULT);

            // Second update
            const res2 = await contentsService.setContentGeoblocking(
                AUTHOR_EMAIL, CONTENT_UUID, 32, [11, 12]);
            const geoBlock2 = await models.geoblocking.findAll({
                where: {targetUUID: CONTENT_UUID},
            });
            expect(geoBlock2).toBeTruthy();
            expect(geoBlock2.length).toBe(1);
            expect(geoBlock2[0].type).toBe(32);

            const countries2 = await geoBlock2[0].getCountries();
            expect(countries2.length).toBe(2);
            expect(countries2.map((country) => country.id))
                .toEqual([11, 12]);
            expect(res2).toMatchObject(T_RESULT);

        });

        test(`Must to delete content geoblocking if it exists`, async () => {
            const CONTENT_UUID = '7cdeff76-c4df-4bf2-a810-c0f054033a28';
            const AUTHOR_EMAIL = 'user1@fakemail.com';
            await contentsService.setContentGeoblocking(
                AUTHOR_EMAIL, CONTENT_UUID, 16, [10, 11, 12]);
            await contentsService.deleteContentGeoblocking(
                AUTHOR_EMAIL, CONTENT_UUID);

            const geoBlock = await models.geoblocking.findOne({
                where: {targetUUID: CONTENT_UUID},
            });

            expect(geoBlock).toBeFalsy();
        });

        test(`Must throw error when deleting geoblocking if content
                belongs to another user`, async () => {
            const CONTENT_UUID = '7cdeff76-c4df-4bf2-a810-c0f054033a28';
            const AUTHOR_EMAIL = 'user2@fakemail.com';
            expect(contentsService.deleteContentGeoblocking(
                AUTHOR_EMAIL, CONTENT_UUID)).rejects.toThrow(ValidationError);
        });

    });

    describe('#Subtitles format check', () => {
        beforeEach(async (done) => {
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        it('Must return is srt subtitle', async () => {
            let srtFile = 'https://s3.amazonaws.com/flixxo-local-mp/content-subtitle/1549903216741';
            await request.get(srtFile, (err, res, body) => {
                if (!err && res.statusCode == 200) {
                    let subdata = contentsService.validate(body);
                    expect(subdata.format).toBe('srt');
                }
            });
        });

        it('Must return is vtt subtitle', async () => {
            let vttFile = 'https://s3.amazonaws.com/flixxo-local-mp/content-subtitle/1549912048705';
            await request.get(vttFile, (err, res, body) => {
                if (!err && res.statusCode == 200) {
                    let subdata = contentsService.validate(body);
                    expect(subdata.format).toBe('vtt');
                }
            });
        });

        it('Must return is invalid subtitle file', async () => {
            let invFile = 'http://www.authord.com/downloads/whatsnew.pm.txt';
            await request.get(invFile, (err, res, body) => {
                if (!err && res.statusCode == 200) {
                    let subdata = contentsService.validate(body);
                    expect(subdata.format.includes('invalid')).toBe(true);
                }
            });
        });
    });

    describe('#convert subtitles formats to vtt', () => {
        beforeEach(async (done) => {
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        it('Must convert srt subtitle to vtt', async () => {
            let srtFile = 'https://s3.amazonaws.com/dev-flixxo-assets/content-subtitle/test.srt';
            await request.get(srtFile, async (err, res, body) => {
                if (!err && res.statusCode == 200) {
                    let subdata = contentsService.validate(body);
                    expect(subdata.format).toBe('srt');
                    let subvtt = contentsService.convert(body, 'vtt');
                    subvtt = contentsService.toValidMilisecond( subvtt );
                    let subvttdata = contentsService.validate(subvtt);
                    expect(subvttdata.format).toBe('vtt');
                }
            });
        });

        it('Must convert srt subtitle to .sub and then .vtt', async () => {
            let srtFile = 'https://s3.amazonaws.com/flixxo-local-mp/content-subtitle/1549903216741';
            await request.get(srtFile, async (err, res, body) => {
                if (!err && res.statusCode == 200) {
                    let subdata = contentsService.validate(body);
                    expect(subdata.format).toBe('srt');
                    let sub = contentsService.convert(body, 'sub');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('sub');
                    sub = contentsService.convert(sub, 'vtt');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('vtt');
                }
            });
        });

        it('Must convert srt subtitle to .sbv and then .vtt', async () => {
            let sbvFile = 'https://amara.org/en/subtitles/NmkV5cbiCqUU/en/1/download/Learn%20about%20Universal%20Subtitles.en.sbv';
            await request.get(sbvFile, async (err, res, body) => {
                if (!err && res.statusCode == 200) {
                    let subdata = contentsService.validate(body);
                    expect(subdata.format).toBe('sbv');
                    let sub = contentsService.convert(body, 'vtt');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('vtt');
                }
            });
        });

        it('Must convert srt subtitle to .ssa and then .vtt', async () => {
            let srtFile = 'https://s3.amazonaws.com/flixxo-local-mp/content-subtitle/1549903216741';
            await request.get(srtFile, async (err, res, body) => {
                if (!err && res.statusCode == 200) {
                    let subdata = contentsService.validate(body);
                    expect(subdata.format).toBe('srt');
                    let sub = contentsService.convert(body, 'ssa');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('ssa');
                    sub = contentsService.convert(sub, 'vtt');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('vtt');
                }
            });
        });

        it('Must convert srt subtitle to .smi and then .vtt', async () => {
            let srtFile = 'https://s3.amazonaws.com/flixxo-local-mp/content-subtitle/1549903216741';
            await request.get(srtFile, async (err, res, body) => {
                if (!err && res.statusCode == 200) {
                    let subdata = contentsService.validate(body);
                    expect(subdata.format).toBe('srt');
                    let sub = contentsService.convert(body, 'smi');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('smi');
                    sub = contentsService.convert(sub, 'vtt');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('vtt');
                }
            });
        });

        it('Must convert srt subtitle to .lrc and then .vtt', async () => {
            let srtFile = 'https://s3.amazonaws.com/flixxo-local-mp/content-subtitle/1549903216741';
            await request.get(srtFile, async (err, res, body) => {
                if (!err && res.statusCode == 200) {
                    let subdata = contentsService.validate(body);
                    expect(subdata.format).toBe('srt');
                    let sub = contentsService.convert(body, 'lrc');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('lrc');
                    sub = contentsService.convert(sub, 'vtt');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('vtt');
                }
            });
        });

        it('Must convert srt subtitle to .json and then .vtt', async () => {
            let srtFile = 'https://s3.amazonaws.com/flixxo-local-mp/content-subtitle/1549903216741';
            await request.get(srtFile, async (err, res, body) => {
                if (!err && res.statusCode == 200) {
                    let subdata = contentsService.validate(body);
                    expect(subdata.format).toBe('srt');
                    let sub = contentsService.convert(body, 'json');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('json');
                    sub = contentsService.convert(sub, 'vtt');
                    subdata = contentsService.validate(sub);
                    expect(subdata.format).toBe('vtt');
                }
            });
        });
    });

    describe('#getSeederPriceAt', () => {

        beforeEach(async () => {
            await loadFixture('contentversion-for-get-last-seederfee-at');
        });

        test('Must to respond versions price', async () => {
            const res = await contentsService.getSeederPriceAt(
                '41a0ff76-faf0-2bf2-a810-c0f054033a28',
                Date.now()
            );

            expect(res).toBe('5.000000000000000000');
        });

        test('Must to respond own price', async () => {
            const res = await contentsService.getSeederPriceAt(
                '41a0ff76-faf0-2bf2-a810-c0f054033a28',
                365000000
            );

            expect(res).toBe('0.000000123000012309');
        });

        test('Must to throw NotFound if does not exists', async () => {
            try {
                await contentsService.getSeederPriceAt(
                    '41a0ff76-faf0-2bf2-a810-c0f054033a2a',
                    365000000
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });

    });

});
