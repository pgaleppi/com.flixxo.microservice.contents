
import fs from 'fs';
import models from '../models';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

/**
 * Export contents
 * @param {object} params
 */
export default async function({path, ids, authors}) {
    debug(`Loading data from ${path || 'stdin'}`);

    ids = ids ? ids.split(',').map((x) => x.trim()) : null;
    authors = authors ?
        authors.split(',').map((a) => parseInt(a.trim(), 10)) : null;

    const file = path ? fs.createWriteStream(path) : process.stdout;

    const contents = await models.contents.scope({method: ['toExport', null]})
        .findAll({
            ...((ids || authors) ? {
                where: {
                    ...(ids ? {uuid: ids} : {}),
                    ...(authors ? {authorId: authors} : {}),
                },
            } : {}),
        });

    const series = contents
        .filter(({serie}) => serie)
        .reduce((srs, {serie}) => {
            if (!srs.find((x) => x.uuid === serie.uuid)) {
                srs.push(serie);
            }

            return srs;
        }, [])
        .map((serie) => {
            const ex = [{
                model: 'serie',
                keys: ['uuid'],
                data: {
                    ...serie.get({plain: true}),
                    author: undefined,
                    moderator: undefined,
                    season: undefined,
                    media: undefined,
                },
            }];

            serie.media.forEach((media) => {
                ex.push({
                    model: 'media',
                    keys: ['type', 'uuid', 'isDraft', 'url'],
                    data: {
                        ...media.get({plain: true}),
                    },
                });
            });

            serie.season.forEach((season) => {
                ex.push({
                    model: 'season',
                    keys: ['uuid'],
                    data: {
                        ...season.get({plain: true}),
                    },
                });
            });

            return ex;
        })
    ;

    const getUsers = (arr, field) => arr
        .map((c) => c[field])
        .filter((x) => x)
        .reduce((usrs, user) => {
            if (!usrs.find((u) => u.uuid === user.uuid)) {
                usrs.push(user);
            }

            return usrs;
        }, [])
        .map((user) => [
            {
                model: 'users',
                keys: ['email', 'uuid'],
                data: {
                    ...user.get({plain: true}),
                    Profile: undefined,
                    followedCategories: user.followedCategories
                        .map((x) => x.id),
                },
            },
            user.Profile ? {
                model: 'profile',
                keys: ['userId'],
                data: {
                    ...user.Profile.get({plain: true}),
                },
            } : undefined,
        ]);

    const users = [
        ...getUsers([...contents, ...series], 'author'),
        ...getUsers([...contents, ...series], 'moderator'),
    ]
        .reduce((unq, users) => {
            const uuid = users
                .find((x) => x.model === 'users')
                .data.uuid;

            const exits = unq
                .find((us) => us
                    .find((u) => u.model === 'users' && u.data.uuid === uuid));

            if (!exits) {
                unq.push(users);
            }

            return unq;
        }, []);

    const contentsList = contents
        .map((content) => {
            const ex = [
                {
                    model: 'contents',
                    keys: ['uuid'],
                    data: {
                        ...content.get({plain: true}),
                        metadata: undefined,
                        media: undefined,
                        author: undefined,
                        moderator: undefined,
                        serie: undefined,
                    },
                },
            ];

            content.metadata.forEach((meta) => {
                ex.push({
                    model: 'metadata',
                    keys: ['entityType', 'entityId', 'lang'],
                    data: {
                        ...meta.get({plain: true}),
                    },
                });
            });

            content.media.forEach((media) => {
                ex.push({
                    model: 'media',
                    keys: ['type', 'uuid', 'isDraft', 'url'],
                    data: {
                        ...media.get({plain: true}),
                    },
                });
            });

            return ex;
        })
    ;

    const result = [...users, ...series, ...contentsList]
        .reduce((r, x) => [...r, ...x], [])
        .filter((x) => x)
    ;

    file.on('finish', () => process.exit(0)); // @TODO hardcore exit
    file.write(JSON.stringify(result, null, 2), 'utf8');
    file.end();

    return;

}
