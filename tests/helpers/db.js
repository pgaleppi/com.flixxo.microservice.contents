jest.mock('../../src/config/env', () => {
    return {
        storage: {
            dialect: 'sqlite',
            host: ':memory:',
        },
        aws: {
            s3: {
                host: 'http://image-bucket.com',
                bucket: '',
            },
        },
    };
});

import path from 'path';
import sequelizeFixtures from 'sequelize-fixtures';
import {getConnection} from '../../src/storage';
import models from '../../src/models';
import {createDebug} from '../../src/logger';

const debug = createDebug(__filename + '/test');

export async function syncDb(force = true) {
    let conn = getConnection();
    await conn.sync({force});

    return conn;
}

export async function loadFixture(name, uopts = {}) {
    debug(`Load fixtures of ${name}`);
    let fpath = path.resolve(__dirname, '../fixtures', `${name}.json`);

    const opts = {
        log: debug,
        ...uopts || {},
    };

    try {
        await sequelizeFixtures.loadFile(
            fpath,
            models,
            opts
        );
    } catch (e) {
        debug(`Error loading ${name} fixtures: `, e);
        throw e;
    }

    debug(`Ready ${name}`);

    return;
}

export async function loadData(name, uopts = {}) {
    debug(`Load data of ${name}`);
    let fpath = path.resolve(__dirname, '../../data', `${name}.json`);

    const opts = {
        log: debug,
        ...uopts || {},
    };

    try {
        await sequelizeFixtures.loadFile(
            fpath,
            models,
            opts
        );
    } catch (e) {
        debug(`Error loading ${name} data: `, e);
        throw e;
    }

    debug(`Ready data of ${name}`);

    return;
}
