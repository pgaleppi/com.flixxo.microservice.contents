
import Promise from 'bluebird';
import {Op} from 'sequelize';
import models from '../models';
import {createDebug} from '../logger';
import config from '../config';
import {getServiceConnection} from '../sqsrpc';

const debug = createDebug(__filename);

const BUCKET_NAME = config.get('aws:s3:bucket');
const BUCKET_HOST = config.get('aws:s3:host');

/**
 * Batch Image Resize
 * @param {array} mediaIds
 */
export default async (mediaIds) => {
    try {
        const contentQueue = getServiceConnection('contents');

        debug(`Getting media list ...`);
        const contentMedia = await models.media.findAll({
            where: {
                id: {
                    [Op.in]: mediaIds,
                },
            },
            raw: true,
        });

        if (contentMedia.length) {
            await Promise.map(contentMedia, (media) => {
                const s3path = media.url
                    .replace(BUCKET_HOST + BUCKET_NAME + '/', '');
                return contentQueue.exec('images:resize', s3path);
            });
        } else {
            debug('There is no media to process.');
        }
    } catch (e) {
        debug(`Error processing content media files ${e}`);
        process.stderr.write(`${e.message}\n`);
    }
};

