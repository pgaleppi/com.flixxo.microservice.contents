jest.mock('axios');
jest.mock('../../src/sqsrpc');

import axios from 'axios';
import {EmptyResultError} from 'sequelize';
import {syncDb, loadFixture, loadData} from '../helpers/db';
import models from '../../src/models';

const lang = 'en';

describe('Check default values', () => {

    beforeEach(async () => {
        await syncDb();
        await loadFixture('users');
        await loadData('categories');
    });

    it('Must return def value', async () => {
        let serie = await models.serie.create({
            title: 'Dummy title',
            body: 'Dummy body',
            categoryId: 1,
            authorId: 100,
        });

        expect(serie.bodyFormat).toBe(models.serie.FORMAT_MARKDOWN);
        expect(serie.audioLang).toBe('en');
        expect(serie.status).toBe(models.serie.STATUS_NEW);
        expect(serie.moderationStatus).toBe(models.serie.MODSTATUS_NEW);
    });

});

describe.skip('Check virtuals', () => {
    beforeEach(async (done) => {
        await syncDb();
        await loadFixture('users');
        await loadData('categories');
        done();
    }, 0);

    describe('isIndexable', () => {
        test('Must to change isIndexable when change status', async () => {
            let serie = models.serie.build({
                title: 'Dummy title',
                body: 'Dummy body',
                categoryId: 1,
                authorId: 100,
                status: models.serie.STATUS_ENABLED,
                moderationStatus: models.serie.MODSTATUS_ENABLED,
            });

            expect(serie.isIndexable).toBeTruthy();

            serie.status = models.serie.STATUS_PAUSED;

            expect(serie.isIndexable).toBeFalsy();
            expect(serie.changed('isIndexable')).toBeTruthy();
            expect(serie.previous('isIndexable')).toBeTruthy();
        });
    });

});

describe('Tags', () => {

    beforeEach(async (done) => {
        await syncDb();
        await loadFixture('users');
        await loadData('categories');
        setTimeout(done, 1000); // for filters
    }, 0);

    it('Must be create a new serie with tags', async () => {
        let m = await models.serie.create({
            title: 'Dummy title',
            body: 'Dummy body',
            categoryId: 1,
            authorId: 100,
            Tags: [
                {tag: 'tag1'},
                {tag: 'tag2'},
                {tag: 'tag3'},
            ],
        }, {
            include: models.tags,
        });

        let tags = await m.getTags({raw: true});

        expect(tags.length).toBe(3);
    });

    it('Must to not repeat tags', async () => {
        await loadFixture('tags');
        await loadFixture('serie-for-tags-check');

        let serie = await models.serie.findAll();

        await serie[0].findAndSetTags([
            'tag10',
            'tag11',
        ]);

        await serie[1].findAndSetTags([
            'tag11',
            'tag12',
        ]);

        let tagsa = await serie[0].getTags();

        let tagsb = await serie[1].getTags();

        expect(tagsa.map((x) => x.tag)).toEqual(['tag10', 'tag11']);
        expect(tagsb.map((x) => x.tag)).toEqual(['tag11', 'tag12']);
    });

    it('Must to create new tags', async () => {
        await loadFixture('tags');
        await loadFixture('serie-for-tags-check');

        let serie = await models.serie.findAll();

        await serie[0].findAndSetTags([
            'tag10',
            'tag11',
            'newtag1',
        ]);

        await serie[1].findAndSetTags([
            'tag11',
            'tag12',
            'newtag1',
            'newtag2',
        ]);

        let tagsa = await serie[0].getTags();

        let tagsb = await serie[1].getTags();

        expect(tagsa.map((x) => x.tag)).toEqual(['tag10', 'tag11', 'newtag1']);
        expect(tagsb.map((x) => x.tag)).toEqual(
            ['tag11', 'tag12', 'newtag1', 'newtag2']);
    });

    it('Must to create all tags', async () => {
        await loadFixture('serie-for-tags-check');

        let serie = await models.serie.findOne();

        await serie.findAndSetTags([
            'newtag1',
            'newtag2',
            'newtag3',
            'newtag4',
        ]);

        let tags = await serie.getTags();
        expect(tags.map((x) => x.tag)).toEqual(
            ['newtag1', 'newtag2', 'newtag3', 'newtag4']
        );
    });

});

describe('#setStatus', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('serie-for-set-status');
    });

    test(`Must to auto set cancelDate on pause`, async () => {
        let serie = await models.serie.findByPk(
            '41a0ff76-a4df-2bf2-a810-c0f054033a28');

        expect(serie.status).toBe(models.serie.STATUS_ENABLED);
        await serie.setStatus(models.serie.STATUS_PAUSED, 'abcdef');
        expect(serie.status).toBe(models.serie.STATUS_PAUSED);
        expect(serie.cancelDate).not.toBeNull();
    });

    test(`Must to auto set cancelDate on cancel`, async () => {
        let serie = await models.serie.findByPk(
            '41a0ff76-a4df-2bf2-a810-c0f054033a28');

        expect(serie.status).toBe(models.serie.STATUS_ENABLED);
        await serie.setStatus(models.serie.STATUS_PAUSED, 'abcde');
        expect(serie.status).toBe(models.serie.STATUS_PAUSED);
        expect(serie.cancelDate).not.toBeNull();
    });

    test(`Must to keep cancelDate on paused -> cancel`, async () => {
        let lastCancelDate = null;
        let serie = await models.serie.findByPk(
            '41a0ff76-c4df-4bf2-a810-c0f054033a29');

        expect(serie.status).toBe(models.serie.STATUS_PAUSED);
        expect(serie.cancelDate).not.toBeNull();
        lastCancelDate = serie.cancelDate;
        await serie.setStatus(models.serie.STATUS_CANCEL, 'I die');
        expect(serie.status).toBe(models.serie.STATUS_CANCEL);
        expect(serie.cancelDate).toBe(lastCancelDate);
    });

    test(`Must to clear cancelDate on re-enable`, async () => {
        let serie = await models.serie.findByPk(
            '41a0ff76-c4df-4bf2-a810-c0f054033a29');

        expect(serie.status).toBe(models.serie.STATUS_PAUSED);
        expect(serie.cancelDate.getTime()).toBe(1534478719143);
        await serie.setStatus(models.serie.STATUS_ENABLED);
        expect(serie.status).toBe(models.serie.STATUS_ENABLED);
        expect(serie.cancelDate).toBeNull();
    });

    test(`Must to throw error if try change status on cenceled`, async () => {
        let serie = await models.serie.findByPk(
            '41a0ff76-c4df-4bf2-a810-c0f054033a30');

        expect(serie.status).toBe(models.serie.STATUS_CANCEL);
        try {
            await serie.setStatus(models.serie.STATUS_ENABLED);
        } catch (err) {
            expect(err.message).toBe(
                'Validation error: A canceled serie can\'t change status');
            return;
        }

        throw new Error('Pass?');
    });

    test(`Must to throw error if comment is not set on pause`, async () => {
        let serie = await models.serie.findByPk(
            '41a0ff76-a4df-2bf2-a810-c0f054033a28');

        try {
            await serie.setStatus(models.serie.STATUS_PAUSED);
        } catch (err) {
            expect(err.message).toBe(
                'Validation error: You must to write a reason');
            return;
        }

        throw new Error('Pass?');
    });

    test(`Must to throw error if comment is not set on cancel`, async () => {
        let serie = await models.serie.findByPk(
            '41a0ff76-a4df-2bf2-a810-c0f054033a28');

        try {
            await serie.setStatus(models.serie.STATUS_CANCEL);
        } catch (err) {
            expect(err.message).toBe(
                'Validation error: You must to write a reason');
            return;
        }

        throw new Error('Pass?');
    });
});

describe('scope:withGeoAvailableSeasons', () => {

    beforeEach(async (done) => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadData('00_countries');
        await loadFixture('series-for-geo-available-seasons');
        done();
    }, 10000);

    test(`Blocked seasons must to not be listed`, async () => {
        const scoped = models.serie.scope(
            {method: ['withGeoAvailableSeasons', 'AR', false, lang]},
        );

        const list = await scoped.findAll({
            where: {uuid: '0000000-fafa-0000-0000-000000000001'},
        });

        expect(list).toHaveLength(1);

        const tree = list.map((se) => ({
            uuid: se.uuid,
            title: se.title,
            seasons: se.season.map((s) => ({
                uuid: s.uuid,
                title: s.title,
                order: s.order,
                contents: s.content.map((c) => ({
                    uuid: c.uuid,
                    title: c.title,

                })),
            })),
        }))[0];

        // Season 1 is not showed because is bloqued to AR (10)
        // Season 2 has not blocking
        //  Content S02E1 is not bloqued
        //  Content S02E2 is bloqued for AR
        //  Content S02E3 is only for AR
        //  Content S02E4 has not downloadStatus
        //  Content S02E5 is status paused
        //  Content S02E6 is not aprobed by moderator
        //  Content S02E7 is not release yet (almost it's over year 2999)
        //  Content S02E8 is finished
        // Season 3 has not contents
        // Season 4 Is only for CN (41)
        expect(tree).toEqual({
            uuid: '0000000-fafa-0000-0000-000000000001',
            title: 'Dummy title 1',
            seasons: [{
                uuid: '1000000-fafa-0000-0000-000000000002',
                title: 'Season 2',
                order: undefined,
                contents: [{
                    title: 'S02E01',
                    uuid: '3000000-fafa-0000-0000-000000000002',
                }, {
                    title: 'S02E03',
                    uuid: '3000000-fafa-0000-0000-000000000004',
                }],
            }],
        });

    });

    test(`Whitelisted seasons must only listed`, async () => {
        const scoped = models.serie.scope({
            method: ['withGeoAvailableSeasons', 'CN', false, lang],
        });

        const list = await scoped.findAll({
            where: {uuid: '0000000-fafa-0000-0000-000000000001'},
        });

        expect(list).toHaveLength(1);

        const tree = list.map((se) => ({
            uuid: se.uuid,
            title: se.title,
            seasons: se.season.map((s) => ({
                uuid: s.uuid,
                title: s.title,
                order: s.order,
                contents: s.content.map((c) => ({
                    uuid: c.uuid,
                    title: c.title,

                })),
            })),
        }))[0];

        // Season 1 is showed because is bloqued to AR (10)
        // Season 2 has not blocking
        //  Content S02E1 is not bloqued
        //  Content S02E2 is bloqued for AR
        //  Content S02E3 is only for AR
        //  Content S02E4 has not downloadStatus
        //  Content S02E5 is status paused
        //  Content S02E6 is not aprobed by moderator
        //  Content S02E7 is not release yet (almost it's over year 2999)
        //  Content S02E8 is finished
        // Season 3 has not contents
        // Season 4 Is only for CN (41)
        expect(tree).toEqual({
            uuid: '0000000-fafa-0000-0000-000000000001',
            title: 'Dummy title 1',
            seasons: [{
                uuid: '1000000-fafa-0000-0000-000000000001',
                title: 'Season 1',
                order: undefined,
                contents: [{
                    title: 'S01E01',
                    uuid: '3000000-fafa-0000-0000-000000000001',
                }],
            }, {
                uuid: '1000000-fafa-0000-0000-000000000002',
                title: 'Season 2',
                order: undefined,
                contents: [{
                    title: 'S02E01',
                    uuid: '3000000-fafa-0000-0000-000000000002',
                }, {
                    title: 'S02E02',
                    uuid: '3000000-fafa-0000-0000-000000000003',
                }],
            }, {
                uuid: '1000000-fafa-0000-0000-000000000004',
                title: 'Season 4',
                order: undefined,
                contents: [{
                    title: 'S04E01',
                    uuid: '3000000-fafa-0000-0000-000000000005',
                }],
            }],
        });

    });
});

describe('#dernormalize', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('series-for-denormalize');
    });

    test('a', async () => {
        const serie = await models.serie.findByPk(
            '0000000-c3d1-0000-0000-000000000001',
            {
                attributes: ['uuid'],
                rejectOnEmpty: true,
            }
        );

        const res = await serie.denormalize();
        expect(res).toMatchSnapshot();
    });

});

describe('#replaceCover', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('series-for-replace-cover');
    });

    test(`Must to replace cover`, async () => {
        const serie = await models.serie.findByPk(
            '0000000-c3d1-0000-0000-000000000001',
            {rejectOnEmpty: true}
        );

        const media = await serie.replaceCover(
            'New Image.png',
            'http://image-bucket.com/serie-media/newimage.png'
        );

        expect(media.get({plain: true})).toMatchObject({
            uuid: '0000000-c3d1-0000-0000-000000000001',
            name: 'New Image.png',
            url: 'http://image-bucket.com/serie-media/newimage.png',
            type: models.media.TYPE_COVER,
            isDraft: false,
        });

        const medias = await serie.getMedia();

        expect(medias).toHaveLength(1);

        return true;
    });

});

describe('#getGeoBlockingCountries', () => {

    beforeAll(async (done) => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadData('00_countries');
        await loadFixture('series-for-get-geoblocking-countries');
        done();
    }, 30000);

    test(`Must to get only the common blocked country`, async () => {
        const serie = await models.serie.findByPk(
            '00000000-c1dd-0000-0000-000000000000'
        );

        expect(serie);
        expect(serie).toBeInstanceOf(models.serie);

        const res = await serie.getGeoBlockingCountries();

        expect(res.geoBlockingType).toBe(models.geoblocking.TYPE_BLACK);
        expect(res.blockedCountries).toEqual(['AR']);
        expect(res.availableCountries).toEqual([]);
    });

    test(`Must to get not blocking is there ara blocks with not interceptions`, async () => {
        const serie = await models.serie.findByPk(
            '00000000-c1dd-0000-0000-000000000001'
        );

        expect(serie);
        expect(serie).toBeInstanceOf(models.serie);

        const res = await serie.getGeoBlockingCountries();

        expect(res.geoBlockingType).toBe(0);
        expect(res.blockedCountries).toEqual([]);
        expect(res.availableCountries).toEqual([]);
    });

    test(`Must to get not blocking is there ara blocks with not blocking`, async () => {
        const serie = await models.serie.findByPk(
            '00000000-c1dd-0000-0000-000000000002'
        );

        expect(serie);
        expect(serie).toBeInstanceOf(models.serie);

        const res = await serie.getGeoBlockingCountries();

        expect(res.geoBlockingType).toBe(0);
        expect(res.blockedCountries).toEqual([]);
        expect(res.availableCountries).toEqual([]);
    });

    test(`Must to unblok countrie if there is whitelisted by season`, async () => {
        const serie = await models.serie.findByPk(
            '00000000-c1dd-0000-0000-000000000003'
        );

        expect(serie);
        expect(serie).toBeInstanceOf(models.serie);

        const res = await serie.getGeoBlockingCountries();

        expect(res.geoBlockingType).toBe(models.geoblocking.TYPE_BLACK);
        expect(res.blockedCountries).toEqual(['AR']);
        expect(res.availableCountries).toEqual([]);
    });

    test(`Must to get a whitelistened serie`, async () => {
        const serie = await models.serie.findByPk(
            '00000000-c1dd-0000-0000-000000000004'
        );

        expect(serie);
        expect(serie).toBeInstanceOf(models.serie);

        const res = await serie.getGeoBlockingCountries();

        expect(res.geoBlockingType).toBe(models.geoblocking.TYPE_WHITE);
        expect(res.blockedCountries).toEqual([]);
        expect(res.availableCountries).toEqual(['AR', 'CC']);
    });

});
