
import {AntiFloodOperationLimit} from '../../errors';

let blocks = [];

/**
 * @override
 */
export async function check(
    type, val, msg = 'Flood prevention error', methods = ['captcha']
) {

    const block = blocks
        .find((b) => b.type === type && val.startsWith(b.prefix))
    ;

    if (block) {
        throw new AntiFloodOperationLimit(
            msg,
            block.strict ? null : methods,
        );
    } else {
        return {
            blocled: false,
            strict: false,
        };
    }
}

export const mock = {
    clear() {
        blocks = [];
    },

    add(type, prefix, strict = false) {
        blocks.push({
            type,
            prefix,
            strict,
        });
    },
};
