import {syncDb, loadFixture, loadData} from '../helpers/db';
import models from '../../src/models';

const TORRENT_FILE = `ZDg6YW5ub3VuY2UzOTpodHRwczovL3RyYWNrZXIucHJvLmZsaXh4by` +
                     `5jb20vYW5ub3VuY2UxMzphbm5vdW5jZS1saXN0bGwzOTpodHRwczov` +
                     `L3RyYWNrZXIucHJvLmZsaXh4by5jb20vYW5ub3VuY2VlZTEwOmNyZW` +
                     `F0ZWQgYnkyODo5MzhlNjEwOThmNDU1YjgzYWQyN2M1ZmJkZmE3MTM6` +
                     `Y3JlYXRpb24gZGF0ZWkxNTI5NjA1MzgzZTg6ZW5jb2Rpbmc1OlVURi` +
                     `04NDppbmZvZDY6bGVuZ3RoaTczMjc5MjAxZTQ6bmFtZTM3OkppbWkg` +
                     `SGVuZHJpeCAtIFZhbGxleXMgT2YgTmVwdHVuZS5tcDQxMjpwaWVjZS` +
                     `BsZW5ndGhpMjA5NzE1MmU2OnBpZWNlczcwMDovnjjtWEKUvuI1oL/P` +
                     `dVwNryjk0qd5QpOpkximfjjcTt2pxV3tbC/3JaOU2OV1TDUJFN0Pwz` +
                     `ElWmMHgGPw8Jkvwl1kWKKp8mOXfJbrXn2IJOjKh/Tg2x9kBhN5jjpH` +
                     `xK1y8Mg7kGKxRmIBHz4Ketn9Lwzt7UdPj+w0lQmLRoGtCzyflcKNEA` +
                     `T7j58n7HJoQJ6Q9+xd1Eeooe3DrGPf2FTta+NyddJCLWh9sAb6ymip` +
                     `QdP0ZT812UiSHzVUpt2texxjcDoS0aKWnQdTPfGkV4k7QScGUAipeR` +
                     `XGUbVDI82yD8JW9h5qtM7EHw5HfYvBwDAlxDDoVuKZuvVIi3ByVIez` +
                     `CyYRX9Egf6wkYxfTbXmo815D4AskuORB4y1RtK0IZxbcub5Tb2Q/cP` +
                     `EVHM8PrFSFGhTohKkyNC9nhLIyJElkRj0OMCsQbaVlLlxeOAYmp7nl` +
                     `a7kr99+mLERVrK/oIpxYEA/0BFfZCrisfsmGnI0cqwO9QfJxH16IeF` +
                     `hwpt/o0M4cHoBkAiz2TtF/nZeW3nqMmVLBonGLOMRjZspLlTSFA/jy` +
                     `AOKtA6lMAgp8wZ7PjvzwFn9EomPUm3kNYrYHhaVkicWzOagWjFMk8W` +
                     `1xhMe1D8VHSbnSA+K8SMxUrz39fXh3HPIelm2Fdyfv3LWUW+PL+s+N` +
                     `ATmjes/rI4nuRq5vJTrObRkf9nLedcguv2pOOCNT4+08nKKjkmahRl` +
                     `l/pRASj70eZVHGdgmn+nGvfLzCoZLFpRQtOhEep61DojTqdtffmYDu` +
                     `Z5Vn8bEvp8jfCJO3RtFppBLL/8pJ1fd8C3rV8mcfUBmm4JRujZWF9E` +
                     `YoGYNb21QCHFXCV7qynC0/8cMd5NjaTwvS56bR3k/RRxJ1yWVEhwQz` +
                     `jlyd5z+581lKCWqy3yxbgz/i+JimZ3+JUMBkHZDqzlXcsmrkPZuDNz` +
                     `pwcml2YXRlaTFlZWU=`;

describe('.fromTorrentFile', () => {

    test(`Must to get instance with correct info`, () => {
        let res = models.torrentfiles.fromTorrentFile(
            Buffer.from(TORRENT_FILE, 'base64'));

        expect(res.infoHash).toBe('b2177406fc4d44036822a908494b15c2e3ee9027');
        expect(res.totalLength).toBe(73279201);
        expect(res.pieceLength).toBe(2097152);
        expect(res.lastPieceLength).toBe(1976033);
        expect(res.torrentFile.toString('base64')).toBe(TORRENT_FILE);
    });
});
