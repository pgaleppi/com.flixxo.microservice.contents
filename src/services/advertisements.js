
import {
    fn,
    ForeignKeyConstraintError,
    Transaction,
} from 'sequelize';
import {Decimal} from 'decimal.js';
import models from '../models';
import {ValidationError} from '../errors';
import config from '../config';
import {loadService} from '../remoteservice';
import {getConnection} from '../storage';
import createLogger, {createDebug} from '../logger';

const ADVERTISEMENT_ACCOUNT_ID = config.get('ads:accountId');
const ADVERTISEMENT_INCENTIVE = new Decimal(config.get('ads:incentive'));
const ADVERTISEMENT_CPM = new Decimal(config.get('ads:CPM'));
const ADVERTISEMENT_MAX_FOUND = new Decimal(config.get('ads:maxRegulatedFund'));
const ADVERTISEMENT_ADMON_CHECKVIEW =
    !(config.get('ads:admob:checkViews') === 'NO');
const ADVERTISEMENT_APPLIXIR_CHECKVIEW =
    !(config.get('ads:applixir:checkViews') === 'NO');

const logger = createLogger(__filename);
const debug = createDebug(__filename);

/**
 * Return random Advertisement
 * @param {string} email
 * @return {Promise<models.advertisements>}
 */
export async function getRandomAd(email) {
    debug(`Getting random advertisement ${email}`);

    const advertisement = await models.advertisements.
        scope('detailed').findOne({
            order: [[fn('RANDOM')]],
        });
    debug(`Ad loaded ${advertisement.title}`);

    return {
        ...advertisement.get({plain: true}),
    };
}

/**
 * Get and validate existent user
 * @param {string} email
 * @return {Promise}
 */
export async function getUser(email) {
    const user = await models.users.findByEmail(email, false);

    if (!user) {
        debug(`User ${email} does not exists`);
        throw new ValidationError('Non-registered user', [{
            field: 'email',
            type: 'Getting user failed',
            message: 'Non-registered user',
        }]);
    }

    return user;
}

/**
 * Advertising information
 *
 * @typedef AdvertisingInfo
 * @property {string} userId
 * @property {string} title
 */

/**
 * Watch own adversiment
 * @param {sequelize.Transaction} dbTxn
 * @param {UserModel} user
 * @param {int} advertisementId
 * @return {Promise<AdvertisingInfo>}
 */
async function watchOwnAdvertisement(dbTxn, user, advertisementId) {
    debug(`Watch own advertisement ${advertisementId} by ${user.email}`);

    // Advertisement validation
    const advertisement = await models.advertisements.
        findByPk(advertisementId, {
            rejectOnEmpty: true,
            transaction: dbTxn,
        });

    await models.userad.create({
        'userId': user.id,
        'advertisementId': advertisement.id,
    }, {
        transaction: dbTxn,
    });

    return {
        userId: advertisement.userId,
        title: advertisement.title,
    };
}

/**
 * Watch AdMob adversiment
 * @param {sequelize.Transaction} dbTxn
 * @param {UserModel} user
 * @param {string} advertisementId
 * @return {Promise<AdvertisingInfo>}
 */
async function watchAdMobAdvertisement(dbTxn, user, advertisementId) {
    debug(`Watch AdMob advertisement ${advertisementId} by ${user.email}`);

    const adView = await models.admobviews.scope([
        'availables',
        {method: ['byUser', user.uuid]},
    ])
        .findByPk(advertisementId, {
            transaction: dbTxn,
            rejectOnEmpty: false,
        });

    if (adView) {
        debug(`\tUpdate adview`);
        await adView.update({
            status: models.admobviews.STATUS_PAYED,
        }, {transaction: dbTxn});
    } else if (ADVERTISEMENT_ADMON_CHECKVIEW) {
        logger.error(`Not view for admode watch ${user.uuid}`);
        throw new Error(`Not view for admode watch`);
    } else {
        logger.warn(`Not admobView for ${advertisementId} of ${user.uuid}`);
    }

    return {
        userId: ADVERTISEMENT_ACCOUNT_ID,
        title: adView ? adView.uuid : 'uncheked',
    };
}

/**
 * Watch AppLixir adversiment
 * @param {sequelize.Transaction} dbTxn
 * @param {UserModel} user
 * @param {string} advertisementId
 * @return {Promise<AdvertisingInfo>}
 */
async function watchAppLixirAdvertisement(dbTxn, user, advertisementId) {
    debug(`Watch AppLixir advertisement ${advertisementId} by ${user.email}`);

    const adView = await models.applixirviews.scope([
        'availables',
        {method: ['byUser', user.uuid]},
    ])
        .findByPk(advertisementId, {
            transaction: dbTxn,
            rejectOnEmpty: false,
        });

    if (adView) {
        debug(`\tUpdate adview`);
        await adView.update({
            status: models.applixirviews.STATUS_PAYED,
        }, {transaction: dbTxn});
    } else if (ADVERTISEMENT_APPLIXIR_CHECKVIEW) {
        logger.error(`Not view for applixir watch ${user.uuid}`);
        throw new Error(`Not view for applixir watch`);
    } else {
        logger.warn(`Not AppLixirView for ${advertisementId} of ${user.uuid}`);
    }

    return {
        userId: ADVERTISEMENT_ACCOUNT_ID,
        title: adView ? adView.uuid : 'uncheked',
    };
}

/**
 * Record watched advertisement
 * @param {string} email
 * @param {integer|string} advertisementId
 * @param {string} [type='own']
 * @return {Promise<integer>} updated flixxos
 */
export async function watchedAdvertisement(
    email, advertisementId, type = 'own'
) {
    debug(`Registering watched advertisement ${email} ${advertisementId}`);
    debug(`Creating payments service connection`);
    const paymentsService = await loadService('@payments');

    const sequelize = getConnection();

    const tops = {
        type: Transaction.TYPES.EXCLUSIVE,
    };

    const [
        newBalance,
        adReward,
    ] = await sequelize.transaction(tops, async (dbTxn) => {
        let advertisement;

        // User validation
        const user = await models.users
            .findOne({
                where: {email},
                rejectOnEmpty: true,
                transaction: dbTxn,
                lock: dbTxn.LOCK.UPDATE,
            });

        switch (type) {
            case 'own':
                advertisement = await watchOwnAdvertisement(
                    dbTxn,
                    user,
                    advertisementId
                );
                break;
            case 'admob':
                advertisement = await watchAdMobAdvertisement(
                    dbTxn,
                    user,
                    advertisementId,
                );
                break;
            case 'applixir':
                advertisement = await watchAppLixirAdvertisement(
                    dbTxn,
                    user,
                    advertisementId,
                );
                break;
            default:
                throw new ValidationError(`Invalid type`, [{
                    field: 'type',
                    type: 'Unknow',
                    message: `Unknow type ${type}`,
                }]);
        }

        const adAccount = await models.users
            .findOne({
                where: {
                    'uuid': advertisement.userId,
                },
                rejectOnEmpty: true,
                transaction: dbTxn,
                lock: dbTxn.LOCK.UPDATE,
            });

        const adReward = await getCurrentReward(dbTxn, user, adAccount);

        const userAdMetter = await user.resolveAdMetter(dbTxn);
        const adAccountMetter = await adAccount.resolveProviderAdMetter(dbTxn);

        // Update metters of user
        await userAdMetter.incrementMetter(adReward, dbTxn);

        // Decrement metter of adAccount
        await adAccountMetter.decrementMetter(adReward, dbTxn);

        debug(`Calling payments service`);
        const userBalance = await paymentsService.execute(
            'users:incrementUserBalance',
            user.uuid,
            advertisement.userId,
            adReward.toFixed(18),
            advertisement.title,
        );

        return [userBalance, adReward];
    }).catch((err) => {
        logger.error(`advertising and/or payment ${err.message} (${err.name})`);
        debug(err);
        throw new ValidationError('Non-associated advertising nor payment', [{
            field: 'undefined',
            type: 'Record advertisement watch failed',
            message: 'Problem relating advertising and/or payment',
        }]);
    });

    debug(`Advertising/Payment assigned`);
    logger.info(
        `[ ADV:PAY ] ` +
        `Pay ${adReward.toFixed(18)} to ${email} for ${type}`
    );

    return {
        success: true,
        newBalance,
        rewards: adReward.toFixed(18),
    };
}

/**
 * Returns user/ad rewards
 * @param {Transaction} dbTx
 * @param {UsersModel} user
 * @param {UsersModel} adUser
 * @return {number} Ad rewards
 */
export async function getCurrentReward(dbTx, user, adUser) {
    const adMetters = await adUser.resolveProviderAdMetter(dbTx);
    const userMetters = await user.resolveAdMetter(dbTx);

    // Calculate regulated fund
    const regulatedFound = adMetters.metter.lte(ADVERTISEMENT_MAX_FOUND) ?
        adMetters.metter.dividedBy(ADVERTISEMENT_MAX_FOUND) :
        ADVERTISEMENT_CPM
    ;

    // @TODO Check if total found is enough
    /* if (regulatedFound.gt(totalFound)) {
        logger.warn(
            `Advertising accout has ${totalFound.toFixed(18)}, and ` +
            `${regulatedFound.toFixed(18)} is needed [ ADV:INSUFFICIENT ]`
        );
        regulatedFound = totalFound;
    }*/

    // Calculate actually reward
    const reward = Decimal.max(
        regulatedFound
            .times(2)
            .minus(userMetters.lastCicle.lt(0) ? 0 : userMetters.lastCicle)
            .dividedBy(
                Decimal.pow(2, userMetters.metter.plus(1))
            )
            .times(ADVERTISEMENT_INCENTIVE),
        0
    );

    logger.info(
        `[ ADV:CALCULATE ] ` +
        `${user.email} fund: ${regulatedFound.toFixed(18)}, ` +
        `last cycle: ${userMetters.lastCicle.toFixed(18)}` +
        `metter: ${userMetters.metter.toFixed(18)}`
    );

    return reward;
}

/**
 * Returns user/ad rewards
 * @param {string} email
 * @return {number} Ad rewards
 */
export async function getUserAdReward(email) {
    debug(`Getting ad reward for ${email}`);

    return getConnection().transaction(async (dbTx) => {
        const user = await models.users.scope(
            'withAdMetter'
        )
            .findOne({
                where: {email},
                rejectOnEmpty: true,
                transaction: dbTx,
            });

        const adAccount = await models.users.scope('withAdMetter')
            .findOne({
                where: {
                    'uuid': ADVERTISEMENT_ACCOUNT_ID,
                },
                rejectOnEmpty: true,
                transaction: dbTx,
            });

        return getCurrentReward(
            dbTx,
            user,
            adAccount
        )
            .then((v) => v.toFixed(18));
    })
        .then((reward) => {
            logger.info(
                `[ ADV:SHOWREWARD ] ` +
                `Reward ${reward} calculated for ${email}`
            );

            return reward;
        });
}

/**
 * Create advertisement
 * @param {string} title
 * @param {string} url
 * @param {string} accountId
 * @return {object}
 */
export async function createAdvertisement(title, url, accountId) {
    debug(`Creating ad ${title} - ${accountId}`);
    await models.advertisements.create({
        title,
        url,
        userId: accountId,
    });

    return {
        success: true,
        url,
    };
}

/**
 * Authorize AdMob video ad view
 * @param {object} params
 * @return {object}
 */
export async function authorizeAdMobView(params) {
    const view = models.admobviews.fromRaw(params);
    try {
        await view.save();
    } catch (e) {
        if (e instanceof ForeignKeyConstraintError) {
            throw new ValidationError('Invalid user', [{
                field: 'user_id',
                message: 'User does not exists',
                type: 'Not exists',
            }]);
        }

        throw e;
    }

    return {
        success: true,
    };
}

/**
 * Authorize AppLixir ad view
 * @param {object} params
 * @return {object}
 */
export async function authorizeAppLixirView(params) {
    const view = models.applixirviews.fromRaw(params);
    try {
        await view.save();
    } catch (e) {
        if (e instanceof ForeignKeyConstraintError) {
            throw new ValidationError('Invalid user', [{
                field: 'user_id',
                message: 'User does not exists',
                type: 'Not exists',
            }]);
        }

        throw e;
    }

    return {
        success: true,
    };
}

/**
 * Reset user metter
 * @param {string} uuid
 * @param {string} expend
 */
export async function resetMetter(uuid, expend) {
    debug(`Reset metter of ${uuid}`);
    expend = new Decimal(expend);

    const user = await models.users.scope('withAdMetter').findOne({
        where: {uuid},
        rejectOnEmpty: true,
    });

    return models.sequelize.transaction(async (dbTx) => {
        const metter = await user.resolveAdMetter(dbTx);
        await metter.resetMetter(
            expend,
            ADVERTISEMENT_INCENTIVE,
            dbTx,
        );

        logger.info(
            '[ ADV:RESETUSER ] ' +
            `User ${user.email} reset metters with ${metter.lastCicle}`
        );
    });
}

