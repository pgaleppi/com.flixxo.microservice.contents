import {createDebug} from '../logger';
import Sequelize from 'sequelize';

const debug = createDebug(__filename);

const TYPE_COVER = 0x01;
const TYPE_THUMB = 0x02;

export default (sequelize, DataTypes) => {

    const instanceMethods = {};

    const TempMedia = sequelize.define('TempMedia', {

        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },

        type: {
            type: DataTypes.INTEGER.UNSIGNED,
            defaultValue: TYPE_THUMB,
            allowNull: false,
            validate: {
                isIn: [[
                    TYPE_COVER,
                    TYPE_THUMB,
                ]],
            },
        },

        name: {
            type: DataTypes.STRING(2014),
            allowNull: true,
            validate: {
                is: /^.{3,}$/,
            },
        },

        url: {
            type: DataTypes.STRING(1024),
            allowNull: false,
            validate: {
                isUrl: true,
            },
        },

        hash: {
            type: DataTypes.STRING(40),
            allowNull: true,
            validate: {
                is: {
                    args: /^[A-F0-9]{40}$/i,
                    msg: 'Invalid SHA1 format',
                },
            },
            set(hash) {
                // Save hash in lower case
                if (hash) {
                    this.setDataValue('hash', hash.toLowerCase());
                }
            },
            index: true,
        },

        userId: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,
            index: true,
        },

        order: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true,
        },

        // id used by content versions
        uuid: {
            type: DataTypes.UUID,
            allowNull: true,
        },

    }, {
        timestamps: true,
        paranoid: false,
        instanceMethods,
        validate: {
            mustBeHashOrUUID() {
                if (!this.hash && !this.uuid) {
                    throw new Error(
                        'Temporary media must to has hash or uuid'
                    );
                }
            },
        },
    });
    if (typeof TempMedia.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                TempMedia.prototype[m] = instanceMethods[m];
            }
        }
    }

    /**
     * Media types
     * @type {integer}
     */
    TempMedia.TYPE_COVER = TYPE_COVER;
    TempMedia.TYPE_THUMB = TYPE_THUMB;

    TempMedia.associate = function(models) {

        TempMedia.belongsTo(models.users, {
            foreignKey: 'userId',
            targetKey: 'id',
        });

        // Scopes

        TempMedia.addScope('byUser', (user) => {
            let userId;

            if (typeof user === 'object') {
                userId = user.id;
            } else {
                userId = parseInt(user, 10);
            }

            return {
                where: {userId},
            };
        });

    };

    TempMedia.addScope('expired', (expirationDate) => {
        return {
            where: {
                createdAt: {
                    [Sequelize.Op.lte]: expirationDate,
                },
            },
        };
    });

    TempMedia.addScope('queryList', (tempMediaIds) => {
        return {
            where: {
                id: {
                    [Sequelize.Op.in]: tempMediaIds,
                },
            },
        };
    });

    return TempMedia;

};
