import {createDebug} from '../logger';
import langs from '../langs.json';

const DEFAULT_LANG = langs.def;

const debug = createDebug(__filename);

/**
 * CountriesModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {
    let CountryNamesModel;

    const instanceMethods = {};

    const CountriesModel = sequelize.define('Country', {

        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true,
        },

        iso: {
            type: DataTypes.STRING(2),
            index: true,
            uniq: true,
            validate: {
                is: {
                    args: [/^[A-Z]{2}$/],
                    msg: `Invalid ISO code`,
                },
            },
        },

        isob: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                is: {
                    args: [/^[A-Z]{2,3}$/],
                    msg: `Invalid ISO code`,
                },
            },
        },

        phonePrefix: {
            type: DataTypes.STRING,
            allowNull: true,
        },

    }, {
        name: {
            singular: 'Country',
            plural: 'Countries',
        },
        instanceMethods: instanceMethods,
    });

    if (typeof CountriesModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                CountriesModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    CountriesModel.associate = function(models) {
        CountryNamesModel = models.countriesnames;

        CountriesModel.hasMany(CountryNamesModel, {
            as: 'name',
            foreignKey: 'countryId',
            targetKey: 'id',
            constraints: true,
        });

        CountriesModel.addScope('simple', {
            attributes: [
                'id',
                'iso',
            ],
        });

        CountriesModel.addScope('byLang', (lang = DEFAULT_LANG) => {
            lang = lang.split('_')[0].toLowerCase();

            if (langs.enabled.indexOf(lang) === -1) {
                lang = DEFAULT_LANG;
            }

            return {
                include: [
                    {
                        model: CountryNamesModel,
                        as: 'name',
                        attributes: ['name'],
                        where: {lang},
                    },
                ],
            };
        });
    };

    return CountriesModel;
};
