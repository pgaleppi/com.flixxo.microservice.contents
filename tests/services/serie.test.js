import {syncDb, loadFixture, loadData} from '../helpers/db';
jest.mock('../../src/sqsrpc');
jest.mock('axios');

import * as serieService from '../../src/services/series';
import * as contentsService from '../../src/services/contents';
import {matchers} from 'jest-json-schema';
import {ValidationError} from '../../src/errors';
import {
    EmptyResultError,
    ValidationError as SequelizeValidationError,
    SequelizeEmptyResultError,
} from 'sequelize';
import models from '../../src/models';
import {fakeResponse as sqsFake, clear as sqsClear} from '../../src/sqsrpc';

expect.extend(matchers);

const iso = {country: 'AR'};
const lang = 'en';

const listItemSchema = {
    type: 'object',
    properties: {
        price: {type: 'number'},
        uuid: {type: 'string'},
        contentType: {type: 'integer'},
        title: {type: 'string'},
        audioLang: {type: 'string'},
        categoryId: {type: 'integer'},
        releaseDate: {type: 'object'},
        updatedAt: {type: 'object'},
        createdAt: {type: 'object'},
        duration: {type: ['integer', 'null']},
        downloadStatus: {type: 'integer'},
        author: {
            type: 'object',
            properties: {
                id: {type: 'integer'},
                Profile: {
                    realName: {type: 'string'},
                },
            },
        },
        media: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    type: {type: 'integer'},
                    order: {type: 'integer'},
                    url: {type: 'string'},
                    name: {type: 'string'},
                    sizes: {
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: {
                                hero: {type: 'string'},
                                mainscreen: {type: 'string'},
                                cover: {type: 'string'},
                                xcover: {type: 'string'},
                            },
                        },
                    },
                },
            },
        },
        season: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    number: {type: 'integer'},
                    uuid: {type: 'string'},
                    title: {type: 'string'},
                },
            },
        },
        contentmedia: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    type: {type: 'integer'},
                    order: {type: 'integer'},
                    url: {type: 'string'},
                    name: {type: 'string'},
                },
            },
        },
    },
};

describe('Testing series service', () => {

    beforeEach(async (done) => {
        await syncDb(true);
        await loadData('categories');
        await loadData('00_countries');
        await loadFixture('users');
        await loadFixture('profiles');
        done();
    }, 10000);


    describe('#createSerie', () => {
        beforeEach(async (done) => {
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must create a new serie and season`, async () => {
            const newSerie = {
                'title': 'Fullmetal Alchemist (2017)',
                'body': `The plot takes place at the beginning of the 
                    20th century, in a reality where alchemy is real,
                    extremely developed and respected. The plot features
                    brothers Edward and Alphonse Elric who, after
                    attempting the forbidden technique of human
                    transmutation, suffer the consequences. Alphonse
                    loses his entire body while Edward loses his left
                    leg. Edward then sacrifices his right arm to save
                    his brother's soul by attaching it to a metal suit
                    of armor. Edward is then given mechanical prosthetics
                    known as "automail" in place of his missing arm and
                    leg. Upon acceptance into the State Alchemists, he is
                    given the nickname "Fullmetal Alchemist", all while
                    searching with Alphonse for the legendary
                    philosopher's stone, that will repair their bodies.`,
                'categoryId': 2,
                'seasonNumber': 1,
            };
            const result = await serieService.createSerie(
                    'user1@fakemail.com', 'user2@fakemail.com', newSerie);

            expect(result.success).toBeTruthy();
            expect(result.uuid).toBeDefined();
            expect(result.seasonUUID).toBeDefined();
        });

    });

    describe('#createSeason', () => {
        beforeEach(async (done) => {
            await loadFixture('season-for-creation');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must create a new season`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000001';
            const newSeason = {
                'title': 'Season 2',
                'number': 2,
                'geoBlocking': {
                    'type': 16,
                    'countries': [10, 11, 12],
                },
            };
            const result = await serieService.createSeason(
                    'user1@fakemail.com', serieUUID, newSeason);

            expect(result.success).toBeTruthy();
            expect(result.uuid).toBeDefined();
            expect(result.created).toBeTruthy();

            const geoBlock = await models.geoblocking.findOne({
                where: {targetUUID: result.uuid},
            });
            expect(geoBlock).toBeTruthy();

            const countries = await geoBlock.getCountries();
            expect(geoBlock.type).toBe(16);
            expect(countries.length).toBe(3);
            expect(countries.map((country) => country.id))
                .toEqual([10, 11, 12]);
        });

        it(`Must return existent season`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000001';
            const newSeason = {
                'title': 'Season X',
                'number': 1,
            };
            const result = await serieService.createSeason(
                    'user1@fakemail.com', serieUUID, newSeason);

            expect(result.success).toBeTruthy();
            expect(result.uuid).toBeDefined();
            expect(result.created).toBeFalsy();
        });

        it(`Must reject for invalid serie uuid`, async () => {
            const serieUUID = '3330000-0000-0000-0000-000000000001';
            const newSeason = {
                'title': 'Season 1',
                'number': 1,
            };

            await expect(serieService.createSeason(
                'user1@fakemail.com', serieUUID, newSeason))
                        .rejects.toThrow(ValidationError);
        });

        it(`Must reject for invalid serie owner`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000001';
            const newSeason = {
                'title': 'Season 2',
                'number': 2,
            };

            await expect(serieService.createSeason(
                'user2@fakemail.com', serieUUID, newSeason))
                        .rejects.toThrow(ValidationError);
        });

        it(`Must reject for invalid season number`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000001';
            const newSeason = {
                'title': 'Season 2',
                'number': 22222,
            };

            await expect(serieService.createSeason(
                'user1@fakemail.com', serieUUID, newSeason))
                        .rejects.toThrow(SequelizeValidationError);
        });
    });

    describe('#getSerieByAuthor', () => {
        beforeEach(async (done) => {
            await loadFixture('medias');
            await loadFixture('series-for-get');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must return serie detail`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000001';
            let serieDB = await serieService.getSerieByAuthor[1](
                lang,
                'user1@fakemail.com',
                serieUUID,
            );
            expect(serieDB.title).toBe('Dummy title 1');
            expect(serieDB.media.length).toBe(1);
        });

        it(`Must fail getting serie detail`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000001';
            await expect(serieService.getSerieByAuthor[1](
                lang,
                'user2@fakemail.com', serieUUID))
                        .rejects.toThrow(SequelizeEmptyResultError);
        });
    });

    describe('#createSerieMedia', () => {
        beforeEach(async (done) => {
            await loadFixture('medias');
            await loadFixture('series-for-get');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must create media`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000001';
            const result = await serieService.createSerieMedia(
                    serieUUID,
                    'https://yts.am/assets/images/movies/fullmetal_alchemist_2017/medium-cover.jpg',
                    'Cover image',
                    1,
                    1,
                    'user1@fakemail.com',
                );
            expect(result.success).toBeTruthy();
            expect(result.url).toBeDefined();
            expect(result.id).toBeDefined();
        });
    });

    describe('#listTopByAuthor', () => {
        beforeEach(async (done) => {
            await loadFixture('series-for-get');
            setTimeout(done, 1000); // For datetime filters
        });

        it(`Must get author series`, async () => {
            let result = await serieService.listTopByAuthor(
                'user1@fakemail.com');

            expect(result.count).toBe(result.series.length);
            expect(result.count).toBe(1);
        });

    });

    describe.skip('#byCategory', () => {

        beforeEach(async (done) => {
            await loadFixture('serie-contents');
            await loadFixture('series-for-get');
            await loadFixture('season-for-get');
            await loadFixture('medias-for-series');
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        it.skip(`Must to get a list of series by category`, async () => {
            let result = await serieService.byCategory[2](5);

            expect(result.count).toBe(result.series.length);
            expect(result.count).toBe(1);
            expect(result.series[0]).toMatchSchema(listItemSchema);
            expect(result.series[0].uuid)
                        .toBe('0000000-0000-0000-0000-000000000003');
            expect(result.series[0].contentType)
                        .toBe(models.contents.TYPE_SERIE);
            expect(result.series[0].contentmedia.length).toBe(2);
            expect(result.series[0].categoryId).toBe(5);
            expect(result.series[0].episodesCount).toBeDefined();
            expect(result.series[0].episodesCount).toBe(3);

        });
    });

    describe('#ElasticSearch', () => {

        beforeEach(async (done) => {
            await loadFixture('serie-contents');
            await loadFixture('series-for-get');
            await loadFixture('season-for-get');
            await loadFixture('medias-for-series');
            setTimeout(done, 1000); // For datetime filters
        }, 10000);

        it(`Must to get a list of series by category`, async () => {
            const SERIE_UUID = '0000000-0000-0000-0000-000000000003';
            await serieService.seriesToElastic(SERIE_UUID, 'add');
            // Wrote for debug purposes
            // TODO: write test
        });
    });

    describe('#createSerieContent', () => {
        let sqsSuggestionHook;

        beforeEach(async (done) => {
            await loadFixture('series-for-create-contents');
            await loadFixture('temp-medias');
            sqsSuggestionHook = jest.fn(() => true);
            sqsFake(
                'suggestions',
                'hooks:changeSerieCaps',
                sqsSuggestionHook
            );
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        afterEach(() => sqsClear());

        it(`Must create a new content`, async () => {
            const hash = 'b2177406fc4d44036822a908494b15c2e3ee9027';
            const newContent = {
                'metadata': [
                  {
                      'lang': 'en',
                      'title': 'Fullmetal Alchemist (2017)',
                      'body': `The plot takes place at the beginning of the 
                          20th century, in a reality where alchemy is real,
                          extremely developed and respected. The plot features
                          brothers Edward and Alphonse Elric who, after
                          attempting the forbidden technique of human
                          transmutation, suffer the consequences. Alphonse
                          loses his entire body while Edward loses his left
                          leg. Edward then sacrifices his right arm to save
                          his brother's soul by attaching it to a metal suit
                          of armor. Edward is then given mechanical prosthetics
                          known as "automail" in place of his missing arm and
                          leg. Upon acceptance into the State Alchemists, he is
                          given the nickname "Fullmetal Alchemist", all while
                          searching with Alphonse for the legendary
                          philosopher's stone, that will repair their bodies.`,

                  },
                ],
                'categoryId': 2,
                'seederFee': 10,
                'authorFee': 60,
                'seasonUUID': '1000000-0000-0000-0000-000000000002',
                hash,
                'mediaIds': [1, 2, 3, 4],
                'torrentFile': `ZDg6YW5ub3VuY2UzOTpodHRwczovL3RyYWNrZXIu
                    cHJvLmZsaXh4by5jb20vYW5ub3VuY2UxMzphbm5vdW5jZS1saXN0
                    bGwzOTpodHRwczovL3RyYWNrZXIucHJvLmZsaXh4by5jb20vYW5u
                    b3VuY2VlZTEwOmNyZWF0ZWQgYnkyODo5MzhlNjEwOThmNDU1Yjgz
                    YWQyN2M1ZmJkZmE3MTM6Y3JlYXRpb24gZGF0ZWkxNTI5NjA1Mzgz
                    ZTg6ZW5jb2Rpbmc1OlVURi04NDppbmZvZDY6bGVuZ3RoaTczMjc5
                    MjAxZTQ6bmFtZTM3OkppbWkgSGVuZHJpeCAtIFZhbGxleXMgT2Yg
                    TmVwdHVuZS5tcDQxMjpwaWVjZSBsZW5ndGhpMjA5NzE1MmU2OnBp
                    ZWNlczcwMDovnjjtWEKUvuI1oL/PdVwNryjk0qd5QpOpkximfjjc
                    Tt2pxV3tbC/3JaOU2OV1TDUJFN0PwzElWmMHgGPw8Jkvwl1kWKKp
                    8mOXfJbrXn2IJOjKh/Tg2x9kBhN5jjpHxK1y8Mg7kGKxRmIBHz4K
                    etn9Lwzt7UdPj+w0lQmLRoGtCzyflcKNEAT7j58n7HJoQJ6Q9+xd
                    1Eeooe3DrGPf2FTta+NyddJCLWh9sAb6ymipQdP0ZT812UiSHzVU
                    pt2texxjcDoS0aKWnQdTPfGkV4k7QScGUAipeRXGUbVDI82yD8JW
                    9h5qtM7EHw5HfYvBwDAlxDDoVuKZuvVIi3ByVIezCyYRX9Egf6wk
                    YxfTbXmo815D4AskuORB4y1RtK0IZxbcub5Tb2Q/cPEVHM8PrFSF
                    GhTohKkyNC9nhLIyJElkRj0OMCsQbaVlLlxeOAYmp7nla7kr99+m
                    LERVrK/oIpxYEA/0BFfZCrisfsmGnI0cqwO9QfJxH16IeFhwpt/o
                    0M4cHoBkAiz2TtF/nZeW3nqMmVLBonGLOMRjZspLlTSFA/jyAOKt
                    A6lMAgp8wZ7PjvzwFn9EomPUm3kNYrYHhaVkicWzOagWjFMk8W1x
                    hMe1D8VHSbnSA+K8SMxUrz39fXh3HPIelm2Fdyfv3LWUW+PL+s+N
                    ATmjes/rI4nuRq5vJTrObRkf9nLedcguv2pOOCNT4+08nKKjkmah
                    Rll/pRASj70eZVHGdgmn+nGvfLzCoZLFpRQtOhEep61DojTqdtff
                    mYDuZ5Vn8bEvp8jfCJO3RtFppBLL/8pJ1fd8C3rV8mcfUBmm4JRu
                    jZWF9EYoGYNb21QCHFXCV7qynC0/8cMd5NjaTwvS56bR3k/RRxJ1
                    yWVEhwQzjlyd5z+581lKCWqy3yxbgz/i+JimZ3+JUMBkHZDqzlXc
                    smrkPZuDNzpwcml2YXRlaTFlZWU=%`,
            };

            const result = await serieService.createSerieContent(
                'user1@fakemail.com', 'user2@fakemail.com',
                '0000000-0000-0000-0000-000000000001', newContent);
            
            expect(result.success).toBeTruthy();
            expect(result.uuid).toBeDefined();
            expect(result.torrentFile).toBeDefined();
            expect(sqsSuggestionHook).toHaveBeenCalledWith('0000000-0000-0000-0000-000000000001');
        });

        it(`Must create a new content and set it as ENABLED 
                if there is already enabled content`, async () => {
            await loadFixture('serie-contents-enabled-for-creation');

            const serieUUID = '0000000-0000-0000-0000-000000000001';
            const hash = 'b2177406fc4d44036822a908494b15c2e3ee9027';
            const newContent = {
                'metadata': [
                    {
                        'lang': 'en',
                        'title': 'Fullmetal Alchemist (2018)',
                        'body': `The plot takes place at the beginning of the 
                            20th century, in a reality where alchemy is real,
                            extremely developed and respected. The plot features
                            brothers Edward and Alphonse Elric who, after
                            attempting the forbidden technique of human
                            transmutation, suffer the consequences. Alphonse
                            loses his entire body while Edward loses his left
                            leg. Edward then sacrifices his right arm to save
                            his brother's soul by attaching it to a metal suit
                            of armor. Edward is then given mechanical prosthetics
                            known as "automail" in place of his missing arm and
                            leg. Upon acceptance into the State Alchemists, he is
                            given the nickname "Fullmetal Alchemist", all while
                            searching with Alphonse for the legendary
                            philosopher's stone, that will repair their bodies.`,
                    },
                ],
                'categoryId': 2,
                'seederFee': 10,
                'authorFee': 60,
                'seasonUUID': '1000000-0000-0000-0000-000000000002',
                hash,
                'mediaIds': [41, 42, 43, 44],
                'torrentFile': `ZDg6YW5ub3VuY2UzOTpodHRwczovL3RyYWNrZXIu
                    cHJvLmZsaXh4by5jb20vYW5ub3VuY2UxMzphbm5vdW5jZS1saXN0
                    bGwzOTpodHRwczovL3RyYWNrZXIucHJvLmZsaXh4by5jb20vYW5u
                    b3VuY2VlZTEwOmNyZWF0ZWQgYnkyODo5MzhlNjEwOThmNDU1Yjgz
                    YWQyN2M1ZmJkZmE3MTM6Y3JlYXRpb24gZGF0ZWkxNTI5NjA1Mzgz
                    ZTg6ZW5jb2Rpbmc1OlVURi04NDppbmZvZDY6bGVuZ3RoaTczMjc5
                    MjAxZTQ6bmFtZTM3OkppbWkgSGVuZHJpeCAtIFZhbGxleXMgT2Yg
                    TmVwdHVuZS5tcDQxMjpwaWVjZSBsZW5ndGhpMjA5NzE1MmU2OnBp
                    ZWNlczcwMDovnjjtWEKUvuI1oL/PdVwNryjk0qd5QpOpkximfjjc
                    Tt2pxV3tbC/3JaOU2OV1TDUJFN0PwzElWmMHgGPw8Jkvwl1kWKKp
                    8mOXfJbrXn2IJOjKh/Tg2x9kBhN5jjpHxK1y8Mg7kGKxRmIBHz4K
                    etn9Lwzt7UdPj+w0lQmLRoGtCzyflcKNEAT7j58n7HJoQJ6Q9+xd
                    1Eeooe3DrGPf2FTta+NyddJCLWh9sAb6ymipQdP0ZT812UiSHzVU
                    pt2texxjcDoS0aKWnQdTPfGkV4k7QScGUAipeRXGUbVDI82yD8JW
                    9h5qtM7EHw5HfYvBwDAlxDDoVuKZuvVIi3ByVIezCyYRX9Egf6wk
                    YxfTbXmo815D4AskuORB4y1RtK0IZxbcub5Tb2Q/cPEVHM8PrFSF
                    GhTohKkyNC9nhLIyJElkRj0OMCsQbaVlLlxeOAYmp7nla7kr99+m
                    LERVrK/oIpxYEA/0BFfZCrisfsmGnI0cqwO9QfJxH16IeFhwpt/o
                    0M4cHoBkAiz2TtF/nZeW3nqMmVLBonGLOMRjZspLlTSFA/jyAOKt
                    A6lMAgp8wZ7PjvzwFn9EomPUm3kNYrYHhaVkicWzOagWjFMk8W1x
                    hMe1D8VHSbnSA+K8SMxUrz39fXh3HPIelm2Fdyfv3LWUW+PL+s+N
                    ATmjes/rI4nuRq5vJTrObRkf9nLedcguv2pOOCNT4+08nKKjkmah
                    Rll/pRASj70eZVHGdgmn+nGvfLzCoZLFpRQtOhEep61DojTqdtff
                    mYDuZ5Vn8bEvp8jfCJO3RtFppBLL/8pJ1fd8C3rV8mcfUBmm4JRu
                    jZWF9EYoGYNb21QCHFXCV7qynC0/8cMd5NjaTwvS56bR3k/RRxJ1
                    yWVEhwQzjlyd5z+581lKCWqy3yxbgz/i+JimZ3+JUMBkHZDqzlXc
                    smrkPZuDNzpwcml2YXRlaTFlZWU=%`,
            };
            const result = await serieService.createSerieContent(
                'user1@fakemail.com', 'user2@fakemail.com',
                serieUUID, newContent);


            let serie = await models.serie.findByPk(serieUUID);
            let serieContents = await serie.getContent({raw: true});
            let dbContent = serieContents
                        .filter((content) => content.title === newContent.title)
                        .pop();

            expect(result.success).toBeTruthy();
            expect(result.uuid).toBeDefined();
            expect(result.torrentFile).toBeDefined();
            expect(dbContent).toBeDefined();
            expect(dbContent.moderationStatus)
                                .toBe(models.contents.MODSTATUS_ENABLED);

            expect(sqsSuggestionHook).toHaveBeenCalledWith('0000000-0000-0000-0000-000000000001');
        });

        it.skip(`Must reject bad torrentfile`, async () => {
            const hash = 'b2177406fc4d44036822a908494b15c2e3ee9027';
            const newContent = {
                'metadata': [
                    {
                        'lang': 'en',
                        'title': 'Fullmetal Alchemist (2017)',
                        'body': `The plot takes place at the beginning of the 
                            20th century, in a reality where alchemy is real,
                            extremely developed and respected. The plot features
                            brothers Edward and Alphonse Elric who, after
                            attempting the forbidden technique of human
                            transmutation, suffer the consequences. Alphonse
                            loses his entire body while Edward loses his left
                            leg. Edward then sacrifices his right arm to save
                            his brother's soul by attaching it to a metal suit
                            of armor. Edward is then given mechanical prosthetics
                            known as "automail" in place of his missing arm and
                            leg. Upon acceptance into the State Alchemists, he is
                            given the nickname "Fullmetal Alchemist", all while
                            searching with Alphonse for the legendary
                            philosopher's stone, that will repair their bodies.`,
                    },
                ],
                'categoryId': 2,
                'seederFee': 10,
                'authorFee': 60,
                'seasonUUID': '1000000-0000-0000-0000-000000000001',
                hash,
                'mediaIds': [1, 2, 3, 4],
                'torrentFile': `ZDg6YW5ub3VuY2UzOTpodHRwczovL3RyYWNrZXIu
                    cHJvLmZsaXh4by5jb20vYW5ub3VuY2UxMzphbm5vdW5jZS1saXN0
                    bGwzOTpodHRwczovL3RyYWNrZXIucHJvLmZsaXh4by5jb20vYW5u
                    b3VuY2VlZTEwOmNyZWF0ZWQgYnkyODo5MzhlNjEwOThmNDU1Yjgz
                    YWQyN2M1ZmJkZmE3MTM6Y3JlYXRpb24gZGF0ZWkxNTI5NjA1Mzgz
                    ZTg6ZW5jb2Rpbmc1OlVURi04NDppbmZvZDY6bGVuZ3RoaTczMjc5
                    MjAxZTQ6bmFtZTM3OkppbWkgSGVuZHJpeCAtIFZhbGxleXMgT2Yg
                    TmVwdHVuZS5tcDQxMjpwaWVjZSBsZW5ndGhpMjA5NzE1MmU2OnBp
                    ZWNlczcwMDovnjjtWEKUvuI1oL/PdVwNryjk0qd5QpOpkximfjjc
                    Tt2pxV3tbC/3JaOU2OV1TDUJFN0PwzElWmMHgGPw8Jkvwl1kWKKp
                    8mOXfJbrXn2IJOjKh/Tg2x9kBhN5jjpHxK1y8Mg7kGKxRmIBHz4K
                    etn9Lwzt7UdPj+w0lQmLRoGtCzyflcKNEAT7j58n7HJoQJ6Q9+xd
                    1Eeooe3DrGPf2FTta+NyddJCLWh9sAb6ymipQdP0ZT812UiSHzVU
                    pt2texxjcDoS0aKWnQdTPfGkV4k7QScGUAipeRXGUbVDI82yD8JW
                    9h5qtM7EHw5HfYvBwDAlxDDoVuKZuvVIi3ByVIezCyYRX9Egf6wk
                    YxfTbXmo815D4AskuORB4y1RtK0IZxbcub5Tb2Q/cPEVHM8PrFSF
                    GhTohKkyNC9nhLIyJElkRj0OMCsQbaVlLlxeOAYmp7nla7kr99+m
                    LERVrK/oIpxYEA/0BFfZCrisfsmGnI0cqwO9QfJxH16IeFhwpt/o
                    0M4cHoBkAiz2TtF/nZeW3nqMmVLBonGLOMRjZspLlTSFA/jyAOKt
                    A6lMAgp8wZ7PjvzwFn9EomPUm3kNYrYHhaVkicWzOagWjFMk8W1x
                    hMe1D8VHSbnSA+K8SMxUrz39fXh3HPIelm2Fdyfv3LWUW+PL+s+N
                    ATmjes/rI4nuRq5vJTrObRkf9nLedcguv2pOOCNT4+08nKKjkmah
                    Rll/pRASj70eZVHGdgmn+nGvfLzCoZLFpRQtOhEep61DojTqdtff
                    mYDuZ5Vn8bEvp8jfCJO3RtFppBLL/8pJ1fd8C3rV8mcfUBmm4JRu
                    jZWF9EYoGYNb21QCHFXCV7qynC0/8cMd5NjaTwvS56bR3k/RRxJ1
                    yWVEhwQzjlyd5z+581lKCWqy3yxbgz/i+JimZ3+JUMBkHZDqzlXc
                    smrkPZuDNzpwcml2YXRlaTFlZWU=%`,
            };
            return expect(serieService.createSerieContent(
                'user1@fakemail.com', 'user2@fakemail.com',
                '0000000-0000-0000-0000-000000000001', newContent))
                .rejects.toThrow(ValidationError);
        });

    });


    describe('#getAvailableSerie', () => {
        beforeEach(async (done) => {
            await loadFixture('serie-contents');
            await loadFixture('series-for-get');
            await loadFixture('season-for-get');
            await loadFixture('medias-for-series');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must return serie detail`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            let serieDB = await serieService.getAvailableSerie[3](
                iso,
                lang,
                serieUUID,
            );
            expect(serieDB.title).toBe('Dummy title 3');
            expect(serieDB.media.length).toBe(2);
        });

        test(`Must to get only geo available seasons`, async () =>{
            await loadFixture('series-for-geo-available-seasons');
            const serieUUID = '0000000-fafa-0000-0000-000000000001';
            let serieDB = await serieService.getAvailableSerie[3](
                iso,
                lang,
                serieUUID,
            );

            const tree = {
                uuid: serieDB.uuid,
                title: serieDB.title,
                seasons: serieDB.season.map((s) => ({
                    uuid: s.uuid,
                    title: s.title,
                    order: s.order,
                    contents: s.content.map((c) => ({
                        uuid: c.uuid,
                        tite: c.title,

                    })),
                })),
            };

            // Season 1 is not showed because is bloqued to AR (10)
            // Season 2 has not blocking
            //  Content S02E1 is not bloqued
            //  Content S02E2 is bloqued for AR
            //  Content S02E3 is only for AR
            //  Content S02E4 has not downloadStatus
            //  Content S02E5 is status paused
            //  Content S02E6 is not aprobed by moderator
            //  Content S02E7 is not release yet (almost it's over year 2999)
            //  Content S02E8 is finished
            // Season 3 has not contents
            // Season 4 Is only for CN (41)
            expect(tree).toEqual({
                uuid: '0000000-fafa-0000-0000-000000000001',
                title: 'Dummy title 1',
                seasons: [{
                    uuid: '1000000-fafa-0000-0000-000000000002',
                    title: 'Season 2',
                    order: undefined,
                    contents: [{
                        tite: 'S02E01',
                        uuid: '3000000-fafa-0000-0000-000000000002',
                    }, {
                        tite: 'S02E03',
                        uuid: '3000000-fafa-0000-0000-000000000004',
                    }],
                }],
            });
        });

    });

    describe('#updateSerie', () => {
        beforeEach(async (done) => {
            await loadFixture('serie-contents');
            await loadFixture('series-for-get');
            await loadFixture('season-for-get');
            await loadFixture('medias-for-series');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must update an existing serie`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const newTitle = 'This is the new title';
            const newBody = 'This is the new title body';
            const updatedSerie = {
                'title': newTitle,
                'body': newBody,
                'tags': ['tg1', 'tg2'],
                'uuid': 'FAKE',
            };
            const result = await serieService.updateSerie(
                    'user2@fakemail.com', serieUUID, updatedSerie);

            const serieDB = await serieService.getSerieByAuthor[1](
                lang,
                'user2@fakemail.com',
                serieUUID,
            );

            expect(result.success).toBeTruthy();
            expect(serieDB.title).toBe(newTitle);
            expect(serieDB.body).toBe(newBody);
            expect(serieDB.uuid).toBe(serieUUID);
            expect(serieDB.Tags[0].tag).toBe('tg1');
            expect(serieDB.Tags[1].tag).toBe('tg2');
        });

        it(`Must reject for invalid serie owner`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const updatedSerie = {
                'title': 'New serie title',
            };

            await expect(serieService.updateSerie(
                'user1@fakemail.com', serieUUID, updatedSerie))
                        .rejects.toThrow(ValidationError);
        });
    });

    describe('#deleteSerie', () => {
        beforeEach(async (done) => {
            await loadFixture('serie-contents');
            await loadFixture('series-for-get');
            await loadFixture('season-for-get');
            await loadFixture('medias-for-series');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it.skip(`Must return delete serie, seasons and videos`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const author = 'user2@fakemail.com';

            let result = await serieService.deleteSerie(
                author,
                serieUUID,
            );

            let canceledSerie = await serieService.getSerieByAuthor[1](
                lang,
                author,
                serieUUID,
            );
            let contents = [];
            canceledSerie.season.map((season) => {
                contents = contents.concat(season.content);
            });

            expect(result.success).toBeTruthy();
            await expect(serieService.getAvailableSerie[3](iso, lang, serieUUID))
                        .rejects.toThrow(EmptyResultError);
            expect(canceledSerie.status).toBe(models.serie.STATUS_CANCEL);
            for (let serieContent of contents) {
                expect(serieContent.status).toBe(models.contents.STATUS_CANCEL);
            }
        });

        it(`Must not delete a serie`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const author = 'user1@fakemail.com';

            await expect(serieService.deleteSerie(
                author,
                serieUUID,
            )).rejects.toThrow(ValidationError);
            const serie = await serieService
                        .getAvailableSerie[3](iso, lang, serieUUID);
            expect(serie).toBeDefined();
        });

    });

    describe('#pauseSeries', () => {
        beforeEach(async (done) => {
            await loadFixture('serie-contents');
            await loadFixture('series-for-get');
            await loadFixture('season-for-get');
            await loadFixture('medias-for-series');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must pause series, seasons and episodes`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const author = 'user2@fakemail.com';

            let result = await serieService.pauseSeries(
                author,
                serieUUID,
                {
                    pause: true,
                    reactivate: false,
                    message: 'testing for pause series',
                },
            );
            expect(result.success).toBeTruthy();

            const pausedSeries = await serieService.getSerieByAuthor[1](
                lang,
                author,
                serieUUID,
            );

            const contents = pausedSeries.season
                .reduce((l, {content}) => [...l, ...content], []);

            await expect(serieService.getAvailableSerie[3](iso, lang, serieUUID))
                        .rejects.toThrow(EmptyResultError);

            expect(pausedSeries.status).toBe(models.serie.STATUS_PAUSED);

            for (const season of pausedSeries.season) {
                expect(season.status).toBe(models.season.STATUS_PAUSED);
            }

            for (const serieContent of contents) {
                expect(serieContent.status).toBe(models.contents.STATUS_PAUSED);
            }
        });

        it(`Must pause seasons and episodes`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const seasonUUID = ['1000000-0000-0000-0000-000000000002',
                    '1000000-0000-0000-0000-000000000005'];
            const author = 'user2@fakemail.com';

            let result = await serieService.pauseSeries(
                author,
                serieUUID,
                {
                    seasons: seasonUUID,
                    pause: true,
                    reactivate: false,
                    message: 'testing for pause series',
                },
            );
            expect(result.success).toBeTruthy();

            let pausedSeries = await serieService.getSerieByAuthor[1](
                lang,
                author,
                serieUUID,
            );

            let contents = [];
            pausedSeries.season.map((season) => {
                if (season.uuid === seasonUUID[0] ||
                        season.uuid === seasonUUID[1]) {
                    contents = contents.concat(season.content);
                }
            });

            expect(pausedSeries.status).toBe(models.serie.STATUS_ENABLED);

            for (let season of pausedSeries.season) {
                if (season === seasonUUID[0] ||
                        season === seasonUUID[1]) {
                    expect(season.status).toBe(models.seasons.STATUS_PAUSED);
                }
            }

            for (let serieContent of contents) {
                expect(serieContent.status).toBe(models.contents.STATUS_PAUSED);
            }
        });

        it(`Must pause episodes`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const seasonUUID = '1000000-0000-0000-0000-000000000002';
            const contentUUID = ['a8daaa66-8ce9-4b05-89d6-cc8cc16c0a59',
                    'a8daaa66-8ce9-4b05-89d6-cc8cc16c0a60'];
            const author = 'user2@fakemail.com';

            let result = await serieService.pauseSeries(
                author,
                serieUUID,
                {
                    seasons: [seasonUUID],
                    contents: contentUUID,
                    pause: true,
                    reactivate: false,
                    message: 'testing for pause series',
                },
            );
            expect(result.success).toBeTruthy();

            let pausedSeries = await serieService.getSerieByAuthor[1](
                lang,
                author,
                serieUUID,
            );

            let contents = [];
            pausedSeries.season.map((season) => {
                if (season.uuid === seasonUUID) {
                    contents = contents.concat(season.content);
                }
            });

            expect(pausedSeries.status).toBe(models.serie.STATUS_ENABLED);

            for (let serieContent of contents) {
                if (serieContent == contentUUID[0] ||
                        serieContent == contentUUID[1]) {
                    expect(serieContent.status)
                        .toBe(models.contents.STATUS_PAUSED);
                }
            }
        });
    });

    describe('#SeasonGeoblocking', () => {
        const T_RESULT = {success: true};
        const GEO_RESULT = {
            blockType: 16,
            countries: [
                {id: 10, iso: 'AR'},
                {id: 11, iso: 'US'},
                {id: 12, iso: 'BR'}],
        };

        beforeEach(async () => {
            await syncDb(true);
            await loadFixture('users');
            await loadData('categories');
            await loadFixture('season-for-geoblocking-check');
            await loadFixture('serie-contents');
            await loadFixture('countries');
        });

        test(`Must to set season geoblocking`, async () => {
            const SEASON_UUID = '10000000-0000-0000-0000-000000000001';
            const AUTHOR_EMAIL = 'user1@fakemail.com';
            const res = await serieService.setSeasonGeoblocking(
                AUTHOR_EMAIL, SEASON_UUID, 16, [10, 11, 12]);
            const geoBlock = await serieService.getSeasonGeoblocking(
                AUTHOR_EMAIL, SEASON_UUID,
            );
            expect(geoBlock).toBeTruthy();
            expect(geoBlock.type).toBe(16);
            expect(geoBlock.countries.length).toBe(3);
            expect(geoBlock.countries.map((country) => country.id))
                .toEqual([10, 11, 12]);
            expect(res).toMatchObject(T_RESULT);
        });

        test.skip(`Must to return geoblocking data
                from getAvailableSerie`, async () => {
            const SEASON_UUID = '10000000-0000-0000-0000-000000000001';
            const AUTHOR_EMAIL = 'user1@fakemail.com';
            await serieService.setSeasonGeoblocking(
                AUTHOR_EMAIL, SEASON_UUID, 16, [10, 11, 12]);

            const SERIE_UUID = '00000000-0000-0000-0000-000000000001';
            let serieDB = await serieService.getAvailableSerie[3](
                iso,
                lang,
                SERIE_UUID,
            );

            expect(serieDB).toBeTruthy();
            expect(serieDB.season.length).toBe(1);
            expect(serieDB.season[0].geoBlocking).toBeTruthy();
            expect(serieDB.season[0].geoBlocking).toMatchObject(GEO_RESULT);
        });

        test(`Must to fail to set season geoblocking`, async () => {
            const SEASON_UUID = '0000000-0000-0000-0000-000000000001';
            const AUTHOR_EMAIL = 'user1@fakemail.com';
            const geoBlock = await models.geoblocking.findOne({
                where: {targetUUID: SEASON_UUID},
            });
            expect(geoBlock).toBeFalsy();
            expect(serieService.setSeasonGeoblocking(
                AUTHOR_EMAIL, SEASON_UUID, 16, [10, 11, 12]))
                .rejects.toThrow(ValidationError);
        });

        test(`Must to replace season geoblocking`, async () => {
            const SEASON_UUID = '10000000-0000-0000-0000-000000000001';
            const AUTHOR_EMAIL = 'user1@fakemail.com';

            // First update
            const res = await serieService.setSeasonGeoblocking(
                AUTHOR_EMAIL, SEASON_UUID, 16, [10]);
            const geoBlock = await models.geoblocking.findOne({
                where: {targetUUID: SEASON_UUID},
            });
            const countries = await geoBlock.getCountries();
            expect(geoBlock).toBeTruthy();
            expect(geoBlock.type).toBe(16);
            expect(countries.length).toBe(1);
            expect(countries.map((country) => country.id))
                .toEqual([10]);
            expect(res).toMatchObject(T_RESULT);

            // Second update
            const res2 = await serieService.setSeasonGeoblocking(
                AUTHOR_EMAIL, SEASON_UUID, 32, [11, 12]);
            const geoBlock2 = await models.geoblocking.findAll({
                where: {targetUUID: SEASON_UUID},
            });
            expect(geoBlock2).toBeTruthy();
            expect(geoBlock2.length).toBe(1);
            expect(geoBlock2[0].type).toBe(32);

            const countries2 = await geoBlock2[0].getCountries();
            expect(countries2.length).toBe(2);
            expect(countries2.map((country) => country.id))
                .toEqual([11, 12]);
            expect(res2).toMatchObject(T_RESULT);

        });

        test(`Must to delete season geoblocking if it exists`, async () => {
            const SEASON_UUID = '10000000-0000-0000-0000-000000000001';
            const AUTHOR_EMAIL = 'user1@fakemail.com';
            await serieService.setSeasonGeoblocking(
                AUTHOR_EMAIL, SEASON_UUID, 16, [10, 11, 12]);
            await serieService.deleteSeasonGeoblocking(
                AUTHOR_EMAIL, SEASON_UUID);

            const geoBlock = await models.geoblocking.findOne({
                where: {targetUUID: SEASON_UUID},
            });

            expect(geoBlock).toBeFalsy();
        });

        test(`Must throw if season belongs to another user`, async () => {
            const SEASON_UUID = '1000000-0000-0000-0000-000000000001';
            const AUTHOR_EMAIL = 'user2@fakemail.com';
            expect(serieService.deleteSeasonGeoblocking(
                AUTHOR_EMAIL, SEASON_UUID)).rejects.toThrow(ValidationError);
        });
    });

    describe('#deleteSerieContent', () => {
        let sqsSuggestionHook;

        beforeEach(async () => {
            await loadFixture('users');
            await loadFixture('serie-contents');
            await loadFixture('series-for-get');
            await loadFixture('season-for-get');
            sqsSuggestionHook = jest.fn(() => true);
            sqsFake(
                'suggestions',
                'hooks:changeSerieCaps',
                sqsSuggestionHook
            );
        });

        afterEach(() => sqsClear());

        it(`Must delete an episode`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const seasonUUID = '1000000-0000-0000-0000-000000000001';
            const AUTHOR_EMAIL = 'user2@fakemail.com';
            const contents = ['ead2ea35-eaad-432d-896b-e17da3f9328f'];
            const body = {contentIds: contents};
            const succed = await serieService
                        .deleteSerieContent(AUTHOR_EMAIL, serieUUID, body);

            expect(succed.success).toBe(true);

            let serieDB = await serieService.getSerieByAuthor[1](
                lang,
                AUTHOR_EMAIL,
                serieUUID,
            );

            let exists = false;
            for (let i = 0; i < serieDB.season.length; i++) {
                if (serieDB.season[i].uuid == seasonUUID) {
                    for (let j = 0; j < serieDB.season[i]
                            .content.length; j++) {
                        if (serieDB.season[i]
                                .content[j].uuid == contents[0]) {
                            exists = true;
                        }
                    }
                }
            }
            expect(exists).toBe(false);

            // Check season was not deleted
            let existsSeason = false;
            for (let i = 0; i < serieDB.season.length; i++) {
                if (serieDB.season[i].uuid == seasonUUID) {
                    existsSeason = true;
                }
            }
            expect(existsSeason).toBe(true);
            expect(sqsSuggestionHook).toHaveBeenCalledWith('0000000-0000-0000-0000-000000000003');
        });

        it(`Must delete an entire season`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const seasonUUID = '1000000-0000-0000-0000-000000000001';
            const AUTHOR_EMAIL = 'user2@fakemail.com';
            const contents = [
                    'ead2ea35-eaad-432d-896b-e17da3f9328f',
                    'd48673e9-94ec-4a07-b9fe-69e5299144b7',
            ];
            const body = {contentIds: contents};
            const succed = await serieService
                        .deleteSerieContent(AUTHOR_EMAIL, serieUUID, body);

            expect(succed.success).toBe(true);

            let serieDB = await serieService.getSerieByAuthor[1](
                lang,
                AUTHOR_EMAIL,
                serieUUID,
            );

            let exists = false;
            for (let i = 0; i < serieDB.season.length; i++) {
                if (serieDB.season[i].uuid == seasonUUID) {
                    exists = true;
                }
            }
            expect(exists).toBe(false);
            expect(sqsSuggestionHook).toHaveBeenCalledWith('0000000-0000-0000-0000-000000000003');
        });

        it(`Must delete entire second season`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const seasonUUID = '1000000-0000-0000-0000-000000000002';
            const AUTHOR_EMAIL = 'user2@fakemail.com';
            const contents = [
                    'a8daaa66-8ce9-4b05-89d6-cc8cc16c0a59',
                    'a8daaa66-8ce9-4b05-89d6-cc8cc16c0a60',
            ];
            const body = {contentIds: contents};
            const succed = await serieService
                        .deleteSerieContent(AUTHOR_EMAIL, serieUUID, body);

            expect(succed.success).toBe(true);

            let serieDB = await serieService.getSerieByAuthor[1](
                lang,
                AUTHOR_EMAIL,
                serieUUID,
            );

            let exists = false;
            for (let i = 0; i < serieDB.season.length; i++) {
                if (serieDB.season[i].uuid == seasonUUID) {
                    exists = true;
                }
            }
            expect(exists).toBe(false);
            expect(sqsSuggestionHook).toHaveBeenCalledWith('0000000-0000-0000-0000-000000000003');
        });

        it.skip(`Must delete an entire serie`, async () => {
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            const AUTHOR_EMAIL = 'user2@fakemail.com';
            const contents = [
                    'ead2ea35-eaad-432d-896b-e17da3f9328f',
                    'd48673e9-94ec-4a07-b9fe-69e5299144b7',
                    'a8daaa66-8ce9-4b05-89d6-cc8cc16c0a59',
                    'a8daaa66-8ce9-4b05-89d6-cc8cc16c0a60',
            ];
            const body = {contentIds: contents};
            const succed = await serieService
                        .deleteSerieContent(AUTHOR_EMAIL, serieUUID, body);

            expect(succed.success).toBe(true);

            // Check serie does not exist
            await expect(serieService.getSerieByAuthor[1](
                lang,
                AUTHOR_EMAIL, serieUUID)).rejects
                        .toThrow(SequelizeEmptyResultError);

            expect(sqsSuggestionHook).toHaveBeenCalledWith('0000000-0000-0000-0000-000000000003');
        });
    });

    describe('#reorderEpisodes', () => {
        let sqsSuggestionHook;

        beforeEach(async () => {
            await loadFixture('users');
            await loadFixture('serie-contents');
            await loadFixture('series-for-get');
            await loadFixture('season-for-get');
            sqsSuggestionHook = jest.fn(() => true);
            sqsFake(
                'suggestions',
                'hooks:changeSerieCaps',
                sqsSuggestionHook
            );
        });

        afterEach(() => sqsClear());

        it(`Must set a new order for chapters`, async () => {
            let newFirstEp = 'ead2ea35-eaad-432d-896b-e17da3f9328f';
            let newSecondEp = 'd48673e9-94ec-4a07-b9fe-69e5299144b7';
            const updateRequest = {reorder: [
                {uuid: newFirstEp, order: 1},
                {uuid: newSecondEp, order: 2},
            ]};
            const authorEmail = 'user2@fakemail.com';
            const result = await serieService
                        .reorderEpisodes(authorEmail, updateRequest);

            expect(result.success).toBe(true);

            // Ask for serie and check episodes order
            const serieUUID = '0000000-0000-0000-0000-000000000003';
            let serieDB = await serieService.getSerieByAuthor[1](
                lang,
                authorEmail,
                serieUUID,
            );

            expect(serieDB.season.length).toBe(3);
            expect(serieDB.season[0].content.length).toBe(2);
            expect(serieDB.season[0].content[0].uuid).toBe(newFirstEp);
            expect(serieDB.season[0].content[1].uuid).toBe(newSecondEp);
            expect(sqsSuggestionHook).toHaveBeenCalledWith('0000000-0000-0000-0000-000000000003');
        });

        it(`Must fail due to content not related to author`, async () => {
            let newFirstEp = 'ead2ea35-eaad-432d-896b-e17da3f9328f';
            let newSecondEp = 'd48673e9-94ec-4a07-b9fe-69e5299144b7';
            const updateRequest = {reorder: [
                {uuid: newFirstEp, order: 1},
                {uuid: newSecondEp, order: 2},
            ]};
            const authorEmail = 'user1@fakemail.com';
            const result = await serieService
                        .reorderEpisodes(authorEmail, updateRequest);

            expect(result.success).toBe(false);
            expect(result.error).toBeTruthy();
        });
    });


    describe('#replaceCover', () => {

        beforeEach(async () => {
            await loadFixture('series-for-replace-cover');
        });

        test('Must to replace cover', async () => {
            await serieService.replaceCover(
                'user1@fakemail.com',
                '0000000-c3d1-0000-0000-000000000001',
                'new image',
                'http://dummy.com/new-image.js'
            );

            const serie = await models.serie.findOne({
                where: {uuid: '0000000-c3d1-0000-0000-000000000001'},
                include: [{
                    model: models.media,
                    as: 'media',
                }],
            });

            expect(serie.media).toHaveLength(1);
            expect(serie.media[0].url).toBe('http://dummy.com/new-image.js');
        });

        test('Must to get NotFound', async () => {
            try {
                await serieService.replaceCover(
                    'user1@fakemail.com',
                    '0000000-c3dd-0000-0000-000000000001',
                    'new image',
                    'http://dummy.com/new-image.js'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });

        test('Must to get NotFound if user is not the owner', async () => {
            try {
                await serieService.replaceCover(
                    'user2@fakemail.com',
                    '0000000-c3d1-0000-0000-000000000001',
                    'new image',
                    'http://dummy.com/new-image.js'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });
    });

    describe('#getCapsInfo', () => {

        beforeEach(async () => {
            await loadFixture('serie-for-caps-info');
        });

        test(`Musto to get caps info`, async () => {
            const res = await serieService.getCapsInfo(
                '0000000-0000-0000-0000-000000000001'
            );

            expect(res).toMatchSnapshot();
        });

        test(`Musto to thorw EmptyResultError with invalid serie`, async () => {
            return expect(serieService.getCapsInfo(
                '0000000-0000-0000-0000-000000000002'
            ))
                .rejects.toBeInstanceOf(EmptyResultError);
        });
    });

});
