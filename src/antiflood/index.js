
import {loadService} from '../remoteservice';
import {AntiFloodOperationLimit} from '../errors';

/**
 * Check resource
 * @param {string} type
 * @param {string} val
 * @param {string} [msg]
 * @param {string[]} methods
 * @return {Promise<number>}
 */
export async function check(
    type, val, msg = 'Flood prevention error', methods = ['captcha']
) {
    const service = await loadService('@antiflood');

    const res = await service.execute(
        'resource:check',
        type,
        `${val}`,
    );

    if (res.blocked) {
        throw new AntiFloodOperationLimit(
            msg,
            res.strict ? null : methods,
        );
    }

    return 0;
}
