import {EventEmitter} from 'events';
import Promise from 'bluebird';
import {wrapError} from '../errors';
import createLogger, {createDebug} from '../logger';
import {nsWrapper} from '../logger/context';
import Meter from './meter';
// import config from '../config';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

/**
 * Remote command server
 */
export default class Server extends EventEmitter {

    /**
     * Create new RPC server
     * @param {amqp.connection} connector
     * @param {string} service Service id, like com.flixxo.microservice.xxx
     */
    constructor(connector, service) {
        super();
        this._connector = connector;
        this._channel = null;
        this._service = service;
        this._commands = [];
        this._meters = {
            received: new Meter(),
            resolved: new Meter(),
            rejected: new Meter(),
        };
        this._middlewares = [];
    }

    /**
     * Get status
     * @return {object}
     */
    getStatus() {
        return {
            received: this._meters.received.getStatus(),
            resolved: this._meters.resolved.getStatus(),
            rejected: this._meters.rejected.getStatus(),
        };
    }

    /**
     * Add middleware
     * @param {string} name Middleware name (identifier for injection)
     * @param {string} context Cam be 'all', 'resolved' or 'reject'
     * @param {function} callback Callback the transform
     * @return {this}
     */
    use(name, context, callback) {
        if (typeof context === 'function') {
            callback = context;
            context = 'all';
        }

        this._middlewares.push({
            name,
            context,
            callback,
        });

        return this;
    }

    /**
     * Resolve response value passing by middlwares
     * @param {string} [context]
     * @param {any} value
     * @return {any} Resolved value
     */
    async resolveResponse(context, value) {
        return Promise.reduce(
            this._middlewares,
            async (resolved, middleware) => {
                const mcontext = middleware.context;
                if (mcontext === 'all' || mcontext === context) {
                    debug(`Tranlating value`);
                    try {
                        resolved = await Promise.resolve(
                            middleware.callback(resolved)
                        );
                    } catch (e) {
                        debug('Error resolving middleware', e);
                        throw e;
                    }
                }

                return resolved;
            },
            value
        );
    }

    /**
     * Create a new channel if does not exists
     * @return {Promise<channel>}
     */
    createChannel() {
        if (!this._channel) {
            debug(`Creating new channel`);
            this._channel = new Promise((resolve, reject) => {
                this._connector.createChannel((err, ch) => {
                    if (err) {
                        debug(`Error creating channel`, err);
                        return reject(err);
                    }

                    debug('channel created');
                    this._channel = ch;
                    return resolve(ch);
                });
            });
        }

        return Promise.resolve(this._channel);
    }

    /**
     * Resolve command parts from a string
     * @param {string} cmdname
     * @return {object} namespace and cmd
     */
    resolveCommandParts(cmdname) {
        let namespace;
        let cmd;

        if (cmdname.indexOf(':') !== -1) {
            const cparts = cmdname.split(':');
            namespace = cparts[0];
            cmd = cparts[1];
        } else {
            namespace = 'default';
            cmd = cmdname;
        }

        return {namespace, cmd};
    }

    /**
     * Listen as service
     */
    async listen() {
        const channel = await this.createChannel();

        debug(`Assert to ${this._service}`);
        await channel.assertQueue(this._service, {durable: false});
        channel.prefetch(1);

        await channel.consume(this._service, (order) => {
            this.handleCommand(order);
        });
    }

    /**
     * Handle command object
     * @param {object} orderPack
     * @return {Promise}
     */
    handleCommand(orderPack) {
        let command;
        const {content, properties} = orderPack;
        debug(`Order ${properties.correlationId} arrives`);

        this._meters.received.push();

        try {
            command = JSON.parse(content.toString());
        } catch (e) {
            debug(`Order ${properties.correlationId} has an invalid format`);
            return this.rejectCommand(orderPack, new Error('Invalid format'));
        }

        return nsWrapper(
            () => this.executeCommand(command, orderPack, properties)
        );
    }

    /**
     * Send command response
     * @param {object} rpack remote package
     * @param {object} data Data
     * @return {object}
     */
    async sendResponse(rpack, data) {
        const channel = await this.createChannel();
        const dpack = JSON.stringify(data);

        debug(`Send response of`, rpack.properties.correlationId);

        channel.sendToQueue(rpack.properties.replyTo, Buffer.from(dpack), {
            correlationId: rpack.properties.correlationId,
        });

        channel.ack(rpack);
        return {rpack, data};
    }

    /**
     * Send resolve to command
     * @param {object} command
     * @param {object} orderPack
     * @param {mixed} response
     * @return {Promise}
     */
    async resolveCommand(command, orderPack, response) {
        const rresponse = await this.resolveResponse('resolved', response);

        const data = {
            uid: orderPack.properties.correlationId,
            succes: true,
            error: null,
            result: rresponse,
        };

        debug(`Resolving command ${command.cmd} (${data.uid})`);
        this._meters.resolved.push();
        return this.sendResponse(orderPack, data);
    }

    /**
     * Get middleware context
     * @param {string} mwName
     * @return {Promise}
     */
    async getMiddlewareContext(mwName) {
        const runMiddleware = this._middlewares.find(
            (middleware) => middleware.name === mwName
        );

        if (!runMiddleware) {
            throw new Error(`Middleware ${mwName} not found.`);
        }

        return runMiddleware.callback();
    }

    /**
     * Send reject to command
     * @param {object} rpack Remote package
     * @param {Error} error Error instance
     * @return {Promise}
     */
    async rejectCommand(rpack, error) {
        const rerror = await this.resolveResponse('rejected', error);
        const errObject = wrapError(rerror);

        const data = {
            uid: rpack.properties.correlationId,
            succes: false,
            error: errObject,
        };

        debug(`Rejecting command ${data.uid}`);

        if (errObject.internal) {
            logger.error(`Unhandled error reject task: ${errObject.message}`);
            debug(error);
        }

        this._meters.rejected.push();
        return this.sendResponse(rpack, data);
    }

    /**
     * Execute local command and send response
     * @param {object} command
     * @param {object} rpack
     * @param {object} properties
     * @return {Promise}
     */
    async executeCommand(command, rpack, properties) {
        const {namespace, cmd} = this.resolveCommandParts(command.cmd);
        const lcommand = this.findCommand(namespace, cmd);
        let params = command.params;

        if (!lcommand) {
            debug(`Called not exisiting command ${namespace}:${cmd}`);
            return this.rejectCommand(
                rpack,
                new Error(`Command ${namespace}:${cmd} does not exists`)
            );
        }

        debug(`Executing command ${command.cmd}`);

        command = await Promise.reduce(
            this._middlewares.filter(({context}) => context === 'pre'),
            (b, middleware) => Promise.resolve(middleware.callback(b)),
            command,
        );

        if (lcommand.contextuable) {
            debug(`Command is contextuable`);

            const contextMwNames = this._middlewares
                .filter((middleware) => middleware.context === 'context')
                .map((middleware) => middleware.name);

            const mwContextNames = lcommand.contextuable
                .filter((value) => contextMwNames.includes(value));

            const apiContextNames = lcommand.contextuable
                .filter((value) => !contextMwNames.includes(value));

            debug(`Calling context middlewares`);

            if (lcommand.contextuable.length === 1 &&
                lcommand.contextuable[0] === null) {
                // Dont touch params
            } else if (lcommand.contextuable.length) {
                const newParams = await Promise.map(lcommand.contextuable,
                    (contextName) => {
                        if (mwContextNames.includes(contextName)) {
                            return this.getMiddlewareContext(contextName);
                        } else if (apiContextNames.includes(contextName)) {
                            return command.context[contextName];
                        }
                    }
                );

                params = [
                    ...newParams,
                    ...params,
                ];
            } else {
                params = [command.context || {}, ...params];
            }
        }

        params = await Promise.reduce(
            this._middlewares.filter(({context}) => context === 'params'),
            (p, middleware) => Promise.resolve(middleware.callback(...p)),
            params,
        );

        try {
            return Promise.resolve(lcommand.func(...params))
                .then((data) => {
                    debug(`${command.cmd} success`);
                    return this.resolveCommand(command, rpack, data);
                })
                .catch((e) => {
                    debug(`${command.cmd} reject`);
                    return this.rejectCommand(rpack, e);
                });
        } catch (e) {
            debug(`${command.cmd} throw an exception`);
            return this.rejectCommand(rpack, e);
        }
    }

    /**
     * Find a command
     * @param {string} namespace
     * @param {string} cmd
     * @return {object|null}
     */
    findCommand(namespace, cmd) {
        return this._commands.find((c) => {
            return c.namespace === namespace && c.cmd === cmd;
        });
    }

    /**
     * Add command to liste
     * @param {string} [namespace]
     * @param {string} cmd
     * @param {function} func
     * @example
     *      // With namespace:
     *      .defineCommand('users', 'list', () => users);
     *
     *      // With namespace short:
     *      .defineCommand('users:list', () => users);
     *
     *      // Without namespace:
     *      .defineCommand('usersList', () => users);
     */
    defineCommand(namespace, cmd, func) {
        let contextuable = false;
        let decorators = [];

        if (typeof cmd === 'function' || Array.isArray(cmd)) {
            func = cmd;
            const parts = this.resolveCommandParts(namespace);
            cmd = parts.cmd;
            namespace = parts.namespace;
        }

        debug(`Defining command ${namespace}:${cmd}`);

        if (Array.isArray(func)) {
            func = [...func];
            contextuable = func;
            func = contextuable.pop();

            while (contextuable.length) {
                const f = contextuable.pop();
                if (typeof f === 'function') {
                    decorators = [
                        f,
                        ...decorators,
                    ];
                } else {
                    contextuable.push(f);
                    break;
                }
            }
        }

        if (decorators.length) {
            // Applu decorators
            func = decorators.reduce((f, d) => {
                let nf;
                debug(`\tApply decorator ${f.name}`);

                try {
                    nf = d(f, namespace, cmd);
                } catch (e) {
                    debug(`Error applying decorator`, e);
                    throw e;
                }

                if (typeof nf !== 'function') {
                    debug(`\tDecorator does not return a new function`);
                    throw new Error(
                        'Result of decorator must to be a funciton');
                }

                return nf;
            }, func);
        }

        if (this.findCommand(namespace, cmd)) {
            debug(`Try to define ${namespace}:${cmd}, but already exists`);
            throw new Error(`${namespace}:${cmd} already defined`);
        }

        this._commands.push({
            namespace,
            cmd,
            func,
            contextuable,
        });
        debug(`Defined command ${namespace}:${cmd}`);
    }

    /**
     * Define a set of commands/namespaces
     * @param {string} [namespace] Namespace for next commands
     * @param {object|function} obj Function or functions object
     * @param {bool} [un] For private use only
     * @return {void}
     * @example
     *      // Simple command define
     *      .define('users::list', () => users);
     *
     *      // Namespaced functions
     *      .define('users', {
     *          list() {
     *              return user;
     *          }
     *      });
     *
     *      // Multi namespaces:
     *      .define({
     *          users: {
     *              list() {
     *                  return users;
     *              }
     *
     *              add(data) {
     *                  users.push(data);
     *              }
     *          },
     *
     *          perms: {
     *              getForUser(uid) {
     *                  return users
     *                      .find(x => x.id === uid)
     *                      .perms
     *                  ;
     *              }
     *          }
     *      });
     */
    define(namespace, obj, un = false) {
        if (typeof namespace !== 'string') {
            un = obj;
            obj = namespace;
            namespace = null;
        }

        if ((typeof obj === 'function' || Array.isArray(obj)) && namespace) {
            return this.defineCommand(namespace, obj);
        } else if (
            !Array.isArray(obj) && typeof obj === 'object' && namespace) {
            for (const fn in obj) {
                if (typeof obj[fn] === 'function' || Array.isArray(obj[fn])) {
                    this.defineCommand(namespace, fn, obj[fn]);
                } else {
                    throw new Error(`${fn} must to be a function`);
                }
            }
            return;
        } else if (typeof obj === 'object' && !namespace && !un) {
            for (const fn in obj) {
                if (typeof obj[fn] === 'function') {
                    this.defineCommand(fn, obj[fn]);
                } else if (typeof obj[fn] === 'object') {
                    this.define(fn, obj[fn], true);
                } else {
                    throw new Error(`${fn} must to be a function`);
                }
            }
            return;
        } else {
            throw new Error('Nothing to add');
        }
    }
}
