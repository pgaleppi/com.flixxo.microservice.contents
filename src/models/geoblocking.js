import {Op} from 'sequelize';
import {createDebug} from '../logger';
import config from '../config';

const DB_DIALECT = config.get('storage:dialect') || null;

const debug = createDebug(__filename);

export const TYPE_BLACK = 0x10;
export const TYPE_WHITE = 0x20;

export const ETYPE_CONTENT = 0x10;
export const ETYPE_SERIE_SEASON = 0x20;

/**
 * Insert where condition in the next and operator
 * @param {object} where
 * @param {object} conds
 * @param {bool} [force=false]
 * @return {object}
 */
export function nextAnd(where, conds, force = false) {
    if (!force && !where[Op.and]) {
        return {
            ...where,
            ...conds,
        };
    }

    where = {...where};

    let a = where;
    while (a[Op.and]) {
        a = a[Op.and];
    }
    a[Op.and] = {
        ...conds,
    };
    return where;
}

export default (sequelize, DataTypes) => {
    let CountryModel;

    const instanceMethods = {};

    const GeoBlockingModel = sequelize.define('GeoBlocking', {

        uuid: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },

        targetUUID: {
            type: DataTypes.UUID,
            allowNull: false,
        },

        entityType: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            validate: {
                isIn: {
                    args: [[
                        ETYPE_CONTENT,
                        ETYPE_SERIE_SEASON,
                    ]],
                    msg: `Unknow entity type`,
                },
            },
        },

        type: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: TYPE_BLACK,
            validate: {
                isIn: {
                    args: [[
                        TYPE_BLACK,
                        TYPE_WHITE,
                    ]],
                    msg: `Unknow type`,
                },
            },
        },

    }, {
        timestamps: true,
        paranoid: false,
        instanceMethods,

        indexes: [
            {
                name: 'geoblocking_entity_unique',
                unique: true,
                fields: ['entityType', 'targetUUID'],
            },
        ],

        scopes: {
            forContents: {
                where: {
                    entityType: ETYPE_CONTENT,
                },
            },
            forSeasons: {
                where: {
                    entityType: ETYPE_SERIE_SEASON,
                },
            },
        },

    });

    if (typeof GeoBlockingModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                GeoBlockingModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    GeoBlockingModel.TYPE_BLACK = TYPE_BLACK;
    GeoBlockingModel.TYPE_WHITE = TYPE_WHITE;

    GeoBlockingModel.ETYPE_CONTENT = ETYPE_CONTENT;
    GeoBlockingModel.ETYPE_SERIE_SEASON = ETYPE_SERIE_SEASON;

    GeoBlockingModel.TYPE_WHITE = TYPE_WHITE;
    GeoBlockingModel.TYPE_BLACK = TYPE_BLACK;

    GeoBlockingModel.associate = function(models) {
        CountryModel = models.countries;

        GeoBlockingModel.belongsToMany(CountryModel, {
            as: 'countries',
            through: {
                model: 'GeoBlockingCountry',
                timestamps: true,
                paranoid: false,
            },
            // foreignKey: ['targetUUID', 'entityType'],
            constraints: true,
        });

        // Scopes

        GeoBlockingModel.addScope('detailled', {
            attributes: [
                'type',
                'createdAt',
                'updatedAt',
            ],
            include: [{
                model: CountryModel.scope('simple'),
                as: 'countries',
                required: false,
                through: {
                    attributes: [],
                },
            }],
        });
    };

    GeoBlockingModel.resolveCountryIso = function(countryIso) {
        if (countryIso === null) {
            debug(`CountryIso is null, taking XX value for unknow country`);
            countryIso = 'XX';
        }

        if (typeof countryIso !== 'string') {
            debug(`Bad type of countryIso:`, typeof countryIso, countryIso);
            throw new TypeError('countryIso must to be a string');
        }

        countryIso = countryIso.toUpperCase();

        if (!countryIso.match(/^[A-Z]{2}$/)) {
            throw new Error('Invalid ISO code');
        }

        return countryIso;
    };

    GeoBlockingModel.makeCountriesSubQuery = function(field, type) {
        const prefix = ((Date.now() * Math.random()))
            .toString(10)
            .substr(0, 4)
            .split(/([0-9]{2})/g)
            .filter((x) => x !== '')
            .map((x) => String.fromCharCode(97 + (parseInt(x, 10) % 25)))
            .join('');

        let scountries;

        switch (DB_DIALECT) {
            case 'sqlite':
                field = field
                    .match(/^([^.]+)\.(.*)$/)
                    .filter((x, y) => y)
                    .map((x) => `\`${x}\``)
                    .join('.');

                scountries = `
                    SELECT
                      ${prefix}qco.iso
                    FROM
                      Countries as ${prefix}qco,
                      GeoBlockingCountry as ${prefix}qgbc,
                      GeoBlockings as ${prefix}qgb
                    WHERE
                      ${prefix}qgb.targetUUID = ${field} AND
                      ${prefix}qgb.entityType = ${type} AND
                      ${prefix}qgbc.geoBlockingUuid = ${prefix}qgb.uuid AND
                      ${prefix}qco.id = ${prefix}qgbc.countryId
                        `;
                break;
            case 'postgres':
                field = field
                    .match(/^([^.]+)\.(.*)$/)
                    .filter((x, y) => y)
                    .map((x) => `"${x}"`)
                    .join('.');

                scountries = `
                    SELECT
                      ${prefix}qco.iso
                    FROM
                      "Countries" as ${prefix}qco,
                      "GeoBlockingCountry" as ${prefix}qgbc,
                      "GeoBlockings" as ${prefix}qgb
                    WHERE
                      ${prefix}qgb."targetUUID" = ${field} AND
                      ${prefix}qgb."entityType" = ${type} AND
                      ${prefix}qgbc."GeoBlockingUuid" = ${prefix}qgb.uuid AND
                      ${prefix}qco.id = ${prefix}qgbc."CountryId"
                    `;
                break;
            default:
                throw new Error(`Unknow dialect ${DB_DIALECT}`);
        }

        return scountries.replace(/[\s\t\v]+/g, ' ');
    };

    GeoBlockingModel.makeGeoBlockingInclude = function(
        as = 'geoBlocking', extra = {}
    ) {
        return {
            model: GeoBlockingModel,
            as,
            attributes: [
                ['type', 'blockType'],
            ],
            required: false,
            ...extra || {},
        };
    };

    GeoBlockingModel.makeGeoBlockingWhere =
    function(countryIso, type, mfield, field = null) {

        field = field || '$geoBlocking.type$';
        const countries = GeoBlockingModel.makeCountriesSubQuery(
            mfield,
            type,
        );

        return {
            [Op.or]: [
                {
                    [`${field}`]: null,
                },
                {
                    [Op.and]: [{
                        [`${field}`]:
                        GeoBlockingModel.TYPE_WHITE,
                    },
                    sequelize.literal(`'${countryIso}' IN (${countries})`),
                    ],
                },
                {
                    [Op.and]: [{
                        [`${field}`]:
                        GeoBlockingModel.TYPE_BLACK,
                    },
                    sequelize.literal(
                        `'${countryIso}' NOT IN (${countries})`),
                    ],
                },
            ],
        };
    };

    GeoBlockingModel.extendScopeDefinition = function(
        countryIso, type, fkField, as = undefined, field = undefined, extra = {}
    ) {
        countryIso = GeoBlockingModel.resolveCountryIso(countryIso);

        return (scope) => {
            const otherwheres = (scope.include || [])
                .filter((s) => s.__geoBlockExtended)
                .filter((s) => s.where)
                .reduce((w, scope) => {
                    const orcond = scope.where[Op.or];
                    delete scope.where[Op.or];

                    if (w[Op.or]) {
                        return nextAnd(
                            w,
                            {
                                [Op.or]: orcond,
                            },
                            true
                        );
                    } else {
                        return {
                            ...w,
                            [Op.or]: orcond,
                        };
                    }
                }, {});

            const result = {
                ...scope,
                include: [
                    ...scope.include || [],
                    GeoBlockingModel.makeGeoBlockingInclude(as, extra),
                ],
                where: {
                    ...(
                        (scope.where || {})[Op.or] ?
                            {[Op.and]: scope.where} :
                            (scope.where || {})
                    ),
                    ...nextAnd(
                        GeoBlockingModel.makeGeoBlockingWhere(
                            countryIso,
                            type,
                            fkField,
                            field,
                        ),
                        otherwheres,
                        true,
                    ),
                },
                __geoBlockExtended: true,
            };

            return result;
        };
    };

    return GeoBlockingModel;
};
