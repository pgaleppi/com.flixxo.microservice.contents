import {createDebug} from '../logger';
import {getUser} from './contents';
import {ValidationError} from '../errors';
import models from '../models';

const debug = createDebug(__filename);

/**
 * Create content subtitle
 * @param {string} contentUUID
 * @param {string} url
 * @param {string} lang
 * @param {string} label
 * @param {boolean} closeCaption
 * @return {Promise}
 */
export async function createSubtitle(
    contentUUID, url, lang, label, closeCaption,
) {
    debug(`Creating content subtitle`);
    // User/Content validated previously

    const subtitle = await models.subtitle.create({
        uuid: contentUUID,
        label,
        url,
        lang,
        closeCaption,
    });
    debug(`Subtitle created ${subtitle.id}`);

    return {
        success: true,
        url: subtitle.url,
        id: subtitle.id,
    };
}

/**
 * Get a Content subtitled allowed by author
 * @param {string} lang
 * @param {string} email
 * @param {string} uuid
 * @return {Promise}
 */
export const getContentByAuthor = [
    'lang', async (lang, email, uuid) => {
        const user = await getUser(email);

        debug(`Content detail ${uuid} by author ${email}`);

        const content = await models.contents.scope(
            {method: ['i18n', lang]},
            {method: ['uploadSubtitlesAllowed', user.id]},
        ).findByPk(uuid);

        // User does not have permissions to this content, it does not exist or
        // it's status is not available for subtitles updates
        if (!content) {
            throw new ValidationError('Content error', [{
                field: 'uuid',
                type: 'ForbiddenError',
                msg: 'Invalid user content id',
            }]);
        }

        return {
            ...content.get({plain: true}),
        };

    }];

