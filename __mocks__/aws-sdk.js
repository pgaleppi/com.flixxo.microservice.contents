export class S3 {

    deleteObjects(params, cb) {
        return cb(null, {
            Deleted: [1, 2, 3],
        });
    }

}

export class SQS {

    constructor() {
        this.reset();
    }

    get calls() {
        return this._calls;
    }

    __setError(err) {
        this._error = err;
    }

    sendMessage({MessageBody}, cb) {
        this._calls.push(MessageBody);
        return cb(this._error);
    }

    reset() {
        this._calls = [];
        this._error = null;
    }
}
