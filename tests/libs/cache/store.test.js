
import path from 'path';
import {Store} from '../../../src/cache';
import data from '../../../package.json';

describe('Test cache storage', () => {

    describe('.getPrefix', () => {
        const fakePath = path.join(__dirname, '../../../src/fake/file.js');

        test('Must to get valid prefix', () => {
            let prefix = Store.getPrefix(fakePath);
            expect(prefix).toBe(`CACHE:${data.name}:fake.file`);
        });

        test('Must to get valid prefix with extra', () => {
            let prefix = Store.getPrefix(fakePath, 'extra');
            expect(prefix).toBe(`CACHE:${data.name}:fake.file:extra`);
        });
    });

});
