import Promise from 'bluebird';
import {nsWrapper} from '../logger/context';
import bootstrapServer from '../services';
import {createServer} from '../rpclambda';
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

let server;

/**
 * Get server
 * @return {Promise<RPCServer>}
 */
export async function getServer() {
    if (!server) {
        server = Promise.resolve((async () => {
            const rpcServer = await createServer();
            return bootstrapServer(rpcServer);
        })());
    }

    return server;
}

/**
 * Lambda Handler
 * @param {object} event
 * @param {object} context
 * @return {Promise<object>}
 */
export default async (event, context) => {
    const startedAt = Date.now();
    let cmd = 'Unknow';

    return nsWrapper(async (callContext) => {
        let response;
        const services = await getServer();
        const {awsRequestId} = context;

        context.callbackWaitsForEmptyEventLoop = false;
        callContext.addPrefix('RequestId', awsRequestId);

        try {
            const body = JSON.parse(event.body);
            cmd = body.cmd;
            response = await services.executeCommand(body);
            const delta = Date.now() - startedAt;

            logger.info(
                `[ RPC:EXEC ] Executted ${cmd} as success in ${delta}ms`
            );

            return {
                statusCode: 200,
                body: JSON.stringify(response.data),
            };

        } catch (err) {
            const delta = Date.now() - startedAt;
            logger.error(
                `[ RPC:EXEC ] Executted ${cmd} as failed in ${delta}ms ` +
                `[ MSG:${err.message} NAME:${err.name} ]`
            );
            debug('body:', event.body);
            debug(err);

            return {
                statusCode: 500,
                body: JSON.stringify({
                    message: 'Internal server error',
                }),
            };
        }
    });
};
