import parseTorrent from 'parse-torrent';
// import config from '../config';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

// const PIECE_SIZE_EXPONENT = config.get('torrent:pieceSizeExponent');

export default (sequelize, DataTypes) => {

    const instanceMethods = {};

    const TorrentFileModel = sequelize.define('TorrentFile', {

        infoHash: {
            type: DataTypes.STRING(40),
            primaryKey: true,
            allowNull: {
                args: false,
                msg: 'Content must to be the torrent hash',
            },
            validate: {
                is: {
                    args: /^[A-F0-9]{40}$/i,
                    msg: 'Invalid SHA1 format',
                },
            },
            set(hash) {
                // Save hash in lower case
                this.setDataValue('infoHash', hash.toLowerCase());
            },
            unique: {
                args: true,
                msg: `This torrent is already added`,
            },
        },

        torrentFile: {
            type: DataTypes.BLOB,
            allowNull: false,
            set(data) {
                if (typeof data === 'string') {
                    data = Buffer.from(data, 'base64');
                }

                this.setDataValue('torrentFile', data);
            },
        },

        torrentData: {
            type: DataTypes.VIRTUAL(
                DataTypes.STRING,
                ['torrentFile'],
            ),
            get() {
                const torrentFile = this.get('torrentFile');
                return (torrentFile ?
                    torrentFile.toString('base64') : undefined);
            },
        },

        totalLength: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,
        },

        pieceLength: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,
        },

        lastPieceLength: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,
        },

    }, {
        timestamps: true,
        paranoid: true,
        instanceMethods,
        validate: {
            validTorrent() {
                let parsed;
                debug(`Validate torrentFile`, this.torrentFile);
                try {
                    parsed = parseTorrent(this.torrentFile);
                } catch (e) {
                    debug(`Error parsing torrent`, e);
                    throw new Error(`Invalid torrentFile format`);
                }
                debug(`Info`, parsed);
                // @TODO check sizes
            },
        },
    });

    if (typeof TorrentFileModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                TorrentFileModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    TorrentFileModel.associate = function(models, db) {
        TorrentFileModel.belongsTo(models.contents, {
            foreignKey: {
                name: 'contentUUID',
                allowNull: false,
            },
            targetKey: 'uuid',
        });
    };

    /**
     * Search one record by infoHash
     * @param {string} infoHash
     * @param {object} [options]
     * @return {Promise<TorrentFileModel|null>}
     */
    TorrentFileModel.findByHash = function(infoHash, options = {}) {
        infoHash = infoHash.toLowerCase();
        return this.findOne({
            where: {infoHash},
            ...options,
        });
    };

    /**
     * Build model instance from torrentFile
     * @param {Buffer} buf
     * @return {TorrentFileModel}
     */
    TorrentFileModel.fromTorrentFile = function(buf) {
        debug(`Creating from torrent file`);

        const info = parseTorrent(buf);

        return TorrentFileModel.build({
            torrentFile: buf,
            infoHash: info.infoHash,
            totalLength: info.length,
            pieceLength: info.pieceLength,
            lastPieceLength: info.lastPieceLength,
        });
    };

    return TorrentFileModel;
};
