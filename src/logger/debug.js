
import debugLib from 'debug';
import {createPrefix} from './utils';
import {getNamespace} from './context';

/**
 * Create debug instance
 * @param {string} filepath File path
 * @return {debug} New debug instance
 */
export default function createDebug(filepath) {
    const debugFn = debugLib(createPrefix(filepath));
    return (...p) => {
        const ns = getNamespace();
        let prefixes = null;
        let prefix = '';

        if (ns) {
            prefixes = ns.get('prefixes');
            prefix = (prefixes || [])
                .reduce(
                    (prx, {key, prefix}) => `${prx} [ ${key}: ${prefix} ]`,
                    prefix,
                );
        }

        return debugFn(prefix, ...p);
    };
}
