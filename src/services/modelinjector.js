
import ModelInjector from '../middleware/modelinjector';

const modelInjector = new ModelInjector();

export default modelInjector;
