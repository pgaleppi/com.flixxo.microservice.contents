
import * as status from './status';
import * as categories from './categories';
import * as users from './users';
import * as countries from './countries';
import * as contents from './contents';
import * as advertisements from './advertisements';
import * as follow from './follow';
import * as contentsversion from './contentsversion';
import * as images from './images';
import * as series from './series';
import * as subtitles from './subtitles';
import * as langs from './langs';
import * as carrusels from './carrusels';
import {createDebug} from '../logger';
import errorMapper from './errormapper';
import apiReqIdInjector from '../middleware/apireqid';
import modelInjector from './modelinjector';

const debug = createDebug(__filename);

/**
 * Start server and load services
 * @param {RPCServer} server
 * @return {rpclib.Server}
 */
export default async function bootstrapServer(server) {
    debug('Add error mapper');
    server.use(...apiReqIdInjector());
    server.use(...errorMapper.applyMiddleware());
    server.use(...modelInjector.applyMiddleware());

    debug('Load services');

    server.define({
        status,
        categories,
        countries,
        users,
        contents,
        advertisements,
        follow,
        contentsversion,
        series,
        subtitles,
        images,
        langs,
        carrusels,
    });

    return server;
}
