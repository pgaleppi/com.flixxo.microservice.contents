import {
    EmptyResultError,
    ValidationError,
    UniqueConstraintError,
} from 'sequelize';
import {syncDb, loadFixture, loadData} from '../helpers/db';
import models from '../../src/models';

const lang = 'en';

describe('.findByUUID', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contentversion-for-status');
    });

    test(`Must to find a exiting version`, async () => {
        const version = await models.contentversion.findByUUID(
            '41a0ff76-0030-2bf2-a810-c0f054033a28',
            '01010101-0030-2bf2-a810-c0f054033a28',
            lang,
        );

        expect(version.contentUUID).toEqual(
                                        '41a0ff76-0030-2bf2-a810-c0f054033a28');
        expect(version.uuid).toEqual(
                                        '01010101-0030-2bf2-a810-c0f054033a28');
        expect(version.title).toEqual('Fake content modified');
    });

    test(`Must to throw EmptyResult`, async () => {
        try {
            await models.contentversion.findByUUID(
                '41a0ff76-0030-2bf2-a810-c0f054033a26',
                '01010101-0030-2bf2-a810-c0f054033a28',
                lang,
            );
        } catch (e) {
            expect(e).toBeInstanceOf(EmptyResultError);
            return;
        }

        throw new Error('Pass');
    });

    test(`Must to accept options`, async () => {
        const version = await models.contentversion.findByUUID(
            '41a0ff76-0030-2bf2-a810-c0f054033a28',
            '01010101-0030-2bf2-a810-c0f054033a28',
            lang,
            {
                attributes: ['uuid'],
            }
        );

        expect(version.get({plain: true})).toEqual({
            'metadata': [
                {'body': 'Fake body large large large large',
                 'contentversion': {'audioLang': 'en'},
                 'context_lang': 'en',
                 'lang': 'en',
                 'title': 'Fake content modified'},
            ],
            'uuid': '01010101-0030-2bf2-a810-c0f054033a28',
        });
    });
});

describe('#setStatus', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contentversion-for-status');
    });

    test(`Must to change version status`, async () => {
        const version = await models.contentversion.findByUUID(
            '41a0ff76-0030-2bf2-a810-c0f054033a28',
            '01010101-0030-2bf2-a810-c0f054033a28',
            lang,
        );

        await version.setStatus(
            models.contentversion.STATUS_REJECTED,
            'Contain uggly words'
        );

        expect(version.status).toBe(models.contentversion.STATUS_REJECTED);
        expect(version.statusComment).toBe('Contain uggly words');
    });

    test(`Must to throw error when invalid status`, async () => {
        const version = await models.contentversion.findByUUID(
            '41a0ff76-0030-2bf2-a810-c0f054033a28',
            '01010101-0030-2bf2-a810-c0f054033a28',
            lang,
        );

        try {
            await version.setStatus(
                666,
                'Contain uggly words'
            );
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.message).toBe('Validation error: Invalid version status');
            return;
        }

        throw new Error('Pass');
    });
});

describe('#clone', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contentversion-for-clone');
    });

    test('Must to clone version (with reference)', async () => {
        const version = await models.contentversion.findByUUID(
            '00000000-0070-0000-0000-000000000000',
            '00000000-0080-0000-0000-000000000000',
            lang,
        );

        let clone = await version.clone();

        expect(clone.uuid).not.toBe(version.uuid);
        expect(clone.title).toBe(version.title);
        expect(clone.internalReference).toBe(version.uuid);
        expect(clone.metadata.length).toBe(version.metadata.length);
        expect(clone.metadata[0].title).toBe(version.metadata[0].title);
        expect(clone.metadata[0].body).toBe(version.metadata[0].body);
        expect(clone.metadata[0].lang).toBe(version.metadata[0].lang);
    });

    test('Must to clone version', async () => {
        const version = await models.contentversion.findByUUID(
            '00000000-0070-0000-0000-000000000000',
            '00000000-0080-0000-0000-000000000000',
            lang
        );

        let clone = await version.clone(false);

        expect(clone.uuid).not.toBe(version.uuid);
        expect(clone.title).toBe(version.title);
        expect(clone.internalReference).toBeNull();
        expect(clone.metadata.length).toBe(version.metadata.length);
        expect(clone.metadata[0].title).toBe(version.metadata[0].title);
        expect(clone.metadata[0].body).toBe(version.metadata[0].body);
        expect(clone.metadata[0].lang).toBe(version.metadata[0].lang);
    });
});

describe('#getPlain', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contentversion-for-get-plain');
    });

    // FLX-2362, temporally skipped
    test.skip(`Must to get a plain version`, async () => {
        const version = await models.contentversion.findByUUID(
            '00000000-0014-0000-0000-000000000000',
            '00000000-0015-0000-0000-000000000000',
            lang,
        );

        const plain = await version.getPlain();
        expect(plain).toEqual({
            audioLang: 'en',
            body: 'Fake body large large large large',
            categoryId: 5,
            credits: null,
            authorFee: 60,
            seederFee: 43,
            duration: 120,
            rating: 3,
            title: 'Fake content modified',
            tags: [],
        });
    });
});

describe('#getChangedFields', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contentversion-for-get-changed-fields');
    });

    test(`Must return an array of changed fields`, async () => {
        const version = await models.contentversion.findByUUID(
            '00000000-0014-0000-0000-000000000000',
            '00000000-0015-0000-0000-000000000000',
            lang,
        );
        const baseContent = await models.contents
            .scope({method: ['i18n', lang]})
            .findByPk(
            '00000000-0014-0000-0000-000000000000',
        );

        const changedFields = await version.getChangedFields(
            baseContent
        );
        expect(changedFields).toEqual([
            'title', 'body', 'audioLang', 'categoryId', 'rating',
        ]);
    });

    test(`Must return an empty array if no fields changed`, async () => {
        const version = await models.contentversion.findByUUID(
            '00000000-0024-0000-0000-000000000000',
            '00000000-0025-0000-0000-000000000000',
            lang,
        );
        const baseContent = await models.contents
            .scope({method: ['i18n', lang]})
            .findByPk(
            '00000000-0024-0000-0000-000000000000',
        );

        const changedFields = await version.getChangedFields(
            baseContent
        );
        expect(changedFields).toEqual([]);
    });

});

describe('#onlyPriceHasChanged', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contentversion-for-price-has-changed');
    });

    // FLX-2362, temporally skipped
    test.skip(`Must return true if price has changed`, async () => {
        const version = await models.contentversion.findByUUID(
            '00000000-0014-0000-0000-000000000000',
            '00000000-0015-0000-0000-000000000000',
            lang,
        );
        const baseContent = await models.contents
            .scope({method: ['i18n', lang]})
            .findByPk(
            '00000000-0014-0000-0000-000000000000',
        );

        const priceChanged = await version.onlyPriceHasChanged(
            baseContent
        );
        expect(priceChanged).toEqual(true);
    });

    test(`Must return false if price and other fields was changed`, async () => {
        const version = await models.contentversion.findByUUID(
            '00000000-0024-0000-0000-000000000000',
            '00000000-0025-0000-0000-000000000000',
            lang,
        );
        const baseContent = await models.contents
            .scope({method: ['i18n', lang]})
            .findByPk(
            '00000000-0024-0000-0000-000000000000',
        );

        const priceChanged = await version.onlyPriceHasChanged(
            baseContent
        );
        expect(priceChanged).toEqual(false);
    });

});

describe('#setContentVersionStatus', () => {
    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contentversion-for-set-content-version-status');
    });

    test(`Must to change content version status`, async () => {
        let version = await models.contentversion.findByUUID(
            '00000000-0014-0000-0000-000000000000',
            '00000000-0015-0000-0000-000000000002',
            lang,
        );

        let content = await version.getContent();
        expect(content.versionStatus).toBe(models.contents.VERSION_STATUS_NONE);

        version.status = models.contentversion.STATUS_REVIEW;
        await version.save();

        content = await version.getContent();

        expect(content.versionStatus).toBe(models.contents.VERSION_STATUS_ACTIVE);
    });

    test(`Must to change content version status to inactive`, async () => {
        let version = await models.contentversion.findByUUID(
            '00000000-0014-0000-0000-000000000001',
            '00000000-0015-0000-0001-000000000002',
            lang,
        );

        let content = await version.getContent();
        expect(content.versionStatus).toBe(models.contents.VERSION_STATUS_ACTIVE);

        version.status = models.contentversion.STATUS_REQUEST_CHANGES;
        await version.save();

        content = await version.getContent();

        expect(content.versionStatus).toBe(models.contents.VERSION_STATUS_NONE);
    });
});

describe('Status flow', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contentversion-for-status');
    });

    test(`Must to allow change a version`, async () => {
        const version = await models.contentversion.findByUUID(
            '41a0ff76-0030-2bf2-a810-c0f054033a28',
            '01010101-0030-2bf2-a810-c0f054033a28',
            lang,
        );

        version.status = models.contentversion.STATUS_REJECTED;
        await version.save();
    });

    test(`Must to throw error with canceled`, async () => {
        const version = await models.contentversion.findByUUID(
            '41a0ff76-0030-2bf2-a810-c0f054033a28',
            '01010101-0030-2bf2-a810-c0f054033a29',
            lang,
        );

        version.status = models.contentversion.STATUS_ACCEPTED;
        try {
            await version.save();
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.message).toBe(
                'Validation error: Canceled version can\'t change status');
            return;
        }

        throw new Error('Pass');
    });

    test(`Must to throw error with rejected`, async () => {
        const version = await models.contentversion.findByUUID(
            '41a0ff76-0030-2bf2-a810-c0f054033a28',
            '01010101-0030-2bf2-a810-c0f054033a30',
            lang,
        );

        version.status = models.contentversion.STATUS_ACCEPTED;
        try {
            await version.save();
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.message).toBe(
                'Validation error: Rejected version can\'t change status');
            return;
        }

        throw new Error('Pass');
    });
});

describe('Validate indexes', () => {

    describe('only_one_active_version', () => {

        beforeEach(async () => {
            await syncDb(true);
            await loadFixture('users');
            await loadData('categories');
            await loadFixture('contentversion-for-index-only-one');
        });

        test('Must to allow create other version draft', async () => {
            const version = await models.contentversion.create({
                contentUUID: '00000000-0040-0000-0000-000000000000',
                metadata: [{
                    entityType: models.metadata.ENTITY_TYPE_CONTENTVERSION,
                    lang: 'en',
                    title: 'Other title',
                    body: 'Other body long',
                }],
                categoryId: 2,
                seederFee: 43,
                authorFee: 60,
            }, {include: ['metadata']});

            expect(version);
        });

        test('Must to throw error with a existing draft', async () => {
            try {
                await models.contentversion.create({
                    contentUUID: '00000000-0040-0000-0000-000000000001',
                    metadata: [{
                        entityType: models.metadata.ENTITY_TYPE_CONTENTVERSION,
                        lang: 'en',
                        title: 'Other title',
                        body: 'Other body long',
                    }],
                    categoryId: 2,
                    seederFee: 43,
                    authorFee: 60,
                }, {include: ['metadata']});
            } catch (e) {
                expect(e).toBeInstanceOf(UniqueConstraintError);
                expect(e.errors).toHaveLength(1);
                expect(e.errors[0].type).toBe('unique violation');
                expect(e.errors[0].path).toBe('contentUUID');
                return;
            }

            throw new Error('Pass');
        });

        test('Must to throw error with a existing in_review', async () => {
            try {
                await models.contentversion.create({
                    contentUUID: '00000000-0040-0000-0000-000000000002',
                    metadata: [{
                        entityType: models.metadata.ENTITY_TYPE_CONTENTVERSION,
                        lang: 'en',
                        title: 'Other title',
                        body: 'Other body long',
                    }],
                    categoryId: 2,
                    seederFee: 43,
                    authorFee: 60,
                }, {include: ['metadata']});
            } catch (e) {
                expect(e).toBeInstanceOf(UniqueConstraintError);
                expect(e.errors).toHaveLength(1);
                expect(e.errors[0].type).toBe('unique violation');
                expect(e.errors[0].path).toBe('contentUUID');
                return;
            }

            throw new Error('Pass');
        });
    });
});

describe('.getSeederFeeAt', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('users');
        await loadData('categories');
        await loadFixture('contentversion-for-get-last-seederfee-at');
    });

    test(`Must to take valid price (last)`, async () => {
        const last = await models.contentversion.getSeederFeeAt(
            '41a0ff76-faf0-2bf2-a810-c0f054033a28',
            Date.now()
        );

        expect(last).toEqual(5);
    });

    test(`Must to take null if there are not modifications yet`, async () => {
        const last = await models.contentversion.getSeederFeeAt(
            '41a0ff76-faf0-2bf2-a810-c0f054033a28',
            1563300999999
        );

        expect(last).toBeNull();
    });

    test(`Must to take one if date is equal`, async () => {
        const last = await models.contentversion.getSeederFeeAt(
            '41a0ff76-faf0-2bf2-a810-c0f054033a28',
            1563301000000
        );

        expect(last).toEqual(1);
    });

    test(`Must to take one if date is one second up`, async () => {
        const last = await models.contentversion.getSeederFeeAt(
            '41a0ff76-faf0-2bf2-a810-c0f054033a28',
            1563301001000
        );

        expect(last).toEqual(1);
    });

    test(`Must to take one if date is one second down two`, async () => {
        const last = await models.contentversion.getSeederFeeAt(
            '41a0ff76-faf0-2bf2-a810-c0f054033a28',
            1563301999999
        );

        expect(last).toEqual(1);
    });

    test(`Must to ignore not confirmed`, async () => {
        const last = await models.contentversion.getSeederFeeAt(
            '41a0ff76-faf0-2bf2-a810-c0f054033a28',
            1563303500000
        );

        expect(last).toEqual(2);
    });

    test(`Must to take null if there are not versions from content`, async () => {
        const last = await models.contentversion.getSeederFeeAt(
            '41a0ff76-faf0-2bf2-a810-c0f054033a29',
            1563300999999
        );

        expect(last).toBeNull();
    });

    test(`Must to ignore null prices versions`, async () => {
        const last = await models.contentversion.getSeederFeeAt(
            '41a0ff76-faf0-2bf2-a810-c0f054033a30',
            1563302900000
        );

        expect(last).toEqual(1);
    });
});
