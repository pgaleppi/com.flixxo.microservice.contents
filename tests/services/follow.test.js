import {syncDb, loadFixture} from '../helpers/db';
import {ForeignKeyConstraintError, EmptyResultError} from 'sequelize';
import * as service from '../../src/services/follow';

describe('#follow', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('followers-validation');
    });

    test(`Must to follow user`, async () => {
        let res = await service.follow('user@1.com', 101);
        expect(res).toBeTruthy();
    });

    test(`Must to throw contrait error with invalid followed`, async () => {
        expect(service.follow('user@1.com', 666))
            .rejects.toThrow(ForeignKeyConstraintError);
    });

    test(`Must to throw not found error with invalid follower`, async () => {
        expect(service.follow('no@user.test', 101))
            .rejects.toThrow(EmptyResultError);
    });

});

describe('#unfollow', () => {

    beforeEach(async (done) => {
        await syncDb(true);
        await loadFixture('followers-validation');
        done();
    });

    test(`Must to unfollow user`, async () => {
        let res = await service.unfollow('user@1.com', 101);
        expect(res).toBeTruthy();
    });

    test(`Must to throw not found error with invalid unfollower`, async () => {
        expect(service.unfollow('no@user.test', 101))
            .rejects.toThrow(EmptyResultError);
    });

});

describe('#getFollowers', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadFixture('followers-lists');
        done();
    });

    test('Must to get a list of followers', async () => {
        let followers = await service.getFollowers('user@4.com');
        expect(followers.map((x) => x.id)).toEqual([100, 101, 102]);
    });

    test(`Must to throw not found error with invalid user`, async () => {
        expect(service.getFollowers('no@user.test'))
            .rejects.toThrow(EmptyResultError);
    });

});

describe('#getFolloweds', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadFixture('followers-lists');
        done();
    });

    test('Must to get a list of followers', async () => {
        let followers = await service.getFolloweds('user@1.com');
        expect(followers.map((x) => x.id)).toEqual([101, 102, 103]);
    });

    test(`Must to throw not found error with invalid user`, async () => {
        expect(service.getFolloweds('no@user.test'))
            .rejects.toThrow(EmptyResultError);
    });

});
