FROM node:12.16.1

RUN mkdir /root/.ssh
COPY id_rsa /root/.ssh

ARG FLIXXO_ENV
ENV FLIXXO_ENV=$FLIXXO_ENV

RUN ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
RUN apt-get install -y python g++ make git libsqlite3-dev

WORKDIR /src

ADD . /src

RUN yarn global add node-pre-gyp &&\
    yarn install --frozen-lockfile --production=false --non-interactive

RUN rm /root/.ssh/id_rsa

RUN echo "Environment $FLIXXO_ENV"

CMD yarn start-serverless $FLIXXO_ENV
