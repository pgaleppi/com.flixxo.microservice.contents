import Command from './command';
import Server from './server';

/**
 * Create RPC Server
 * @return {Server}
 */
export function createServer() {
    return new Server();
}

/**
 * Get service connector
 * @param {string} service
 * @return {Promise<Command>}
 */
export async function getServiceConnection(service) {
    return new Command(null, service);
}
