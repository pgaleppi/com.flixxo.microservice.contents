
import {createDebug} from '../logger';
import langs from '../langs.json';

const debug = createDebug(__filename);

/**
 * LangModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {

    const instanceMethods = {};

    const LangsModel = sequelize.define('Lang', {

        lang: {
            type: DataTypes.STRING(2),
            allowNull: false,
            primaryKey: true,
            validate: {
                is: {
                    args: [/^[a-z]{2}$/],
                    msg: 'Invalid lang code',
                },
                isIn: {
                    args: [langs.enabled],
                    msg: 'Language not enabled',
                },
            },
        },

        nameEn: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        nameNative: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        enabled: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },

        name: {
            type: DataTypes.VIRTUAL(
                DataTypes.STRING,
                ['nameEn', 'nameNative']
            ),
            get() {
                return `${this.get('nameEn')} (${this.get('nameNative')})`;
            },
        },

    }, {
        name: {
            singular: 'Lang',
            plural: 'Langs',
        },

        defaultScope: {
            attributes: [
                'lang',
                'nameEn',
                'nameNative',
                'name',
            ],
        },

        instanceMethods: instanceMethods,
    });

    if (typeof LangsModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                LangsModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    /**
     * @override
     */
    LangsModel.associate = function(models) {
        const noop = () =>{};
        noop();
    };

    return LangsModel;
};
