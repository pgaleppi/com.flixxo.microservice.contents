import {syncDb, loadFixture} from '../helpers/db';
import {ValidationError, UniqueConstraintError} from 'sequelize';
import models from '../../src/models';

describe('Validations', () => {

    beforeEach(async () => {
        await syncDb(true);
        await loadFixture('followers-validation');
    });

    test('Must to validate no autofollow', async () => {
        return expect(models.followers.create({
            followerId: 100,
            followedId: 100,
        })).rejects.toThrow(ValidationError);
    });

    test('Must to validate no repeat', async () => {
        return expect(models.followers.create({
            followerId: 103,
            followedId: 100,
        })).rejects.toThrow(UniqueConstraintError);
    });

});
