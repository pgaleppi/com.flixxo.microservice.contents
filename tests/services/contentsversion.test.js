jest.mock('axios');
jest.mock('../../src/sqsrpc');

import {
    EmptyResultError,
} from 'sequelize';
import {syncDb, loadFixture, loadData} from '../helpers/db';

import * as contentsService from '../../src/services/contents';
import * as contentsVersionService from '../../src/services/contentsversion';
import models from '../../src/models';
import {ValidationError} from '../../src/errors';

const lang = 'en';

describe('Testing contents version service', () => {

    beforeEach(async (done) => {
        await syncDb(true);
        await loadData('categories');
        await loadData('00_countries');
        await loadFixture('users');
        await loadFixture('profiles');
        done();
    }, 10000);

    describe('#listByContent', () => {

        beforeEach(async () => {
            await loadFixture('contents-for-versions-list-service');
        });

        test('Must to list versions of valid conentent', async () => {
            let res = await contentsVersionService.listByContent[1](
                lang,
                '00000000-00A0-0000-0000-000000000000'
            );

            expect(res).toHaveLength(5);
        });

        test('Must to get an empty list of valid conetnt without', async () => {
            let res = await contentsVersionService.listByContent[1](
                lang,
                '00000000-00A0-0000-0000-000000000001'
            );

            expect(res).toEqual([]);
        });

        test('Must throw error with invalid content', async () => {
            try {
                await contentsVersionService.listByContent[1](
                    lang,
                    '00000000-00A0-0000-0000-0000000000AA'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });
    });

    describe('#listByUserContent', () => {

        beforeEach(async () => {
            await loadFixture('contents-for-versions-list-service');
        });

        test('Must to list versions of valid conentent', async () => {
            let res = await contentsVersionService.listByUserContent[1](
                lang,
                'user1@fakemail.com',
                '00000000-00A0-0000-0000-000000000000'
            );

            expect(res).toHaveLength(5);
        });

        test('Must to get an empty list of valid conetnt without', async () => {
            let res = await contentsVersionService.listByUserContent[1](
                lang,
                'user1@fakemail.com',
                '00000000-00A0-0000-0000-000000000001'
            );

            expect(res).toEqual([]);
        });

        test('Must throw error with invalid content', async () => {
            try {
                await contentsVersionService.listByUserContent[1](
                    lang,
                    'user1@fakemail.com',
                    '00000000-00A0-0000-0000-0000000000AA'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });

        test('Must throw error with unowned content', async () => {
            try {
                await contentsVersionService.listByUserContent[1](
                    lang,
                    'user1@fakemail.com',
                    '00000000-00A0-0000-0000-000000000002'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });
    });

    describe('#listForModerator', () => {

        beforeEach(async () => {
            await loadFixture('contents-for-versions-list-service');
        });

        test('Must to list versions of valid conentent', async () => {
            let res = await contentsVersionService.listForModerator[1](
                lang,
                'user2@fakemail.com',
                '00000000-00A0-0000-0000-000000000000'
            );

            expect(res).toHaveLength(5);
        });

        test('Must to get an empty list of valid conetnt without', async () => {
            let res = await contentsVersionService.listForModerator[1](
                lang,
                'user2@fakemail.com',
                '00000000-00A0-0000-0000-000000000001'
            );

            expect(res).toEqual([]);
        });

        test('Must throw error with invalid content', async () => {
            try {
                await contentsVersionService.listForModerator[1](
                    lang,
                    'user2@fakemail.com',
                    '00000000-00A0-0000-0000-0000000000AA'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });

        test('Must throw error with unowned content', async () => {
            try {
                await contentsVersionService.listForModerator[1](
                    lang,
                    'user1@fakemail.com',
                    '00000000-00A0-0000-0000-000000000002'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });
    });

    describe('#getForModerator', () => {

        beforeEach(async () => {
            await loadFixture('contents-for-versions-list-service');
        });

        test('Must to get from valid conentent', async () => {
            let res = await contentsVersionService.getForModerator[1](
                lang,
                'user2@fakemail.com',
                '00000000-00A0-0000-0000-000000000000',
                '00000000-00A1-0000-0000-000000000000'
            );

            expect(res.uuid).toBe('00000000-00A1-0000-0000-000000000000');
            expect(res.contentUUID)
                            .toBe('00000000-00A0-0000-0000-000000000000');
            expect(res.title).toBe('Fake content modified');
        });

        test('Must get error if user is not moderator', async () => {
            try {
                await contentsVersionService.getForModerator[1](
                    lang,
                    'user1@fakemail.com',
                    '00000000-00A0-0000-0000-000000000000',
                    '00000000-00A1-0000-0000-000000000000'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });
    });

    describe('#getVersionForUpdate', () => {
        beforeEach(async (done) => {
            await loadFixture('contents-for-add-version');
            await loadFixture('contentversion-for-clone');
            await loadFixture('medias');
            await loadFixture('subtitles-for-contentsversion');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must create new draft version`, async () => {
            const UUID = '41a0ff76-0001-2bf2-a810-c0f054033a28';
            let originalContent = await contentsService.getContentByAuthor[1](
                lang,
                'user1@fakemail.com',
                UUID,
            );
            const result = await contentsVersionService.getVersionForUpdate(
                'user1@fakemail.com', UUID, true
            );

            expect(result.status).toBe(models.contentversion.STATUS_DRAFT);
            expect(result.metadata[0].title).toBe(originalContent.title);
            expect(result.Content.contentType)
                .toBe(originalContent.contentType);
            expect(result.contentUUID).toBe(originalContent.uuid);
            expect(result.createdAt).not.toBe(originalContent.createdAt);
            expect(result.updatedAt).not.toBe(originalContent.updatedAt);
            expect(result.Tags.length).toBe(originalContent.Tags.length);
            expect(result.media.length)
                .toBe(originalContent.media.length);
            expect(result.subtitle.length)
                .toBe(originalContent.subtitle.length);
        });

        it(`Must get version on req changes status`, async () => {
            const UUID = '00000000-0070-0000-0000-000000000000';

            const result = await contentsVersionService.getVersionForUpdate(
                'user1@fakemail.com', UUID
            );

            expect(result.status).toBe(models.contentversion.STATUS_REQUEST_CHANGES);
            expect(result.uuid).toBe('00000000-0080-0000-0000-000000000000');
            expect(result.Tags.length).toBe(0);
        });


        it(`Must throw error if version non-exists`, async () => {
            const UUID = '41a0ff76-0001-2bf2-a810-c0f054033a28';

            await expect(contentsVersionService.getVersionForUpdate(
                'user1@fakemail.com', UUID
            )).rejects.toThrow(ValidationError);
        });

    });

    describe('#setStatusOfUserContent', () => {
        beforeEach(async () => {
            await loadFixture('contentversion-for-set-status-service');
        });

        test(`Must to change status`, async () => {
            let res = await contentsVersionService.setStatusOfUserContent(
                'user1@fakemail.com',
                '00000000-00B0-0000-0000-000000000000',
                models.contentversion.STATUS_CANCELED
            );

            expect(res).toBeTruthy();
        });
    });

    describe('#moderateStatus', () => {

        beforeEach(async () => {
            await loadFixture('contentversion-for-moderate-status-service');
        });

        test(`Must to reject correct and enabled content`, async () => {
            let res = await contentsVersionService.moderateStatus(
                'user2@fakemail.com',
                '00000000-00E0-0000-0000-000000000000',
                models.contentversion.STATUS_REJECTED,
                'To uggly, until for you'
            );

            expect(res).toBeTruthy();

            const content = await models.contents.findByPk(
                '00000000-00E0-0000-0000-000000000000',
                {rejectOnEmpty: true}
            );

            expect(content.versionStatus).toBe(
                models.contents.VERSION_STATUS_NONE
            );
        });

        test(`Must to request changes of and enabled content`, async () => {
            let res = await contentsVersionService.moderateStatus(
                'user2@fakemail.com',
                '00000000-00E0-0000-0000-000000000000',
                models.contentversion.STATUS_REQUEST_CHANGES,
                'Delete that thumb'
            );

            expect(res).toBeTruthy();

            const content = await models.contents.findByPk(
                '00000000-00E0-0000-0000-000000000000',
                {rejectOnEmpty: true}
            );

            expect(content.versionStatus).toBe(
                models.contents.VERSION_STATUS_NONE
            );
        });

        test(`Must to apply changes of and enabled content`, async () => {
            let res = await contentsVersionService.moderateStatus(
                'user2@fakemail.com',
                '00000000-00E0-0000-0000-000000000000',
                models.contentversion.STATUS_ACCEPTED,
                'Congrats, its fine'
            );

            expect(res).toBeTruthy();

            const content = await models.contents
            .scope({method: ['i18n', lang]})
            .findByPk(
                '00000000-00E0-0000-0000-000000000000',
                {rejectOnEmpty: false}
            );

            expect(content.versionStatus).toBe(
                models.contents.VERSION_STATUS_NONE
            );

            expect(content.title).toBe('Fake content modified');
        });

        test(`Must throw error trying to cancel`, async () => {
            try {
                await contentsVersionService.moderateStatus(
                    'user2@fakemail.com',
                    '00000000-00E0-0000-0000-000000000000',
                    models.contentversion.STATUS_CANCELED,
                    'Congrats, its fine'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(ValidationError);
                expect(e.errors).toEqual([{
                    field: 'status',
                    type: 'Invalid',
                    message: 'Invalid status',
                }]);
                return;
            }

            throw new Error('Pass');
        });

        test(`Must throw error on draft version`, async () => {
            try {
                await contentsVersionService.moderateStatus(
                    'user2@fakemail.com',
                    '00000000-00E0-0000-0000-000000000001',
                    models.contentversion.STATUS_ACCEPTED,
                    'Congrats, its fine'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });

        test(`Must throw error on canceled version`, async () => {
            try {
                await contentsVersionService.moderateStatus(
                    'user2@fakemail.com',
                    '00000000-00E0-0000-0000-000000000002',
                    models.contentversion.STATUS_ACCEPTED,
                    'Congrats, its fine'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });

        test(`Must throw error on historical version`, async () => {
            try {
                await contentsVersionService.moderateStatus(
                    'user2@fakemail.com',
                    '00000000-00E0-0000-0000-000000000003',
                    models.contentversion.STATUS_ACCEPTED,
                    'Congrats, its fine'
                );
            } catch (e) {
                expect(e).toBeInstanceOf(EmptyResultError);
                return;
            }

            throw new Error('Pass');
        });
    });

    describe('#updateApprovedContent', () => {
        beforeEach(async (done) => {
            await loadFixture('contents-for-create-version');
            await loadFixture('medias');
            await loadFixture('temp-medias');
            await loadFixture('subtitles-for-contentsversion');
            await loadFixture('temp-subtitles-for-contentsversion');
            setTimeout(done, 1000); // For datetime filters
        }, 0);

        it(`Must create and update version`, async () => {
            const UUID = '41a0ff76-0001-2bf2-a810-c0f054033a28';
            // Create new version
            const activeVersion = await contentsVersionService.getVersionForUpdate(
                'user1@fakemail.com', UUID, true
            );
            const tempMedia = await contentsService.createTempContentMedia(
                    null,
                    'https://example.com/cover.jpg',
                    'Cover image',
                    1,
                    1,
                    'user1@fakemail.com',
                    activeVersion.uuid
                );

            const metadata = [{
                lang: 'en',
                title: 'new title test',
                body: 'new body test',
            }];

            const actualMedia = activeVersion.media.find((x) => x.type === 1);
            await contentsVersionService.updateActiveVersion(
                'user1@fakemail.com',
                UUID,
                {
                    metadata,
                    status: models.contentversion.STATUS_REVIEW,
                    tags: ['one'],
                    newMediaIds: [tempMedia.id],
                    deleteMediaIds: [actualMedia.id],
                },
            );
            const newVersion = await models.contentversion.findOne(
                {
                    where: {uuid: activeVersion.uuid},
                    include: [{
                        model: models.media,
                        as: 'media',
                        attributes: ['name', 'type', 'url', 'order', 'isDraft'],
                        required: false,
                        where: {isDraft: true},
                    }, {
                        model: models.subtitle,
                        as: 'subtitle',
                        attributes: [
                            'label', 'lang', 'url',
                            'closeCaption', 'isDraft'],
                        required: false,
                        where: {isDraft: true},
                    }, {
                        model: models.tags,
                        attributes: ['tag'],
                        required: false,
                        through: {
                            attributes: [],
                        },
                    },
                    {
                        model: models.metadata,
                        as: 'metadata',
                    }],
                }
            );
            expect(newVersion.status).toBe(models.contentversion.STATUS_REVIEW);
            expect(newVersion.title).toBe(metadata[0].title);
            expect(activeVersion.Tags.length).toBe(2);
            expect(newVersion.Tags.length).toBe(1);
            expect(newVersion.Tags[0].tag).toBe('one');
            expect(newVersion.media.length).toBe(activeVersion.media.length);
            expect(newVersion.media.length).toBe(4);
            expect(newVersion.media[0].isDraft).toBeTruthy();
            expect(newVersion.subtitle.length).toBe(activeVersion.subtitle.length);
            expect(newVersion.subtitle.length).toBe(4);
            expect(newVersion.subtitle[0].isDraft).toBeTruthy();
        });

        it(`Must create, update and auto-accept a version if content type is SERIE`, async () => {
            const UUID = '11111111-0002-2bf2-a810-c0f054033a28';
            // Create new version
            const activeVersion = await contentsVersionService.getVersionForUpdate(
                'user1@fakemail.com', UUID, true
            );

            const metadata = [{
                lang: 'en',
                title: 'new serie content title',
                body: 'new body test',
            }];
            await contentsVersionService.updateActiveVersion(
                'user1@fakemail.com',
                UUID,
                {
                    metadata,
                    status: models.contentversion.STATUS_REVIEW,
                    tags: ['one'],
                    newMediaIds: [],
                    deleteMediaIds: [],
                },
            );
            const newVersion = await models.contents.findOne(
                {
                    where: {uuid: activeVersion.contentUUID},
                    include: [{
                        model: models.media,
                        as: 'media',
                        attributes: ['name', 'type', 'url', 'order', 'isDraft'],
                        required: false,
                        where: {isDraft: true},
                    }, {
                        model: models.subtitle,
                        as: 'subtitle',
                        attributes: [
                            'label', 'lang', 'url',
                            'closeCaption', 'isDraft'],
                        required: false,
                        where: {isDraft: true},
                    }, {
                        model: models.tags,
                        attributes: ['tag'],
                        required: false,
                        through: {
                            attributes: [],
                        },
                    },
                    {
                        model: models.metadata,
                        as: 'metadata',
                        required: false,
                    }],
                }
            );
            expect(newVersion.status).toBe(models.contents.STATUS_ENABLED);
            expect(newVersion.moderationStatus).toBe(models.contents.MODSTATUS_ENABLED);
            expect(newVersion.title).toBe(metadata[0].title);
            expect(newVersion.Tags.length).toBe(1);
            expect(newVersion.Tags[0].tag).toBe('one');
            expect(newVersion.media.length).toBe(0);
            expect(newVersion.subtitle.length).toBe(0);
        });

        // FLX-2362, temporally skipped
        it.skip(`Must create, update and auto-accept a version if only the price was changed`, async () => {
            const UUID = '11111112-0002-2bf2-a810-c0f054033a28';
            // Create new version
            const activeVersion = await contentsVersionService.getVersionForUpdate(
                'user1@fakemail.com', UUID, true
            );
            const title = 'Fake video content';
            await contentsVersionService.updateActiveVersion(
                'user1@fakemail.com',
                UUID,
                {
                    title,
                    status: models.contentversion.STATUS_REVIEW,
                    tags: [],
                    authorFee: 20,
                    seederFee: 10,
                    newMediaIds: [],
                    deleteMediaIds: [],
                },
            );
            const newVersion = await models.contents.findOne(
                {
                    where: {uuid: activeVersion.contentUUID},
                    include: [{
                        model: models.media,
                        as: 'media',
                        attributes: ['name', 'type', 'url', 'order', 'isDraft'],
                        required: false,
                        where: {isDraft: true},
                    }, {
                        model: models.subtitle,
                        as: 'subtitle',
                        attributes: [
                            'label', 'lang', 'url',
                            'closeCaption', 'isDraft'],
                        required: false,
                        where: {isDraft: true},
                    }, {
                        model: models.tags,
                        attributes: ['tag'],
                        required: false,
                        through: {
                            attributes: [],
                        },
                    }],
                }
            );
            expect(newVersion.status).toBe(models.contents.STATUS_ENABLED);
            expect(newVersion.authorFee).toBe(20);
            expect(newVersion.seederFee).toBe(10);
            expect(newVersion.moderationStatus).toBe(models.contents.MODSTATUS_ENABLED);
            expect(newVersion.title).toBe(title);
            expect(newVersion.Tags.length).toBe(0);
            expect(newVersion.media.length).toBe(0);
            expect(newVersion.subtitle.length).toBe(0);
        });

        it(`Must create, update and no auto-accept a version price and other field was changed`, async () => {
            const UUID = '11111112-0002-2bf2-a810-c0f054033a28';
            // Create new version
            const activeVersion = await contentsVersionService.getVersionForUpdate(
                'user1@fakemail.com', UUID, true
            );
            const metadata = [{
                lang: 'en',
                title: 'Fake video content changed',
                body: 'Fake video body changed',
            }];
            const old_title = 'Fake video content';
            await contentsVersionService.updateActiveVersion(
                'user1@fakemail.com',
                UUID,
                {
                    metadata,
                    status: models.contentversion.STATUS_REVIEW,
                    tags: [],
                    authorFee: 20,
                    seederFee: 10,
                    newMediaIds: [],
                    deleteMediaIds: [],
                },
            );
            const newVersion = await models.contents.findOne(
                {
                    where: {uuid: activeVersion.contentUUID},
                    include: [{
                        model: models.media,
                        as: 'media',
                        attributes: ['name', 'type', 'url', 'order', 'isDraft'],
                        required: false,
                        where: {isDraft: true},
                    }, {
                        model: models.subtitle,
                        as: 'subtitle',
                        attributes: [
                            'label', 'lang', 'url',
                            'closeCaption', 'isDraft'],
                        required: false,
                        where: {isDraft: true},
                    }, {
                        model: models.tags,
                        attributes: ['tag'],
                        required: false,
                        through: {
                            attributes: [],
                        },
                    },
                    {
                        model: models.metadata,
                        as: 'metadata',
                    }],
                }
            );
            expect(newVersion.status).toBe(models.contents.STATUS_ENABLED);
            expect(newVersion.authorFee).toBe(60);
            expect(newVersion.seederFee).toBe(43);
            expect(newVersion.moderationStatus).toBe(models.contents.MODSTATUS_ENABLED);
            expect(newVersion.title).toBe(old_title);
            expect(newVersion.Tags.length).toBe(0);
            expect(newVersion.media.length).toBe(0);
            expect(newVersion.subtitle.length).toBe(0);
        });

        it(`Must update historic version and create a new one`, async () => {
            const UUID = '41a0ff76-0002-2bf2-a810-c0f054033a28';
            const existentVersion = await contentsVersionService.getVersionForUpdate(
                'user1@fakemail.com', UUID
            );
            // console.log('EXISTENT %O',existentVersion.media.length);
            const metadata = [{
                lang: 'en',
                title: 'new title test',
                body: 'new body test',
            }];
            await contentsVersionService.updateActiveVersion(
                'user1@fakemail.com',
                UUID,
                {
                    metadata,
                    status: models.contentversion.STATUS_DRAFT,
                    tags: ['one'],
                    newMediaIds: [31, 32],
                    deleteMediaIds: [14, 17],
                    newSubtitleIds: [31, 32],
                    deleteSubtitleIds: [14, 17],
                },
            );
            const oldVersion = await models.contentversion.findOne(
                {
                    where: {uuid: existentVersion.uuid},
                    include: [{
                        model: models.media,
                        as: 'media',
                        attributes: ['name', 'type', 'url', 'order', 'isDraft', 'uuid'],
                        required: false,
                    }, {
                        model: models.subtitle,
                        as: 'subtitle',
                        attributes: [
                            'label', 'lang', 'url',
                            'closeCaption', 'isDraft'],
                        required: false,
                    }, {
                        model: models.tags,
                        attributes: ['tag'],
                        required: false,
                        through: {
                            attributes: [],
                        },
                    }, {
                        model: models.metadata,
                        as: 'metadata',
                    }],
                }
            );
            // console.log('OLD %O', oldVersion);
            const newVersion = await contentsVersionService.getVersionForUpdate(
                'user1@fakemail.com', UUID
            );
            // console.log('NEW %O', newVersion);
            expect(newVersion.status).toBe(models.contentversion.STATUS_DRAFT);
            expect(oldVersion.status).toBe(models.contentversion.STATUS_REQUEST_CHANGES_HISTORIC);
            expect(oldVersion.uuid).not.toBe(newVersion.uuid);
            expect(oldVersion.contentUUID).toBe(newVersion.contentUUID);
            expect(oldVersion.media.length).toBe(existentVersion.media.length);
            expect(oldVersion.subtitle.length).toBe(existentVersion.subtitle.length);
            expect(newVersion.title).toBe(metadata[0].title);
            expect(newVersion.Tags.length).toBe(1);
            expect(newVersion.Tags[0].tag).toBe('one');
            expect(newVersion.media.length).toBe(existentVersion.media.length);
            expect(newVersion.subtitle.length).toBe(existentVersion.subtitle.length);
        });

        it(`Must create and update version without images`, async () => {
            const UUID = '41a0ff76-0001-2bf2-a810-c0f054033a28';
            // Create new version
            const activeVersion = await contentsVersionService.getVersionForUpdate(
                'user1@fakemail.com', UUID, true
            );
            const tempMedia = await contentsService.createTempContentMedia(
                    null,
                    'https://example.com/cover.jpg',
                    'Cover image',
                    1,
                    1,
                    'user1@fakemail.com',
                    activeVersion.uuid
                );
            const metadata = [{
                lang: 'en',
                title: 'new title test',
                body: 'new body test',
            }];
            const actualMedia = activeVersion.media.find((x) => x.type === 1);
            await contentsVersionService.updateActiveVersion(
                'user1@fakemail.com',
                UUID,
                {
                    metadata,
                    status: models.contentversion.STATUS_REVIEW,
                    tags: ['one'],
                },
            );
            const newVersion = await models.contentversion.findOne(
                {
                    where: {uuid: activeVersion.uuid},
                    include: [{
                        model: models.media,
                        as: 'media',
                        attributes: ['name', 'type', 'url', 'order', 'isDraft'],
                        required: false,
                        where: {isDraft: true},
                    }, {
                        model: models.subtitle,
                        as: 'subtitle',
                        attributes: [
                            'label', 'lang', 'url',
                            'closeCaption', 'isDraft'],
                        required: false,
                        where: {isDraft: true},
                    }, {
                        model: models.tags,
                        attributes: ['tag'],
                        required: false,
                        through: {
                            attributes: [],
                        },
                    }, {
                        model: models.metadata,
                        as: 'metadata',
                    }],
                }
            );
            expect(newVersion.status).toBe(models.contentversion.STATUS_REVIEW);
            expect(newVersion.title).toBe(metadata[0].title);
            expect(activeVersion.Tags.length).toBe(2);
            expect(newVersion.Tags.length).toBe(1);
            expect(newVersion.Tags[0].tag).toBe('one');
            expect(newVersion.media.length).toBe(activeVersion.media.length);
            expect(newVersion.media.length).toBe(4);
            expect(newVersion.media[0].isDraft).toBeTruthy();
            expect(newVersion.subtitle.length).toBe(activeVersion.subtitle.length);
            expect(newVersion.subtitle.length).toBe(4);
            expect(newVersion.subtitle[0].isDraft).toBeTruthy();
        });
    });
});

