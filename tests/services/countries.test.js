
import {syncDb, loadData} from '../helpers/db';
import * as countries from '../../src/services/countries';

describe('#list', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadData('00_countries');
        await loadData('10_countries-names-en');
        await loadData('10_countries-names-es');
        done();
    }, 30000);

    test(`Must to get countries`, async () => {
        let res = await countries.list[1]('en');
        expect(Array.isArray(res)).toBeTruthy();
    });
});
