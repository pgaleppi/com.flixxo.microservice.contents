import models from '../models';
import {createDebug} from '../logger';
import axios from 'axios';
import config from '../config';

const SLS_HOST = config.get('serverless:host');
const SLS_CONTENTS = config.get('serverless:contents');

const debug = createDebug(__filename);

/**
 * Fill elastic search contents index from DB
 */
export default async () => {

    try {
        debug(`To find series`);

        const series = await models.serie.scope(
            'enabled',
            'detailed',
            'withSeasons',
        ).findAll({});

        for (const serie of series) {
            try {
                const wrappedSerie = await serie.denormalize();
                let authorId = '';
                try {
                    if (wrappedSerie.authorId) {
                        authorId = wrappedSerie.authorId.toString();
                    }
                } catch (e) {
                    authorId = wrappedSerie.authorId;
                }
                wrappedSerie.authorId = authorId;

                const url = SLS_HOST + SLS_CONTENTS;
                debug(`Calling to Serverless SERIE:
                        ${JSON.stringify(wrappedSerie, null, 4)}`);
                await axios.post(url, wrappedSerie);
            } catch (e) {
                process.stderr.write(
                    `Content ${serie.uuid} ` +
                    `not processed - ${e.message}\n`
                );
            }
        }

        debug(`${series.length} series processed`);
    } catch (e) {
        process.stderr.write(`${e.message}\n`);
    }

    try {

        const contents = await models.contents.scope(
            'enabled',
        ).findAll({
            where: {
                moderationStatus: models.contents.MODSTATUS_ENABLED,
                contentType: 1,
            },
            include: [{
                model: models.media,
                as: 'media',
                attributes: ['name', 'type', 'url', 'sizes'],
                required: false,
            }, {
                model: models.users.scope('minimal'),
                as: 'author',
                required: true,
            }, {
                model: models.categories.scope('public'),
                as: 'category',
                required: true,
            }, {
                model: models.tags,
                attributes: ['tag'],
                required: false,
                through: {
                    attributes: [],
                },
            }],
        });

        for (const content of contents) {
            try {
                const wrappedContent = await content.denormalize();
                const url = SLS_HOST + SLS_CONTENTS;
                debug(`Calling to Serverless VIDEO:
                        ${JSON.stringify(wrappedContent, null, 4)}`);
                await axios.post(url, wrappedContent);
            } catch (e) {
                process.stderr.write(
                    `Content ${content.uuid} ` +
                    `not processed - ${e.message}\n`
                );
            }
        }

        debug(`${contents.length} contents processed`);

    } catch (e) {
        process.stderr.write(`${e.message}\n`);
    }
};
