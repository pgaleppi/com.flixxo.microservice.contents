
import {getServiceConnection} from '../rpclib';
import {getServiceConnection as getLambdaServiceConnection}
    from '../rpclambda';
import config from '../config';
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

const lambdaServices = config.get('microservices') || {};
const inLambda = Object.keys(lambdaServices)
    .map((k) => `com.flixxo.microservice.${k}`);

const loadServices = [];

/**
 * Get the service shortname
 * @param {string} serviceName
 * @return {string}
 */
export function getServiceShortName(serviceName) {
    return serviceName.split('.').pop();
}

/**
 * Resolve service name
 * @param {string} serviceName
 * @return {string}
 */
export function resolveName(serviceName) {
    return serviceName
        .replace(/^@/, 'com.flixxo.microservice.')
    ;
}

/**
 * Load a service
 * @param {string} serviceName
 * @param {object} [context]
 * @return {Promise}
 */
export async function loadService(serviceName, context = null) {
    debug(`Loading service ${serviceName}`);
    serviceName = resolveName(serviceName);

    const loaded = loadServices.find((x) => x.name === serviceName);
    let service;

    if (loaded) {
        debug(`\tService previously loaded, returning`);
        service = loaded.service;
    } else if (inLambda.indexOf(serviceName) !== -1) {
        debug(`\tService ${serviceName} is lambda`);
        service = await getLambdaServiceConnection(serviceName);
    } else {
        debug(`\tService ${serviceName} is not lambda`);
        service = await getServiceConnection(serviceName);
    }

    logger.info(`\tService connection resolved for ${serviceName}`);

    if (!loaded && !context) {
        debug(`\tService has not context, saving in pool`);
        loadServices.push({
            name: serviceName,
            service,
        });
    }

    if (context) {
        debug(`\tBind service`, context);
        service = service.bindContext(context);
    }

    return service;
}
