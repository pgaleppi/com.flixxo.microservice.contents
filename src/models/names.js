import {createDebug} from '../logger';

const debug = createDebug(__filename);

/**
 * NamesModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {

    const instanceMethods = {};

    const NamesModel = sequelize.define('Name', {

        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },

        entityType: {
            type: DataTypes.STRING(20),
            allowNull: false,
        },

        entityId: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
        },

        lang: {
            type: DataTypes.STRING(5),
            allowNull: false,
        },

        value: {
            type: DataTypes.STRING,
            allowNull: true,
        },

    }, {
        name: {
            singular: 'Name',
            plural: 'Names',
        },

        indexes: [{
            name: 'entitiy_translation',
            uniq: true,
            fields: ['entityType', 'entityId', 'lang'],
        }],

        instanceMethods: instanceMethods,
    });

    if (typeof NamesModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                NamesModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    NamesModel.associate = function(models) {

        NamesModel.addScope('byEntityType', (entityType) => {
            return {
                where: {entityType},
            };
        });
    };

    return NamesModel;
};
