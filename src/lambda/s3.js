import Promise from 'bluebird';
import bootstrapServer from '../services';
import {createServer} from '../rpclambda';
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

let server;

/**
 * Get server
 * @return {Promise<RPCServer>}
 */
export async function getServer() {
    if (!server) {
        server = Promise.resolve((async () => {
            const rpcServer = await createServer();
            return bootstrapServer(rpcServer);
        })());
    }

    return server;
}

/**
 * Lambda Handler for S3 Content Availability Update
 * @param {object} event
 * @param {object} context
 * @return {Promise<object>}
 */
export async function contentAvailability(event, context) {
    const services = await getServer();

    context.callbackWaitsForEmptyEventLoop = false;

    try {
        const file = event.Records[0].s3.object.key;
        const hash = file.split('.')[0];

        const response = await services.executeCommand({
            'cmd': 'contents:setS3Availability',
            'params': [hash, true],
        });

        return {
            statusCode: 200,
            body: JSON.stringify(response.data),
        };
    } catch (err) {
        logger.error(
            `Error when calling service: ${err.message} (${err.name})`
        );
        debug('body:', event.body);
        debug(err);
    }
}

