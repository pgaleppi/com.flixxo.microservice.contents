
import sequelizeFixtures from 'sequelize-fixtures';
import models from '../models';
import {nsWrapper} from '../logger/context';
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

/**
 * Lambda raw importer
 * @param {object} event
 * @param {object} context
 * @return {Promise<object>}
 */
export default async (event, context) => nsWrapper(async (callContext) => {
    let response;
    const {awsRequestId} = context;
    let body;

    context.callbackWaitsForEmptyEventLoop = true;
    callContext.addPrefix('RequestId', awsRequestId);

    try {
        debug(`Parsing body`);
        body = JSON.parse(event.body);
        if (!Array.isArray(body)) {
            throw new Error(`Input data must to be an array`);
        }
    } catch (e) {
        debug(`Data not parsed `, e);
        return {
            statusCode: 400,
            body: {
                type: `Error parsing input data ${e.name}`,
                message: e.message,
            },
        };
    }

    try {
        debug(`Importing data`);
        await models.sequelize.transaction(
            (transaction) => sequelizeFixtures.loadFixtures(
                body,
                models,
                {transaction}
            )
        )
            .catch((e) => {
                debug(`Transaction rollback ${e}`);
                throw e;
            });

        return {
            statusCode: 200,
            body: JSON.stringify(response.data),
        };

    } catch (err) {
        debug('Error [body]:', event.body);
        logger.error(
            'Error importing raw data ' +
            err.message +
            ` (${err.name})`
        );
        debug(err);

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: err.message,
                name: err.name,
                stack: err.stack,
            }),
        };
    }
});
