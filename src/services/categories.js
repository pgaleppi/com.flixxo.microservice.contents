
import models from '../models';
import {createDebug} from '../logger';

const debug = createDebug(__filename);

/**
 * List categories
 * @return {Promise<Array<object>>}
 */
export async function list() {
    debug('get categories');
    const categories = await models.categories.findAll({
        attributes: ['id', 'name', 'thumb'],
    });
    return categories.map((c) => c.get({plain: true}));
}

/**
 * Get a category
 * @param {integer} id Category id
 * @return {Promise<object>}
 */
export async function get(id) {
    debug(`Get a category by id ${id}`);
    const category = await models.categories.findByPk(id, {
        attributes: ['id', 'name', 'thumb'],
        rejectOnEmpty: true,
    });

    return category.get({plain: true});
}
