import Promise from 'bluebird';
import Redis from 'ioredis-mock';

Redis.Promise = Promise;

let connection;

/**
 * Get redis connection
 * @return {Redis}
 */
export function getConnection() {
    if (!connection) {
        connection = new Redis();

        connection.__rebuild = function(...params) {
            const rb = connection.__rebuild;
            connection = new Redis(...params);
            connection.__rebuild = rb;
        };

    }

    return connection;
}
