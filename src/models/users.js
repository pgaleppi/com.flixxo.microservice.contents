import {compareLower} from '../storage';
import {Op} from 'sequelize';
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);
const ADS_METER_MIN_ERROR = 'Min validation failed';

/**
 * UsersModel factory
 * @param {sequelize} sequelize
 * @param {sequelize.DataTypes} DataTypes
 * @return {sequelize.model}
 */
export default (sequelize, DataTypes) => {
    let ProfileModel;
    let ContentModel;
    let UserWatchedAd;
    let AdvertisementsModel;
    let FollowersModel;
    let CategoryModel;
    let MettersModel;

    const instanceMethods = {

        /**
         * Returns watched ads count
         * @param {Date} dateSince
         * @return {integer}
         */
        async getWatchedAdsCount(dateSince) {
            debug(`Get watched ads count for ${this.id}`);
            const now = new Date();
            debug(`Date since ${dateSince} - Now ${now}`);
            const watchedAdsCount = await UserWatchedAd.scope('historical')
                .count({
                    where: {
                        'userId': this.id,
                        'createdAt': {between: [dateSince, now]},
                    },
                });
            return watchedAdsCount;
        },

        /**
         * Get profile and create if does not exsits
         * @return {ProfileModel}
         */
        async getOrCreateProfile() {
            if (!this.profile) {
                debug(`Profile not loaded, getting`);
                this.profile = await this.getProfile();

                if (!this.profile) {
                    debug(`Profile does not defined, creating`);
                    this.profile = await ProfileModel.create({
                        userId: this.id,
                    });
                }
            }

            return this.profile;
        },

        /**
         * Save profile
         * @param {object} data
         * @param {object} [options]
         * @return {Promise}
         */
        async saveProfile(data, options = {}) {
            debug(`Saving profile of ${this.email}`);
            const profile = await this.getOrCreateProfile();
            await profile.update(data, options);
            return profile;
        },

        /**
         * Cancel user creation, for remote implementation
         * @param {bool} [force=false] Force, dont check cration time
         * @return {Promise}
         */
        async cancel(force = false) {
            const now = Date.now();
            const created = this.createdAt.getTime();
            const delta = now - created;
            if (!force && delta > 60000) {
                logger.error(
                    `User ${this.email} not canceled because 1 minute pass ` +
                    `from creation`
                );
                throw new Error(`User created more than 1 minute`);
            }
            return this.destroy({
                force: true,
                onDelete: 'CASCADE',
            });
        },

        /**
         * Get AdMetter
         * @param {Sequelize.Transaction} [dbTx]
         * @return {MettersModel}
         */
        async resolveAdMetter(dbTx = null) {
            debug(`Getting AdMetter of ${this.email}`);
            if (this.AdMetter) {
                return this.AdMetter;
            }

            return MettersModel.findOrCreate({
                where: {
                    userUUID: this.uuid,
                    type: MettersModel.TYPE_USER,
                },
                ...(dbTx ? {
                    transaction: dbTx,
                    lock: dbTx.LOCK.UPDATE,
                } : {}),
            })
                .then(([model]) => model);
        },

        /**
         * Get Provider AdMetter
         * @param {Sequelize.Transaction} [dbTx]
         * @return {MettersModel}
         */
        async resolveProviderAdMetter(dbTx = null) {
            debug(`Getting Provider AdMetter of ${this.email}`);
            if (this.AdMetter) {
                return this.AdProviderMetter;
            }

            return MettersModel.findOrCreate({
                where: {
                    userUUID: this.uuid,
                    type: MettersModel.TYPE_PROVIDER,
                },
                ...(dbTx ? {transaction: dbTx} : {}),
            })
                .then(([model]) => model);
        },

    };

    const UsersModel = sequelize.define('User', {

        uuid: {
            type: DataTypes.UUID,
            allowNull: false,
            unique: true,
            index: true,
        },

        id: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },

        email: {
            type: DataTypes.STRING(1024),
            allowNull: false,
            index: true,
            set(val) {
                this.setDataValue('email', val.toLowerCase());
            },
            validate: {
                isEmail: true,
            },
        },

        nickname: {
            type: DataTypes.STRING(1024),
            allowNull: false,
        },

        mobileNumber: {
            type: DataTypes.STRING(32),
            allowNull: true,
            unique: true,
        },

        enabled: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },

        advertisementsMeter: {
            type: DataTypes.DECIMAL(36, 18),
            defaultValue: 0,
            validate: {
                min: {args: [0], msg: ADS_METER_MIN_ERROR},
            },
        },

        displayName: {
            type: DataTypes.VIRTUAL(
                DataTypes.STRING(1024),
                ['nickname']
            ),
            get() {
                if (this.Profile && this.Profile.realName) {
                    return this.Profile.realName;
                } else {
                    return this.getDataValue('nickname');
                }
            },
        },

    }, {
        defaultScope: {
            where: {
                enabled: true,
            },
        },

        indexes: [
            {
                name: 'unique_nickname',
                unique: true,
                fields: [sequelize.fn('lower', sequelize.col('nickname'))],
            },
            {
                name: 'unique_email',
                unique: true,
                fields: [sequelize.fn('lower', sequelize.col('email'))],
            },
        ],

        instanceMethods: instanceMethods,
    });

    if (typeof UsersModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                UsersModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    UsersModel.associate = function(models, db) {
        ProfileModel = models.profile;
        ContentModel = models.contents;
        UserWatchedAd = models.userad;
        AdvertisementsModel = models.advertisements;
        FollowersModel = models.followers;
        CategoryModel = models.categories;
        MettersModel = models.admetters;

        UsersModel.hasMany(AdvertisementsModel, {
            as: 'userAd',
            foreignKey: 'uuid',
            targetKey: 'userId',
            onDelete: 'CASCADE',
        });

        UsersModel.hasMany(UserWatchedAd, {
            as: 'userWatchedAd',
            foreignKey: 'userId',
            targetKey: 'id',
            onDelete: 'CASCADE',
        });

        UsersModel.hasOne(ProfileModel, {
            as: 'Profile',
            foreignKey: 'userId',
            targetKey: 'id',
            onDelete: 'CASCADE',
        });

        UsersModel.hasOne(MettersModel, {
            as: 'AdMetter',
            foreignKey: 'userUUID',
            sourceKey: 'uuid',
            scope: {
                type: MettersModel.TYPE_USER,
            },
            onDelete: 'CASCADE',
        });

        UsersModel.hasOne(MettersModel, {
            as: 'AdProviderMetter',
            foreignKey: 'userUUID',
            sourceKey: 'uuid',
            scope: {
                type: MettersModel.TYPE_PROVIDER,
            },
            onDelete: 'CASCADE',
        });

        UsersModel.belongsToMany(CategoryModel, {
            as: 'followedCategories',
            through: 'UserCategories',
            foreignKey: {
                name: 'userId',
                allowNull: false,
            },
            otherKey: {
                name: 'categoryId',
                allowNull: false,
            },
            constraints: true,
        });

        UsersModel.hasMany(ContentModel, {
            as: 'contentModerator',
            foreignKey: 'id',
            targetKey: 'moderatorId',
            constraints: true,
        });

        UsersModel.belongsToMany(UsersModel, {
            as: {
                plural: 'Followers',
                singular: 'Follower',
            },
            foreignKey: 'followedId',
            otherKey: {
                name: 'followerId',
                unique: false,
            },
            unique: false,
            through: {
                model: FollowersModel,
                unique: false,
            },
        });

        UsersModel.belongsToMany(UsersModel, {
            as: {
                singular: 'Followed',
                plural: 'Followeds',
            },
            foreignKey: 'followerId',
            otherKey: {
                name: 'followedId',
                unique: false,
            },
            unique: false,
            through: {
                model: FollowersModel,
                unique: false,
            },
        });

        // Scopes!!

        UsersModel.addScope('profile', {
            attributes: [
                'id',
                'uuid',
                'nickname',
                'mobileNumber',
                'displayName',
            ],
            include: [{
                model: ProfileModel,
                as: 'Profile',
                attributes: [
                    'realName',
                    'gender',
                    'lang',
                    'avatar',
                ],
                required: false,
            }],
        });

        UsersModel.addScope('withAdMetter', {
            include: [{
                model: MettersModel,
                as: 'AdMetter',
                required: false,
            }],
        });

        UsersModel.addScope('minimal', {
            attributes: [
                'id',
                'displayName',
            ],
            include: [{
                model: ProfileModel,
                as: 'Profile',
                attributes: [
                    'realName',
                ],
                required: false,
            }],
        });

        UsersModel.addScope('information', {
            attributes: [
                'id',
                'email',
                'nickname',
                'createdAt',
                'updatedAt',
            ],
        });

        UsersModel.addScope('minimalProfile', {
            attributes: [
                'id',
                'uuid',
                'nickname',
                'displayName',
            ],
            include: [{
                model: ProfileModel,
                as: 'Profile',
                attributes: [
                    'avatar',
                ],
                required: false,
            }],
        });
        /*
         *
            as: 'followedCategories',
            through: 'UserCategories',
            foreignKey: {
                name: 'userId',
                allowNull: false,
            },
            otherKey: {
                name: 'categoryId',
                allowNull: false,
            },
            constraints: true,
            */
        UsersModel.addScope('toExport', {
            paranoid: false,
            attributes: {
                exclude: ['displayName'],
            },
            include: [{
                model: ProfileModel,
                as: 'Profile',
                required: false,
            }, {
                model: CategoryModel,
                as: 'followedCategories',
                attributes: ['id'],
                through: {
                    attributes: [],
                },
                required: false,
            }],
        });

        UsersModel.addScope('withFollowersMetters', () => {

            const followers = db.dialect.QueryGenerator.selectQuery(
                models.followers.tableName,
                {
                    attributes: [
                        [sequelize.fn('count', 1), '_fol'],
                    ],
                    where: sequelize.where(
                        sequelize.col('followedId'),
                        '=',
                        sequelize.col('User.id'),
                    ),
                }
            ).slice(0, -1);

            const followeds = db.dialect.QueryGenerator.selectQuery(
                models.followers.tableName,
                {
                    attributes: [
                        [sequelize.fn('count', 1), '_fod'],
                    ],
                    where: sequelize.where(
                        sequelize.col('followerId'),
                        '=',
                        sequelize.col('User.id'),
                    ),
                }
            ).slice(0, -1);

            return {
                attributes: [
                    [sequelize.literal(`(${followers})`), 'followers'],
                    [sequelize.literal(`(${followeds})`), 'following'],
                ],
            };
        });

        UsersModel.addScope('isFollowing', (id) => {

            const isFollowing = db.dialect.QueryGenerator.selectQuery(
                models.followers.tableName,
                {
                    attributes: [
                        [sequelize.fn('count', 1), '_isfol'],
                    ],
                    where: {
                        [Op.and]: [
                            sequelize.where(
                                sequelize.col('followerId'),
                                '=',
                                sequelize.col('User.id'),
                            ),
                            sequelize.where(
                                sequelize.col('followedId'),
                                '=',
                                id,
                            ),
                        ],
                    },
                }
            ).slice(0, -1);

            const isFollower = db.dialect.QueryGenerator.selectQuery(
                models.followers.tableName,
                {
                    attributes: [
                        [sequelize.fn('count', 1), '_isfod'],
                    ],
                    where: {
                        [Op.and]: [
                            sequelize.where(
                                sequelize.col('followerId'),
                                '=',
                                id,
                            ),
                            sequelize.where(
                                sequelize.col('followedId'),
                                '=',
                                sequelize.col('User.id'),
                            ),
                        ],
                    },
                }
            ).slice(0, -1);

            return {
                attributes: [
                    [sequelize.literal(`(${isFollowing})`), 'isFollowing'],
                    [sequelize.literal(`(${isFollower})`), 'isFollower'],
                ],
            };

        });

        UsersModel.addScope('withContentMetters', () => {

            const contents = db.dialect.QueryGenerator.selectQuery(
                models.contents.tableName,
                {
                    attributes: [
                        [sequelize.fn('count', 1), '_comt'],
                    ],
                    where: [
                        sequelize.where(
                            sequelize.col('authorId'),
                            '=',
                            sequelize.col('User.id'),
                        ),
                        ContentModel.enabledWhere(),
                    ],
                }
            ).slice(0, -1);

            return {
                attributes: [
                    [sequelize.literal(`(${contents})`), 'numContents'],
                ],
            };
        });

        UsersModel.addScope('txInfo', {
            attributes: [
                'uuid',
                'displayName',
                'nickname',
            ],
            include: [{
                model: ProfileModel,
                as: 'Profile',
                attributes: [
                    'realName',
                    'avatar',
                ],
            }],
        });
    };

    /**
     * Find user by email
     * @param {string} email
     * @param {boolean} [rejectOnEmpty=true]
     * @return {Promise<UsersModel|null>}
     */
    UsersModel.findByEmail = async function(email, rejectOnEmpty = true) {
        debug(`Finding user ${email}`);
        const user = await this.findOne({
            where: compareLower('email', email),
            rejectOnEmpty,
        });

        return user;
    };

    UsersModel.ADS_METER_MIN_ERROR = ADS_METER_MIN_ERROR;

    return UsersModel;
};
