import Promise from 'bluebird';
import {v4 as uuid} from 'uuid';
import {createDebug} from '../logger';
import config from '../config';
import axios from 'axios';
import {RPCRemoteError} from './errors';

const debug = createDebug(__filename);
const HTTP_DEFAULT_TIMEOUT = 30000;

/**
 * Remote command interface
 */
export default class Command {

    /**
     * Create new remote service client
     * @param {amqp.connection} connector
     * @param {string} service Service id, like com.flixxo.microservice.xxx
     * @param {object|Promise|function} [context] Context for instance
     */
    constructor(connector, service, context = {}) {
        this._service = service;
        this._context = typeof context === 'function' ? context() : context;
        const serviceName = service.split('.').pop();
        debug(config.get(`microservices:${serviceName}:endpoint`));
        this._instance = axios.create({
            baseURL: config.get(`microservices:${serviceName}:endpoint`),
            timeout: HTTP_DEFAULT_TIMEOUT,
        });

        debug('baseURL is:' +
            config.get(`microservices:${serviceName}:endpoint`));
    }

    /**
     * Get context
     * @return {Promise<object>}
     */
    async getContext() {
        const context = await Promise.resolve(this._context);
        this._context = context;
        return context;
    }

    /**
     * Set context
     * @param {object|Pomise} context
     */
    setConext(context) {
        this._context = context;
    }

    /**
     * Get new instance of Command with new context
     * @param {object|Promise|function} context
     * @return {Command}
     */
    bindContext(context = {}) {
        return new Command(null, this._service, context);
    }

    /**
     * Reject active command
     * @param {string} uid
     * @param {Error} error
     */
    rejectCommand(uid, error) {
    }

    /**
     * Execute remote command
     * @param {string} cmd Command
     * @param {Array<mixed>} [...params] Params
     * @return {Promise<mixed|Error>}
     */
    async execute(cmd, ...params) {
        const uid = uuid();
        debug(`execute ${this._service}::${cmd} as ${uid}`);

        const context = await this.getContext();

        const pack = {
            uid,
            cmd,
            params,
            context,
        };

        const result = await this._instance.post('/', pack);
        debug(`${uid}: result`, result.data);
        if (result.data.error) {
            debug(`${uid}: rejected`, result.data.error);
            throw new RPCRemoteError(result.data.error);
        }

        return result.data.result;
    }
}
