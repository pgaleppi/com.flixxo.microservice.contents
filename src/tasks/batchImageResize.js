
import Promise from 'bluebird';
import models from '../models';
import createLogger, {createDebug} from '../logger';

const logger = createLogger(__filename);
const debug = createDebug(__filename);

/**
 * Batch Image Resize
 */
export default async ({force = false, limit = 1000}) => {
    try {
        const mediaFilter = {};
        if (!force) {
            mediaFilter.where = {processed: false};
        }

        const contentMedia = await models.media.findAll({
            attributes: ['url'],
            ...mediaFilter,
        });

        if (contentMedia.length) {
            await Promise.map(contentMedia, (media) => media.resize());
        } else {
            logger.info(`There is no media to process`);
        }

        logger.info(`${contentMedia.length} send to resize`);
    } catch (e) {
        logger.error(
            `Error processing content media files ` +
            `${e.message} (${e.name})`
        );
        debug(e);
    }
};
