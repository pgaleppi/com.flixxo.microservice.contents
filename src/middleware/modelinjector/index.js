
import {createDebug} from '../../logger';
import models from '../../models';

const debug = createDebug(__filename);

/**
 * ModelInjector
 */
export default class ModelInjector {

    /**
     * Apply as middlware
     * @return {array}
     */
    applyMiddleware() {
        return ['models', 'context', async () => {
            debug(`Inject models`);
            return models;
        }];
    }
}

