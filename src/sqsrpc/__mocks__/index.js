import Promise from 'bluebird';
import {createDebug} from '../../logger';

const debug = createDebug(__filename);

let fakeMethods = [];

/**
 * Service instance
 */
export class Service {

    /**
     * Create new service instance
     * @param {string} name - Get config from aws.sqs.services.[name]
     */
    constructor(name) {
        debug(`Creating service for ${name}`);
        this.__reset();
        this._name = name;
    }

    /**
     * Reset mock
     */
    __reset() {
        this._calls = [];
        this._error = null;
    }

    /**
     * Set error to thorw in the next call
     * @param {Error} err
     */
    __setError(err) {
        this._error = err;
    }

    /**
     * Send RPC command to the pool
     * @param {string} cmd
     * @param {any[]} [...params]
     */
    async exec(cmd, ...params) {
        debug(`Exec fake method for ${this._name}/${cmd}`);

        const meth = fakeMethods.find(
            (x) => x.service === this._name && x.name === cmd);

        if (meth) {
            try {
                await Promise.resolve(meth.callback(...params))
                    .then((result) => {
                        debug(`${cmd} resolve`);
                        if (meth.onSuccess) {
                            meth.onSuccess(result, this);
                        }
                        return;
                    });
            } catch (error) {
                debug(`${cmd} reject: `, error.message);

                if (meth.onError) {
                    meth.onError(error, this);
                }

                return;
            }
        } else {
            debug(`\tNo method faked, wait for a timeout`);
        }

        this._calls.push({cmd, params});
    }

}

export const getServiceConnection = (() => {
    const services = {};

    return function(name) {
        if (!services[name]) {
            services[name] = new Service(name);
        }

        return services[name];
    };

})();

/**
 * Define a fake response to a service/method call
 * @param {string} service
 * @param {string} name
 * @param {function} callback
 * @param {object} [options]
 */
export function fakeResponse(service, name, callback, options = {}) {
    fakeMethods.push({
        ...options,
        service,
        name,
        callback,
    });
}

/**
 * Clear fake methods definitions
 */
export function clear() {
    fakeMethods = [];
}
