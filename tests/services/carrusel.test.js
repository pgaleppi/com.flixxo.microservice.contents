
import {syncDb, loadFixture, loadData} from '../helpers/db';
import {EmptyResultError} from 'sequelize';
import models from '../../src/models';
import {ValidationError} from '../../src/errors';
import {matchers} from 'jest-json-schema';
import * as carrusels from '../../src/services/carrusels';

expect.extend(matchers);

const CARRUSEL_LISTING_BODY = {
    type: 'object',
    properties: {
        uuid: {
            type: 'string',
            format: 'uuid',
        },
        name: {
            type: 'string',
        },
        title: {
            type: 'string',
        },
        groupId: {
            type: ['string', 'null'],
            format: 'uuid',
        },
    },
    additionalProperties: false,
    patternRequired: ['.*'],
};

const CARRUSEL_LISTING_ADMIN_SCHEMA_BODY = {
    ...CARRUSEL_LISTING_BODY,
    properties: {
        ...CARRUSEL_LISTING_BODY.properties,
        enabled: {
            type: 'boolean',
        },
        priority: {
            type: 'integer',
        },
        groupId: {
            type: ['string', 'null'],
            format: 'uuid',
        },
        createdAt: {
            format: 'date-time',
        },
        updatedAt: {
            format: 'date-time',
        },
    },
};

const CARRUSEL_ADMIN_DETAILLED_SCHEMA = {
    ...CARRUSEL_LISTING_BODY,
    properties: {
        ...CARRUSEL_LISTING_BODY.properties,
        enabled: {
            type: 'boolean',
        },
        priority: {
            type: 'integer',
        },
        groupId: {
            type: ['string', 'null'],
            format: 'uuid',
        },
        createdAt: {
            format: 'date-time',
        },
        updatedAt: {
            format: 'date-time',
        },
        Metadata: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    lang: {
                        type: 'string',
                    },
                    title: {
                        type: 'string',
                    },
                    body: {
                        type: 'string',
                    },
                },
                patternRequired: ['.*'],
            },
        }
    },
};

const CARRUSEL_LISTING_SCHEMA = {
    type: 'array',
    items: {
        ...CARRUSEL_LISTING_BODY,
    },
};

const CARRUSEL_LISTING_ADMIN_SCHEMA = {
    type: 'array',
    items: {
        ...CARRUSEL_LISTING_ADMIN_SCHEMA_BODY,
    },
};

const CARRUSEL_DETAILLED_BODY = {
    ...CARRUSEL_LISTING_BODY,
    properties: {
        ...CARRUSEL_LISTING_BODY.properties,
        body: {
            type: 'string',
        },
    },
};

describe('#list', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadFixture('carrusel-for-list-service');
        done();
    }, 30000);

    test(`Must to list enabled carrusels in english`, async () => {
        const res = await carrusels.list[1]('en');
        const ores = res.map((o) => o.toJSON());
        expect(Array.isArray(ores)).toBeTruthy();
        expect(ores).toMatchSchema(CARRUSEL_LISTING_SCHEMA);
        expect(ores).toEqual([{
            name: 'test2',
            title: 'Eng title 2',
            uuid: 'e312b57e-7cd5-41f0-a545-41d890e09f74',
            groupId: null,
        }, {
            name: 'test',
            title: 'Eng title',
            uuid: '862924dc-1553-463f-b5e5-a23cf7487f65',
            groupId: '862924dc-1553-463f-b6e5-a24af5591f13',
        }]);
    });

    test(`Must to list enabled carrusels in spanish by groupId`, async () => {
        const res = await carrusels.list[1]('es', '862924dc-1553-463f-b6e5-a24af5591f13');
        const ores = res.map((o) => o.toJSON());
        expect(Array.isArray(ores)).toBeTruthy();
        expect(ores).toMatchSchema(CARRUSEL_LISTING_SCHEMA);
        expect(ores).toEqual([{
            name: 'test',
            title: 'Titulo es',
            uuid: '862924dc-1553-463f-b5e5-a23cf7487f65',
            groupId: '862924dc-1553-463f-b6e5-a24af5591f13',
        }]);
    });

    test(`Must to list enabled carrusels in default lang`, async () => {
        const res = await carrusels.list[1]();
        const ores = res.map((o) => o.toJSON());
        expect(Array.isArray(ores)).toBeTruthy();

        expect(ores).toEqual([{
            name: 'test2',
            title: 'Eng title 2',
            uuid: 'e312b57e-7cd5-41f0-a545-41d890e09f74',
            groupId: null,
        }, {
            name: 'test',
            title: 'Eng title',
            uuid: '862924dc-1553-463f-b5e5-a23cf7487f65',
            groupId: '862924dc-1553-463f-b6e5-a24af5591f13',
        }]);
    });

    test(`Must to list enabled carrusels in other lang`, async () => {
        const res = await carrusels.list[1]('es');
        const ores = res.map((o) => o.toJSON());
        expect(Array.isArray(ores)).toBeTruthy();

        expect(ores).toEqual([{
            name: 'test2',
            title: 'Titulo es 2',
            uuid: 'e312b57e-7cd5-41f0-a545-41d890e09f74',
            groupId: null,
        }, {
            name: 'test',
            title: 'Titulo es',
            uuid: '862924dc-1553-463f-b5e5-a23cf7487f65',
            groupId: '862924dc-1553-463f-b6e5-a24af5591f13',
        }]);
    });

    test(`Must to list enabled carrusels in other lang not common`, async () => {
        const res = await carrusels.list[1]('pt');
        const ores = res.map((o) => o.toJSON());
        expect(Array.isArray(ores)).toBeTruthy();

        expect(ores).toEqual([{
            name: 'test2',
            title: 'Eng title 2',
            uuid: 'e312b57e-7cd5-41f0-a545-41d890e09f74',
            groupId: null,
        }, {
            name: 'test',
            title: 'Titulo em pt',
            uuid: '862924dc-1553-463f-b5e5-a23cf7487f65',
            groupId: '862924dc-1553-463f-b6e5-a24af5591f13',
        }]);
    });
});

describe('#listAll', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadFixture('carrusel-for-list-service');
        done();
    }, 30000);

    test(`Must to list enabled carrusels in english`, async () => {
        const res = await carrusels.listAll[1]('en');
        const ores = res.map((o) => o.toJSON());
        expect(Array.isArray(ores)).toBeTruthy();
        expect(ores).toMatchSchema(CARRUSEL_LISTING_ADMIN_SCHEMA);
        expect(ores.map((o) => o.title)).toEqual([
            'Eng title',
            'Eng title 2',
            'Disabled title',
        ]);
    });
});

describe('#get', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadFixture('carrusel-for-list-service');
        done();
    }, 30000);

    test(`Must to get enabled carrusel in english`, async () => {
        const res = await carrusels.get[1](
            'en',
            '862924dc-1553-463f-b5e5-a23cf7487f65'
        )
        ;
        const ores = res.toJSON();
        expect(ores).toMatchSchema(CARRUSEL_DETAILLED_BODY);
        expect(ores).toEqual({
            name: 'test',
            title: 'Eng title',
            body: 'Eng body',
            uuid: '862924dc-1553-463f-b5e5-a23cf7487f65',
        });
    });

    test(`Must to get enabled carrusel in espanish`, async () => {
        const res = await carrusels.get[1](
            'es',
            '862924dc-1553-463f-b5e5-a23cf7487f65'
        )
        ;
        const ores = res.toJSON();
        expect(ores).toMatchSchema(CARRUSEL_DETAILLED_BODY);
        expect(ores).toEqual({
            name: 'test',
            title: 'Titulo es',
            body: 'Cuerpo es',
            uuid: '862924dc-1553-463f-b5e5-a23cf7487f65',
        });
    });

    test(`Must to throw EmptyResultError on non-existing`, async () => {
        try {
            await carrusels.get[1](
                'en',
                '862924dc-1553-463f-b5e5-a23cf7487f60'
            );
        } catch (e) {
            expect(e).toBeInstanceOf(EmptyResultError);
            return;
        }

        throw new Error('Pass');
    });

    test(`Must to throw EmptyResultError on disabled`, async () => {
        try {
            await carrusels.get[1](
                'en',
                '789f422f-551f-42e7-bc0c-54cd8bc882da',
            );
        } catch (e) {
            expect(e).toBeInstanceOf(EmptyResultError);
            return;
        }

        throw new Error('Pass');
    });
});

describe('#getComplete', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadFixture('carrusel-for-list-service');
        done();
    }, 30000);

    test(`Must to get enabled carrusel`, async () => {
        const res = await carrusels.getComplete(
            '862924dc-1553-463f-b5e5-a23cf7487f65'
        )
        ;
        const ores = res.toJSON();
        expect(ores).toMatchSchema(CARRUSEL_ADMIN_DETAILLED_SCHEMA);
    });

    test(`Must to get disabled carrusel`, async () => {
        const res = await carrusels.getComplete(
            '789f422f-551f-42e7-bc0c-54cd8bc882da',
        )
        ;
        const ores = res.toJSON();
        expect(ores).toMatchSchema(CARRUSEL_ADMIN_DETAILLED_SCHEMA);
    });

    test(`Must to throw EmptyResultError on non-existing`, async () => {
        try {
            await carrusels.getComplete(
                '862924dc-1553-463f-b5e5-a23cf7487f60'
            );
        } catch (e) {
            expect(e).toBeInstanceOf(EmptyResultError);
            return;
        }

        throw new Error('Pass');
    });
});

describe('#assignContent', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadData('categories');
        await loadFixture('users');
        await loadFixture('carrusel-for-list-service');
        await loadFixture('contents-for-carrusel-assign');
        done();
    }, 30000);

    test('Must to add valid content to valid carrusel', async () => {
        const res = await carrusels.assignContent(
            '862924dc-1553-463f-b5e5-a23cf7487f65',
            '3000000-c3d1-0000-0000-000000000001'
        );
        expect(res).toBeTruthy();
    });

    test('Must to add valid content to valid carrusel with order', async () => {
        const res = await carrusels.assignContent(
            '862924dc-1553-463f-b5e5-a23cf7487f65',
            '3000000-c3d1-0000-0000-000000000001',
            3
        );
        expect(res).toBeTruthy();

        const order = await models.carruselitem.findOne({
            where: {
                carruselUUID: '862924dc-1553-463f-b5e5-a23cf7487f65',
                entityType: models.carruselitem.TYPE_VIDEO,
                entityId: '3000000-c3d1-0000-0000-000000000001',
            },
            attributes: ['order'],
            raw: true,
        })
            .then((x) => x.order);

        expect(order).toBe(3);
    });

    test('Must to get NotFoundError on not-existing carrusel ', async () => {
        try {
            await carrusels.assignContent(
                '862924dc-1553-463f-b5e5-a23cf7487f64',
                '3000000-c3d1-0000-0000-000000000001'
            );
        } catch (e) {
            expect(e).toBeInstanceOf(EmptyResultError);
            return;
        }

        throw new Error('Pass');
    });

    test('Must to get ValidationError on not-existing content', async () => {
        try {
            await carrusels.assignContent(
                '862924dc-1553-463f-b5e5-a23cf7487f65',
                '3000000-c3d1-0000-0000-00000000000f'
            );
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.message).toBe('Unknow content 3000000-c3d1-0000-0000-00000000000f');
            expect(e.errors).toHaveLength(1);
            expect(e.errors[0].field).toBe('contentUUID');
            return;
        }

        throw new Error('Pass');
    });

    test('Must to get ValidationError on content with type serie', async () => {
        try {
            await carrusels.assignContent(
                '862924dc-1553-463f-b5e5-a23cf7487f65',
                '3000000-c3d1-0000-0000-000000000002'
            );
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError);
            expect(e.message).toBe('Unknow content 3000000-c3d1-0000-0000-000000000002');
            expect(e.errors).toHaveLength(1);
            expect(e.errors[0].field).toBe('contentUUID');
            return;
        }

        throw new Error('Pass');
    });

});

describe('#getContents', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadData('categories');
        await loadFixture('users');
        await loadFixture('carrusel-for-list-service');
        await loadFixture('contents-for-carrusel-getcontents');
        done();
    }, 30000);

    test('Must to list carrusel contents', async () => {
        const {contents} = await carrusels.getContents[3](
            'es',
            {country: 'ar'},
            '862924dc-1553-463f-b5e5-a23cf7487f65',
        );

        const ids = contents.map((r) => r.uuid);

        expect(ids[0]).toBe('3000000-c3d1-0000-0000-000000000003');
        expect(ids[1]).toBe('3000000-c3d1-0000-0000-000000000001');
        expect(ids[2]).toBe('3000000-c3d1-0000-0000-000000000002');
    });

    test('Must to reject if carrousel does not exists', async () => {
        try {
            await carrusels.getContents[3](
                'es',
                {country: 'ar'},
                '762924dc-1553-463f-b5e5-a23cf7487f65',
            );
        } catch (e) {
            expect(e).toBeInstanceOf(EmptyResultError);
            return;
        }

        throw new Error('Pass');
    });
});

describe('#getSeries', () => {
    beforeEach(async (done) => {
        await syncDb(true);
        await loadData('categories');
        await loadFixture('users');
        await loadFixture('carrusel-for-list-service');
        await loadFixture('series-for-carrusel-getseries');
        done();
    }, 30000);

    test('Must to list carrusel contents', async () => {
        const {contents} = await carrusels.getSeries[3](
            'es',
            {country: 'ar'},
            '862924dc-1553-463f-b5e5-a23cf7487f65',
        );

        const ids = contents.map((r) => r.uuid);

        expect(ids[0]).toBe('0000000-0000-0000-0000-000000000002');
        expect(ids[1]).toBe('0000000-0000-0000-0000-000000000003');
        expect(ids[2]).toBe('0000000-0000-0000-0000-000000000001');
    });

    test('Must to reject if carrousel does not exists', async () => {
        try {
            await carrusels.getSeries[3](
                'es',
                {country: 'ar'},
                '762924dc-1553-463f-b5e5-a23cf7487f65',
            );
        } catch (e) {
            expect(e).toBeInstanceOf(EmptyResultError);
            return;
        }

        throw new Error('Pass');
    });
});
