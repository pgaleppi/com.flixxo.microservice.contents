import path from 'path';
import {getServiceConnection} from '../sqsrpc';
import {getMediaConfig} from '../services/images';
import {createDebug} from '../logger';
import Promise from 'bluebird';
import Sequelize from 'sequelize';
import config from '../config';

const debug = createDebug(__filename);

const TYPE_COVER = 0x01;
const TYPE_THUMB = 0x02;
const TYPE_AVATAR = 0x03;

const BUCKET_NAME = config.get('aws:s3:bucket');
const BUCKET_HOST = config.get('aws:s3:host');
const MEDIA_SIZES = config.get('media:sizes') || {};

export default (sequelize, DataTypes) => {

    let ContentModel;
    const instanceMethods = {

        /**
         * Send SQS command to resize image
         * @param {bool} [force=false]
         * @return {Promise<void>}
         */
        async resize(force = false) {
            debug(`Send to resize media`);

            if (!force && this.processed) {
                throw new Error('Media is already processed');
            }

            debug(`Send SQS command to resize`);
            const contentsQueue = getServiceConnection('contents');
            await contentsQueue.exec(
                'images:resize',
                this.url.replace(
                    BUCKET_HOST +
                    BUCKET_NAME +
                    '/', ''),
            );
        },
    };

    const MediaModel = sequelize.define('Media', {

        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },

        type: {
            type: DataTypes.INTEGER.UNSIGNED,
            defaultValue: TYPE_THUMB,
            allowNull: false,
            validate: {
                isIn: [[
                    TYPE_COVER,
                    TYPE_THUMB,
                    TYPE_AVATAR,
                ]],
            },
        },

        name: {
            type: DataTypes.STRING(2014),
            allowNull: true,
            validate: {
                is: /^.{3,}$/,
            },
        },

        url: {
            type: DataTypes.STRING(1024),
            allowNull: false,
            index: true,
            validate: {
                isUrl: true,
            },
        },

        uuid: {
            type: DataTypes.UUID,
            allowNull: false,
        },

        order: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: true,
        },

        isDraft: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },

        processed: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },

        sizes: {
            type: DataTypes.VIRTUAL(
                DataTypes.HSTORE,
                ['url', 'processed']
            ),
            get() {
                const baseUrl = this.url.replace(
                    BUCKET_HOST +
                    BUCKET_NAME +
                    '/', '');
                const mediaConfig = getMediaConfig(baseUrl);

                const mediaSizes = MEDIA_SIZES[mediaConfig.sizesSet];
                const sizes = Object.keys(mediaSizes);

                debug('Media Config Selected: ');
                debug(JSON.stringify(mediaConfig));

                debug(`Sizes:`);
                debug(JSON.stringify(sizes));

                debug(`Processed: ${this.processed}`);

                if (!this.processed || !mediaConfig) {
                    return sizes.reduce((s, k) => ({...s, [k]: this.url}), {});
                }

                return sizes.reduce((s, k) => (
                    {
                        ...s,
                        [k]: BUCKET_HOST +
                            path.join(
                                BUCKET_NAME,
                                mediaConfig.processedFolder,
                                k,
                                path.basename(this.url)
                            ),
                    }
                ), {});
            },
        },

    }, {
        timestamps: true,
        paranoid: true,
        instanceMethods,

        defaultScope: {
            isDraft: false,
        },

        scopes: {
            historical: {
                paranoid: false,
            },
        },

        hooks: {

            /**
             * Sent RPC for image optimization
             * @override
             */
            afterCreate(instance) {
                // @see https://github.com/sequelize/sequelize/issues/6800
                return Promise.resolve((async () => {
                    debug(`Send RPC to images:reize`);

                    if (!instance.processed) {
                        debug(`\t Instanced not processed, send to resize`);
                        await instance.resize();
                    }
                })());
            },

            afterBulkCreate(instances) {
                // @see https://github.com/sequelize/sequelize/issues/6800
                return Promise.resolve((async () => {
                    debug(`Send RPC to images:resize for bulk creation`);

                    const notProcessed = instances.filter((x) => !x.processed);

                    if (!notProcessed.length) {
                        debug(`\tNone instance is not processed, skipping`);
                        return;
                    }

                    const contentsQueue = getServiceConnection('contents');

                    return Promise.map(notProcessed, (instance) => {
                        debug(`\tSending cmd for ${instance.url}`);
                        return contentsQueue.exec(
                            'images:resize',
                            instance.url.replace(
                                BUCKET_HOST +
                                BUCKET_NAME +
                                '/', '')
                        );
                    });
                })());
            },
        },

    });

    if (typeof MediaModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                MediaModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    /**
     * Media types
     * @type {integer}
     */
    MediaModel.TYPE_COVER = TYPE_COVER;
    MediaModel.TYPE_THUMB = TYPE_THUMB;

    MediaModel.associate = function(models) {
        ContentModel = models.contents;

        MediaModel.belongsTo(ContentModel, {
            as: 'content',
            foreignKey: 'uuid',
            targetKey: 'uuid',
            constraints: false,
        });


        // Scopes
        MediaModel.addScope('show', {
            attributes: [
                'name',
                'type',
                'url',
                'order',
                'sizes',
            ],
        });

        MediaModel.addScope('toExport', {
            paranoid: false,
            attributes: {
                exclude: ['sizes'],
            },
        });
    };

    MediaModel.addScope('queryList', (mediaIds) => {
        return {
            where: {
                id: {
                    [Sequelize.Op.in]: mediaIds,
                },
            },
        };
    });

    return MediaModel;
};
