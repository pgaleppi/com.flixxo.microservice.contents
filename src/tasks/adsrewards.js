
import models from '../models';
import {loadService} from '../remoteservice';
import createLogger, {createDebug} from '../logger';
import config from '../config';
const ADVERTISEMENT_INCREMENT_AMOUNT = config.get('ads:incrementAmount');

const logger = createLogger(__filename);
const debug = createDebug(__filename);

/**
 * Process advertisements rewards logic
 */
export default async () => {

    const paymentsService = await loadService('@payments');

    try {
        debug(`Getting providers ad metters`);
        const adProvidersMetters = await models.admetters.scope('providers')
            .findAll();

        debug(`Getting balances`);
        const balances = await paymentsService.execute(
            'users:getUsersBalances',
            adProvidersMetters.map((x) => x.userUUID),
        );
        debug(`\t${balances}`);

        for (const metter of adProvidersMetters) {
            debug(`Processing ${metter.userUUID} metter`);

            if (!balances[metter.userUUID]) {
                logger.error(
                    `[ ADV:NOT_BALANCE ] ` +
                    `No balance for metter of ${metter.userUUID}`
                );
                break;
            }

            await models.sequelize.transaction(async (dbTx) => {
                await metter.increment({
                    metter: ADVERTISEMENT_INCREMENT_AMOUNT,
                }, {transaction: dbTx});

                if (metter.metter.gt(balances[metter.userUUID])) {
                    logger.warn(
                        `[ ADV:INSUFFICIENT ] ` +
                        `Metter of ${metter.userUUID} incremented to ` +
                        `${metter.metter.toFixed(18)} but total balance ` +
                        `is ${balances[metter.userUUID]}, limitting.`
                    );
                    await metter.update({
                        metter: balances[metter.userUUID],
                    }, {transaction: dbTx});
                }

            });

            logger.info(
                '[ ADV:UPDATEMETTER ] ' +
                `Metter of ${metter.userUUID} incremented to ` +
                `${metter.metter.toFixed(18)}`
            );
        }
    } catch (e) {
        process.stderr.write(`${e.message}\n`);
    }
};
