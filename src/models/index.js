import sequelizeImport from 'sequelize-import';
import {getConnection} from '../storage';
import {createDebug} from '../logger';

const debug = createDebug(__dirname);

const sequelize = getConnection();

const models = sequelizeImport(__dirname, sequelize, {
    exclude: ['index.js'],
});

for (const model in models) {
    if (model.match(/^[a-z0-9]+$/i)) {
        if (typeof models[model].associate === 'function') {
            debug(`Associating ${model}`);
            models[model].associate(models, sequelize);
        }
    }
}

export default {
    ...models,
    sequelize,
};

