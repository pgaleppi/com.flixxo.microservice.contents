
import {createDebug} from '../logger';

const debug = createDebug(__filename);

const TYPE_VIDEO = 0x01;

const STATUS_NEW = 0x10;
const STATUS_ENABLED = 0x20;
const STATUS_CANCEL = 0x30;
const STATUS_PAUSED = 0x40;

export default (sequelize, DataTypes) => {

    let UserWatchedAd;
    let UsersModel;

    const instanceMethods = {};

    const AdModel = sequelize.define('Advertisement', {

        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },

        contentType: {
            type: DataTypes.INTEGER.UNSIGNED,
            defaultValue: TYPE_VIDEO,
            allowNull: false,
            validate: {
                isIn: [[
                    TYPE_VIDEO,
                ]],
            },
        },

        title: {
            type: DataTypes.STRING(2014),
            allowNull: false,
            validate: {
                is: /^.{3,}$/,
            },
        },

        status: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: STATUS_NEW,
            validate: {
                isIn: [[
                    STATUS_NEW,
                    STATUS_CANCEL,
                    STATUS_PAUSED,
                    STATUS_ENABLED,
                ]],
            },
        },

        audioLang: {
            type: DataTypes.STRING(5),
            allowNull: false,
            defaultValue: 'en',
            validate: {
                is: /[a-z](?:_[A-Z])?/,
            },
        },

        url: {
            type: DataTypes.STRING(1024),
            allowNull: false,
            validate: {
                isUrl: true,
            },
        },

        userId: {
            type: DataTypes.UUID,
            allowNull: false,
        },

    }, {
        timestamps: true,
        paranoid: true,
        instanceMethods,

        scopes: {
            historical: {
                paranoid: false,
            },
        },
    });

    if (typeof AdModel.prototype !== 'undefined') {
        for (const m in instanceMethods) {
            if (m.indexOf('$') === -1) {
                debug(`Move method to ${m} to model`);
                AdModel.prototype[m] = instanceMethods[m];
            }
        }
    }

    /**
     * Type of ad
     * @type {integer}
     */
    AdModel.TYPE_VIDEO = TYPE_VIDEO;

    /**
     * Formats
     * @type {integer}
     */
    AdModel.STATUS_NEW = STATUS_NEW;
    AdModel.STATUS_ENABLED = STATUS_ENABLED;
    AdModel.STATUS_CANCEL = STATUS_CANCEL;
    AdModel.STATUS_PAUSED = STATUS_PAUSED;

    AdModel.associate = function(models) {
        UserWatchedAd = models.userad;
        UsersModel = models.users;

        AdModel.belongsTo(UsersModel, {
            as: 'owner',
            foreignKey: 'userId',
            targetKey: 'uuid',
            constraints: true,
        });

        AdModel.hasMany(UserWatchedAd, {
            as: 'userAd',
            foreignKey: 'advertisementId',
            targetKey: 'id',
            onDelete: 'CASCADE',
        });

        // Scopes
        AdModel.addScope('detailed', () => {
            return {
                attributes: [
                    'id',
                    'title',
                    'audioLang',
                    'url',
                    'status',
                ],
            };
        });
    };

    return AdModel;
};
